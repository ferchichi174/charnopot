<?php 
/*
Template Name: WooCommerce
*/ 
?>
<?php

$creator_elated_id = get_option('woocommerce_shop_page_id');
$creator_elated_shop = get_post($creator_elated_id);
$creator_elated_sidebar = creator_elated_sidebar_layout();

if(get_post_meta($creator_elated_id, 'eltd_page_background_color', true) != ''){
	$creator_elated_background_color = 'background-color: '.esc_attr(get_post_meta($creator_elated_id, 'eltd_page_background_color', true));
}else{
	$creator_elated_background_color = '';
}

$creator_elated_content_style = '';
if(get_post_meta($creator_elated_id, 'eltd_content-top-padding', true) != '') {
	if(get_post_meta($creator_elated_id, 'eltd_content-top-padding-mobile', true) == 'yes') {
		$creator_elated_content_style = 'padding-top:'.esc_attr(get_post_meta($creator_elated_id, 'eltd_content-top-padding', true)).'px !important';
	} else {
		$creator_elated_content_style = 'padding-top:'.esc_attr(get_post_meta($creator_elated_id, 'eltd_content-top-padding', true)).'px';
	}
}

if ( get_query_var('paged') ) {
	$creator_elated_paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
	$creator_elated_paged = get_query_var('page');
} else {
	$creator_elated_paged = 1;
}

get_header();

creator_elated_get_title();
get_template_part('slider');

$creator_elated_full_width = false;

if ( creator_elated_options()->getOptionValue('eltd_woo_products_list_full_width') == 'yes' && !is_singular('product') ) {
	$creator_elated_full_width = true;
}

if ( $creator_elated_full_width ) { ?>
	<div class="eltd-full-width" <?php creator_elated_inline_style($creator_elated_background_color); ?>>
<?php } else { ?>
	<div class="eltd-container" <?php creator_elated_inline_style($creator_elated_background_color); ?>>
<?php }
		if ( $creator_elated_full_width ) { ?>
			<div class="eltd-full-width-inner" <?php creator_elated_inline_style($creator_elated_content_style); ?>>
		<?php } else { ?>
			<div class="eltd-container-inner clearfix" <?php creator_elated_inline_style($creator_elated_content_style); ?>>
		<?php }

			//Woocommerce content
			if ( ! is_singular('product') ) {

				switch( $creator_elated_sidebar ) {

					case 'sidebar-33-right': ?>
						<div class="eltd-two-columns-66-33 grid2 eltd-woocommerce-with-sidebar clearfix">
							<div class="eltd-column1">
								<div class="eltd-column-inner">
									<?php creator_elated_woocommerce_content(); ?>
								</div>
							</div>
							<div class="eltd-column2">
								<?php get_sidebar();?>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-right': ?>
						<div class="eltd-two-columns-75-25 grid2 eltd-woocommerce-with-sidebar clearfix">
							<div class="eltd-column1 eltd-content-left-from-sidebar">
								<div class="eltd-column-inner">
									<?php creator_elated_woocommerce_content(); ?>
								</div>
							</div>
							<div class="eltd-column2">
								<?php get_sidebar();?>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-33-left': ?>
						<div class="eltd-two-columns-33-66 grid2 eltd-woocommerce-with-sidebar clearfix">
							<div class="eltd-column1">
								<?php get_sidebar();?>
							</div>
							<div class="eltd-column2">
								<div class="eltd-column-inner">
									<?php creator_elated_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-left': ?>
						<div class="eltd-two-columns-25-75 grid2 eltd-woocommerce-with-sidebar clearfix">
							<div class="eltd-column1">
								<?php get_sidebar();?>
							</div>
							<div class="eltd-column2 eltd-content-right-from-sidebar">
								<div class="eltd-column-inner">
									<?php creator_elated_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					default:
						creator_elated_woocommerce_content();
				}

			} else {
				creator_elated_woocommerce_content();
			} ?>

			</div>
	</div>
<?php get_footer();