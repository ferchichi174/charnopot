<?php
	/*
	Template Name: Blog: Masonry Full Width
	*/

	get_header();
	creator_elated_get_title();
	get_template_part('slider');

?>

	<div class="eltd-full-width">
		<div class="eltd-full-width-inner clearfix">
			<?php
			if (have_posts()) : while (have_posts()) : the_post();
				the_content();
				creator_elated_get_blog('masonry-full-width');
				do_action('creator_elated_page_after_content');
			endwhile;
			endif;
			?>
		</div>
	</div>

<?php get_footer(); ?>