<?php
$creator_elated_sidebar = creator_elated_sidebar_layout();
get_header();
$creator_elated_blog_page_range = creator_elated_get_blog_page_range();
$creator_elated_max_number_of_pages = creator_elated_get_max_number_of_pages();

if ( get_query_var('paged') ) { $creator_elated_paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $creator_elated_paged = get_query_var('page'); }
else { $creator_elated_paged = 1; }
?>
<?php creator_elated_get_title(); ?>
	<div class="eltd-container">

		<?php do_action('creator_elated_after_container_open'); ?>
		<div class="eltd-container-inner clearfix">

			<?php if(($creator_elated_sidebar == 'default') || ($creator_elated_sidebar == '')) { ?>

				<div class="eltd-blog-holder eltd-blog-type-standard">
					<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="eltd-post-content">
								<div class="eltd-post-text">
									<div class="eltd-post-text-inner">
										<?php creator_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
										<?php $creator_elated_excerpt = get_the_excerpt();
										if ($creator_elated_excerpt !== '') { ?>
											<p class="eltd-post-excerpt"><?php echo do_shortcode($creator_elated_excerpt); ?></p>
										<?php }

										$creator_elated_args_pages = array(
											'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
											'after'            => '</div></div>',
											'link_before'      => '<span>',
											'link_after'       => '</span>',
											'pagelink'         => '%'
										);

										wp_link_pages($creator_elated_args_pages);
										creator_elated_read_more_button();?>

										<div class="eltd-post-info eltd-bottom-section clearfix">

											<div class="eltd-left-section">
												<?php creator_elated_post_info(array(
													'author' => 'yes'
												)) ?>
											</div>

											<div class="eltd-right-section">
												<?php creator_elated_post_info(array(
													'comments' => 'yes',
													'share' => 'yes',
												)) ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</article>
					<?php endwhile; ?>
						<?php
						if(creator_elated_options()->getOptionValue('pagination') == 'yes') {
							creator_elated_pagination($creator_elated_max_number_of_pages, $creator_elated_blog_page_range, $creator_elated_paged);
						}
						?>
					<?php else: ?>
						<div class="entry">
							<p><?php esc_html_e('No posts were found.', 'creator'); ?></p>
						</div>
					<?php endif; ?>

				</div>

			<?php }

			elseif($creator_elated_sidebar == 'sidebar-33-right' || $creator_elated_sidebar == 'sidebar-25-right'){?>

				<div <?php echo creator_elated_sidebar_columns_class(); ?>>
					<div class="eltd-column1 eltd-content-left-from-sidebar">
						<div class="eltd-column-inner">
							<div class="eltd-blog-holder eltd-blog-type-standard">
								<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										<div class="eltd-post-content">
											<div class="eltd-post-text">
												<div class="eltd-post-text-inner">
													<?php creator_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
													<?php $creator_elated_excerpt = get_the_excerpt();
													if ($creator_elated_excerpt !== '') { ?>
														<p class="eltd-post-excerpt"><?php echo do_shortcode($creator_elated_excerpt); ?></p>
													<?php }

													$creator_elated_args_pages = array(
														'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
														'after'            => '</div></div>',
														'link_before'      => '<span>',
														'link_after'       => '</span>',
														'pagelink'         => '%'
													);

													wp_link_pages($creator_elated_args_pages); ?>

													<div class="eltd-post-info eltd-bottom-section clearfix">

														<div class="eltd-left-section">
															<?php creator_elated_post_info(array(
																'author' => 'yes'
															)) ?>
														</div>

														<div class="eltd-right-section">
															<?php creator_elated_post_info(array(
																'comments' => 'yes',
																'share' => 'yes',
															)) ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</article>
								<?php endwhile; ?>
									<?php
									if(creator_elated_options()->getOptionValue('pagination') == 'yes') {
										creator_elated_pagination($creator_elated_max_number_of_pages, $creator_elated_blog_page_range, $creator_elated_paged);
									}
									?>
								<?php else: ?>
									<div class="entry">
										<p><?php esc_html_e('No posts were found.', 'creator'); ?></p>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
					<div class="eltd-column2">
						<?php get_sidebar(); ?>
					</div>
				</div>

			<?php }

			elseif($creator_elated_sidebar == 'sidebar-33-left' || $creator_elated_sidebar == 'sidebar-25-left'){?>
				<div <?php echo creator_elated_sidebar_columns_class(); ?>>
					<div class="eltd-column1">
						<?php get_sidebar(); ?>
					</div>
					<div class="eltd-column2 eltd-content-right-from-sidebar">
						<div class="eltd-column-inner">
							<div class="eltd-blog-holder eltd-blog-type-standard">
								<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										<div class="eltd-post-content">
											<div class="eltd-post-text">
												<div class="eltd-post-text-inner">
													<?php creator_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
													<?php $creator_elated_excerpt = get_the_excerpt();
													if ($creator_elated_excerpt !== '') { ?>
														<p class="eltd-post-excerpt"><?php echo do_shortcode($creator_elated_excerpt); ?></p>
													<?php }

													$creator_elated_args_pages = array(
														'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
														'after'            => '</div></div>',
														'link_before'      => '<span>',
														'link_after'       => '</span>',
														'pagelink'         => '%'
													);

													wp_link_pages($creator_elated_args_pages);
													creator_elated_read_more_button();?>

													<div class="eltd-post-info eltd-bottom-section clearfix">

														<div class="eltd-left-section">
															<?php creator_elated_post_info(array(
																'author' => 'yes'
															)) ?>
														</div>

														<div class="eltd-right-section">
															<?php creator_elated_post_info(array(
																'comments' => 'yes',
																'share' => 'yes',
															)) ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</article>
								<?php endwhile; ?>
									<?php
									if(creator_elated_options()->getOptionValue('pagination') == 'yes') {
										creator_elated_pagination($creator_elated_max_number_of_pages, $creator_elated_blog_page_range, $creator_elated_paged);
									}
									?>
								<?php else: ?>
									<div class="entry">
										<p><?php esc_html_e('No posts were found.', 'creator'); ?></p>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</div>
			<?php } ?>


		</div>

	</div>
<?php get_footer();