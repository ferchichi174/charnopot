<?php
get_header();
creator_elated_get_title();
get_template_part('slider');
if (have_posts()) {
	while(have_posts()) {
		the_post();
		creator_elated_single_portfolio();
	}
}
get_footer();