<?php
get_header();
creator_elated_get_title();
get_template_part('slider');
if (have_posts()) {
	while (have_posts()){
		the_post();
		?>
		<div class="eltd-container">
			<?php do_action('creator_elated_after_container_open'); ?>
			<div class="eltd-container-inner">
				<?php creator_elated_get_blog_single(); ?>
			</div>
			<?php do_action('creator_elated_before_container_close'); ?>
		</div>
		<?php
	}
}
get_footer();