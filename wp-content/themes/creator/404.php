<?php
$creator_elated_error_404_class = '';
$creator_elated_dark_skin = false;
if(creator_elated_options()->getOptionValue( '404_skin' )==='dark'){
	$creator_elated_error_404_class = 'eltd-404-dark';
	$creator_elated_dark_skin = true;
}
$creator_elated_error_404_style = '';
$creator_elated_error_404_bck_image =  creator_elated_options()->getOptionValue( '404_background_image' );
if( $creator_elated_error_404_bck_image !=='' ) {
	$creator_elated_error_404_style  = 'background-image:url('.esc_url($creator_elated_error_404_bck_image).')';
}

//get first color
$creator_elated_first_main_color = '#27d7ab';
$creator_elated_changed_first_color = creator_elated_options()->getOptionValue('first_color');
if($creator_elated_changed_first_color !== ''){
	$creator_elated_first_main_color = $creator_elated_changed_first_color;
}

//generate button params array
$creator_elated_button_params = array(
	'text' => creator_elated_options()->getOptionValue( '404_back_to_home' ) !== '' ? creator_elated_options()->getOptionValue( '404_back_to_home' ) : esc_html__( 'Back to Home', 'creator' ),
	'link' => esc_url( home_url( '/' ) ),
	'type' => 'solid'
);

if($creator_elated_dark_skin){
	$creator_elated_button_params['background_color'] = $creator_elated_first_main_color;
	$creator_elated_button_params['border_color'] = $creator_elated_first_main_color;
	$creator_elated_button_params['color'] = '#fff';
	$creator_elated_button_params['hover_background_color'] = '#ebebeb';
	$creator_elated_button_params['hover_border_color'] = '#ebebeb';
	$creator_elated_button_params['hover_color'] = $creator_elated_first_main_color;
}
else{
	$creator_elated_button_params['background_color'] = '#fff';
	$creator_elated_button_params['border_color'] = '#fff';
	$creator_elated_button_params['color'] = $creator_elated_first_main_color;
}

get_header();

creator_elated_get_title(); ?>
	<div class="eltd-full-width ">
		<?php do_action( 'creator_elated_after_container_open' ); ?>
		<div class="eltd-full-width-inner eltd-404-page <?php echo esc_attr($creator_elated_error_404_class); ?>"  <?php echo creator_elated_get_inline_style($creator_elated_error_404_style); ?>>
			<div class="eltd-page-not-found">
				<div class="eltd-grid">
					<h2>
						<?php if ( creator_elated_options()->getOptionValue( '404_title' ) ) {
							printf(
								__( '%s', 'creator' ),
								esc_html(creator_elated_options()->getOptionValue( '404_title' ))
							);
						} else {
							esc_html_e( '404 error page', 'creator' );
						} ?>
					</h2>
					<h5>
						<?php if ( creator_elated_options()->getOptionValue( '404_excerpt' ) ) {
							printf(
								__( '%s', 'creator' ),
								esc_html( creator_elated_options()->getOptionValue( '404_excerpt' ))
							);
						}  ?>
					</h5>
					<p>
						<?php if ( creator_elated_options()->getOptionValue( '404_text' ) ) {
							printf(
								__( '%s', 'creator' ),
								esc_html( creator_elated_options()->getOptionValue( '404_text' ))
							);
						} else {
							esc_html_e( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.', 'creator' );
						} ?>
					</p>
					<?php
					echo creator_elated_get_button_html( $creator_elated_button_params );
					?>
				</div>
			</div>
		</div>
		<?php do_action( 'creator_elated_before_container_close' ); ?>
	</div>

<?php get_footer(); ?>