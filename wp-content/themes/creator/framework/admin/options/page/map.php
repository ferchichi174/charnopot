<?php

if ( ! function_exists('creator_elated_page_options_map') ) {

    function creator_elated_page_options_map() {

        creator_elated_add_admin_page(
            array(
                'slug'  => '_page_page',
                'title' => esc_html__('Page','creator'),
                'icon'  => 'fa fa-institution'
            )
        );

        $custom_sidebars = creator_elated_get_custom_sidebars();

        $panel_sidebar = creator_elated_add_admin_panel(
            array(
                'page'  => '_page_page',
                'name'  => 'panel_sidebar',
                'title' => esc_html__('Design Style', 'creator')
            )
        );

        creator_elated_add_admin_field(array(
            'name'        => 'page_sidebar_layout',
            'type'        => 'select',
            'label'       => esc_html__('Sidebar Layout', 'creator'),
            'description' => esc_html__('Choose a sidebar layout for pages', 'creator'),
            'default_value' => 'default',
            'parent'      => $panel_sidebar,
            'options'     => array(
                'default'			=> esc_html__('No Sidebar', 'creator'),
                'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'creator'),
                'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'creator'),
                'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'creator'),
                'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'creator'),
            )
        ));


        if(count($custom_sidebars) > 0) {
            creator_elated_add_admin_field(array(
                'name' => 'page_custom_sidebar',
                'type' => 'selectblank',
                'label' => esc_html__('Sidebar to Display', 'creator'),
                'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'creator'),
                'parent' => $panel_sidebar,
                'options' => $custom_sidebars
            ));
        }

        creator_elated_add_admin_field(array(
            'name'        => 'page_show_comments',
            'type'        => 'yesno',
            'label'       => esc_html__('Show Comments', 'creator'),
            'description' => esc_html__('Enabling this option will show comments on your page', 'creator'),
            'default_value' => 'yes',
            'parent'      => $panel_sidebar
        ));

    }

    add_action( 'creator_elated_options_map', 'creator_elated_page_options_map', 9);

}