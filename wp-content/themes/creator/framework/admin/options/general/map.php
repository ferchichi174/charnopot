<?php

if ( ! function_exists('creator_elated_general_options_map') ) {
    /**
     * General options page
     */
    function creator_elated_general_options_map() {

        creator_elated_add_admin_page(
            array(
                'slug'  => '',
                'title' => esc_html__('General', 'creator'),
                'icon'  => 'fa fa-institution'
            )
        );

        $panel_design_style = creator_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_design_style',
                'title' => esc_html__('Design Style', 'creator')
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'google_fonts',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Google Font Family', 'creator'),
                'description'   => esc_html__('Choose a default Google font for your site', 'creator'),
                'parent' => $panel_design_style
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_fonts',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Additional Google Fonts', 'creator'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    'dependence' => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_additional_google_fonts_container'
                )
            )
        );

        $additional_google_fonts_container = creator_elated_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'additional_google_fonts_container',
                'hidden_property'   => 'additional_google_fonts',
                'hidden_value'      => 'no'
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font1',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'creator'),
                'description'   => esc_html__('Choose additional Google font for your site', 'creator'),
                'parent'        => $additional_google_fonts_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font2',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'creator'),
                'description'   => esc_html__('Choose additional Google font for your site', 'creator'),
                'parent'        => $additional_google_fonts_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font3',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'creator'),
                'description'   => esc_html__('Choose additional Google font for your site', 'creator'),
                'parent'        => $additional_google_fonts_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font4',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'creator'),
                'description'   => esc_html__('Choose additional Google font for your site', 'creator'),
                'parent'        => $additional_google_fonts_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'additional_google_font5',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'creator'),
                'description'   => esc_html__('Choose additional Google font for your site', 'creator'),
                'parent'        => $additional_google_fonts_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name' => 'google_font_weight',
                'type' => 'checkboxgroup',
                'default_value' => '',
                'label' => esc_html__('Google Fonts Style & Weight', 'creator'),
                'description' => esc_html__('Choose a default Google font weights for your site. Impact on page load time', 'creator'),
                'parent' => $panel_design_style,
                'options' => array(
                    '100'       => esc_html__('100 Thin', 'creator'),
                    '100italic' => esc_html__('100 Thin Italic', 'creator'),
                    '200'       => esc_html__('200 Extra-Light', 'creator'),
                    '200italic' => esc_html__('200 Extra-Light Italic', 'creator'),
                    '300'       => esc_html__('300 Light', 'creator'),
                    '300italic' => esc_html__('300 Light Italic', 'creator'),
                    '400'       => esc_html__('400 Regular', 'creator'),
                    '400italic' => esc_html__('400 Regular Italic', 'creator'),
                    '500'       => esc_html__('500 Medium', 'creator'),
                    '500italic' => esc_html__('500 Medium Italic', 'creator'),
                    '600'       => esc_html__('600 Semi-Bold', 'creator'),
                    '600italic' => esc_html__('600 Semi-Bold Italic', 'creator'),
                    '700'       => esc_html__('700 Bold', 'creator'),
                    '700italic' => esc_html__('700 Bold Italic', 'creator'),
                    '800'       => esc_html__('800 Extra-Bold', 'creator'),
                    '800italic' => esc_html__('800 Extra-Bold Italic', 'creator'),
                    '900'       => esc_html__('900 Ultra-Bold', 'creator'),
                    '900italic' => esc_html__('900 Ultra-Bold Italic', 'creator'),
                ),
                'args' => array(
                    'enable_empty_checkbox' => true,
                    'inline_checkbox_class' => true
                )
            )
        );

        creator_elated_add_admin_field(
            array(
                'name' => 'google_font_subset',
                'type' => 'checkboxgroup',
                'default_value' => '',
                'label' => esc_html__('Google Fonts Subset', 'creator'),
                'description' => esc_html__('Choose a default Google font subsets for your site', 'creator'),
                'parent' => $panel_design_style,
                'options' => array(
                    'latin' => esc_html__('Latin', 'creator'),
                    'latin-ext' => esc_html__('Latin Extended', 'creator'),
                    'cyrillic' => esc_html__('Cyrillic', 'creator'),
                    'cyrillic-ext' => esc_html__('Cyrillic Extended', 'creator'),
                    'greek' => esc_html__('Greek', 'creator'),
                    'greek-ext' => esc_html__('Greek Extended', 'creator'),
                    'vietnamese' => esc_html__('Vietnamese', 'creator')
                ),
                'args' => array(
                    'enable_empty_checkbox' => true,
                    'inline_checkbox_class' => true
                )
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'first_color',
                'type'          => 'color',
                'label'         => esc_html__('First Main Color', 'creator'),
                'description'   => esc_html__('Choose the most dominant theme color. Default color is #ff1d4d', 'creator'),
                'parent'        => $panel_design_style
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'page_background_color',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'creator'),
                'description'   => esc_html__('Choose the background color for page content. Default color is #ffffff', 'creator'),
                'parent'        => $panel_design_style
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'main_style',
                'type'          => 'select',
                'default_value' => 'default_style',
                'label'         => esc_html__('Predefined Font Style', 'creator'),
                'description'   => esc_html__('Choose predefined font style', 'creator'),
                'parent'        => $panel_design_style,
                'options'       => array(
                    'default_style'   => esc_html__('Default Style', 'creator'),
                    'oswald_style'   => esc_html__('Style with Oswald Font', 'creator'),
                    'poppins_style'   => esc_html__('Style with Poppins Font', 'creator'),
                    'raleway_style'   => esc_html__('Style with Raleway Font', 'creator'),
                    'yesteryear_style'   => esc_html__('Style with Yesteryear Font', 'creator'),
                    'greatvibes_style'   => esc_html__('Style with Great Vibes Font', 'creator')
                )
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'selection_color',
                'type'          => 'color',
                'label'         => esc_html__('Text Selection Color', 'creator'),
                'description'   => esc_html__('Choose the color users see when selecting text', 'creator'),
                'parent'        => $panel_design_style
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'boxed',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Boxed Layout', 'creator'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    'dependence' => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_boxed_container'
                )
            )
        );

        $boxed_container = creator_elated_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'boxed_container',
                'hidden_property'   => 'boxed',
                'hidden_value'      => 'no'
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'page_background_color_in_box',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'creator'),
                'description'   => esc_html__('Choose the page background color outside box.', 'creator'),
                'parent'        => $boxed_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'boxed_background_image',
                'type'          => 'image',
                'label'         => esc_html__('Background Image', 'creator'),
                'description'   => esc_html__('Choose an image to be displayed in background', 'creator'),
                'parent'        => $boxed_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'boxed_pattern_background_image',
                'type'          => 'image',
                'label'         => esc_html__('Background Pattern', 'creator'),
                'description'   => esc_html__('Choose an image to be used as background pattern', 'creator'),
                'parent'        => $boxed_container
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'boxed_background_image_attachment',
                'type'          => 'select',
                'default_value' => 'fixed',
                'label'         => esc_html__('Background Image Attachment', 'creator'),
                'description'   => esc_html__('Choose background image attachment', 'creator'),
                'parent'        => $boxed_container,
                'options'       => array(
                    'fixed'     => esc_html__('Fixed', 'creator'),
                    'scroll'    => esc_html__('Scroll', 'creator')
                )
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'initial_content_width',
                'type'          => 'select',
                'default_value' => 'grid-1200',
                'label'         => esc_html__('Initial Width of Content', 'creator'),
                'description'   => esc_html__('Choose the initial width of content which is in grid Applies to pages set to "Default Template" and rows set to "In Grid"', 'creator'),
                'parent'        => $panel_design_style,
                'options'       => array(
                    ''          => '1100px',
                    'grid-1300' => '1300px',
                    'grid-1200' => '1200px - default',
                    'grid-1000' => '1000px',
                    'grid-800'  => '800px'
                )
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'preload_pattern_image',
                'type'          => 'image',
                'label'         => esc_html__('Preload Pattern Image', 'creator'),
                'description'   => esc_html__('Choose preload pattern image to be displayed until images are loaded ', 'creator'),
                'parent'        => $panel_design_style
            )
        );

        creator_elated_add_admin_field(
            array(
                'name' => 'element_appear_amount',
                'type' => 'text',
                'label' => esc_html__('Element Appearance', 'creator'),
                'description' => esc_html__('For animated elements, set distance (related to browser bottom) to start the animation', 'creator'),
                'parent' => $panel_design_style,
                'args' => array(
                    'col_width' => 2,
                    'suffix' => 'px'
                )
            )
        );

        $panel_settings = creator_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_settings',
                'title' => esc_html__('Settings', 'creator')
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'smooth_scroll',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Scroll', 'creator'),
                'description'   => esc_html__('Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'creator'),
                'parent'        => $panel_settings
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'smooth_page_transitions',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Page Transitions', 'creator'),
                'description'   => esc_html__('Enabling this option will perform a smooth transition between pages when clicking on links.', 'creator'),
                'parent'        => $panel_settings,
                'args'          => array(
                    'dependence' => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_page_transitions_container'
                )
            )
        );

        $page_transitions_container = creator_elated_add_admin_container(
            array(
                'parent'            => $panel_settings,
                'name'              => 'page_transitions_container',
                'hidden_property'   => 'smooth_page_transitions',
                'hidden_value'      => 'no'
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'smooth_pt_bgnd_color',
                'type'          => 'color',
                'label'         => esc_html__('Page Loader Background Color', 'creator'),
                'parent'        => $page_transitions_container
            )
        );

        $group_pt_spinner_animation = creator_elated_add_admin_group(array(
            'name'          => 'group_pt_spinner_animation',
            'title'         => esc_html__('Loader Style', 'creator'),
            'description'   => esc_html__('Define styles for loader spinner animation', 'creator'),
            'parent'        => $page_transitions_container
        ));

        $row_pt_spinner_animation = creator_elated_add_admin_row(array(
            'name'      => 'row_pt_spinner_animation',
            'parent'    => $group_pt_spinner_animation
        ));

        creator_elated_add_admin_field(array(
            'type'          => 'selectsimple',
            'name'          => 'smooth_pt_spinner_type',
            'default_value' => '',
            'label'         => esc_html__('Spinner Type', 'creator'),
            'parent'        => $row_pt_spinner_animation,
            'options'       => array(
                'progress_circle' => esc_html__('Progress Circle', 'creator'),
                'square' => esc_html__('Square', 'creator'),
                'pulse' => esc_html__('Pulse', 'creator'),
                'double_pulse' => esc_html__('Double Pulse', 'creator'),
                'cube' => esc_html__('Cube', 'creator'),
                'rotating_cubes' => esc_html__('Rotating Cubes', 'creator'),
                'stripes' => esc_html__('Stripes', 'creator'),
                'wave' => esc_html__('Wave', 'creator'),
                'two_rotating_circles' => esc_html__('2 Rotating Circles', 'creator'),
                'five_rotating_circles' => esc_html__('5 Rotating Circles', 'creator'),
                'atom' => esc_html__('Atom', 'creator'),
                'clock' => esc_html__('Clock', 'creator'),
                'mitosis' => esc_html__('Mitosis', 'creator'),
                'lines' => esc_html__('Lines', 'creator'),
                'fussion' => esc_html__('Fussion', 'creator'),
                'wave_circles' => esc_html__('Wave Circles', 'creator'),
                'pulse_circles' => esc_html__('Pulse Circles', 'creator')
            )
        ));

        creator_elated_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'smooth_pt_spinner_color',
            'default_value' => '',
            'label'         => esc_html__('Spinner Color', 'creator'),
            'parent'        => $row_pt_spinner_animation
        ));

        creator_elated_add_admin_field(
            array(
                'name'          => 'show_back_button',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Show "Back To Top Button"', 'creator'),
                'description'   => esc_html__('Enabling this option will display a Back to Top button on every page', 'creator'),
                'parent'        => $panel_settings
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'enable_paspartu',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Passepartout', 'creator'),
                'description'   => esc_html__('Enabling this option will display passepartout around site content', 'creator'),
                'parent'        => $panel_settings,
                'args'          => array(
                    'dependence' => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_paspartu_container'
                )
            )
        );

        $paspartu_container = creator_elated_add_admin_container(
            array(
                'parent'            => $panel_settings,
                'name'              => 'paspartu_container',
                'hidden_property'   => 'enable_paspartu',
                'hidden_value'      => 'no'
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'paspartu_color',
                'type'          => 'color',
                'default_value' => '',
                'label'         => esc_html__('Passepartout Color', 'creator'),
                'description'   => esc_html__('Choose passepartout color.Default value is #fff', 'creator'),
                'parent'        => $paspartu_container,
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'paspartu_size',
                'type'          => 'text',
                'default_value' => '',
                'label'         => esc_html__('Passepartout Size', 'creator'),
                'description'   => esc_html__('Enter size amount for passepartout.Default value is 10px', 'creator'),
                'parent'        => $paspartu_container,
                'args' => array(
                    'col_width' => 3
                )
            )
        );


        creator_elated_add_admin_field(
            array(
                'name'          => 'responsiveness',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Responsiveness', 'creator'),
                'description'   => esc_html__('Enabling this option will make all pages responsive','creator'),
                'parent'        => $panel_settings
            )
        );

        $panel_custom_code = creator_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_custom_code',
                'title' => esc_html__('Custom Code', 'creator')
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'custom_css',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom CSS','creator'),
                'description'   => esc_html__('Enter your custom CSS here','creator'),
                'parent'        => $panel_custom_code
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'          => 'custom_js',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom JS','creator'),
                'description'   => esc_html__('Enter your custom Javascript here','creator'),
                'parent'        => $panel_custom_code
            )
        );

        $panel_google_api = creator_elated_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_google_api',
                'title' => esc_html__('Google API', 'creator')
            )
        );

        creator_elated_add_admin_field(
            array(
                'name'        => 'google_maps_api_key',
                'type'        => 'text',
                'label'       => esc_html__('Google Maps Api Key','creator'),
                'description' => esc_html__('Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our to our documentation.', 'creator'),
                'parent'      => $panel_google_api
            )
        );

    }



    add_action( 'creator_elated_options_map', 'creator_elated_general_options_map', 5);

}