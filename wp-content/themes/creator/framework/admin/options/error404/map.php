<?php

if ( ! function_exists('creator_elated_error_404_options_map') ) {

	function creator_elated_error_404_options_map() {

		creator_elated_add_admin_page(array(
			'slug' => '__404_error_page',
			'title' => esc_html__('404 Error Page', 'creator'),
			'icon' => 'fa fa-exclamation-triangle'
		));

		$panel_404_options = creator_elated_add_admin_panel(array(
			'page' => '__404_error_page',
			'name'	=> 'panel_404_options',
			'title'	=> esc_html__('404 Page Option',  'creator')
		));

		creator_elated_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_title',
			'default_value' => '',
			'label' => esc_html__('Title', 'creator'),
			'description' => esc_html__('Enter title for 404 page',  'creator')
		));
		creator_elated_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_excerpt',
			'default_value' => '',
			'label' => esc_html__('Excerpt', 'creator'),
			'description' => esc_html__('Enter excerpt for 404 page', 'creator')
		));

		creator_elated_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_text',
			'default_value' => '',
			'label' => esc_html__('Text', 'creator'),
			'description' => esc_html__('Enter text for 404 page', 'creator')
		));

		creator_elated_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'text',
			'name' => '404_back_to_home',
			'default_value' => '',
			'label' => esc_html__('Back to Home Button Label', 'creator'),
			'description' => esc_html__('Enter label for "Back to Home" button', 'creator')
		));
		creator_elated_add_admin_field(array(
			'parent' => $panel_404_options,
			'type' => 'image',
			'name' => '404_background_image',
			'default_value' => '',
			'label' => esc_html__('Background Image', 'creator'),
			'description' => esc_html__('Upload background image for 404 page', 'creator')
		));
		creator_elated_add_admin_field(array(
			'name'        => '404_skin',
			'type'        => 'selectblank',
			'label'       => 'Skin',
			'description' => esc_html__('Choose skin for 404 page', 'creator'),
			'parent'      => $panel_404_options,
			'default_value' => '',
			'options'     => array(
				'light'   => esc_html__('Light', 'creator'),
				'dark'   => esc_html__('Dark', 'creator')
			)
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_error_404_options_map', 21);

}