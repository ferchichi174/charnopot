<?php

if ( ! function_exists('creator_elated_load_elements_map') ) {
	/**
	 * Add Elements option page for shortcodes
	 */
	function creator_elated_load_elements_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_elements_page',
				'title' => esc_html__('Elements', 'creator'),
				'icon' => 'fa fa-header'
			)
		);

		do_action( 'creator_elated_options_elements_map' );

	}

	add_action('creator_elated_options_map', 'creator_elated_load_elements_map',17);

}