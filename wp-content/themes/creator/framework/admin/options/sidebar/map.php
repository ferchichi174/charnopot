<?php

if ( ! function_exists('creator_elated_sidebar_options_map') ) {

	function creator_elated_sidebar_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug'  => '_sidebar_page',
				'title' => esc_html__('Sidebar', 'creator'),
				'icon'  => 'fa fa-bars'
			)
		);

		$panel_widgets = creator_elated_add_admin_panel(
			array(
				'page'  => '_sidebar_page',
				'name'  => 'panel_widgets',
				'title' => esc_html__('Widgets', 'creator')
			)
		);

		/**
		 * Navigation style
		 */
		creator_elated_add_admin_field(array(
			'type'			=> 'color',
			'name'			=> 'sidebar_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Sidebar Background Color', 'creator'),
			'description'	=> esc_html__('Choose background color for sidebar', 'creator'),
			'parent'		=> $panel_widgets
		));

		$group_sidebar_padding = creator_elated_add_admin_group(array(
			'name'		=> 'group_sidebar_padding',
			'title'		=> esc_html__('Padding', 'creator'),
			'parent'	=> $panel_widgets
		));

		$row_sidebar_padding = creator_elated_add_admin_row(array(
			'name'		=> 'row_sidebar_padding',
			'parent'	=> $group_sidebar_padding
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Top Padding', 'creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Right Padding', 'creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Bottom Padding', 'creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Left Padding', 'creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'select',
			'name'			=> 'sidebar_alignment',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Alignment', 'creator'),
			'description'	=> esc_html__('Choose text aligment', 'creator'),
			'options'		=> array(
				'left' => esc_html__('Left', 'creator'),
				'center' => esc_html__('Center', 'creator'),
				'right' => esc_html__('Right', 'creator')
			),
			'parent'		=> $panel_widgets
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_sidebar_options_map', 11);

}