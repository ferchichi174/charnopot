<?php

if ( ! function_exists('creator_elated_reset_options_map') ) {
	/**
	 * Reset options panel
	 */
	function creator_elated_reset_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__('Reset', 'creator'),
				'icon'  => 'fa fa-retweet'
			)
		);

		$panel_reset = creator_elated_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__('Reset', 'creator')
			)
		);

		creator_elated_add_admin_field(array(
			'type'	=> 'yesno',
			'name'	=> 'reset_to_defaults',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Reset to Defaults', 'creator'),
			'description'	=> esc_html__('This option will reset all Elated Options values to defaults', 'creator'),
			'parent'		=> $panel_reset
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_reset_options_map', 100 );

}