<?php

if(!function_exists('creator_elated_promotion_meta_box_settings_map')){

	function creator_elated_promotion_meta_box_settings_map(){

		$promotion_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array( 'promotion-item' ),
				'title' => esc_html__('Promotions', 'creator'),
				'name'  => 'promotion_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_promotion_price',
				'type'        => 'text',
				'label'       => esc_html__('Price', 'creator'),
				'description' => esc_html__('Enter promotion price', 'creator'),
				'parent'      => $promotion_meta_box
			)
		);


		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_promotion_discount_price',
				'type'        => 'text',
				'label'       => esc_html__('Discount Price', 'creator'),
				'description' => esc_html__('Enter promotion discount price', 'creator'),
				'parent'      => $promotion_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_promotion_period',
				'type'        => 'text',
				'label'       => esc_html__('Promotion Period', 'creator'),
				'description' => esc_html__('Enter promotion period', 'creator'),
				'parent'      => $promotion_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_promotion_destination',
				'type'        => 'text',
				'label'       => esc_html__('Promotion Destination', 'creator'),
				'description' => esc_html__('Enter promotion destination', 'creator'),
				'parent'      => $promotion_meta_box
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_promotion_meta_box_settings_map');
}