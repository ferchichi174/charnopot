<?php

if(!function_exists('creator_elated_content_bottom_meta_box_settings_map')){
	function creator_elated_content_bottom_meta_box_settings_map(){

		$creator_elated_content_bottom_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array('page', 'portfolio-item', 'post'),
				'title' => esc_html__('Content Bottom', 'creator'),
				'name' => 'content_bottom_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_enable_content_bottom_area_meta',
				'type' => 'selectblank',
				'default_value' => '',
				'label' => esc_html__('Enable Content Bottom Area', 'creator'),
				'description' => esc_html__('This option will enable Content Bottom area on pages', 'creator'),
				'parent' => $creator_elated_content_bottom_meta_box,
				'options' => array(
					'no' => esc_html__('No', 'creator'),
					'yes' => esc_html__('Yes', 'creator')
				),
				'args' => array(
					'dependence' => true,
					'hide' => array(
						'' => '#eltd_eltd_show_content_bottom_meta_container',
						'no' => '#eltd_eltd_show_content_bottom_meta_container'
					),
					'show' => array(
						'yes' => '#eltd_eltd_show_content_bottom_meta_container'
					)
				)
			)
		);

		$creator_elated_show_content_bottom = creator_elated_add_admin_container(
			array(
				'parent' => $creator_elated_content_bottom_meta_box,
				'name' => 'eltd_show_content_bottom_meta_container',
				'hidden_property' => 'eltd_enable_content_bottom_area_meta',
				'hidden_value' => '',
				'hidden_values' => array('','no')
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_content_bottom_sidebar_custom_display_meta',
				'type' => 'selectblank',
				'default_value' => '',
				'label' => esc_html__('Sidebar to Display', 'creator'),
				'description' => esc_html__('Choose a Content Bottom sidebar to display', 'creator'),
				'options' => creator_elated_get_custom_sidebars(),
				'parent' => $creator_elated_show_content_bottom
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'type' => 'selectblank',
				'name' => 'eltd_content_bottom_in_grid_meta',
				'default_value' => '',
				'label' => esc_html__('Display in Grid', 'creator'),
				'description' => esc_html__('Enabling this option will place Content Bottom in grid', 'creator'),
				'options' => array(
					'no' => esc_html__('No', 'creator'),
					'yes' => esc_html__('Yes', 'creator')
				),
				'parent' => $creator_elated_show_content_bottom
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'type' => 'color',
				'name' => 'eltd_content_bottom_background_color_meta',
				'default_value' => '',
				'label' => esc_html__('Background Color', 'creator'),
				'description' => esc_html__('Choose a background color for Content Bottom area', 'creator'),
				'parent' => $creator_elated_show_content_bottom
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_content_bottom_meta_box_settings_map');
}