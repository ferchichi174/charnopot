<?php

if(!function_exists('creator_elated_product_meta_box_settings_map')){

	function creator_elated_product_meta_box_settings_map(){
		$product_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array('product'),
				'title' => esc_html__('Product', 'creator'),
				'name' => 'product_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_product_subtitle_meta',
				'type' => 'text',
				'default_value' => '',
				'label' => esc_html__('Product Subtitle', 'creator'),
				'description' => esc_html__('Insert product subtitle', 'creator'),
				'parent' => $product_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_image_product_slider_meta',
				'type' => 'image',
				'label'       => esc_html__('Background Image Product Slider', 'creator'),
				'description' => esc_html__('Choose background image for product slider', 'creator'),
				'parent'      => $product_meta_box
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_product_meta_box_settings_map');
}