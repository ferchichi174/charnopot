<?php
if(!function_exists('creator_elated_audio_meta_box_settings_map')){
	function creator_elated_audio_meta_box_settings_map(){

		/*** Audio Post Format ***/

		$creator_elated_audio_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Audio Post Format', 'creator'),
				'name' 	=> 'post_format_audio_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_audio_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Link', 'creator'),
				'description' => esc_html__('Enter audion link', 'creator'),
				'parent'      => $creator_elated_audio_meta_box,

			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_audio_meta_box_settings_map');
}