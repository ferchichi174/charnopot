<?php

if(!function_exists('creator_elated_blog_meta_box_setting_map')) {

    function creator_elated_blog_meta_box_setting_map() {

        $eltd_blog_categories = array();
        $categories = get_categories();
        foreach($categories as $category) {
            $eltd_blog_categories[$category->term_id] = $category->name;
        }

        $blog_meta_box = creator_elated_create_meta_box(
            array(
                'scope' => array('page'),
                'title' => esc_html__('Blog' , 'creator'),
                'name' => 'blog_meta'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_blog_category_meta',
                'type'        => 'selectblank',
                'label'       => esc_html__('Blog Category', 'creator'),
                'description' => esc_html__('Choose category of posts to display (leave empty to display all categories)', 'creator'),
                'parent'      => $blog_meta_box,
                'options'     => $eltd_blog_categories
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_show_posts_per_page_meta',
                'type'        => 'text',
                'label'       => esc_html__('Number of Posts', 'creator'),
                'description' => esc_html__('Enter the number of posts to display', 'creator'),
                'parent'      => $blog_meta_box,
                'options'     => $eltd_blog_categories,
                'args'        => array("col_width" => 3)
            )
        );

        $post_layout_meta_box = creator_elated_create_meta_box(
            array(
                'scope' => array('post'),
                'title' => esc_html__('Post Layout', 'creator'),
                'name' => 'post-layout-meta'
            )
        );
        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_post_layout_meta',
                'type'        => 'selectblank',
                'label'       => esc_html__('Post Layout', 'creator'),
                'default'	 => 'standard',
                'description' => esc_html__('Choose post layout on standard blog list', 'creator'),
                'parent'      => $post_layout_meta_box,
                'options'     => array(
                    'default' => esc_html__('Default', 'creator'),
                    'split' => esc_html__('Split', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_post_featured_blog_slider',
                'type'        => 'select',
                'label'       => esc_html__('Featured Image for Blog Slider', 'creator'),
                'default'	  => 'landscape',
                'description' => '',
                'parent'      => $post_layout_meta_box,
                'options'     => array(
                    'landscape' => esc_html__('Landscape', 'creator'),
                    'portrait' => esc_html__('Portrait', 'creator')
                )
            )
        );
    }
    add_action('creator_elated_meta_boxes_map', 'creator_elated_blog_meta_box_setting_map');
}