<?php

/*** Post Format ***/

if(!function_exists('creator_elated_masonry_gallery_meta_box_settings')){

	function creator_elated_masonry_gallery_meta_box_settings(){

		$creator_elated_blog_post_format_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Masonry Gallery Type', 'creator'),
				'name' 	=> 'post_format_general_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_masonry_gallery_type_meta',
				'type'        => 'select',
				'label'       => esc_html__('Masonry Gallery Type', 'creator'),
				'description' => esc_html__('Choose masonry gallery type', 'creator'),
				'parent'      => $creator_elated_blog_post_format_meta_box,
				'default_value' => 'default',
				'options'     => array(
					'default' => esc_html__('Default', 'creator'),
					'large_height_width' => esc_html__('Large Height/Width', 'creator'),
					'large_width' => esc_html__('Large Width', 'creator'),
					'large_height' => esc_html__('Large Height', 'creator')
				)
			)
		);


		$creator_elated_pinboard_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Blog Pinboard Type', 'creator'),
				'name' 	=> 'post_format_pinboard_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_pinboard_type_meta',
				'type'        => 'select',
				'label'       => esc_html__('Pinboard Type', 'creator'),
				'description' => esc_html__('Choose Pinboard type', 'creator'),
				'parent'      => $creator_elated_pinboard_meta_box,
				'default_value' => 'default',
				'options'     => array(
					'portrait' => esc_html__('Portrait', 'creator'),
					'landscape' => esc_html__('Landscape', 'creator')
				)
			)
		);

		$creator_elated_blog_list_overlay_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Blog List Shortcode - Overlay Type', 'creator'),
				'name' 	=> 'post_format_overlay_meta'
			)
		);
		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_overlay_type_meta',
				'type'          => 'image',
				'default_value' => 'default',
				'parent'        => $creator_elated_blog_list_overlay_meta_box,
				'label'         => esc_html__('Hover Image', 'creator'),
				'description'   => esc_html__('Set hover image for blog list overlay', 'creator'),
			)
		);
	}

	add_action('creator_elated_meta_boxes_map', 'creator_elated_masonry_gallery_meta_box_settings');
}