<?php
if(!function_exists('creator_elated_map_portfolio_settings')) {
    function creator_elated_map_portfolio_settings() {

        $meta_box = creator_elated_create_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Settings', 'creator'),
            'name'  => 'portfolio_settings_meta_box'
        ));

        creator_elated_create_meta_box_field(array(
            'name'        => 'eltd_portfolio_single_template_meta',
            'type'        => 'select',
            'label'       => esc_html__('Portfolio Type', 'creator'),
            'description' => esc_html__('Choose a default type for Single Project pages', 'creator'),
            'parent'      => $meta_box,
            'options'     => array(
                ''                  => esc_html__('Default', 'creator'),
                'small-images'      => esc_html__('Portfolio small images', 'creator'),
                'small-slider'      => esc_html__('Portfolio small slider', 'creator'),
                'info-slider'       => esc_html__('Portfolio info slider', 'creator'),
                'big-images'        => esc_html__('Portfolio big images', 'creator'),
                'big-slider'        => esc_html__('Portfolio big slider', 'creator'),
                'custom'            => esc_html__('Portfolio custom', 'creator'),
                'full-width-custom' => esc_html__('Portfolio full width custom', 'creator'),
                'gallery'           => esc_html__('Portfolio gallery', 'creator'),
                'masonry'           => esc_html__('Portfolio masonry', 'creator'),
                'masonry-wide'      => esc_html__('Portfolio masonry wide', 'creator')
            )
        ));

        $all_pages = array();
        $pages     = get_pages();
        foreach($pages as $page) {
            $all_pages[$page->ID] = $page->post_title;
        }

        creator_elated_create_meta_box_field(array(
            'name'        => 'portfolio_single_back_to_link',
            'type'        => 'select',
            'label'       => esc_html__( '"Back To" Link', 'creator'),
            'description' => esc_html__('Choose "Back To" page to link from portfolio Single Project page', 'creator'),
            'parent'      => $meta_box,
            'options'     => $all_pages
        ));

        creator_elated_create_meta_box_field(array(
            'name'        => 'portfolio_external_link',
            'type'        => 'text',
            'label'       => esc_html__('Portfolio External Link', 'creator'),
            'description' => esc_html__('Enter URL to link from Portfolio List page', 'creator'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        creator_elated_create_meta_box_field(array(
            'name'        => 'portfolio_masonry_dimenisions',
            'type'        => 'select',
            'label'       => esc_html__('Dimensions for Masonry', 'creator'),
            'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists', 'creator'),
            'parent'      => $meta_box,
            'options'     => array(
                'default'            => esc_html__('Default', 'creator'),
                'large_width'        => esc_html__('Large width', 'creator'),
                'large_height'       => esc_html__('Large height', 'creator'),
                'large_width_height' => esc_html__('Large width/height', 'creator')
            )
        ));

        creator_elated_create_meta_box_field(array(
            'name'        => 'portfolio_masonry_dimenisions',
            'type'        => 'select',
            'label'       => esc_html__('Dimensions for Masonry', 'creator'),
            'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists', 'creator'),
            'parent'      => $meta_box,
            'options'     => array(
                'default'            => esc_html__('Default', 'creator'),
                'large_width'        => esc_html__('Large width', 'creator'),
                'large_height'       => esc_html__('Large height', 'creator'),
                'large_width_height' => esc_html__('Large width/height', 'creator')
            )
        ));

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_portfolio_parallax_item_meta',
                'type' => 'yesno',
                'default_value' => 'no',
                'label' => esc_html__('Set as parallax item?', 'creator'),
                'description' => esc_html__('Enable paralax layout in Masonry Parallax List', 'creator'),
                'parent' => $meta_box,
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'portfolio_featured_blog_slider',
                'type'        => 'select',
                'label'       => esc_html__('Featured Image for Portfolio Slider(Variable Image Size)', 'creator'),
                'default'	  => 'landscape',
                'description' => '',
                'parent'      => $meta_box,
                'options'     => array(
                    'landscape' => esc_html__('Landscape', 'creator'),
                    'portrait' => esc_html__('Portrait', 'creator')
                )
            )
        );
    }

    add_action('creator_elated_meta_boxes_map', 'creator_elated_map_portfolio_settings');
}