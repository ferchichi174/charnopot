<?php
/*** Video Post Format ***/
if(!function_exists('creator_elated_video_post_meta_box_settings_map')){

	function creator_elated_video_post_meta_box_settings_map(){

		$video_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Video Post Format', 'creator'),
				'name' 	=> 'post_format_video_meta'
			)
		);


		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_video_type_meta',
				'type'        => 'select',
				'label'       => 'Video Type',
				'description' => 'Choose video type',
				'parent'      => $video_meta_box,
				'default_value' => 'youtube',
				'options'     => array(
					'social' => esc_html__('Youtube or Vimeo', 'creator'),
					'self' => esc_html__('Self Hosted', 'creator')
				),
				'args' => array(
					'dependence' => true,
					'hide' => array(
						'social' => '#eltd_eltd_video_self_hosted_container',
						'self' => '#eltd_eltd_video_embedded_container'
					),
					'show' => array(
						'social' => '#eltd_eltd_video_embedded_container',
						'self' => '#eltd_eltd_video_self_hosted_container')
				)
			)
		);

		$eltd_video_embedded_container = creator_elated_add_admin_container(
			array(
				'parent' => $video_meta_box,
				'name' => 'eltd_video_embedded_container',
				'hidden_property' => 'eltd_video_type_meta',
				'hidden_value' => 'self'
			)
		);

		$eltd_video_self_hosted_container = creator_elated_add_admin_container(
			array(
				'parent' => $video_meta_box,
				'name' => 'eltd_video_self_hosted_container',
				'hidden_property' => 'eltd_video_type_meta',
				'hidden_values' => array('youtube', 'vimeo')
			)
		);



		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_video_link_meta',
				'type'        => 'text',
				'label'       => 'Video Link',
				'description' => 'Enter Video Link',
				'parent'      => $eltd_video_embedded_container,

			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_video_image_meta',
				'type'        => 'image',
				'label'       => 'Video Image',
				'description' => 'Upload video image',
				'parent'      => $eltd_video_self_hosted_container,

			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_video_webm_link_meta',
				'type'        => 'text',
				'label'       => 'Video WEBM',
				'description' => 'Enter video URL for WEBM format',
				'parent'      => $eltd_video_self_hosted_container,

			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_video_mp4_link_meta',
				'type'        => 'text',
				'label'       => 'Video MP4',
				'description' => 'Enter video URL for MP4 format',
				'parent'      => $eltd_video_self_hosted_container,

			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_video_ogv_link_meta',
				'type'        => 'text',
				'label'       => 'Video OGV',
				'description' => 'Enter video URL for OGV format',
				'parent'      => $eltd_video_self_hosted_container,

			)
		);


	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_video_post_meta_box_settings_map');
}