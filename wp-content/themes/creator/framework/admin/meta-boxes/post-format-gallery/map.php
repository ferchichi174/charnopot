<?php
/*** Gallery Post Format ***/

if(!function_exists('creator_elated_gallery_post_meta_box_settings_map')){
	function creator_elated_gallery_post_meta_box_settings_map(){

		$creator_elated_gallery_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Gallery Post Format', 'creator'),
				'name' 	=> 'post_format_gallery_meta'
			)
		);

		creator_elated_add_multiple_images_field(
			array(
				'name'        => 'eltd_post_gallery_images_meta',
				'label'       => esc_html__('Gallery Images', 'creator'),
				'description' => esc_html__('Choose your gallery images', 'creator'),
				'parent'      => $creator_elated_gallery_meta_box,
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        	=> 'eltd_post_gallery_type',
				'type'        	=> 'select',
				'default_value' => 'slider',
				'label'       	=> esc_html__('Gallery Type', 'creator'),
				'description' 	=> esc_html__('Choose between masonry and slider gallery type', 'creator'),
				'parent'      	=> $creator_elated_gallery_meta_box,
				'options' 		=> array(
					'slider'	=> esc_html__('Slider', 'creator'),
					'masonry'	=> esc_html__('Masonry', 'creator')
				)
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_gallery_post_meta_box_settings_map');
}