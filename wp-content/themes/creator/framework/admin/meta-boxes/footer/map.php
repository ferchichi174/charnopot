<?php
if(!function_exists('creator_elated_footer_meta_box_settings_map')){
    function creator_elated_footer_meta_box_settings_map(){

        $footer_meta_box = creator_elated_create_meta_box(
            array(
                'scope' => array('page', 'portfolio-item', 'post'),
                'title' => esc_html__('Footer', 'creator'),
                'name' => 'footer_meta'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_disable_footer_meta',
                'type' => 'yesno',
                'default_value' => 'no',
                'label' => esc_html__('Disable Footer for this Page', 'creator'),
                'description' => esc_html__('Enabling this option will hide footer on this page', 'creator'),
                'parent' => $footer_meta_box,
            )
        );
        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_footer_background_image_meta',
                'type' => 'image',
                'default_value' => '',
                'label' => esc_html__('Footer Background Image for this Page', 'creator'),
                'parent' => $footer_meta_box,
            )
        );
        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_show_footer_image_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Show Footer Background Image For This Page', 'creator'),
                'description' => esc_html__('Enabling this option will show footer background image for this page', 'creator'),
                'parent' => $footer_meta_box,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No', 'creator'),
                    'yes' => esc_html__('Yes', 'creator')
                )
            )


        );
    }
    add_action('creator_elated_meta_boxes_map', 'creator_elated_footer_meta_box_settings_map');
}