<?php
if(!function_exists('creator_elated_portfolio_meta_box_settings_map')){
	function creator_elated_portfolio_meta_box_settings_map(){

		$eltd_pages = array();
		$pages = get_pages();

		global $creator_elated_Framework;
		foreach($pages as $page) {
			$eltd_pages[$page->ID] = $page->post_title;
		}


		//Portfolio Images
		$eltdPortfolioImages = new CreatorElatedMetaBox('portfolio-item', esc_html__('Portfolio Images (multiple upload)','creator'), '', '', 'portfolio_images');
		$creator_elated_Framework->eltdMetaBoxes->addMetaBox('portfolio_images',$eltdPortfolioImages);

		$eltd_portfolio_image_gallery = new CreatorElatedMultipleImages('eltd_portfolio-image-gallery', esc_html__('Portfolio Images','creator'), esc_html__('Choose your portfolio images','creator'));
		$eltdPortfolioImages->addChild('eltd_portfolio-image-gallery',$eltd_portfolio_image_gallery);

		//Portfolio Images/Videos 2

		$eltdPortfolioImagesVideos2 = new CreatorElatedMetaBox('portfolio-item', esc_html__('Portfolio Images/Videos (single upload)','creator'));
		$creator_elated_Framework->eltdMetaBoxes->addMetaBox('portfolio_images_videos2',$eltdPortfolioImagesVideos2);

		$eltd_portfolio_images_videos2 = new CreatorElatedImagesVideosFramework(esc_html__('Portfolio Images/Videos 2' , 'creator'), esc_html__('ThisIsDescription', 'creator'));
		$eltdPortfolioImagesVideos2->addChild('eltd_portfolio_images_videos2',$eltd_portfolio_images_videos2);

		//Portfolio Additional Sidebar Items

		$eltdAdditionalSidebarItems = creator_elated_create_meta_box(
			array(
				'scope' => array('portfolio-item'),
				'title' => esc_html__('Additional Portfolio Sidebar Items', 'creator'),
				'name' => 'portfolio_properties'
			)
		);

		$eltd_portfolio_properties = creator_elated_add_options_framework(
			array(
				'label' => 'Portfolio Properties',
				'name' => esc_html__('eltd_portfolio_properties', 'creator'),
				'parent' => $eltdAdditionalSidebarItems
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_portfolio_meta_box_settings_map');
}