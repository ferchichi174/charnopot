<?php
if(!function_exists('creator_elated_carousel_meta_box_settings_map')){
    function creator_elated_carousel_meta_box_settings_map(){

        //Carousels
        $carousel_meta_box = creator_elated_create_meta_box(
            array(
                'scope' => array('carousels'),
                'title' => esc_html__('Carousel', 'creator'),
                'name' => 'carousel_meta'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_carousel_image',
                'type'        => 'image',
                'label'       => esc_html__('Carousel Image', 'creator'),
                'description' => esc_html__('Choose carousel image (min width needs to be 215px)', 'creator'),
                'parent'      => $carousel_meta_box
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_carousel_hover_image',
                'type'        => 'image',
                'label'       => esc_html__('Carousel Hover Image', 'creator'),
                'description' => esc_html__('Choose carousel hover image (min width needs to be 215px)', 'creator'),
                'parent'      => $carousel_meta_box
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_carousel_item_link',
                'type'        => 'text',
                'label'       => esc_html__('Link', 'creator'),
                'description' => esc_html__('Enter the URL to which you want the image to link to (e.g. http://www.example.com)', 'creator'),
                'parent'      => $carousel_meta_box
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'        => 'eltd_carousel_item_target',
                'type'        => 'selectblank',
                'label'       => esc_html__('Target', 'creator'),
                'description' => esc_html__('Specify where to open the linked document', 'creator'),
                'parent'      => $carousel_meta_box,
                'options' => array(
                    '_self' => esc_html__('Self', 'creator'),
                    '_blank' => esc_html__('Blank', 'creator')
                )
            )
        );

    }
    add_action('creator_elated_meta_boxes_map', 'creator_elated_carousel_meta_box_settings_map');
}