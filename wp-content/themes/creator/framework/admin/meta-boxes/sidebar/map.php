<?php
if(!function_exists('creator_elated_sidebar_meta_box_settings_map')) {

	function creator_elated_sidebar_meta_box_settings_map() {
		$custom_sidebars = creator_elated_get_custom_sidebars();

		$sidebar_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array('page', 'portfolio-item', 'post'),
				'title' => esc_html__('Sidebar','creator'),
				'name' => 'sidebar_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_sidebar_meta',
				'type'        => 'select',
				'label'       => esc_html__('Layout','creator'),
				'description' => esc_html__('Choose the sidebar layout','creator'),
				'parent'      => $sidebar_meta_box,
				'options'     => array(
					''			=> esc_html__('Default','creator'),
					'no-sidebar'		=> esc_html__('No Sidebar','creator'),
					'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right','creator'),
					'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right','creator'),
					'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left','creator'),
					'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left','creator')
				)
			)
		);

		if(count($custom_sidebars) > 0) {
			creator_elated_create_meta_box_field(array(
				'name' => 'eltd_custom_sidebar_meta',
				'type' => 'selectblank',
				'label' => esc_html__('Choose Widget Area in Sidebar','creator'),
				'description' => esc_html__('Choose Custom Widget area to display in Sidebar"', 'creator'),
				'parent' => $sidebar_meta_box,
				'options' => $custom_sidebars
			));
		}

	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_sidebar_meta_box_settings_map');
}