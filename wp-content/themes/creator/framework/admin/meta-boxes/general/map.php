<?php
if(!function_exists('creator_elated_general_meta_box_settings_map')){
	function creator_elated_general_meta_box_settings_map(){

		$general_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array('page', 'portfolio-item', 'post'),
				'title' => esc_html__('General', 'creator'),
				'name' => 'general_meta'
			)
		);
		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_boxed_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Boxed Layout', 'creator'),
				'description' => esc_html__('Enable Boxed Layout', 'creator'),
				'options' => array(
					'' => '',
					'no' => esc_html__('No','creator'),
					'yes' => esc_html__('Yes','creator')
				),
				'parent' => $general_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_page_background_color_meta',
				'type' => 'color',
				'default_value' => '',
				'label' => esc_html__('Page Background Color', 'creator'),
				'description' => esc_html__('Choose background color for page content', 'creator'),
				'parent' => $general_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_page_padding_meta',
				'type' => 'text',
				'default_value' => '',
				'label' => esc_html__('Page Padding', 'creator'),
				'description' => esc_html__('Insert padding in format 10px 10px 10px 10px', 'creator'),
				'parent' => $general_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_main_style_meta',
				'type'        => 'select',
				'label'       => esc_html__('Predefined Font Style', 'creator'),
				'description' => esc_html__('Choose predefined font style', 'creator'),
				'parent'      => $general_meta_box,
				'default_value' => '',
				'options'     => array(
					''  => '',
					'default_style'   => esc_html__('Default Style', 'creator'),
					'oswald_style'   => esc_html__('Style with Oswald Font', 'creator'),
					'poppins_style'   => esc_html__('Style with Poppins Font', 'creator'),
					'raleway_style'   => esc_html__('Style with Raleway Font', 'creator'),
					'yesteryear_style'   => esc_html__('Style with Yesteryear Font', 'creator'),
					'greatvibes_style'   => esc_html__('Style with Great Vibes Font', 'creator')
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_page_slider_meta',
				'type' => 'text',
				'default_value' => '',
				'label' => esc_html__('Slider Shortcode', 'creator'),
				'description' => esc_html__('Paste your slider shortcode here', 'creator'),
				'parent' => $general_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_enable_paspartu_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Passepartout', 'creator'),
				'description' => esc_html__('Enabling this option will display passepartout around site content', 'creator'),
				'parent' => $general_meta_box,
				'options' => array(
					'' => '',
					'no' => esc_html__('No', 'creator'),
					'yes' => esc_html__('Yes', 'creator')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"" => "",
						"no" => "#eltd_eltd_paspartu_meta_container",
						"yes" => ""
					),
					"show" => array(
						"" => "#eltd_eltd_paspartu_meta_container",
						"no" => "",
						"yes" => "#eltd_eltd_paspartu_meta_container"
					)
				)
			)
		);

		$paspartu_meta_container = creator_elated_add_admin_container(
			array(
				'parent' => $general_meta_box,
				'name' => 'eltd_paspartu_meta_container',
				'hidden_property' => 'eltd_enable_paspartu_meta',
				'hidden_value' => 'no'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_paspartu_color_meta',
				'type'          => 'color',
				'default_value' => '',
				'label'         => esc_html__('Passepartout Color', 'creator'),
				'description'   => esc_html__('Choose passepartout color.Default value is #fff', 'creator'),
				'parent'        => $paspartu_meta_container,
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_paspartu_size_meta',
				'type'          => 'text',
				'default_value' => '',
				'label'         => esc_html__('Passepartout Size', 'creator'),
				'description'   => esc_html__('Enter size amount for passepartout.Default value is 10px', 'creator'),
				'parent'        => $paspartu_meta_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);


		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_page_comments_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__('Show Comments', 'creator'),
				'description' => esc_html__('Enabling this option will show comments on your page', 'creator'),
				'parent'      => $general_meta_box,
				'options'     => array(
					'yes' => esc_html__('Yes', 'creator'),
					'no' => esc_html__('No', 'creator')
				)
			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_general_meta_box_settings_map');
}