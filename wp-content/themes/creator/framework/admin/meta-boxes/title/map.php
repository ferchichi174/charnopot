<?php

if(!function_exists('creator_elated_title_meta_box_settings_map')){

    function creator_elated_title_meta_box_settings_map(){
        $title_meta_box = creator_elated_create_meta_box(
            array(
                'scope' => array('page', 'portfolio-item', 'post'),
                'title' => esc_html__('Title', 'creator'),
                'name' => 'title_meta'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_show_title_area_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Show Title Area', 'creator'),
                'description' => esc_html__('Disabling this option will turn off page title area', 'creator'),
                'parent' => $title_meta_box,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No', 'creator'),
                    'yes' => esc_html__('Yes', 'creator')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        '' => '',
                        'no' => '#eltd_eltd_show_title_area_meta_container',
                        'yes' => ''
                    ),
                    'show' => array(
                        '' => '#eltd_eltd_show_title_area_meta_container',
                        'no' => '',
                        'yes' => '#eltd_eltd_show_title_area_meta_container'
                    )
                )
            )
        );

        $show_title_area_meta_container = creator_elated_add_admin_container(
            array(
                'parent' => $title_meta_box,
                'name' => 'eltd_show_title_area_meta_container',
                'hidden_property' => 'eltd_show_title_area_meta',
                'hidden_value' => 'no'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_type_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Title Area Type', 'creator'),
                'description' => esc_html__('Choose title type', 'creator'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'standard' => esc_html__('Standard', 'creator'),
                    'breadcrumb' => esc_html__('Breadcrumb', 'creator')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        'standard' => '',
                        'standard' => '',
                        'breadcrumb' => '#eltd_eltd_title_area_type_meta_container'
                    ),
                    'show' => array(
                        '' => '#eltd_eltd_title_area_type_meta_container',
                        'standard' => '#eltd_eltd_title_area_type_meta_container',
                        'breadcrumb' => ''
                    )
                )
            )
        );

        $title_area_type_meta_container = creator_elated_add_admin_container(
            array(
                'parent' => $show_title_area_meta_container,
                'name' => 'eltd_title_area_type_meta_container',
                'hidden_property' => 'eltd_title_area_type_meta',
                'hidden_value' => '',
                'hidden_values' => array('breadcrumb'),
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_enable_breadcrumbs_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Enable Breadcrumbs', 'creator'),
                'description' => esc_html__('This option will display Breadcrumbs in Title Area', 'creator'),
                'parent' => $title_area_type_meta_container,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No', 'creator'),
                    'yes' => esc_html__('Yes', 'creator')
                ),
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_animation_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Animations', 'creator'),
                'description' => esc_html__('Choose an animation for Title Area', 'creator'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No Animation', 'creator'),
                    'right-left' => esc_html__('Text right to left', 'creator'),
                    'left-right' => esc_html__('Text left to right', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_vertial_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Vertical Alignment', 'creator'),
                'description' => esc_html__('Specify title vertical alignment', 'creator'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'header_bottom' => esc_html__('From Bottom of Header', 'creator'),
                    'window_top' => esc_html__('From Window Top', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_size_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Title Size', 'creator'),
                'description' => esc_html__('Specify title size', 'creator'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'small' => esc_html__('Small', 'creator'),
                    'medium' => esc_html__('Medium', 'creator'),
                    'large' => esc_html__('Large', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_content_alignment_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Horizontal Alignment', 'creator'),
                'description' => esc_html__('Specify title horizontal alignment', 'creator'),
                'parent' => $show_title_area_meta_container,
                'options' => array(
                    '' => '',
                    'left' => esc_html__('Left', 'creator'),
                    'center' => esc_html__('Center', 'creator'),
                    'right' => esc_html__('Right', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_text_color_meta',
                'type' => 'color',
                'label' => esc_html__('Title Color', 'creator'),
                'description' => esc_html__('Choose a color for title text', 'creator'),
                'parent' => $show_title_area_meta_container
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_text_background_color',
                'type' => 'color',
                'label' => esc_html__('Title Background Color', 'creator'),
                'description' => esc_html__('Choose a color for title text background', 'creator'),
                'parent' => $show_title_area_meta_container
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_breadcrumb_color_meta',
                'type' => 'color',
                'label' => esc_html__('Breadcrumb Color', 'creator'),
                'description' => esc_html__('Choose a color for breadcrumb text', 'creator'),
                'parent' => $show_title_area_meta_container
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_color_meta',
                'type' => 'color',
                'label' => esc_html__('Background Color', 'creator'),
                'description' => esc_html__('Choose a background color for Title Area', 'creator'),
                'parent' => $show_title_area_meta_container
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_hide_background_image_meta',
                'type' => 'yesno',
                'default_value' => 'no',
                'label' => esc_html__('Hide Background Image', 'creator'),
                'description' => esc_html__('Enable this option to hide background image in Title Area', 'creator'),
                'parent' => $show_title_area_meta_container,
                'args' => array(
                    'dependence' => true,
                    'dependence_hide_on_yes' => '#eltd_eltd_hide_background_image_meta_container',
                    'dependence_show_on_yes' => ''
                )
            )
        );

        $hide_background_image_meta_container = creator_elated_add_admin_container(
            array(
                'parent' => $show_title_area_meta_container,
                'name' => 'eltd_hide_background_image_meta_container',
                'hidden_property' => 'eltd_hide_background_image_meta',
                'hidden_value' => 'yes'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_image_meta',
                'type' => 'image',
                'label' => esc_html__('Background Image', 'creator'),
                'description' => esc_html__('Choose an Image for Title Area', 'creator'),
                'parent' => $hide_background_image_meta_container
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_image_responsive_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Background Responsive Image', 'creator'),
                'description' => esc_html__('Enabling this option will make Title background image responsive', 'creator'),
                'parent' => $hide_background_image_meta_container,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No', 'creator'),
                    'yes' => esc_html__('Yes', 'creator')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        '' => '',
                        'no' => '',
                        'yes' => '#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta'
                    ),
                    'show' => array(
                        '' => '#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta',
                        'no' => '#eltd_eltd_title_area_background_image_responsive_meta_container, #eltd_eltd_title_area_height_meta',
                        'yes' => ''
                    )
                )
            )
        );

        $title_area_background_image_responsive_meta_container = creator_elated_add_admin_container(
            array(
                'parent' => $hide_background_image_meta_container,
                'name' => 'eltd_title_area_background_image_responsive_meta_container',
                'hidden_property' => 'eltd_title_area_background_image_responsive_meta',
                'hidden_value' => 'yes'
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_title_area_background_image_parallax_meta',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Background Image in Parallax', 'creator'),
                'description' => esc_html__('Enabling this option will make Title background image parallax', 'creator'),
                'parent' => $title_area_background_image_responsive_meta_container,
                'options' => array(
                    '' => '',
                    'no' => esc_html__('No', 'creator'),
                    'yes' => esc_html__('Yes', 'creator'),
                    'yes_zoom' => esc_html__('Yes, with zoom out', 'creator')
                )
            )
        );

        creator_elated_create_meta_box_field(array(
            'name' => 'eltd_title_area_height_meta',
            'type' => 'text',
            'label' => esc_html__('Height', 'creator'),
            'description' => esc_html__('Set a height for Title Area', 'creator'),
            'parent' => $show_title_area_meta_container,
            'args' => array(
                'col_width' => 2,
                'suffix' => 'px'
            )
        ));

        creator_elated_create_meta_box_field(array(
            'name' => 'eltd_title_area_subtitle_meta',
            'type' => 'text',
            'default_value' => '',
            'label' => esc_html__('Subtitle Text', 'creator'),
            'description' => esc_html__('Enter your subtitle text', 'creator'),
            'parent' => $show_title_area_meta_container,
            'args' => array(
                'col_width' => 6
            )
        ));

        creator_elated_create_meta_box_field(
            array(
                'name' => 'eltd_subtitle_color_meta',
                'type' => 'color',
                'label' => esc_html__('Subtitle Color', 'creator'),
                'description' => esc_html__('Choose a color for subtitle text', 'creator'),
                'parent' => $show_title_area_meta_container
            )
        );
    }
    add_action('creator_elated_meta_boxes_map', 'creator_elated_title_meta_box_settings_map');
}