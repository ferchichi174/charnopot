<?php
if(!function_exists('creator_elated_header_meta_box_setting_map')){

	function creator_elated_header_meta_box_setting_map(){

		$header_meta_box = creator_elated_create_meta_box(
			array(
				'scope' => array( 'page', 'portfolio-item', 'post' ),
				'title' => esc_html__('Header', 'creator'),
				'name'  => 'header_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_logo_image_meta',
				'type' => 'image',
				'default_value' => "",
				'label' => esc_html__('Logo Image', 'creator'),
				'description' => esc_html__('Choose a default logo image to display ', 'creator'),
				'parent' => $header_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name' => 'eltd_hide_logo_meta',
				'type'          => 'select',
				'default_value' => '',
				'label' => esc_html__('Hide Logo', 'creator'),
				'description' => esc_html__('Hide logo image for this page', 'creator'),
				'parent' => $header_meta_box,
				'options'       => array(
					''                => '',
					'yes' => esc_html__('Yes', 'creator'),
					'no' => esc_html__('No', 'creator')
				)
			)
		);


		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_top_bar_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Display Top Bar', 'creator'),
				'description'   => esc_html__('Enabling this option will show top bar area', 'creator'),
				'parent'        => $header_meta_box,
				'options'       => array(
					''                => '',
					'yes' => esc_html__('Yes', 'creator'),
					'no' => esc_html__('No', 'creator')
				)
			)
		);
		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_top_bar_skin_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Top Bar Skin', 'creator'),
				'description'   => esc_html__('Choose top bar skin', 'creator'),
				'parent'        => $header_meta_box,
				'options'       => array(
					''                => '',
					'light' => esc_html__('Light', 'creator'),
					'dark' => esc_html__('Dark', 'creator')
				)
			)
		);
        creator_elated_create_meta_box_field(
            array(
                'name'          => 'eltd_top_bar_background_color_meta',
                'type'          => 'color',
                'default_value' => '',
                'label'         => esc_html__('Top Bar Background Color', 'creator'),
                'description'   => esc_html__('Set background color for top bar', 'creator'),
                'parent'        => $header_meta_box,
                'args'        => array(
                    'col_width' => 3
                )
            )
        );

        creator_elated_create_meta_box_field(
            array(
                'name'          => 'eltd_top_bar_background_transparency_meta',
                'type'          => 'text',
                'default_value' => '',
                'label'         => esc_html__('Top Bar Background Transparency', 'creator'),
                'description'   => esc_html__('Set background transparency for top bar', 'creator'),
                'parent'        => $header_meta_box,
                'args'        => array(
                    'col_width' => 3
                )
            )
        );


		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_header_type_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Header Type', 'creator'),
				'description'   => esc_html__('Select the type of header you would like to use', 'creator'),
				'parent'        => $header_meta_box,
				'options'       => array(
					''                => '',
					'header-standard' => esc_html__('Header Standard', 'creator'),
					'header-classic' => esc_html__('Header Classic', 'creator'),
					'header-centered' => esc_html__('Header Centered', 'creator'),
					'header-divided'  => esc_html__('Header Divided', 'creator'),
					'header-dual'  => esc_html__('Header Dual', 'creator'),
					'header-compound'  => esc_html__('Header Compound', 'creator'),
					'header-vertical' => esc_html__('Header Vertical', 'creator'),
					'header-simple' => esc_html__('Header Simple', 'creator'),
					'header-full-screen' => esc_html__('Header Full Screen', 'creator')
				),
				'args' => array(
					'use_images' => true,
					'hide_labels' => true,
					'dependence' => true,
					'show' => array(
						'header-standard' => '',
						'header-classic' =>'',
						'header-centered' => '#eltd_eltd_logo_area_background_color_meta',
						'header-divided' => '',
						'header-vertical' => '',
						'header-simple' => '',
						'header-compound' => '#eltd_eltd_logo_area_background_color_meta',
						'header-dual' => '#eltd_eltd_logo_area_background_color_meta',
						'header-full-screen' =>''
					),
					'hide' => array(
						'header-standard' => '#eltd_eltd_logo_area_background_color_meta',
						'header-classic' => '#eltd_eltd_logo_area_background_color_meta',
						'header-centered' => '',
						'header-divided' => '#eltd_eltd_logo_area_background_color_meta',
						'header-vertical' => '#eltd_eltd_logo_area_background_color_meta',
						'header-simple' => '#eltd_eltd_logo_area_background_color_meta',
						'header-compound' => '',
						'header-dual' => '',
						'header-full-screen' => '#eltd_eltd_logo_area_background_color_meta'
					)
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_header_in_grid_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Header in grid', 'creator'),
				'description'   => esc_html__('Set header content to be in grid', 'creator'),
				'parent'        => $header_meta_box,
				'options'       => array(
					''    => '',
					'yes' => esc_html__('Yes', 'creator'),
					'no'  => esc_html__('No', 'creator')
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_header_style_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Header Skin', 'creator'),
				'description'   => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'creator'),
				'parent'        => $header_meta_box,
				'options'       => array(
					''             => '',
					'light-header' => esc_html__('Light', 'creator'),
					'dark-header'  => esc_html__('Dark', 'creator')
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'parent' => $header_meta_box,
				'type' => 'selectblank',
				'name' => 'eltd_header_standard_align_meta',
				'default_value' => '',
				'label' => esc_html__('Header Standard Menu area alignment','creator'),
				'description' => '',
				'options' => array(
					'' => '',
					'center' => esc_html__('Center' , 'creator'),
					'left' => esc_html__('Left' , 'creator'),
					'right' => esc_html__('Right' , 'creator'),
				),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_header_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__('Menu Area Background Color', 'creator'),
				'description' => esc_html__('Choose a background color for header menu area', 'creator'),
				'parent'      => $header_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_logo_area_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__('Logo Area Background Color', 'creator'),
				'description' => esc_html__('Choose a background color for header logo area.', 'creator'),
				'parent'      => $header_meta_box,
				'hidden_property' => 'eltd_header_type_meta',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-vertical',
					'header-standard',
					'header-simple',
					'header-classic',
					'header-divided',
					'header-full-screen'
				),
			)
		);


		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_header_background_transparency_meta',
				'type'        => 'text',
				'label'       => esc_html__('Transparency', 'creator'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'creator'),
				'parent'      => $header_meta_box,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_header_height_meta',
				'type'        => 'text',
				'label'       => esc_html__('Menu Area Height', 'creator'),
				'description' => esc_html__('Set Menu Area Height', 'creator'),
				'parent'      => $header_meta_box,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_logo_height_meta',
				'type'        => 'text',
				'label'       => esc_html__('Logo Area Height', 'creator'),
				'description' => esc_html__('Set Logo Area Height', 'creator'),
				'parent'      => $header_meta_box,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'parent'        => $header_meta_box,
				'type'          => 'select',
				'name'          => 'eltd_enable_header_style_on_scroll_meta',
				'default_value' => '',
				'label'         => esc_html__('Enable Header Style on Scroll', 'creator'),
				'description'   => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'creator'),
				'options'       => array(
					''    => '',
					'no'  => esc_html__('No', 'creator'),
					'yes' => esc_html__('Yes', 'creator')
				)
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_vertical_header_background_image_meta',
				'type'          => 'image',
				'default_value' => '',
				'label'         => esc_html__('Background Image', 'creator'),
				'description'   => esc_html__('Set background image for vertical menu', 'creator'),
				'parent'        => $header_meta_box
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'          => 'eltd_disable_vertical_header_background_image_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Disable Background Image', 'creator'),
				'description'   => esc_html__('Enabling this option will hide background image in Vertical Menu', 'creator'),
				'parent'        => $header_meta_box
			)
		);

		if ( creator_elated_options()->getOptionValue( 'header_type' ) !== 'header-vertical' ) {
			creator_elated_create_meta_box_field(
				array(
					'name'            => 'eltd_scroll_amount_for_sticky_meta',
					'type'            => 'text',
					'label'           => esc_html__('Scroll amount for sticky header appearance', 'creator'),
					'description'     => esc_html__('Define scroll amount for sticky header appearance', 'creator'),
					'parent'          => $header_meta_box,
					'args'            => array(
						'col_width' => 2,
						'suffix'    => 'px'
					),
					'hidden_property' => 'eltd_header_behaviour',
					'hidden_values'   => array( "sticky-header-on-scroll-up", "fixed-on-scroll" )
				)
			);
		}

	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_header_meta_box_setting_map');
}