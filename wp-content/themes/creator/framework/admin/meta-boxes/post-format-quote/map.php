<?php

/*** Quote Post Format ***/
if(!function_exists('creator_elated_quote_post_meta_box_settings_map')){
	function creator_elated_quote_post_meta_box_settings_map(){
		$quote_meta_box = creator_elated_create_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Quote Post Format', 'creator'),
				'name' 	=> 'post_format_quote_meta'
			)
		);

		creator_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__('Quote Text','creator'),
				'description' => esc_html__('Enter Quote text','creator'),
				'parent'      => $quote_meta_box,

			)
		);
	}
	add_action('creator_elated_meta_boxes_map', 'creator_elated_quote_post_meta_box_settings_map');
}