<?php

if(!function_exists('creator_elated_header_register_main_navigation')) {
    /**
     * Registers main navigation
     */
    function creator_elated_header_register_main_navigation() {
        register_nav_menus(
            array(
                'main-navigation' => esc_html__('Main Navigation', 'creator'),
                'vertical-navigation' => esc_html__('Vertical Navigation', 'creator'),
                'left-navigation' => esc_html__('Left Navigation', 'creator'),
                'right-navigation' => esc_html__('Right Navigation', 'creator')
            )
        );
    }

    add_action('after_setup_theme', 'creator_elated_header_register_main_navigation');
}

if(!function_exists('creator_elated_is_top_bar_transparent')) {
    /**
     * Checks if top bar is transparent or not
     *
     * @return bool
     */
    function creator_elated_is_top_bar_transparent() {
        $top_bar_enabled = creator_elated_is_top_bar_enabled();

        $top_bar_bg_color = creator_elated_options()->getOptionValue('top_bar_background_color');
        $top_bar_transparency = creator_elated_options()->getOptionValue('top_bar_background_transparency');

        if($top_bar_enabled && $top_bar_bg_color !== '' && $top_bar_transparency !== '') {
            return $top_bar_transparency >= 0 && $top_bar_transparency < 1;
        }

        return false;
    }
}

if(!function_exists('creator_elated_is_top_bar_completely_transparent')) {
    function creator_elated_is_top_bar_completely_transparent() {
        $top_bar_enabled = creator_elated_is_top_bar_enabled();

        $top_bar_bg_color = creator_elated_options()->getOptionValue('top_bar_background_color');
        $top_bar_transparency = creator_elated_options()->getOptionValue('top_bar_background_transparency');

        if($top_bar_enabled && $top_bar_bg_color !== '' && $top_bar_transparency !== '') {
            return $top_bar_transparency === '0';
        }

        return false;
    }
}

if(!function_exists('creator_elated_is_top_bar_enabled')) {
    function creator_elated_is_top_bar_enabled() {
        $top_bar_enabled = creator_elated_options()->getOptionValue('top_bar') == 'yes';

        return $top_bar_enabled;
    }
}

if(!function_exists('creator_elated_get_top_bar_height')) {
    /**
     * Returns top bar height
     *
     * @return bool|int|void
     */
    function creator_elated_get_top_bar_height() {
        if(creator_elated_get_meta_field_intersect('top_bar')==='yes') {
            $top_bar_height = creator_elated_filter_px(creator_elated_options()->getOptionValue('top_bar_height'));

            return $top_bar_height !== '' ? intval($top_bar_height) : 42;
        }

        return 0;
    }
}

if(!function_exists('creator_elated_get_sticky_header_height')) {
    /**
     * Returns top sticky header height
     *
     * @return bool|int|void
     */
    function creator_elated_get_sticky_header_height() {
        //sticky menu height, needed only for sticky header on scroll up
        if(creator_elated_options()->getOptionValue('header_type') !== 'header-vertical' &&
           in_array(creator_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up'))) {

            $sticky_header_height = creator_elated_filter_px(creator_elated_options()->getOptionValue('sticky_header_height'));

            return $sticky_header_height !== '' ? intval($sticky_header_height) : 60;
        }

        return 0;

    }
}

if(!function_exists('creator_elated_get_sticky_header_height_of_complete_transparency')) {
    /**
     * Returns top sticky header height it is fully transparent. used in anchor logic
     *
     * @return bool|int|void
     */
    function creator_elated_get_sticky_header_height_of_complete_transparency() {

        if(creator_elated_options()->getOptionValue('header_type') !== 'header-vertical') {
            if(creator_elated_options()->getOptionValue('sticky_header_transparency') === '0') {
                $stickyHeaderTransparent = creator_elated_options()->getOptionValue('sticky_header_grid_background_color') !== '' &&
                                           creator_elated_options()->getOptionValue('sticky_header_grid_transparency') === '0';
            } else {
                $stickyHeaderTransparent = creator_elated_options()->getOptionValue('sticky_header_background_color') !== '' &&
                                           creator_elated_options()->getOptionValue('sticky_header_transparency') === '0';
            }

            if($stickyHeaderTransparent) {
                return 0;
            } else {
                $sticky_header_height = creator_elated_filter_px(creator_elated_options()->getOptionValue('sticky_header_height'));

                return $sticky_header_height !== '' ? intval($sticky_header_height) : 60;
            }
        }
        return 0;
    }
}

if(!function_exists('creator_elated_get_sticky_scroll_amount')) {
    /**
     * Returns top sticky scroll amount
     *
     * @return bool|int|void
     */
    function creator_elated_get_sticky_scroll_amount() {

        //sticky menu scroll amount
        if(creator_elated_options()->getOptionValue('header_type') !== 'header-vertical' && in_array(creator_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {

            $sticky_scroll_amount = creator_elated_filter_px(creator_elated_options()->getOptionValue('scroll_amount_for_sticky'));

            return $sticky_scroll_amount !== '' ? intval($sticky_scroll_amount) : 0;
        }

        return 0;
    }
}

if(!function_exists('creator_elated_get_sticky_scroll_amount_per_page')) {
    /**
     * Returns top sticky scroll amount
     *
     * @return bool|int|void
     */
    function creator_elated_get_sticky_scroll_amount_per_page() {
        $post_id =  get_the_ID();
        //sticky menu scroll amount
        if(creator_elated_options()->getOptionValue('header_type') !== 'header-vertical'  && in_array(creator_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {

            $sticky_scroll_amount_per_page = creator_elated_filter_px(get_post_meta($post_id, "eltd_scroll_amount_for_sticky_meta", true));

            return $sticky_scroll_amount_per_page !== '' ? intval($sticky_scroll_amount_per_page) : 0;
        }

        return 0;
    }
}