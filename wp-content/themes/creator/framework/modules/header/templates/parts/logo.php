<?php do_action('creator_elated_before_site_logo'); ?>

<div class="eltd-logo-wrapper">
    <a href="<?php echo esc_url(home_url('/')); ?>" <?php creator_elated_inline_style($logo_styles); ?>>
        <img class="eltd-normal-logo" src="<?php echo esc_url($logo_image); ?>" alt="<?php esc_attr_e('Logo','creator'); ?>"/>
        <?php if(!empty($logo_image_dark)){ ?><img class="eltd-dark-logo" src="<?php echo esc_url($logo_image_dark); ?>" alt="<?php esc_attr_e('Dark Logo','creator'); ?>o"/><?php } ?>
        <?php if(!empty($logo_image_light)){ ?><img class="eltd-light-logo" src="<?php echo esc_url($logo_image_light); ?>" alt="<?php esc_attr_e('Light Logo','creator'); ?>"/><?php } ?>
    </a>
</div>

<?php do_action('creator_elated_after_site_logo'); ?>