<?php do_action('creator_elated_before_mobile_logo'); ?>

<div class="eltd-mobile-logo-wrapper">
    <a href="<?php echo esc_url(home_url('/')); ?>" <?php creator_elated_inline_style($logo_styles); ?>>
        <img src="<?php echo esc_url($logo_image); ?>" alt="<?php esc_attr_e('Mobile Logo','creator'); ?>"/>
    </a>
</div>

<?php do_action('creator_elated_after_mobile_logo'); ?>