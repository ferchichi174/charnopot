<?php do_action('creator_elated_before_top_navigation'); ?>

    <nav data-navigation-type='<?php echo creator_elated_get_module_part( $vertical_menu_type ); ?>' class="eltd-vertical-menu <?php echo creator_elated_get_module_part( $vertical_menu_class ); ?>">
        <?php
        wp_nav_menu(array(
            'theme_location'  => 'vertical-navigation',
            'container'       => '',
            'container_class' => '',
            'menu_class'      => '',
            'menu_id'         => '',
            'fallback_cb'     => 'top_navigation_fallback',
            'link_before'     => '<span>',
            'link_after'      => '</span>',
            'walker'          => new CreatorElatedTopNavigationWalker()
        ));
        ?>
    </nav>

<?php do_action('creator_elated_after_top_navigation'); ?>