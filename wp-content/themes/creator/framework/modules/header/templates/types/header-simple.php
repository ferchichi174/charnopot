<?php do_action('creator_elated_before_page_header'); ?>
    <header class="eltd-page-header <?php echo esc_attr($grid_class) ?>">
        <div class="eltd-logo-area" <?php creator_elated_inline_style($logo_area_background_color); ?>>
            <?php if($menu_area_in_grid) { ?>
            <div class="eltd-grid" <?php creator_elated_inline_style($logo_area_grid_background_color); ?>>
                <?php } ?>
                <div class="eltd-vertical-align-containers">
                    <div class="eltd-position-left">
                        <div class="eltd-position-left-inner">
                            <div class="eltd-left-from-menu-widget-holder">
                                <?php if(is_active_sidebar('eltd-left-from-main-menu-simple')) : ?>
                                    <?php dynamic_sidebar('eltd-left-from-main-menu-simple'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="eltd-position-center">
                        <div class="eltd-position-center-inner">
                            <?php if(!$hide_logo) {
                                creator_elated_get_logo();
                            } ?>
                        </div>
                    </div>
                    <div class="eltd-position-right">
                        <div class="eltd-position-right-inner">
                            <?php if(is_active_sidebar('eltd-right-from-main-menu-simple')) : ?>
                                <?php dynamic_sidebar('eltd-right-from-main-menu-simple'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div><?php if($menu_area_in_grid) { ?>
            </div>
        <?php } ?>
        </div>
        <div class="eltd-menu-area" <?php creator_elated_inline_style($menu_area_background_color); ?>>
            <?php if($menu_area_in_grid) { ?>
            <div class="eltd-grid" <?php creator_elated_inline_style($menu_area_grid_background_color); ?>>
                <?php } ?>
                <div class="eltd-vertical-align-containers">

                    <div class="eltd-position-center">
                        <div class="eltd-position-center-inner">
                            <?php creator_elated_get_main_menu(); ?>
                        </div>
                    </div>

                </div>
                <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
        </div>

        <?php if($show_sticky) {
            creator_elated_get_sticky_header();
        } ?>
    </header>

<?php do_action('creator_elated_after_page_header'); ?>