<?php do_action('creator_elated_before_page_header'); ?>

<header class="eltd-page-header">
    <?php if($show_fixed_wrapper) : ?>
    <div class="eltd-fixed-wrapper">
        <?php endif; ?>
        <div class="eltd-menu-area" <?php creator_elated_inline_style($menu_area_background_color); ?>>
            <?php if($menu_area_in_grid) : ?>
            <div class="eltd-grid" <?php creator_elated_inline_style($menu_area_grid_background_color); ?>>
                <?php endif; ?>
                <?php do_action( 'creator_elated_after_header_menu_area_html_open' )?>
                <div class="eltd-vertical-align-containers">
                    <div class="eltd-position-left">
                        <div class="eltd-position-left-inner">
                            <?php if(!$hide_logo) {
                                creator_elated_get_logo();
                            } ?>
                        </div>
                    </div>
                    <div class="eltd-position-center">
                    </div>
                    <div class="eltd-position-right">
                        <div class="eltd-position-right-inner">
                            <?php creator_elated_get_main_menu(); ?>
                        </div>
                    </div>
                </div>
                <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
        </div>
        <?php if($show_fixed_wrapper) : ?>
    </div>
<?php endif; ?>
    <?php if($show_sticky) {
        creator_elated_get_sticky_header();
    } ?>
</header>

<?php do_action('creator_elated_after_page_header'); ?>

