<?php do_action('creator_elated_before_page_header'); ?>
<aside class="eltd-vertical-menu-area">
    <div class="eltd-vertical-menu-area-inner">
        <div class="eltd-vertical-area-background" <?php  echo  creator_elated_get_inline_style(array($menu_area_background_color,$vertical_header_opacity,$vertical_background_image)); ?>></div>
        <?php if(!$hide_logo) {
            creator_elated_get_logo();
        } ?>
        <?php creator_elated_get_vertical_main_menu(); ?>
        <div class="eltd-vertical-area-widget-holder">
            <?php if(is_active_sidebar('eltd-vertical-area')) : ?>
                <?php dynamic_sidebar('eltd-vertical-area'); ?>
            <?php endif; ?>
        </div>
    </div>
</aside>

<?php do_action('creator_elated_after_page_header'); ?>