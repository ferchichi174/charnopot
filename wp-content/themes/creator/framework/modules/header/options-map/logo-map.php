<?php

if ( ! function_exists('creator_elated_logo_options_map') ) {

	function creator_elated_logo_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_logo_page',
				'title' =>esc_html__( 'Logo','creator'),
				'icon' => 'fa fa-coffee'
			)
		);

		$panel_logo = creator_elated_add_admin_panel(
			array(
				'page' => '_logo_page',
				'name' => 'panel_logo',
				'title' => esc_html__('Logo','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_logo,
				'type' => 'yesno',
				'name' => 'hide_logo',
				'default_value' => 'no',
				'label' =>esc_html__( 'Hide Logo','creator'),
				'description' =>esc_html__( 'Enabling this option will hide logo image','creator'),
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "#eltd_hide_logo_container",
					"dependence_show_on_yes" => ""
				)
			)
		);

		$hide_logo_container = creator_elated_add_admin_container(
			array(
				'parent' => $panel_logo,
				'name' => 'hide_logo_container',
				'hidden_property' => 'hide_logo',
				'hidden_value' => 'yes'
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'logo_image',
				'type' => 'image',
				'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Default','creator'),
				'description' =>esc_html__( 'Choose a default logo image to display ','creator'),
				'parent' => $hide_logo_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'logo_image_dark',
				'type' => 'image',
				'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Dark','creator'),
				'description' =>esc_html__( 'Choose a default logo image to display ','creator'),
				'parent' => $hide_logo_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'logo_image_light',
				'type' => 'image',
				'default_value' => ELATED_ASSETS_ROOT."/img/logo_white.png",
				'label' => esc_html__('Logo Image - Light','creator'),
				'description' => esc_html__('Choose a default logo image to display ','creator'),
				'parent' => $hide_logo_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'logo_image_sticky',
				'type' => 'image',
				'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Sticky','creator'),
				'description' => esc_html__('Choose a default logo image to display ','creator'),
				'parent' => $hide_logo_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'logo_image_mobile',
				'type' => 'image',
				'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Mobile','creator'),
				'description' =>esc_html__( 'Choose a default logo image to display ','creator'),
				'parent' => $hide_logo_container
			)
		);

	}

	add_action( 'creator_elated_options_map', 'creator_elated_logo_options_map', 6);

}