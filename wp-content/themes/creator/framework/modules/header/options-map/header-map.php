<?php

if ( ! function_exists('creator_elated_header_options_map') ) {

	function creator_elated_header_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_header_page',
				'title' =>esc_html__( 'Header','creator'),
				'icon' => 'fa fa-header'
			)
		);

		$panel_header = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header',
				'title' =>esc_html__( 'Header','creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'radiogroup',
				'name' => 'header_type',
				'default_value' => 'header-standard',
				'label' => esc_html__('Choose Header Type','creator'),
				'description' => esc_html__('Select the type of header you would like to use','creator'),
				'options' => array(
					'header-standard' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/standard-header.png'
					),
					'header-classic' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/classic-header.png'
					),
					'header-simple' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/simple-header.png'
					),
					'header-compound' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/compound-header.png'
					),
					'header-dual' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/dual-header.png'
					),
					'header-centered' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/centered-header.png'
					),
					'header-divided' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/divided-header.png'
					),
					'header-full-screen' => array(
						'image' => ELATED_ASSETS_ROOT . '/img/full-screen-header.png'
					),
                    'header-vertical' => array(
	                    'image' => ELATED_ASSETS_ROOT . '/img/vertical-header.png'
                    )
				),
				'args' => array(
					'use_images' => true,
					'hide_labels' => true,
					'dependence' => true,
					'show' => array(
						'header-standard' => '#eltd_panel_header_standard,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-classic' =>'#eltd_panel_header_classic,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-centered' => '#eltd_logo_area_background_color, #eltd_panel_header_centered,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-divided' => '#eltd_panel_header_divided,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
                        'header-vertical' => '#eltd_panel_header_vertical,#eltd_panel_vertical_main_menu',
						'header-simple' => '#eltd_panel_header_simple,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-compound' =>'#eltd_logo_area_background_color, #eltd_panel_header_compound,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-dual' =>'#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu',
						'header-full-screen' =>'#eltd_panel_header_full_screen,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu'
					),
					'hide' => array(
						'header-standard' => '#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_vertical,#eltd_panel_header_centered,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_full_screen,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_compound',
						'header-classic' =>'#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_standard,#eltd_panel_header_vertical,#eltd_panel_header_centered,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_full_screen,#eltd_panel_header_simple,#eltd_panel_header_compound',
						'header-centered' => '#eltd_panel_header_dual,#eltd_panel_header_divided,#eltd_panel_header_standard,#eltd_panel_header_vertical,#eltd_panel_vertical_main_menu,#eltd_panel_header_classic,#eltd_panel_header_full_screen,#eltd_panel_header_simple,#eltd_panel_header_compound',
						'header-divided' => '#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_vertical,#eltd_panel_header_centered,#eltd_panel_vertical_main_menu,#eltd_panel_header_standard,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_compound,#eltd_panel_header_full_screen',
                        'header-vertical' => '#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_standard,#eltd_panel_header_centered,#eltd_header_behaviour,#eltd_panel_fixed_header,#eltd_panel_sticky_header,#eltd_panel_main_menu,#eltd_panel_header_divided,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_compound,#eltd_panel_header_full_screen',
						'header-simple' => '#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_vertical,#eltd_panel_header_centered,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_classic,#eltd_panel_header_standard,#eltd_panel_header_compound,#eltd_panel_header_full_screen',
						'header-compound' => '#eltd_panel_header_dual,#eltd_panel_header_vertical,#eltd_panel_header_standard,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_full_screen,#eltd_panel_header_centered',
						'header-dual' => '#eltd_panel_header_vertical,#eltd_panel_header_standard,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_full_screen,#eltd_panel_header_centered,#eltd_panel_header_compound',
						'header-full-screen' => '#eltd_logo_area_background_color, #eltd_panel_header_dual,#eltd_panel_header_vertical,#eltd_panel_header_standard,#eltd_panel_vertical_main_menu,#eltd_panel_header_divided,#eltd_panel_header_classic,#eltd_panel_header_simple,#eltd_panel_header_compound,#eltd_panel_header_centered'
					)
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_behaviour',
				'default_value' => 'sticky-header-on-scroll-up',
				'label' => esc_html__('Choose Header behaviour','creator'),
				'description' => esc_html__('Select the behaviour of header when you scroll down to page','creator'),
				'options' => array(
					'sticky-header-on-scroll-up' => esc_html__('Sticky on scrol up','creator'),
					'sticky-header-on-scroll-down-up' =>esc_html__( 'Sticky on scrol up/down','creator'),
					'fixed-on-scroll' => esc_html__('Fixed on scroll','creator'),
				),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array('header-vertical'),
				'args' => array(
					'dependence' => true,
					'show' => array(
						'sticky-header-on-scroll-up' => '#eltd_panel_sticky_header',
						'sticky-header-on-scroll-down-up' => '#eltd_panel_sticky_header',
						'fixed-on-scroll' => '#eltd_panel_fixed_header'
					),
					'hide' => array(
						'sticky-header-on-scroll-up' => '#eltd_panel_fixed_header',
						'sticky-header-on-scroll-down-up' => '#eltd_panel_fixed_header',
						'fixed-on-scroll' => '#eltd_panel_sticky_header',
					)
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'top_bar',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Top Bar','creator'),
				'description' =>esc_html__( 'Enabling this option will show top bar area','creator'),
				'parent' => $panel_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#eltd_top_bar_container"
				)
			)
		);

		$top_bar_container = creator_elated_add_admin_container(array(
			'name' => 'top_bar_container',
			'parent' => $panel_header,
			'hidden_property' => 'top_bar',
			'hidden_value' => 'no'
		));

		creator_elated_add_admin_field(
			array(
				'parent' => $top_bar_container,
				'type' => 'select',
				'name' => 'top_bar_layout',
				'default_value' => 'three-columns',
				'label' => esc_html__('Choose top bar layout','creator'),
				'description' => esc_html__('Select the layout for top bar','creator'),
				'options' => array(
					'two-columns' =>esc_html__( 'Two columns','creator'),
					'three-columns' =>esc_html__( 'Three columns','creator')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"two-columns" => "#eltd_top_bar_layout_container",
						"three-columns" => ""
					),
					"show" => array(
						"two-columns" => "",
						"three-columns" => "#eltd_top_bar_layout_container"
					)
				)
			)
		);

		$top_bar_layout_container = creator_elated_add_admin_container(array(
			'name' => 'top_bar_layout_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_layout',
			'hidden_value' => '',
			'hidden_values' => array("two-columns"),
		));

		creator_elated_add_admin_field(
			array(
				'parent' => $top_bar_layout_container,
				'type' => 'select',
				'name' => 'top_bar_column_widths',
				'default_value' => '30-30-30',
				'label' => esc_html__('Choose column widths','creator'),
				'description' => '',
				'options' => array(
					'30-30-30' => '33% - 33% - 33%',
					'25-50-25' => '25% - 50% - 25%'
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $top_bar_layout_container,
				'type' => 'select',
				'name' => 'top_bar_skin',
				'default_value' => 'light',
				'label' => esc_html__('Top Bar Skin','creator'),
				'description' => '',
				'options' => array(
					'' =>'',
					'light' => 'Light',
					'dark' => 'Dark'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'top_bar_in_grid',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__('Top Bar in grid','creator'),
				'description' => esc_html__('Set top bar content to be in grid','creator'),
				'parent' => $top_bar_container,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#eltd_top_bar_in_grid_container"
				)
			)
		);

		$top_bar_in_grid_container = creator_elated_add_admin_container(array(
			'name' => 'top_bar_in_grid_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_in_grid',
			'hidden_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name' => 'top_bar_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color','creator'),
			'description' =>esc_html__( 'Set grid background color for top bar','creator'),
			'parent' => $top_bar_in_grid_container
		));


		creator_elated_add_admin_field(array(
			'name' => 'top_bar_grid_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Grid Background Transparency','creator'),
			'description' => esc_html__('Set grid background transparency for top bar','creator'),
			'parent' => $top_bar_in_grid_container,
			'args' => array('col_width' => 3)
		));

		creator_elated_add_admin_field(array(
			'name' => 'top_bar_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color','creator'),
			'description' =>esc_html__( 'Set background color for top bar','creator'),
			'parent' => $top_bar_container
		));

		creator_elated_add_admin_field(array(
			'name' => 'top_bar_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Background Transparency','creator'),
			'description' => esc_html__('Set background transparency for top bar','creator'),
			'parent' => $top_bar_container,
			'args' => array('col_width' => 3)
		));

		creator_elated_add_admin_field(array(
			'name' => 'top_bar_height',
			'type' => 'text',
			'label' =>esc_html__( 'Top bar height','creator'),
			'description' =>esc_html__( 'Enter top bar height (Default is 40px)','creator'),
			'parent' => $top_bar_container,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_style',
				'default_value' => '',
				'label' => esc_html__('Header Skin','creator'),
				'description' =>esc_html__( 'Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style','creator'),
				'options' => array(
					'' => '',
					'light-header' =>esc_html__( 'Light','creator'),
					'dark-header' => esc_html__('Dark','creator')
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'yesno',
				'name' => 'enable_header_style_on_scroll',
				'default_value' => 'no',
				'label' => esc_html__('Enable Header Style on Scroll','creator'),
				'description' => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'yesno',
				'name' => 'header_in_grid',
				'default_value' => 'yes',
				'label' => esc_html__('Menu area in grid','creator'),
				'description' => esc_html__('Set menu area content to be in grid','creator')
			)
		);


		creator_elated_add_admin_field(array(
			'name' => 'header_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Menu Area Background Color','creator'),
			'description' =>esc_html__( 'Set menu area background color','creator'),
			'parent' => $panel_header
		));

		creator_elated_add_admin_field(array(
			'name' => 'logo_area_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Logo Area Background Color','creator'),
			'hidden_property' => 'header_type',
			'hidden_value' => '',
			'hidden_values' => array(
				'header-vertical',
				'header-standard',
				'header-simple',
				'header-classic',
				'header-divided',
				'header-full-screen',
			),
			'description' =>esc_html__( 'Set logo area background color.','creator'),
			'parent' => $panel_header
		));

		creator_elated_add_admin_field(array(
			'name' => 'header_background_transparency',
			'type' => 'text',
			'label' =>esc_html__( 'Header Transparency','creator'),
			'description' =>esc_html__( 'Enter header transparency (value from 0 to 1)','creator'),
			'parent' => $panel_header,
			'args' => array(
				'col_width' => 1
			)
		));


		$panel_header_standard = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_standard',
				'title' =>esc_html__( 'Header Standard','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-centered',
					'header-divided',
                    'header-vertical',
					'header-classic',
					'header-simple',
					'header-compound',
					'header-dual',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_standard,
				'name' => 'menu_area_title',
				'title' => esc_html__('Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'text',
				'name' => 'menu_area_height_header_standard',
				'default_value' => '',
				'label' => esc_html__('Height','creator'),
				'description' =>esc_html__( 'Enter header height (default is 96px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'selectblank',
				'name' => 'header_standard_align',
				'default_value' => 'center',
				'label' => esc_html__('Menu area alignment','creator'),
				'description' => '',
				'options' => array(
					'center' => esc_html__('Center' , 'creator'),
					'left' => esc_html__('Left' , 'creator'),
					'right' => esc_html__('Right' , 'creator'),
				),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		$panel_header_classic = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_classic',
				'title' =>esc_html__( 'Header Classic','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-centered',
					'header-divided',
					'header-vertical',
					'header-standard',
					'header-simple',
					'header-full-screen',
					'header-dual',
					'header-compound',
				)
			)
		);
		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_classic,
				'name' => 'menu_area_title',
				'title' => esc_html__('Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_classic,
				'type' => 'text',
				'name' => 'menu_area_height_header_classic',
				'default_value' => '',
				'label' => esc_html__('Height','creator'),
				'description' =>esc_html__( 'Enter header height (default is 100px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		$panel_header_centered = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_centered',
				'title' => esc_html__('Header Centered','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-divided',
					'header-standard',
					'header-vertical',
					'header-simple',
					'header-classic',
					'header-compound',
					'header-dual',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_centered,
				'name' => 'logo_area_title',
				'title' => esc_html__('Logo Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'text',
				'name' => 'logo_area_height_header_centered',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter logo area height (default is 58px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);



		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_centered,
				'name' => 'menu_area_title',
				'title' =>esc_html__( 'Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_centered,
				'type' => 'text',
				'name' => 'menu_area_height_header_centered',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter menu area height (default is 67px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		$panel_header_simple = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_simple',
				'title' => esc_html__('Simple Header','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-divided',
					'header-centered',
					'header-standard',
					'header-vertical',
					'header-classic',
					'header-compound',
					'header-dual',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_simple,
				'name' => 'logo_area_title',
				'title' => esc_html__('Logo Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_simple,
				'type' => 'text',
				'name' => 'logo_area_height_header_simple',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter logo area height (default is 33px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);
		
		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_simple,
				'name' => 'menu_area_title',
				'title' =>esc_html__( 'Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_simple,
				'type' => 'text',
				'name' => 'menu_area_height_header_simple',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter menu area height (default is 82px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);
		$panel_header_compound = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_compound',
				'title' => esc_html__('Compound Header','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-divided',
					'header-centered',
					'header-standard',
					'header-vertical',
					'header-classic',
					'header-simple',
					'header-dual',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_compound,
				'name' => 'logo_area_title',
				'title' => esc_html__('Logo Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_compound,
				'type' => 'text',
				'name' => 'logo_area_height_header_compound',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter logo area height (default is 61px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_compound,
				'name' => 'menu_area_title',
				'title' =>esc_html__( 'Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_compound,
				'type' => 'text',
				'name' => 'menu_area_height_header_compound',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter menu area height (default is 87px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		$panel_header_dual = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_dual',
				'title' => esc_html__('Dual Header','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-divided',
					'header-centered',
					'header-standard',
					'header-vertical',
					'header-classic',
					'header-simple',
					'header-compound',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_dual,
				'name' => 'logo_area_title',
				'title' => esc_html__('Logo Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_dual,
				'type' => 'text',
				'name' => 'logo_area_height_header_dual',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter logo area height (default is 240px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_header_dual,
				'name' => 'menu_area_title',
				'title' =>esc_html__( 'Menu Area','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_dual,
				'type' => 'text',
				'name' => 'menu_area_height_header_dual',
				'default_value' => '',
				'label' =>esc_html__( 'Height','creator'),
				'description' => esc_html__('Enter menu area height (default is 50px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		$panel_header_divided = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_divided',
				'title' => esc_html__('Header Divided','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-standard',
					'header-centered',
					'header-vertical',
					'header-simple',
					'header-classic',
					'header-compound',
					'header-dual',
					'header-full-screen'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_divided,
				'type' => 'text',
				'name' => 'menu_area_height_header_divided',
				'default_value' => '',
				'label' => esc_html__('Height','creator'),
				'description' => esc_html__('Enter header height (default is 85px)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		$panel_header_full_screen = creator_elated_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_full_screen',
				'title' => esc_html__('Header Full Screen','creator'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-standard',
					'header-centered',
					'header-vertical',
					'header-simple',
					'header-classic',
					'header-compound',
					'header-dual',
					'header-divided'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'text',
				'name' => 'menu_area_height_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Height', 'creator'),
				'description' => esc_html__('Enter header height (default is 78)','creator'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

        $panel_header_vertical = creator_elated_add_admin_panel(
            array(
                'page' => '_header_page',
                'name' => 'panel_header_vertical',
                'title' =>esc_html__( 'Header Vertical','creator'),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array(
                    'header-standard',
                    'header-centered',
	                'header-divided',
					'header-simple',
					'header-classic',
					'header-compound',
                    'header-dual',
                    'header-full-screen'
                )
            )
        );


			creator_elated_add_admin_field(array(
				'name' => 'vertical_header_dropdown_style',
				'type' => 'select',
				'label' =>esc_html__( 'Vertical Menu Dropdown Appearance','creator'),
				'description' => '',
				'default_value' => 'dropdown-toggle-click',
				'options' => array(
					'float' => esc_html__('Float','creator'),
					'dropdown-toggle-click' => esc_html__('Toggle','creator')
				),
				'parent' => $panel_header_vertical,
				'args' => array(
					'col_width' => 1
				)
			));

            creator_elated_add_admin_field(
                array(
                    'name' => 'vertical_header_background_image',
                    'type' => 'image',
                    'default_value' => '',
                    'label' => esc_html__('Background Image','creator'),
                    'description' => esc_html__('Set background image for vertical menu','creator'),
                    'parent' => $panel_header_vertical
                )
            );

		$panel_sticky_header = creator_elated_add_admin_panel(
			array(
				'title' => esc_html__('Sticky Header','creator'),
				'name' => 'panel_sticky_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array(
					'fixed-on-scroll'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'scroll_amount_for_sticky',
				'type' => 'text',
				'label' => esc_html__('Scroll Amount for Sticky','creator'),
				'description' => esc_html__('Enter scroll amount for Sticky Menu to appear (deafult is header height)','creator'),
				'parent' => $panel_sticky_header,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'sticky_header_in_grid',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Sticky Header in grid','creator'),
				'description' => esc_html__('Set sticky header content to be in grid','creator'),
				'parent' => $panel_sticky_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#eltd_sticky_header_in_grid_container"
				)
			)
		);

		$sticky_header_in_grid_container = creator_elated_add_admin_container(array(
			'name' => 'sticky_header_in_grid_container',
			'parent' => $panel_sticky_header,
			'hidden_property' => 'sticky_header_in_grid',
			'hidden_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_header_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color','creator'),
			'description' => esc_html__('Set grid background color for sticky header','creator'),
			'parent' => $sticky_header_in_grid_container
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_header_grid_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Grid Transparency','creator'),
			'description' =>esc_html__( 'Enter transparency for sticky header grid (value from 0 to 1)','creator'),
			'parent' => $sticky_header_in_grid_container,
			'args' => array(
				'col_width' => 1
			)
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_header_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color','creator'),
			'description' =>esc_html__( 'Set background color for sticky header','creator'),
			'parent' => $panel_sticky_header
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_header_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Transparency','creator'),
			'description' => esc_html__('Enter transparency for sticky header (value from 0 to 1)','creator'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 1
			)
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_header_height',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Height','creator'),
			'description' =>esc_html__( 'Enter height for sticky header (default is 60px)','creator'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		$group_sticky_header_menu = creator_elated_add_admin_group(array(
			'title' =>esc_html__( 'Sticky Header Menu','creator'),
			'name' => 'group_sticky_header_menu',
			'parent' => $panel_sticky_header,
			'description' => esc_html__('Define styles for sticky menu items','creator'),
		));

		$row1_sticky_header_menu = creator_elated_add_admin_row(array(
			'name' => 'row1',
			'parent' => $group_sticky_header_menu
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_color',
			'type' => 'colorsimple',
			'label' =>esc_html__( 'Text Color','creator'),
			'description' => '',
			'parent' => $row1_sticky_header_menu
		));

		creator_elated_add_admin_field(array(
			'name' => 'sticky_hovercolor',
			'type' => 'colorsimple',
			'label' => esc_html__('Hover/Active color','creator'),
			'description' => '',
			'parent' => $row1_sticky_header_menu
		));

		$row2_sticky_header_menu = creator_elated_add_admin_row(array(
			'name' => 'row2',
			'parent' => $group_sticky_header_menu
		));

		creator_elated_add_admin_field(
			array(
				'name' => 'sticky_google_fonts',
				'type' => 'fontsimple',
				'label' => esc_html__('Font Family','creator'),
				'default_value' => '-1',
				'parent' => $row2_sticky_header_menu,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_fontsize',
				'label' => esc_html__('Font Size','creator'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_lineheight',
				'label' => esc_html__('Line height','creator'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_texttransform',
				'label' =>esc_html__( 'Text transform','creator'),
				'default_value' => '',
				'options' => creator_elated_get_text_transform_array(),
				'parent' => $row2_sticky_header_menu
			)
		);

		$row3_sticky_header_menu = creator_elated_add_admin_row(array(
			'name' => 'row3',
			'parent' => $group_sticky_header_menu
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font Style','creator'),
				'options' => creator_elated_get_font_style_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font Weight','creator'),
				'options' => creator_elated_get_font_weight_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_letterspacing',
				'label' => esc_html__('Letter Spacing','creator'),
				'default_value' => '',
				'parent' => $row3_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$panel_fixed_header = creator_elated_add_admin_panel(
			array(
				'title' =>esc_html__( 'Fixed Header','creator'),
				'name' => 'panel_fixed_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array('sticky-header-on-scroll-up', 'sticky-header-on-scroll-down-up')
			)
		);

		creator_elated_add_admin_field(array(
			'name' => 'fixed_header_grid_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' =>esc_html__( 'Grid Background Color','creator'),
			'description' => esc_html__('Set grid background color for fixed header','creator'),
			'parent' => $panel_fixed_header
		));

		creator_elated_add_admin_field(array(
			'name' => 'fixed_header_grid_transparency',
			'type' => 'text',
			'default_value' => '',
			'label' => esc_html__('Header Transparency Grid','creator'),
			'description' =>esc_html__( 'Enter transparency for fixed header grid (value from 0 to 1)','creator'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));

		creator_elated_add_admin_field(array(
			'name' => 'fixed_header_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Background Color','creator'),
			'description' =>esc_html__( 'Set background color for fixed header','creator'),
			'parent' => $panel_fixed_header
		));

		creator_elated_add_admin_field(array(
			'name' => 'fixed_header_transparency',
			'type' => 'text',
			'label' =>esc_html__( 'Header Transparency','creator'),
			'description' =>esc_html__( 'Enter transparency for fixed header (value from 0 to 1)','creator'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));


		$panel_main_menu = creator_elated_add_admin_panel(
			array(
				'title' =>esc_html__( 'Main Menu','creator'),
				'name' => 'panel_main_menu',
				'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array('header-vertical')
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' => $panel_main_menu,
				'name' => 'main_menu_area_title',
				'title' =>esc_html__( 'Main Menu General Settings','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_dropdown_appearance',
				'default_value' => 'default',
				'label' =>esc_html__( 'Main Dropdown Menu Appearance','creator'),
				'description' => esc_html__('Choose appearance for dropdown menu','creator'),
				'options' => array(
					'dropdown-default' =>esc_html__( 'Default','creator'),
					'dropdown-slide-from-bottom' => esc_html__('Slide From Bottom','creator'),
					'dropdown-slide-from-top' =>esc_html__( 'Slide From Top','creator'),
					'dropdown-animate-height' =>esc_html__( 'Animate Height','creator'),
					'dropdown-slide-from-left' =>esc_html__( 'Slide From Left','creator')
				)
			)
		);

        $panel_vertical_main_menu = creator_elated_add_admin_panel(
            array(
                'title' =>esc_html__( 'Vertical Main Menu','creator'),
                'name' => 'panel_vertical_main_menu',
                'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array(
	                'header-standard',
	                'header-centered',
	                'header-divided'
                )
            )
        );

        $drop_down_group = creator_elated_add_admin_group(
            array(
                'parent' => $panel_vertical_main_menu,
                'name' => 'vertical_drop_down_group',
                'title' => esc_html__('Main Dropdown Menu','creator'),
                'description' =>esc_html__( 'Set a style for dropdown menu','creator')
            )
        );

        $vertical_drop_down_row1 = creator_elated_add_admin_row(
            array(
                'parent' => $drop_down_group,
                'name' => 'eltd_drop_down_row1',
            )
        );

        creator_elated_add_admin_field(
            array(
                'parent' => $vertical_drop_down_row1,
                'type' => 'colorsimple',
                'name' => 'vertical_dropdown_background_color',
                'default_value' => '',
                'label' => esc_html__('Background Color','creator'),
            )
        );

        $group_vertical_first_level = creator_elated_add_admin_group(array(
            'name'			=> 'group_vertical_first_level',
            'title'			=> esc_html__('1st level','creator'),
            'description'	=> esc_html__('Define styles for 1st level menu','creator'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_first_level_1 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_1',
                'parent'	=> $group_vertical_first_level
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color','creator'),
                'parent'		=> $row_vertical_first_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color','creator'),
                'parent'		=> $row_vertical_first_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_fontsize',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Size','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_lineheight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Line Height','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            $row_vertical_first_level_2 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_2',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform','creator'),
                'options'		=> creator_elated_get_text_transform_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_1st_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family','creator'),
                'parent'		=> $row_vertical_first_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style','creator'),
                'options'		=> creator_elated_get_font_style_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontweight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Weight','creator'),
                'options'		=> creator_elated_get_font_weight_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            $row_vertical_first_level_3 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_3',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_letter_spacing',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Letter Spacing','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_3
            ));

        $group_vertical_second_level = creator_elated_add_admin_group(array(
            'name'			=> 'group_vertical_second_level',
            'title'			=> esc_html__('2nd level','creator'),
            'description'	=> esc_html__('Define styles for 2nd level menu','creator'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_second_level_1 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_1',
                'parent'	=> $group_vertical_second_level
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color','creator'),
                'parent'		=> $row_vertical_second_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_hover_color',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Hover/Active Color','creator'),
                'parent'		=> $row_vertical_second_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_fontsize',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Size','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_lineheight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Line Height','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            $row_vertical_second_level_2 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_2',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_texttransform',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Text Transform','creator'),
                'options'		=> creator_elated_get_text_transform_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_2nd_google_fonts',
                'default_value'	=> '-1',
                'label'			=>esc_html__( 'Font Family','creator'),
                'parent'		=> $row_vertical_second_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontstyle',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Style','creator'),
                'options'		=> creator_elated_get_font_style_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontweight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Weight','creator'),
                'options'		=> creator_elated_get_font_weight_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            $row_vertical_second_level_3 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_3',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_letter_spacing',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Letter Spacing','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_3
            ));

        $group_vertical_third_level = creator_elated_add_admin_group(array(
            'name'			=> 'group_vertical_third_level',
            'title'			=> esc_html__('3rd level','creator'),
            'description'	=> esc_html__('Define styles for 3rd level menu','creator'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_third_level_1 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_1',
                'parent'	=> $group_vertical_third_level
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color','creator'),
                'parent'		=> $row_vertical_third_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color','creator'),
                'parent'		=> $row_vertical_third_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_lineheight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Line Height','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            $row_vertical_third_level_2 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_2',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_texttransform',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Text Transform','creator'),
                'options'		=> creator_elated_get_text_transform_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_3rd_google_fonts',
                'default_value'	=> '-1',
                'label'			=>esc_html__( 'Font Family','creator'),
                'parent'		=> $row_vertical_third_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontstyle',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Style','creator'),
                'options'		=> creator_elated_get_font_style_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontweight',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Font Weight','creator'),
                'options'		=> creator_elated_get_font_weight_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            $row_vertical_third_level_3 = creator_elated_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_3',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            creator_elated_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_letter_spacing',
                'default_value'	=> '',
                'label'			=>esc_html__( 'Letter Spacing','creator'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_3
            ));
	}

	add_action( 'creator_elated_options_map', 'creator_elated_header_options_map', 7);

}