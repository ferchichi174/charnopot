<?php

if ( ! function_exists('creator_elated_mobile_header_options_map') ) {

	function creator_elated_mobile_header_options_map() {

		$panel_mobile_header = creator_elated_add_admin_panel(array(
			'title' => esc_html__('Mobile header','creator'),
			'name'  => 'panel_mobile_header',
			'page'  => '_header_page'
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_header_height',
			'type'        => 'text',
			'label'       =>  esc_html__('Mobile Header Height','creator'),
			'description' =>  esc_html__('Enter height for mobile header in pixels','creator'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_header_background_color',
			'type'        => 'color',
			'label'       =>  esc_html__('Mobile Header Background Color','creator'),
			'description' =>  esc_html__('Choose color for mobile header','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_menu_background_color',
			'type'        => 'color',
			'label'       => esc_html__( 'Mobile Menu Background Color','creator'),
			'description' =>  esc_html__('Choose color for mobile menu','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_menu_separator_color',
			'type'        => 'color',
			'label'       =>  esc_html__('Mobile Menu Item Separator Color','creator'),
			'description' =>  esc_html__('Choose color for mobile menu horizontal separators','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_logo_height',
			'type'        => 'text',
			'label'       =>  esc_html__('Logo Height For Mobile Header','creator'),
			'description' =>  esc_html__('Define logo height for screen size smaller than 1000px','creator'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_logo_height_phones',
			'type'        => 'text',
			'label'       => esc_html__( 'Logo Height For Mobile Devices','creator'),
			'description' => esc_html__( 'Define logo height for screen size smaller than 480px','creator'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		creator_elated_add_admin_section_title(array(
			'parent' => $panel_mobile_header,
			'name'   => 'mobile_header_fonts_title',
			'title'  =>  esc_html__('Typography','creator'),
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_text_color',
			'type'        => 'color',
			'label'       => esc_html__( 'Navigation Text Color','creator'),
			'description' => esc_html__( 'Define color for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_text_hover_color',
			'type'        => 'color',
			'label'       => esc_html__( 'Navigation Hover/Active Color','creator'),
			'description' =>  esc_html__('Define hover/active color for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_font_family',
			'type'        => 'font',
			'label'       =>  esc_html__('Navigation Font Family','creator'),
			'description' =>  esc_html__('Define font family for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_font_size',
			'type'        => 'text',
			'label'       =>  esc_html__('Navigation Font Size','creator'),
			'description' => esc_html__( 'Define font size for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_line_height',
			'type'        => 'text',
			'label'       =>  esc_html__('Navigation Line Height','creator'),
			'description' => esc_html__( 'Define line height for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header,
			'args'        => array(
				'col_width' => 3,
				'suffix'    => 'px'
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_text_transform',
			'type'        => 'select',
			'label'       =>  esc_html__('Navigation Text Transform','creator'),
			'description' => esc_html__( 'Define text transform for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header,
			'options'     => creator_elated_get_text_transform_array(true)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_font_style',
			'type'        => 'select',
			'label'       => esc_html__( 'Navigation Font Style','creator'),
			'description' => esc_html__( 'Define font style for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header,
			'options'     => creator_elated_get_font_style_array(true)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_font_weight',
			'type'        => 'select',
			'label'       => esc_html__( 'Navigation Font Weight','creator'),
			'description' =>  esc_html__('Define font weight for mobile navigation text','creator'),
			'parent'      => $panel_mobile_header,
			'options'     => creator_elated_get_font_weight_array(true)
		));

		creator_elated_add_admin_section_title(array(
			'name' => 'mobile_opener_panel',
			'parent' => $panel_mobile_header,
			'title' => esc_html__( 'Mobile Menu Opener','creator')
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_icon_pack',
			'type'        => 'select',
			'label'       =>  esc_html__('Mobile Navigation Icon Pack','creator'),
			'default_value' => 'font_awesome',
			'description' => esc_html__( 'Choose icon pack for mobile navigation icon','creator'),
			'parent'      => $panel_mobile_header,
			'options'     => creator_elated_icon_collections()->getIconCollectionsExclude(array('linea_icons', 'simple_line_icons'))
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_icon_color',
			'type'        => 'color',
			'label'       =>  esc_html__('Mobile Navigation Icon Color','creator'),
			'description' => esc_html__( 'Choose color for icon header','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_icon_hover_color',
			'type'        => 'color',
			'label'       =>  esc_html__('Mobile Navigation Icon Hover Color','creator'),
			'description' =>  esc_html__('Choose hover color for mobile navigation icon ','creator'),
			'parent'      => $panel_mobile_header
		));

		creator_elated_add_admin_field(array(
			'name'        => 'mobile_icon_size',
			'type'        => 'text',
			'label'       =>  esc_html__('Mobile Navigation Icon size','creator'),
			'description' =>  esc_html__('Choose size for mobile navigation icon ','creator'),
			'parent'      => $panel_mobile_header,
			'args' => array(
				'col_width' => 3,
				'suffix' => 'px'
			)
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_mobile_header_options_map');

}