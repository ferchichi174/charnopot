<?php
namespace CreatorElated\Modules\Header\Types;

use CreatorElated\Modules\Header\Lib\HeaderType;

/**
 * Class that represents Header Standard layout and option
 *
 * Class HeaderStandard
 */
class HeaderDivided extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;
    protected $mobileHeaderHeight;

    /**
     * Sets slug property which is the same as value of option in DB
     */
    public function __construct() {
        $this->slug = 'header-divided';

        if(!is_admin()) {

            $page_id = creator_elated_get_page_id();
            $menuAreaHeight = '';

            $meta_header_height = get_post_meta($page_id ,'eltd_header_height_meta', true);
            $default_header_height  = creator_elated_options()->getOptionValue( 'menu_area_height_header_divided' );

            if($meta_header_height && $meta_header_height !== ''){
                $menuAreaHeight = $meta_header_height;
            }elseif( $default_header_height !== ''){
                $menuAreaHeight = $default_header_height;
            }

            $this->menuAreaHeight = $menuAreaHeight !== '' ? (int)$menuAreaHeight : 85;

            $mobileHeaderHeight       = creator_elated_filter_px(creator_elated_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? (int)$mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('creator_elated_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('creator_elated_per_page_js_vars', array($this, 'getPerPageJSVariables'));

        }
    }

    /**
     * Loads template file for this header type
     *
     * @param array $parameters associative array of variables that needs to passed to template
     */
    public function loadTemplate($parameters = array()) {


        $logo_image = creator_elated_get_meta_field_intersect('logo_image');
        $logo_dimensions = creator_elated_get_image_dimensions($logo_image);

        $logo_holder_width = '';
        if(is_array($logo_dimensions) && array_key_exists('width', $logo_dimensions)) {
            $logo_width = $logo_dimensions['width'];
            $logo_holder_width = 'width:' . intval($logo_width / 2 + 50) . 'px';
        }

        $parameters['logo_holder_width'] = $logo_holder_width !== '' ? $logo_holder_width : '';

        $parameters = apply_filters('creator_elated_header_divided_parameters', $parameters);

        creator_elated_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps(){
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
        $this->mobileHeaderHeight           = $this->calculateMobileHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id = creator_elated_get_page_id();
        $transparencyHeight = 0;
        $menuAreaTransparent = false;

        //take values from current page
        $meta_bckg_color = get_post_meta($id, 'eltd_header_background_color_meta', true);
        $meta_bckg_transparency  =  get_post_meta($id, 'eltd_header_background_transparency_meta', true);


        //take values from global options
        $option_bckg_color = creator_elated_options()->getOptionValue('header_background_color');
        $option_bckg_transparency = creator_elated_options()->getOptionValue('header_background_transparency');

        if(isset($meta_bckg_color) && $meta_bckg_color !== '' && isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
            if($meta_bckg_transparency !== '1'){
                $menuAreaTransparent = true;
            }
        }
        elseif(isset($option_bckg_color) && $option_bckg_color !== '' && isset($option_bckg_transparency) && $option_bckg_transparency !== '' ) {
            if($option_bckg_transparency !== '1'){
                $menuAreaTransparent = true;
            }
        }

        $sliderExists = get_post_meta($id, 'eltd_page_slider_meta', true) !== '';

        if($sliderExists){
            $menuAreaTransparent = true;
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;

            if(($sliderExists && creator_elated_is_top_bar_enabled())
               || creator_elated_is_top_bar_enabled() && creator_elated_is_top_bar_transparent()) {
                $transparencyHeight += creator_elated_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id = creator_elated_get_page_id();
        $transparencyHeight = 0;
        $menuAreaTransparent = false;

        //take values from current page
        $meta_bckg_color = get_post_meta($id, 'eltd_header_background_color_meta', true);
        $meta_bckg_transparency  =  get_post_meta($id, 'eltd_header_background_transparency_meta', true);


        //take values from global options
        $option_bckg_color = creator_elated_options()->getOptionValue('header_background_color');
        $option_bckg_transparency = creator_elated_options()->getOptionValue('header_background_transparency');

        if(isset($meta_bckg_color) && $meta_bckg_color !== '' && isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
            if($meta_bckg_transparency === '0'){
                $menuAreaTransparent = true;
            }
        }

        elseif(isset($option_bckg_color) && $option_bckg_color !== '' && isset($option_bckg_transparency) && $option_bckg_transparency !== '' ) {
            if($option_bckg_transparency === '0'){
                $menuAreaTransparent = true;
            }
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;

            if(creator_elated_is_top_bar_enabled() && creator_elated_is_top_bar_completely_transparent()) {
                $transparencyHeight += creator_elated_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }


    /**
     * Returns total height of header
     *
     * @return int|string
     */
    public function calculateHeaderHeight() {
        $headerHeight = $this->menuAreaHeight;
        if(creator_elated_is_top_bar_enabled()) {
            $headerHeight += creator_elated_get_top_bar_height();
        }

        return $headerHeight;
    }

    /**
     * Returns total height of mobile header
     *
     * @return int|string
     */
    public function calculateMobileHeaderHeight() {
        $mobileHeaderHeight = $this->mobileHeaderHeight;

        return $mobileHeaderHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {

        $globalVariables['eltdLogoAreaHeight'] = 0;
        $globalVariables['eltdMenuAreaHeight'] = $this->headerHeight;
        $globalVariables['eltdMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(creator_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            $perPageVars['eltdHeaderTransparencyHeight'] = $this->headerHeight - (creator_elated_get_top_bar_height() + $this->heightOfCompleteTransparency);
        }else{
            $perPageVars['eltdHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }
}