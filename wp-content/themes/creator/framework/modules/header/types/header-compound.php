<?php
namespace CreatorElated\Modules\Header\Types;

use CreatorElated\Modules\Header\Lib\HeaderType;

class HeaderCompound extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;

    public function __construct() {
        $this->slug = 'header-compound';

        if(!is_admin()) {
            $page_id = creator_elated_get_page_id();
            $menuAreaHeight = '';
			$logoAreaHeight = '';

			$meta_logo_height = creator_elated_filter_px(get_post_meta($page_id ,'eltd_logo_height_meta', true));
			$default_logo_height = creator_elated_filter_px(creator_elated_options()->getOptionValue('logo_area_height_header_compound'));

			if($meta_logo_height && $meta_logo_height !== ''){
				$logoAreaHeight = $meta_logo_height;
			}elseif( $default_logo_height !== ''){
				$logoAreaHeight = $default_logo_height;
			}

            $this->logoAreaHeight = $logoAreaHeight !== '' ? $logoAreaHeight : 61;

            $meta_header_height = get_post_meta($page_id ,'eltd_header_height_meta', true);
            $default_header_height  = creator_elated_options()->getOptionValue( 'menu_area_height_header_compound' );

            if($meta_header_height && $meta_header_height !== ''){
                $menuAreaHeight = $meta_header_height;
            }elseif( $default_header_height !== ''){
                $menuAreaHeight = $default_header_height;
            }

            $this->menuAreaHeight = $menuAreaHeight !== '' ? $menuAreaHeight : 87;

            $mobileHeaderHeight       = creator_elated_filter_px(creator_elated_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? (int)$mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('creator_elated_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('creator_elated_per_page_js_vars', array($this, 'getPerPageJSVariables'));
        }
    }

    /**
     * Loads template for header type
     *
     * @param array $parameters associative array of variables to pass to template
     */
    public function loadTemplate($parameters = array()) {

        $parameters = apply_filters('creator_elated_header_compound_parameters', $parameters);

        creator_elated_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps(){
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id = creator_elated_get_page_id();
        $transparencyHeight = 0;
        $menuAreaTransparent = false;

        //take values from current page
        $meta_bckg_color = get_post_meta($id, 'eltd_header_background_color_meta', true);
        $meta_bckg_transparency  =  get_post_meta($id, 'eltd_header_background_transparency_meta', true);


        //take values from global options
        $option_bckg_color = creator_elated_options()->getOptionValue('header_background_color');
        $option_bckg_transparency = creator_elated_options()->getOptionValue('header_background_transparency');

        if(isset($meta_bckg_color) && $meta_bckg_color !== '' && isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
            if($meta_bckg_transparency !== '1'){
                $menuAreaTransparent = true;
            }
        }
        elseif(isset($option_bckg_color) && $option_bckg_color !== '' && isset($option_bckg_transparency) && $option_bckg_transparency !== '' ) {
            if($option_bckg_transparency !== '1'){
                $menuAreaTransparent = true;
            }
        }

        $sliderExists = get_post_meta($id, 'eltd_page_slider_meta', true) !== '';

        if($sliderExists){
            $menuAreaTransparent = true;
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->logoAreaHeight + $this->menuAreaHeight;

            if(($sliderExists && creator_elated_is_top_bar_enabled())
                || creator_elated_is_top_bar_enabled() && creator_elated_is_top_bar_transparent()) {
                $transparencyHeight += creator_elated_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id = creator_elated_get_page_id();
        $transparencyHeight = 0;
        $menuAreaTransparent = false;

        //take values from current page
        $meta_bckg_color = get_post_meta($id, 'eltd_header_background_color_meta', true);
        $meta_bckg_transparency  =  get_post_meta($id, 'eltd_header_background_transparency_meta', true);


        //take values from global options
        $option_bckg_color = creator_elated_options()->getOptionValue('header_background_color');
        $option_bckg_transparency = creator_elated_options()->getOptionValue('header_background_transparency');

        if(isset($meta_bckg_color) && $meta_bckg_color !== '' && isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
            if($meta_bckg_transparency === '0'){
                $menuAreaTransparent = true;
            }
        }

        elseif(isset($option_bckg_color) && $option_bckg_color !== '' && isset($option_bckg_transparency) && $option_bckg_transparency !== '' ) {
            if($option_bckg_transparency === '0'){
                $menuAreaTransparent = true;
            }
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->logoAreaHeight + $this->menuAreaHeight;

            if(creator_elated_is_top_bar_enabled() && creator_elated_is_top_bar_completely_transparent()) {
                $transparencyHeight += creator_elated_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }

    public function calculateHeaderHeight() {
        $headerHeight = $this->logoAreaHeight + $this->menuAreaHeight;
        if(creator_elated_is_top_bar_enabled()) {
            $headerHeight += creator_elated_get_top_bar_height();
        }

        return $headerHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {

        $globalVariables['eltdLogoAreaHeight'] = $this->logoAreaHeight;;
        $globalVariables['eltdMenuAreaHeight'] = $this->headerHeight;
        $globalVariables['eltdMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(creator_elated_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            $perPageVars['eltdHeaderTransparencyHeight'] = $this->headerHeight - (creator_elated_get_top_bar_height() + $this->heightOfCompleteTransparency);
        }else{
            $perPageVars['eltdHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }
}