<?php

use CreatorElated\Modules\Header\Lib\HeaderFactory;

if(!function_exists('creator_elated_get_header')) {
    /**
     * Loads header HTML based on header type option. Sets all necessary parameters for header
     * and defines creator_elated_header_type_parameters filter
     */
    function creator_elated_get_header() {

        //will be read from options
        $header_type     = creator_elated_get_meta_field_intersect('header_type');
        $header_behavior = creator_elated_options()->getOptionValue('header_behaviour');
        $menu_area_in_grid = creator_elated_get_meta_field_intersect( 'header_in_grid' ) == 'yes' ? true : false;
        $full_screen_icon_size = creator_elated_options()->getOptionValue( 'fullscreen_menu_icon_size' );
        extract(creator_elated_get_page_options());

        $menu_area_grid_bck_color = $menu_area_background_color;
        //if is set menu area in grid, disable background color from eltd-menu-area holder

        $grid_class = 'eltd-header-out-grid';
        if($menu_area_in_grid){
            $menu_area_background_color = '';
            $grid_class = 'eltd-header-in-grid';
        }

        $logo_area_grid_bck_color = $logo_area_background_color;
        //if is set menu area in grid, disable background color from eltd-menu-area holder
        if($menu_area_in_grid){
            $logo_area_background_color = '';
        }

        if(HeaderFactory::getInstance()->validHeaderObject()) {
            $parameters = array(
                'hide_logo'          => creator_elated_get_meta_field_intersect('hide_logo') == 'yes' ? true : false,
                'show_sticky'        => in_array($header_behavior, array(
                    'sticky-header-on-scroll-up',
                    'sticky-header-on-scroll-down-up'
                )) ? true : false,
                'show_fixed_wrapper' => in_array($header_behavior, array('fixed-on-scroll')) ? true : false,
                'menu_area_in_grid'  => $menu_area_in_grid,
                'menu_area_background_color' => $menu_area_background_color,
                'menu_area_grid_background_color' => $menu_area_grid_bck_color,
                'logo_area_background_color' => $logo_area_background_color,
                'logo_area_grid_background_color' => $logo_area_grid_bck_color,
                'grid_class'        => $grid_class,
                'header_standard_position'         => $header_standard_position,
                'vertical_header_background_color' => $vertical_header_background_color,
                'vertical_header_opacity' => $vertical_header_opacity,
                'vertical_background_image' => $vertical_background_image,
                'full_screen_icon_size'     => $full_screen_icon_size
            );

            $parameters = apply_filters('creator_elated_header_type_parameters', $parameters, $header_type);

            HeaderFactory::getInstance()->getHeaderObject()->loadTemplate($parameters);
        }
    }
}

if(!function_exists('creator_elated_get_header_top')) {
    /**
     * Loads header top HTML and sets parameters for it
     */
    function creator_elated_get_header_top() {
        $top_bar_skin='';
        //generate column width class
        switch(creator_elated_options()->getOptionValue('top_bar_layout')) {
            case ('two-columns'):
                $column_widht_class = '50-50';
                break;
            case ('three-columns'):
                $column_widht_class = creator_elated_options()->getOptionValue('top_bar_column_widths');
                break;
        }

        if(creator_elated_get_meta_field_intersect('top_bar_skin')!==''){
            $top_bar_skin= creator_elated_get_meta_field_intersect('top_bar_skin') == 'light' ? 'eltd-top-bar-light' : 'eltd-top-bar-dark';
        }
        $params = array(
            'column_widths'      => $column_widht_class,
            'show_widget_center' => creator_elated_options()->getOptionValue('top_bar_layout') == 'three-columns' ? true : false,
            'show_header_top'    =>  creator_elated_get_meta_field_intersect('top_bar') == 'yes' ? true : false,
            'top_bar_skin'       =>   $top_bar_skin,
            'top_bar_in_grid'    =>  creator_elated_options()->getOptionValue('top_bar_in_grid') == 'yes' ? true : false
        );

        $params = apply_filters('creator_elated_header_top_params', $params);

        creator_elated_get_module_template_part('templates/parts/header-top', 'header', '', $params);
    }
}

if(!function_exists('creator_elated_get_logo')) {
    /**
     * Loads logo HTML
     *
     * @param $slug
     */
    function creator_elated_get_logo($slug = '') {

        $slug = $slug !== '' ? $slug : creator_elated_options()->getOptionValue('header_type');

        if($slug == 'sticky'){
            $logo_image = creator_elated_options()->getOptionValue('logo_image_sticky');
        }else{
            $logo_image = creator_elated_get_meta_field_intersect('logo_image');
        }
        $id = creator_elated_get_page_id();
        $logo_per_page = get_post_meta($id, 'eltd_logo_image_meta',true);

        if(!isset($logo_per_page) || $logo_per_page === ''){
            $logo_image_dark = creator_elated_options()->getOptionValue('logo_image_dark');
            $logo_image_light = creator_elated_options()->getOptionValue('logo_image_light');
        }else{
            $logo_image_dark = $logo_per_page;
            $logo_image_light = $logo_per_page;
        }

        //get logo image dimensions and set style attribute for image link.
        $logo_dimensions = creator_elated_get_image_dimensions($logo_image);

        $logo_styles = '';
        if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
            $logo_height = $logo_dimensions['height'];
            $logo_styles = 'height: '.intval($logo_height / 2).'px;'; //divided with 2 because of retina screens
        }

        $params = array(
            'logo_image'  => $logo_image,
            'logo_image_dark' => $logo_image_dark,
            'logo_image_light' => $logo_image_light,
            'logo_styles' => $logo_styles
        );

        creator_elated_get_module_template_part('templates/parts/logo', 'header', $slug, $params);
    }
}

if(!function_exists('creator_elated_get_main_menu')) {
    /**
     * Loads main menu HTML
     *
     * @param string $additional_class addition class to pass to template
     */
    function creator_elated_get_main_menu($additional_class = 'eltd-default-nav') {
        creator_elated_get_module_template_part('templates/parts/navigation', 'header', '', array('additional_class' => $additional_class));
    }
}

if(!function_exists('creator_elated_get_sticky_menu')) {
	/**
	 * Loads sticky menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function creator_elated_get_sticky_menu($additional_class = 'eltd-default-nav') {
		creator_elated_get_module_template_part('templates/parts/sticky-navigation', 'header', '', array('additional_class' => $additional_class));
	}
}


if(!function_exists('creator_elated_get_vertical_main_menu')) {
    /**
     * Loads vertical menu HTML
     */
    function creator_elated_get_vertical_main_menu() {
        $params = array();
        $dropdown_type = creator_elated_options()->getOptionValue('vertical_header_dropdown_style');
        $params['vertical_menu_type'] = $dropdown_type;
        switch($dropdown_type){
            case 'float':
                $params['vertical_menu_class'] = 'eltd-vertical-dropdown-float';
                break;
            case 'dropdown-toggle-click':
                $params['vertical_menu_class'] = 'eltd-vertical-dropdown-toggle';
                break;
        }
        creator_elated_get_module_template_part('templates/parts/vertical-navigation', 'header', '', $params);
    }
}

if(!function_exists('creator_elated_get_left_menu')) {
    /**
     * Loads left menu HTML for divided header
     *
     * @param string $additional_class
     */
    function creator_elated_get_left_menu($additional_class = 'eltd-left-nav') {
        creator_elated_get_module_template_part('templates/parts/left-navigation', 'header', '', array('additional_class' => $additional_class));
    }
}

if(!function_exists('creator_elated_get_right_menu')) {
    /**
     * Loads right menu HTML for divided header
     *
     * @param string $additional_class
     */
    function creator_elated_get_right_menu($additional_class = 'eltd-right-nav') {
        creator_elated_get_module_template_part('templates/parts/right-navigation', 'header', '', array('additional_class' => $additional_class));
    }
}



if(!function_exists('creator_elated_get_sticky_header')) {
    /**
     * Loads sticky header behavior HTML
     */
    function creator_elated_get_sticky_header() {

        $parameters = array(
            'hide_logo'             => creator_elated_options()->getOptionValue('hide_logo') == 'yes' ? true : false,
            'sticky_header_in_grid' => creator_elated_options()->getOptionValue('sticky_header_in_grid') == 'yes' ? true : false
        );

        creator_elated_get_module_template_part('templates/behaviors/sticky-header', 'header', '', $parameters);
    }
}

if(!function_exists('creator_elated_get_mobile_header')) {
    /**
     * Loads mobile header HTML only if responsiveness is enabled
     */
    function creator_elated_get_mobile_header() {
        if(creator_elated_is_responsive_on()) {
            $header_type = creator_elated_options()->getOptionValue('header_type');

            //this could be read from theme options
            $mobile_header_type = 'mobile-header';

            $parameters = array(
                'show_logo'              => creator_elated_options()->getOptionValue('hide_logo') == 'yes' ? false : true,
                'menu_opener_icon'       => creator_elated_icon_collections()->getMobileMenuIcon(creator_elated_options()->getOptionValue('mobile_icon_pack'), true),
                'show_navigation_opener' => has_nav_menu('main-navigation')
            );

            creator_elated_get_module_template_part('templates/types/'.$mobile_header_type, 'header', $header_type, $parameters);
        }
    }
}

if(!function_exists('creator_elated_get_mobile_logo')) {
    /**
     * Loads mobile logo HTML. It checks if mobile logo image is set and uses that, else takes normal logo image
     *
     * @param string $slug
     */
    function creator_elated_get_mobile_logo($slug = '') {

        $slug = $slug !== '' ? $slug : creator_elated_options()->getOptionValue('header_type');

        //check if mobile logo has been set and use that, else use normal logo
        if(creator_elated_options()->getOptionValue('logo_image_mobile') !== '') {
            $logo_image = creator_elated_options()->getOptionValue('logo_image_mobile');
        } else {
            $logo_image = creator_elated_get_meta_field_intersect('logo_image');
        }

        //get logo image dimensions and set style attribute for image link.
        $logo_dimensions = creator_elated_get_image_dimensions($logo_image);

        $logo_height = '';
        $logo_styles = '';
        if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
            $logo_height = $logo_dimensions['height'];
            $logo_styles = 'height: '.intval($logo_height / 2).'px'; //divided with 2 because of retina screens
        }

        //set parameters for logo
        $parameters = array(
            'logo_image'      => $logo_image,
            'logo_dimensions' => $logo_dimensions,
            'logo_height'     => $logo_height,
            'logo_styles'     => $logo_styles
        );

        creator_elated_get_module_template_part('templates/parts/mobile-logo', 'header', $slug, $parameters);
    }
}

if(!function_exists('creator_elated_get_mobile_nav')) {
    /**
     * Loads mobile navigation HTML
     */
    function creator_elated_get_mobile_nav() {

        $slug = creator_elated_options()->getOptionValue('header_type');

        creator_elated_get_module_template_part('templates/parts/mobile-navigation', 'header', $slug);
    }
}

if(!function_exists('creator_elated_get_page_options')) {
    /**
     * Gets options from page
     */
    function creator_elated_get_page_options() {
        $id = creator_elated_get_page_id();
        $page_options = array();
        $menu_area_background_color_rgba = '';
        $logo_area_background_color_rgba = '';
        $menu_area_background_color = '';
        $menu_area_background_transparency = '';
        $vertical_background_image = '';

        $header_type = creator_elated_get_meta_field_intersect('header_type', $id);

        //take values from current page
        $meta_bckg_color = get_post_meta($id, 'eltd_header_background_color_meta', true);
        $logo_bckg_color_meta  = get_post_meta($id, 'eltd_logo_area_background_color_meta', true);
        $meta_bckg_transparency  =  get_post_meta($id, 'eltd_header_background_transparency_meta', true);

        //take values from global options
        $option_bckg_color = creator_elated_options()->getOptionValue('header_background_color');
        $option_bckg_transparency = creator_elated_options()->getOptionValue('header_background_transparency');
        $logo_bckg_color_options = creator_elated_options()->getOptionValue('logo_area_background_color');

        if(isset($meta_bckg_color) && $meta_bckg_color !== ''){
            if($meta_bckg_color !== 'transparent'){
                $menu_area_background_transparency = 1;

                if(isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
                    $menu_area_background_transparency = $meta_bckg_transparency;
                }

                $menu_area_background_color_rgba = 'background-color:'.creator_elated_rgba_color($meta_bckg_color, $menu_area_background_transparency);
            }else{
                $menu_area_background_color_rgba = 'background-color: transparent';
            }

        }

        elseif(isset($option_bckg_color) && $option_bckg_color !== ''){

            if($option_bckg_color !== 'transparent'){
                $menu_area_background_transparency = 1;
                if(isset($option_bckg_transparency) && $option_bckg_transparency !== ''){
                    $menu_area_background_transparency = $option_bckg_transparency;
                }
                $menu_area_background_color_rgba = 'background-color:'.creator_elated_rgba_color($option_bckg_color, $menu_area_background_transparency);
            }else{
                $menu_area_background_color_rgba = 'background-color: transparent';
            }

        }
        if(isset($logo_bckg_color_meta) && $logo_bckg_color_meta !== ''){
            if($logo_bckg_color_meta !=='transparent'){
                $logo_area_background_transparency = 1;

                if(isset($meta_bckg_transparency) && $meta_bckg_transparency !== ''){
                    $logo_area_background_transparency = $meta_bckg_transparency;
                }

                $logo_area_background_color_rgba = 'background-color:'.creator_elated_rgba_color($logo_bckg_color_meta, $logo_area_background_transparency);
            }else{
                $logo_area_background_color_rgba = 'background-color: transparent';
            }
        }
        elseif(isset($logo_bckg_color_options) && $logo_bckg_color_options !== ''){

            if($logo_bckg_color_options !== 'transparent'){
                $logo_area_background_transparency = 1;
                if(isset($option_bckg_transparency) && $option_bckg_transparency !== ''){
                    $logo_area_background_transparency = $option_bckg_transparency;
                }
                $logo_area_background_color_rgba = 'background-color:'.creator_elated_rgba_color($logo_bckg_color_options, $logo_area_background_transparency);
            }
            else{
                $logo_area_background_color_rgba = 'background-color: transparent';
            }
        }

        switch ($header_type) {

            case 'header-vertical':

                if(get_post_meta($id, 'eltd_disable_vertical_header_background_image_meta', true) == 'yes'){
                    $vertical_background_image = 'background-image:none';
                }elseif(($meta_temp = get_post_meta($id, 'eltd_vertical_header_background_image_meta', true)) !== ''){
                    $vertical_background_image = 'background-image:url('.$meta_temp.')';
                }

                break;
        }

        $header_standard_alignment = creator_elated_get_meta_field_intersect('header_standard_align', $id);
        $page_options['header_standard_position'] = $header_standard_alignment;

        $page_options['menu_area_background_color'] = $menu_area_background_color_rgba;
        $page_options['logo_area_background_color'] = $logo_area_background_color_rgba;
        $page_options['vertical_header_background_color'] = $menu_area_background_color_rgba;
        $page_options['vertical_header_opacity'] = $menu_area_background_transparency;
        $page_options['vertical_background_image'] = $vertical_background_image;

        return $page_options;
    }
}