<?php

if ( ! function_exists( 'creator_elated_register_top_header_areas' ) ) {
	/**
	 * Registers widget areas for top header bar when it is enabled
	 */
	function creator_elated_register_top_header_areas() {
		$top_bar_layout  = creator_elated_options()->getOptionValue( 'top_bar_layout' );

	
			register_sidebar( array(
				'name'          => esc_html__( 'Top Bar Left', 'creator' ),
				'id'            => 'eltd-top-bar-left',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget">',
				'after_widget'  => '</div>',
				'description'   => esc_html__( 'Widgets added here will appear on the left side in top header', 'creator' )
			) );

			//register this widget area only if top bar layout is three columns
			if ( $top_bar_layout === 'three-columns' ) {
				register_sidebar( array(
					'name'          => esc_html__( 'Top Bar Center', 'creator' ),
					'id'            => 'eltd-top-bar-center',
					'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget">',
					'after_widget'  => '</div>',
					'description'   => esc_html__( 'Widgets added here will appear on the center side in top header', 'creator' )
				) );
			}

			register_sidebar( array(
				'name'          => esc_html__( 'Top Bar Right', 'creator' ),
				'id'            => 'eltd-top-bar-right',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget">',
				'after_widget'  => '</div>',
				'description'   => esc_html__( 'Widgets added here will appear on the right side in top header', 'creator' )
			) );
		
	}

	add_action( 'widgets_init', 'creator_elated_register_top_header_areas' );
}

if ( ! function_exists( 'creator_elated_header_standard_widget_areas' ) ) {
	/**
	 * Registers widget areas for standard header type
	 */
	function creator_elated_header_standard_widget_areas() {
		if (creator_elated_core_installed()) {
			register_sidebar(array(
				'name' => esc_html__('Header Standard Right From Main Menu', 'creator'),
				'id' => 'eltd-right-from-main-menu-standard',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget">',
				'after_widget' => '</div>',
				'description' => esc_html__('Widgets added here will appear on the right hand side from the main menu', 'creator')
			));
		}
	}

	add_action( 'widgets_init', 'creator_elated_header_standard_widget_areas' );
}

if ( ! function_exists( 'creator_elated_header_centered_widget_areas' ) ) {
	/**
	 * Registers widget areas for centered header type
	 */
	function creator_elated_header_centered_widget_areas() {
		register_sidebar( array(
			'name'          => esc_html__( 'Header Centered Right From Main Menu', 'creator' ),
			'id'            => 'eltd-right-from-main-menu-centered',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget">',
			'after_widget'  => '</div>',
			'description'   => esc_html__( 'Widgets added here will appear on the right hand side from the main menu', 'creator' )
		) );
	}


	add_action( 'widgets_init', 'creator_elated_header_centered_widget_areas' );
}
if ( ! function_exists( 'creator_elated_header_simple_widget_areas' ) ) {
	/**
	 * Registers widget areas for simple header type
	 */
	function creator_elated_header_simple_widget_areas() {
		register_sidebar( array(
			'name'          => esc_html__( 'Header Simple Right From Main Menu', 'creator' ),
			'id'            => 'eltd-right-from-main-menu-simple',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget">',
			'after_widget'  => '</div>',
			'description'   => esc_html__( 'Widgets added here will appear on the right hand side from the main menu', 'creator' )
		) );
		register_sidebar(array(
			'name'          => esc_html__('Header Simple Left From Main Menu', 'creator'),
			'id'            => 'eltd-left-from-main-menu-simple',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-left-from-main-menu-widget">',
			'after_widget'  => '</div>',
			'description'   => esc_html__('Widgets added here will appear on the left hand side from the main menu', 'creator')
		));
	}

	add_action( 'widgets_init', 'creator_elated_header_simple_widget_areas' );
}

if ( ! function_exists( 'creator_elated_header_dual_widget_areas' ) ) {
	/**
	 * Registers widget areas for dual header type
	 */
	function creator_elated_header_dual_widget_areas() {
		register_sidebar( array(
			'name'          => esc_html__( 'Header Dual Right From Main Menu', 'creator' ),
			'id'            => 'eltd-header-dual',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget">',
			'after_widget'  => '</div>',
			'description'   => esc_html__( 'Widgets added here will appear on the right hand side from the main menu', 'creator' )
		) );
	}

	add_action( 'widgets_init', 'creator_elated_header_dual_widget_areas' );
}

if ( ! function_exists( 'creator_elated_header_compound_widget_areas' ) ) {
	/**
	 * Registers widget areas for compound header type
	 */
	function creator_elated_header_compound_widget_areas() {
		register_sidebar( array(
			'name'          => esc_html__( 'Header Compound Bottom Area', 'creator' ),
			'id'            => 'eltd-header-compound-bottom',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-header-compound-bottom-widgets">',
			'after_widget'  => '</div>',
			'description'   => esc_html__( 'Widgets added here will appear on the bottom area', 'creator' )
		) );
		register_sidebar(array(
			'name'          => esc_html__('Header Compound Top Right Area', 'creator'),
			'id'            => 'eltd-header-compound-top-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-header-compound-top-right-widgets">',
			'after_widget'  => '</div>',
			'description'   => esc_html__('Widgets added here will appear on the top right area', 'creator')
		));
		register_sidebar(array(
			'name'          => esc_html__('Header Compound Top Left Area', 'creator'),
			'id'            => 'eltd-header-compound-top-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-header-compound-top-right-widgets">',
			'after_widget'  => '</div>',
			'description'   => esc_html__('Widgets added here will appear on the top left area', 'creator')
		));
	}

	add_action( 'widgets_init', 'creator_elated_header_compound_widget_areas' );
}
if ( ! function_exists( 'creator_elated_header_vertical_widget_areas' ) ) {
	/**
	 * Registers widget areas for vertical header
	 */
	function creator_elated_header_vertical_widget_areas() {
		register_sidebar( array(
			'name'        => esc_html__( 'Vertical Area', 'creator' ),
			'description' => esc_html__( 'Widgets added here will appear on the bottom of vertical menu', 'creator' ),
			'id'            => 'eltd-vertical-area',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-vertical-area-widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="eltd-vertical-area-widget-title">',
			'after_title'   => '</h6>'
		) );
	}

	add_action( 'widgets_init', 'creator_elated_header_vertical_widget_areas' );
}

if ( ! function_exists( 'creator_elated_register_mobile_header_areas' ) ) {
	/**
	 * Registers widget areas for mobile header
	 */
	function creator_elated_register_mobile_header_areas() {
		if ( creator_elated_is_responsive_on() && creator_elated_core_installed() ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Right From Mobile Logo', 'creator' ),
				'id'            => 'eltd-right-from-mobile-logo',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-mobile-logo">',
				'after_widget'  => '</div>',
				'description'   => esc_html__( 'Widgets added here will appear on the right hand side from the mobile logo', 'creator' )
			) );
		}
	}

	add_action( 'widgets_init', 'creator_elated_register_mobile_header_areas' );
}

if ( ! function_exists( 'creator_elated_register_sticky_header_areas' ) ) {
	/**
	 * Registers widget area for sticky header
	 */
	function creator_elated_register_sticky_header_areas() {
		if ( in_array( creator_elated_options()->getOptionValue( 'header_behaviour' ), array(
			'sticky-header-on-scroll-up',
			'sticky-header-on-scroll-down-up'
		) ) ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Sticky Right', 'creator' ),
				'id'            => 'eltd-sticky-right',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-sticky-right">',
				'after_widget'  => '</div>',
				'description'   => esc_html__( 'Widgets added here will appear on the right hand side in sticky menu', 'creator' )
			) );
		}
	}

	add_action( 'widgets_init', 'creator_elated_register_sticky_header_areas' );
}