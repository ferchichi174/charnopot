<?php

//top header bar
add_action('creator_elated_before_page_header', 'creator_elated_get_header_top');

//mobile header
add_action('creator_elated_after_page_header', 'creator_elated_get_mobile_header');