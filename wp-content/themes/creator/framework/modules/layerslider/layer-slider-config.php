<?php
	if(!function_exists('creator_elated_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function creator_elated_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'creator_elated_layerslider_overrides');
	}
?>