<?php

class CreatorElatedLatestPosts extends CreatorElatedWidget {
	protected $params;
	public function __construct() {
		parent::__construct(
			'eltd_latest_posts_widget', // Base ID
			esc_html__('Elated Latest Post', 'creator'), // Name
			array( 'description' => esc_html__( 'Display posts from your blog', 'creator' ), ) // Args
		);

		$this->setParams();
	}

	protected function setParams() {
		$this->params = array(
			array(
				'name' => 'title',
				'type' => 'textfield',
				'title' => esc_html__( 'Title','creator' ),
			),
			array(
				'name' => 'number_of_posts',
				'type' => 'textfield',
				'title' => esc_html__( 'Number of posts','creator' )
			),
			array(
				'name' => 'order_by',
				'type' => 'dropdown',
				'title' =>esc_html__(  'Order By','creator' ),
				'options' => array(
					'title' =>esc_html__(  'Title','creator' ),
					'date' =>esc_html__(  'Date','creator' ),
				)
			),
			array(
				'name' => 'order',
				'type' => 'dropdown',
				'title' => esc_html__( 'Order','creator' ),
				'options' => array(
					'ASC' => esc_html__( 'ASC','creator' ),
					'DESC' =>esc_html__(  'DESC','creator' )
				)
			),
			array(
				'name' => 'category',
				'type' => 'textfield',
				'title' => esc_html__( 'Category Slug','creator' )
			),
			array(
				'name' => 'text_length',
				'type' => 'textfield',
				'title' =>esc_html__(  'Number of characters','creator' )
			),
			array(
				'name' => 'title_tag',
				'type' => 'dropdown',
				'title' =>esc_html__(  'Title Tag','creator' ),
				'options' => array(
					""   => "",
					"h2" => "h2",
					"h3" => "h3",
					"h4" => "h4",
					"h5" => "h5",
					"h6" => "h6"
				)
			)			
		);
	}

	public function widget($args, $instance) {
		extract($args);

		//prepare variables
		$content        = '';
		$params         = array();
		$params['type'] = 'minimal';
		//is instance empty?
		if(is_array($instance) && count($instance)) {
			//generate shortcode params
			foreach($instance as $key => $value) {
				$params[$key] = $value;
			}
		}
		if(empty($params['title_tag'])){
			$params['title_tag'] = 'h4';
		}

		echo creator_elated_get_module_part( $args['before_widget'] );
		echo creator_elated_get_module_part( $args['before_title'] );
		echo creator_elated_get_module_part( $instance['title'] );
		echo creator_elated_get_module_part( $args['after_title'] );
		print creator_elated_execute_shortcode('eltd_blog_list', $params);
		echo creator_elated_get_module_part( $args['after_widget'] );

	}
}
