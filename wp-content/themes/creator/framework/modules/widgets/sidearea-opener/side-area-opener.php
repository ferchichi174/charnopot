<?php

class CreatorElatedSideAreaOpener extends CreatorElatedWidget {
	public function __construct() {
		parent::__construct(
			'eltd_side_area_opener', // Base ID
			'Elated Side Area Opener' // Name
		);

		$this->setParams();
	}

	protected function setParams() {

		$this->params = array(
			array(
				'name'        => 'side_area_opener_icon_color',
				'type'        => 'textfield',
				'title'       => esc_html__('Icon Color','creator'),
				'description' => esc_html__('Define color for Side Area opener icon','creator')
			)
		);

	}


	public function widget( $args, $instance ) {

		$sidearea_icon_styles = array();

		if ( ! empty( $instance['side_area_opener_icon_color'] ) ) {
			$sidearea_icon_styles[] = 'color: ' . $instance['side_area_opener_icon_color'];
		}

		$icon_size = '';
		if ( creator_elated_options()->getOptionValue( 'side_area_predefined_icon_size' ) ) {
			$icon_size = creator_elated_options()->getOptionValue( 'side_area_predefined_icon_size' );
		}
		echo creator_elated_get_module_part( $args['before_widget'] );
		?>
		<a class="eltd-side-menu-button-opener <?php echo esc_attr( $icon_size ); ?>" <?php creator_elated_inline_style( $sidearea_icon_styles ) ?>
		   href="javascript:void(0)">
			<?php echo creator_elated_get_side_menu_icon_html(); ?>
		</a>
		<?php
		echo creator_elated_get_module_part( $args['after_widget'] );
	}

}