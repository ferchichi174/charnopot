<?php

if ( ! function_exists('creator_elated_woocommerce_options_map') ) {

	/**
	 * Add Woocommerce options page
	 */
	function creator_elated_woocommerce_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_woocommerce_page',
				'title' => esc_html__('Woocommerce','creator'),
				'icon' => 'fa fa-shopping-cart'
			)
		);

		/**
		 * Product List Settings
		 */
		$panel_product_list = creator_elated_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_product_list',
				'title' => esc_html__( 'Product List','creator'),
			)
		);
		creator_elated_add_admin_field(array(
			'name'        	=> 'woo_product_list_type',
			'type'        	=> 'select',
			'label'       	=>  esc_html__('Product List Type','creator'),
			'default_value'	=> 'type1',
			'description' 	=>  esc_html__('Choose product listing type','creator'),
			'options'		=> array(
				'type1' =>  esc_html__('Type 1','creator'),
				'type2' =>  esc_html__('Type 2','creator')
			),
			'parent'      	=> $panel_product_list,
		));

		creator_elated_add_admin_field(array(
			'name'        	=> 'eltd_woo_products_list_full_width',
			'type'        	=> 'yesno',
			'label'       	=>  esc_html__('Enable Full Width Template','creator'),
			'default_value'	=> 'no',
			'description' 	=>  esc_html__('Enabling this option will enable full width template for shop page','creator'),
			'parent'      	=> $panel_product_list,
		));

		creator_elated_add_admin_field(array(
			'name'        	=> 'eltd_woo_product_list_columns',
			'type'        	=> 'select',
			'label'       	=>  esc_html__('Product List Columns','creator'),
			'default_value'	=> 'eltd-woocommerce-columns-3',
			'description' 	=>  esc_html__('Choose number of columns for product listing and related products on single product','creator'),
			'options'		=> array(
				'eltd-woocommerce-columns-3' =>  esc_html__('3 Columns (2 with sidebar)','creator'),
				'eltd-woocommerce-columns-4' =>  esc_html__('4 Columns (3 with sidebar)','creator')
			),
			'parent'      	=> $panel_product_list,
		));

		creator_elated_add_admin_field(array(
			'name'        	=> 'eltd_woo_products_per_page',
			'type'        	=> 'text',
			'label'       	=>  esc_html__('Number of products per page','creator'),
			'default_value'	=> '',
			'description' 	=>  esc_html__('Set number of products on shop page','creator'),
			'parent'      	=> $panel_product_list,
			'args' 			=> array(
				'col_width' => 3
			)
		));

		/**
		 * Product Single Settings
		 */
		$panel_product_single = creator_elated_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_product_single',
				'title' =>  esc_html__('Product Single','creator')
			)
		);

		creator_elated_add_admin_field(array(
			'name'        	=> 'woo_product_single_padding_meta',
			'type'          => 'text',
			'label'         =>  esc_html__('Padding on Shop Single Pages','creator'),
			'description'   =>  esc_html__('Set the padding for single product pages.Insert padding in format 10px 10px 10px 10px','creator'),
			'parent'        => $panel_product_single,
			'default_value' => ''
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_woocommerce_options_map', 24);

}