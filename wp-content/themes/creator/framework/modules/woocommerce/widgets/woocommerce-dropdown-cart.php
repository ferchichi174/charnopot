<?php

class CreatorElatedWoocommerceDropdownCart extends WP_Widget
{
	public function __construct()
	{
		parent::__construct(
			'eltd_woocommerce_dropdown_cart', // Base ID
			'Elated Woocommerce Dropdown Cart', // Name
			array('description' => esc_html__('Elated Woocommerce Dropdown Cart', 'creator'),) // Args
		);
	}

	public function widget($args, $instance)
	{
		extract($args);
		global $woocommerce;

		$cart_products_number = $woocommerce->cart->get_cart_contents_count();
		$first_color = creator_elated_get_first_main_color();

		echo creator_elated_get_module_part($before_widget); ?>
		<div class="eltd-shopping-cart-widget">
			<div class="eltd-shopping-cart">
				<a href="<?php echo esc_url(wc_get_cart_url()); ?>">
					<span class="eltd-shopping-cart-number">
						<?php echo esc_html($cart_products_number); ?>
					</span>
					<i class="icon_cart_alt"></i>
				</a>
			</div>
			<div class="eltd-shopping-cart-dropdown">
				<ul>
					<?php if (!$woocommerce->cart->is_empty()) {
						foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) {
							$product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
							$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

							if ($product && $product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)) {
								$product_name = apply_filters('woocommerce_cart_item_name', $product->get_title(), $cart_item, $cart_item_key);
								$product_image = apply_filters('woocommerce_cart_item_thumbnail', $product->get_image(), $cart_item, $cart_item_key);
								$product_price = apply_filters('woocommerce_cart_item_price', $woocommerce->cart->get_product_price($product), $cart_item, $cart_item_key);

								?>
								<li class="eltd-shopping-cart-item">
									<?php
									echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
										'<a href="%s" class="eltd-shopping-cart-remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url(wc_get_cart_remove_url($cart_item_key)),
										esc_html__('Remove this item', 'creator'),
										esc_attr($product_id),
										esc_attr($product->get_sku())
									), $cart_item_key);
									?>
									<div class="eltd-shopping-cart-item-image">
										<?php echo str_replace(array(
											'http:',
											'https:'
										), '', $product_image); ?>
									</div>
									<div class="eltd-shopping-cart-item-details">
										<h6>
											<?php if (!$product->is_visible()) {
												echo esc_html($product_name);
											} else { ?>
												<a href="<?php echo esc_url($product->get_permalink($cart_item)); ?>">
													<?php echo esc_html($product_name); ?>
												</a>
											<?php } ?>
										</h6>
										<div class="eltd-shopping-cart-price">
											<?php echo creator_elated_get_module_part($product_price); ?>
										</div>
										<div class="eltd-shopping-cart-quantity">
											<?php echo esc_html__('Quantity:', 'creator') . ' ' . $cart_item['quantity']; ?>
										</div>
									</div>
								</li>
								<?php
							}
						}
					} else {
						?>
						<li class="eltd-empty-cart">
							<?php esc_html_e('No products in the cart.', 'creator'); ?>
						</li>
						<?php
					} ?>
				</ul>
				<?php if (!$woocommerce->cart->is_empty()) : ?>
					<div class="eltd-shopping-cart-footer">

						<p class="eltd-shopping-cart-total"><?php esc_html_e('Subtotal', 'creator'); ?>
							: <?php echo creator_elated_get_module_part($woocommerce->cart->get_cart_subtotal()); ?></p>

						<div class="eltd-shopping-cart-buttons clearfix">
							<?php
							echo creator_elated_get_button_html(array(
								'link' => wc_get_cart_url(),
								'text' => esc_html__('View Cart', 'creator'),
								'custom_class' => 'button wc-forward',
								'type' => 'solid',
								'color' => '#fff',
								'background_color' => $first_color,
								'border_color' => $first_color,
								'hover_color' => '#fff',
								'hover_background_color' => '#333',
								'hover_border_color' => '#333',
							));
							?>
							<?php
							echo creator_elated_get_button_html(array(
								'link' => wc_get_checkout_url(),
								'text' => esc_html__('Checkout', 'creator'),
								'custom_class' => 'button checkout wc-forward',
								'type' => 'solid',
								'color' => '#fff',
								'background_color' => $first_color,
								'border_color' => $first_color,
								'hover_color' => '#fff',
								'hover_background_color' => '#333',
								'hover_border_color' => '#333',
							));
							?>
						</div>

					</div>

				<?php endif; ?>
			</div>
		</div>
		<?php
		echo creator_elated_get_module_part($after_widget);
	}

}

if ( ! function_exists( 'creator_elated_woocommerce_dropdown_cart_ajax' ) ) {

	function creator_elated_woocommerce_dropdown_cart_ajax( $fragments ) {
		global $woocommerce;
		$cart_products_number = $woocommerce->cart->get_cart_contents_count();
		$first_color = creator_elated_get_first_main_color();

		ob_start();
		?>
		<div class="eltd-shopping-cart-widget">
			<div class="eltd-shopping-cart">
				<a href="<?php echo esc_url( wc_get_cart_url() ); ?>">
					<span class="eltd-shopping-cart-number">
						<?php echo esc_html( $cart_products_number ); ?>
					</span>
					<i class="icon_cart_alt"></i>
				</a>
			</div>
			<div class="eltd-shopping-cart-dropdown">
				<ul>
					<?php if ( ! $woocommerce->cart->is_empty() ) {
						foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
							$product    = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

							if ( $product && $product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
								$product_name  = apply_filters( 'woocommerce_cart_item_name', $product->get_title(), $cart_item, $cart_item_key );
								$product_image = apply_filters( 'woocommerce_cart_item_thumbnail', $product->get_image(), $cart_item, $cart_item_key );
								$product_price = apply_filters( 'woocommerce_cart_item_price', $woocommerce->cart->get_product_price( $product ), $cart_item, $cart_item_key );

								?>
								<li class="eltd-shopping-cart-item">
									<?php
									echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
										'<a href="%s" class="eltd-shopping-cart-remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'creator' ),
										esc_attr( $product_id ),
										esc_attr( $product->get_sku() )
									), $cart_item_key );
									?>
									<div class="eltd-shopping-cart-item-image">
										<?php echo str_replace( array(
											'http:',
											'https:'
										), '', $product_image ); ?>
									</div>
									<div class="eltd-shopping-cart-item-details">
										<h6>
											<?php if ( ! $product->is_visible() ) {
												echo esc_html( $product_name );
											} else { ?>
												<a href="<?php echo esc_url( $product->get_permalink( $cart_item ) ); ?>">
													<?php echo esc_html( $product_name ); ?>
												</a>
											<?php } ?>
										</h6>
										<div class="eltd-shopping-cart-price">
											<?php echo creator_elated_get_module_part( $product_price ); ?>
										</div>
										<div class="eltd-shopping-cart-quantity">
											<?php echo esc_html__( 'Quantity:', 'creator' ) . ' ' . $cart_item['quantity']; ?>
										</div>
									</div>
								</li>
								<?php
							}
						}
					} else {
						?>
						<li class="eltd-empty-cart">
							<?php esc_html_e( 'No products in the cart.', 'creator' ); ?>
						</li>
						<?php
					} ?>
				</ul>
				<?php if ( ! $woocommerce->cart->is_empty() ) : ?>
					<div class="eltd-shopping-cart-footer">

						<p class="eltd-shopping-cart-total"><?php esc_html_e( 'Subtotal', 'creator' ); ?>
							: <?php echo creator_elated_get_module_part( $woocommerce->cart->get_cart_subtotal() ); ?></p>

						<div class="eltd-shopping-cart-buttons clearfix">
							<?php
							echo creator_elated_get_button_html( array(
								'link'                   => wc_get_cart_url(),
								'text'                   => esc_html__( 'View Cart', 'creator' ),
								'custom_class'           => 'button wc-forward',
								'type'                   => 'solid',
								'color'           		 => '#fff',
							) );
							?>
							<?php
							echo creator_elated_get_button_html( array(
								'link'                   => wc_get_checkout_url(),
								'text'                   => esc_html__( 'Checkout', 'creator' ),
								'custom_class'           => 'button checkout wc-forward',
								'type'                   => 'solid',
								'color'           		 => '#fff',
							) );
							?>
						</div>

					</div>

				<?php endif; ?>
			</div>
		</div>
		<?php
		$fragments['div.eltd-shopping-cart-widget'] = ob_get_clean();

		return $fragments;
	}

	add_filter( 'woocommerce_add_to_cart_fragments', 'creator_elated_woocommerce_dropdown_cart_ajax' );

}

?>