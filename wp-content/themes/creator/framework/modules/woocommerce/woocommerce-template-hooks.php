<?php

if ( ! function_exists( 'creator_elated_woocommerce_products_per_page' ) ) {
	/**
	 * Function that sets number of products per page. Default is 12
	 * @return int number of products to be shown per page
	 */
	function creator_elated_woocommerce_products_per_page() {

		$products_per_page = 12;

		if ( creator_elated_options()->getOptionValue( 'eltd_woo_products_per_page' ) ) {
			$products_per_page = creator_elated_options()->getOptionValue( 'eltd_woo_products_per_page' );
		}

		return $products_per_page;

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_related_products_args' ) ) {
	/**
	 * Function that sets number of displayed related products. Hooks to woocommerce_output_related_products_args filter
	 *
	 * @param $args array array of args for the query
	 *
	 * @return mixed array of changed args
	 */
	function creator_elated_woocommerce_related_products_args( $args ) {

		if ( creator_elated_options()->getOptionValue( 'eltd_woo_product_list_columns' ) ) {

			$related = creator_elated_options()->getOptionValue( 'eltd_woo_product_list_columns' );
			switch ( $related ) {
				case 'eltd-woocommerce-columns-4':
					$args['posts_per_page'] = 4;
					break;
				case 'eltd-woocommerce-columns-3':
					$args['posts_per_page'] = 3;
					break;
				default:
					$args['posts_per_page'] = 3;
			}

		} else {
			$args['posts_per_page'] = 3;
		}

		return $args;

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_template_loop_product_title' ) ) {
	/**
	 * Function for overriding product title template in Product List Loop
	 */
	function creator_elated_woocommerce_template_loop_product_title() {

		echo '<h5 class="eltd-product-list-product-title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_template_single_title' ) ) {
	/**
	 * Function for overriding product title template in Single Product template
	 */
	function creator_elated_woocommerce_template_single_title() {

		the_title( '<h4 itemprop="name" class="eltd-single-product-title">', '</h4>' );

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_sale_flash' ) ) {
	/**
	 * Function for overriding Sale Flash Template
	 *
	 * @return string
	 */
	function creator_elated_woocommerce_sale_flash() {

		return '<span class="eltd-onsale"><span class="eltd-onsale-inner">' . esc_html__( 'Sale', 'creator' ) . '</span></span>';

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_out_of_stock' ) ) {
	/**
	 * Out of Stock notice
	 *
	 * @return string
	 */
	function creator_elated_woocommerce_out_of_stock() {

		global $product;

		if ( ! $product->is_in_stock() ) {
			echo '<span class="eltd-out-of-stock"><span class="eltd-out-of-stock-inner">' . esc_html__( 'Out of Stock', 'creator' ) . '</span></span>';
		}

	}

}

if ( ! function_exists( 'creator_elated_custom_override_checkout_fields' ) ) {
	/**
	 * Overrides placeholder values for checkout fields
	 *
	 * @param array all checkout fields
	 *
	 * @return array checkout fields with overriden values
	 */
	function creator_elated_custom_override_checkout_fields( $fields ) {
		//billing fields
		$args_billing = array(
			'first_name' => esc_html__( 'First name', 'creator' ),
			'last_name'  => esc_html__( 'Last name', 'creator' ),
			'company'    => esc_html__( 'Company name', 'creator' ),
			'address_1'  => esc_html__( 'Address', 'creator' ),
			'email'      => esc_html__( 'Email', 'creator' ),
			'phone'      => esc_html__( 'Phone', 'creator' ),
			'postcode'   => esc_html__( 'Postcode / ZIP', 'creator' ),
			'state'      => esc_html__( 'State / County', 'creator' ),
			'city'      => esc_html__( 'City', 'creator' )

		);

		//shipping fields
		$args_shipping = array(
			'first_name' => esc_html__( 'First name', 'creator' ),
			'last_name'  => esc_html__( 'Last name', 'creator' ),
			'company'    => esc_html__( 'Company name', 'creator' ),
			'address_1'  => esc_html__( 'Address', 'creator' ),
			'postcode'   => esc_html__( 'Postcode / ZIP', 'creator' )
		);

		//override billing placeholder values
		foreach ( $args_billing as $key => $value ) {
			$fields["billing"]["billing_{$key}"]["placeholder"] = $value;
		}

		//override shipping placeholder values
		foreach ( $args_shipping as $key => $value ) {
			$fields["shipping"]["shipping_{$key}"]["placeholder"] = $value;
		}

		return $fields;
	}

}

if ( ! function_exists( 'creator_elated_woocommerce_loop_add_to_cart_link' ) ) {
	/**
	 * Function that overrides default woocommerce add to cart button on product list
	 * Uses HTML from eltd_button
	 *
	 * @return mixed|string
	 */
	function creator_elated_woocommerce_loop_add_to_cart_link() {

		global $product;


		$button_url     = $product->add_to_cart_url();
		$button_classes = sprintf( '%s %s product_type_%s',
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
			esc_attr($product->get_type())
		);
		$button_text    = $product->add_to_cart_text();
		$button_attrs   = array(
			'rel'              => 'nofollow',
			'data-product_id'  => esc_attr( $product->get_id() ),
			'data-product_sku' => esc_attr( $product->get_sku() ),
			'data-quantity'    => esc_attr( isset( $quantity ) ? $quantity : 1 )
		);
		$first_color = creator_elated_get_first_main_color();
		$button_params = array(
			'type'                   => 'solid',
			'link'                   => $button_url,
			'custom_class'           => $button_classes,
			'text'                   => $button_text,
			'color'           		 => '#333333',
			'background_color'		 => '#ffffff',
			'border_color'     		 => '#ffffff',
			'hover_color'            => '#ffffff',
			'hover_background_color' => $first_color,
			'hover_border_color'     => $first_color,
			'custom_attrs'           => $button_attrs
		);

		$add_to_cart_button = creator_elated_get_button_html($button_params);

		return $add_to_cart_button;

	}

}

if ( ! function_exists( 'creator_elated_get_woocommerce_add_to_cart_button' ) ) {
	/**
	 * Function that overrides default woocommerce add to cart button on simple and grouped product single template
	 * Uses HTML from eltd_button
	 */
	function creator_elated_get_woocommerce_add_to_cart_button() {

		global $product;
		$first_color = creator_elated_get_first_main_color();

		$button_attrs   = array(
			'name'  => 'add-to-cart',
			'value' => esc_attr( $product->get_id() )
		);

		$add_to_cart_button = creator_elated_get_button_html(
			array(
				'custom_class'           => 'single_add_to_cart_button button alt',
				'text'                   => $product->single_add_to_cart_text(),
				'html_type'              => 'button',
				'type'                   => 'solid',
				'color'           		 => '#ffffff',
				'background_color'		 => $first_color,
				'border_color'     		 => $first_color,
				'custom_attrs'           => $button_attrs
			)
		);

		echo creator_elated_get_module_part( $add_to_cart_button );
	}
}

if ( ! function_exists( 'creator_elated_get_woocommerce_add_to_cart_button_external' ) ) {
	/**
	 * Function that overrides default woocommerce add to cart button on external product single template
	 * Uses HTML from eltd_button
	 */
	function creator_elated_get_woocommerce_add_to_cart_button_external() {

		global $product;

		$add_to_cart_button = creator_elated_get_button_html(
			array(
				'link'                   => $product->add_to_cart_url(),
				'custom_class'           => 'single_add_to_cart_button alt',
				'text'                   => $product->single_add_to_cart_text(),
				'type'                   => 'solid',
				'custom_attrs'           => array(
					'rel' => 'nofollow'
				)
			)
		);

		echo creator_elated_get_module_part( $add_to_cart_button );

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_single_variation_add_to_cart_button' ) ) {
	/**
	 * Function that overrides default woocommerce add to cart button on variable product single template
	 * Uses HTML from eltd_button
	 */
	function creator_elated_woocommerce_single_variation_add_to_cart_button() {
		global $product;

		$html = '<div class="variations_button">';
		woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) );

		$button = creator_elated_get_button_html( array(
			'html_type'              => 'button',
			'custom_class'           => 'single_add_to_cart_button alt',
			'type'                   => 'solid',
			'hover_color'            => '#fff',
			'hover_background_color' => '',
			'hover_border_color'     => '',
			'text'                   => $product->single_add_to_cart_text()
		) );

		$html .= $button;

		$html .= '<input type="hidden" name="add-to-cart" value="' . absint( $product->get_id() ) . '" />';
		$html .= '<input type="hidden" name="product_id" value="' . absint( $product->get_id() ) . '" />';
		$html .= '<input type="hidden" name="variation_id" class="variation_id" value="" />';
		$html .= '</div>';

		echo creator_elated_get_module_part( $html );

	}

}

if ( ! function_exists( 'creator_elated_get_woocommerce_apply_coupon_button' ) ) {
	/**
	 * Function that overrides default woocommerce apply coupon button
	 * Uses HTML from eltd_button
	 */
	function creator_elated_get_woocommerce_apply_coupon_button() {

		$coupon_button = creator_elated_get_button_html( array(
			'html_type'              => 'input',
			'input_name'             => 'apply_coupon',
			'type'                   => 'solid',
			'hover_color' 			 => '#fff',
			'hover_border_color'	 => '',
			'hover_background_color' => '',
			'text'                   => esc_html__( 'Apply Coupon', 'creator' )
		) );

		echo creator_elated_get_module_part( $coupon_button );

	}

}

if ( ! function_exists( 'creator_elated_get_woocommerce_update_cart_button' ) ) {
	/**
	 * Function that overrides default woocommerce update cart button
	 * Uses HTML from eltd_button
	 */
	function creator_elated_get_woocommerce_update_cart_button() {

		$update_cart_button = creator_elated_get_button_html( array(
			'html_type'              => 'input',
			'input_name'             => 'update_cart',
			'type'                   => 'solid',
			'hover_color' 			 => '#fff',
			'hover_border_color'	 => '',
			'hover_background_color' => '',
			'text'                   => esc_html__( 'Update Cart', 'creator' )
		) );

		echo creator_elated_get_module_part( $update_cart_button );

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_button_proceed_to_checkout' ) ) {
	/**
	 * Function that overrides default woocommerce proceed to checkout button
	 * Uses HTML from eltd_button
	 */
	function creator_elated_woocommerce_button_proceed_to_checkout() {

		$proceed_to_checkout_button = creator_elated_get_button_html( array(
			'link'                   => wc_get_checkout_url(),
			'custom_class'           => 'checkout-button alt wc-forward',
			'type'                   => 'solid',
			'hover_color' 			 => '#fff',
			'hover_border_color'	 => '',
			'hover_background_color' => '',
			'text'                   => esc_html__( 'Proceed to Checkout', 'creator' )
		) );

		echo creator_elated_get_module_part( $proceed_to_checkout_button );

	}

}

if ( ! function_exists( 'creator_elated_get_woocommerce_update_totals_button' ) ) {
	/**
	 * Function that overrides default woocommerce update totals button
	 * Uses HTML from eltd_button
	 */
	function creator_elated_get_woocommerce_update_totals_button() {

		$update_totals_button = creator_elated_get_button_html( array(
			'html_type'              => 'button',
			'text'                   => esc_html__( 'Update Totals', 'creator' ),
			'type'                   => 'solid',
			'custom_attrs'           => array(
				'value' => 1,
				'name'  => 'calc_shipping'
			)
		) );

		echo creator_elated_get_module_part( $update_totals_button );

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_pay_order_button_html' ) ) {
	/**
	 * Function that overrides default woocommerce pay order button on checkout page
	 * Uses HTML from eltd_button
	 */
	function creator_elated_woocommerce_pay_order_button_html() {

		$pay_order_button_text = esc_html__( 'Pay for order', 'creator' );

		$place_order_button = creator_elated_get_button_html( array(
			'html_type'              => 'input',
			'custom_class'           => 'alt',
			'type'                   => 'solid',
			'custom_attrs'           => array(
				'id'         => 'place_order',
				'data-value' => $pay_order_button_text
			),
			'text'                   => $pay_order_button_text,
		) );

		return $place_order_button;

	}

}

if ( ! function_exists( 'creator_elated_woocommerce_order_button_html' ) ) {
	/**
	 * Function that overrides default woocommerce place order button on checkout page
	 * Uses HTML from eltd_button
	 */
	function creator_elated_woocommerce_order_button_html() {

		$pay_order_button_text = esc_html__( 'Place Order', 'creator' );

		$place_order_button = creator_elated_get_button_html( array(
			'html_type'              => 'button',
			'custom_class'           => 'button alt',
			'type'                   => 'solid',
			'hover_color' 			 => '#fff',
			'hover_border_color'	 => '',
			'hover_background_color' => '',
			'custom_attrs'           => array(
				'id'         => 'place_order',
				'data-value' => $pay_order_button_text,
				'name'       => 'woocommerce_checkout_place_order'
			),
			'text'                   => $pay_order_button_text,
		) );

		return $place_order_button;

	}

}

if ( ! function_exists( 'eltd_woocommerce_template_single_subtitle' ) ) {

	function eltd_woocommerce_template_single_subtitle() {

		$product_subtitle = get_post_meta( get_the_ID(), 'eltd_product_subtitle_meta', true );
		if ( $product_subtitle !== '' ) {
			echo '<p class="eltd-single-product-subtitle">' . esc_html($product_subtitle) . '</p>';
		}

	}

}

if(!function_exists('creator_elated_woocommerce_review_display_gravatar')){

	function creator_elated_woocommerce_review_display_gravatar($comment){
		$html = '';
		$html .= '<div class="eltd-woocommerce-comment-image">';
		$html .= get_avatar( $comment, apply_filters( 'woocommerce_review_gravatar_size', '60' ), '' );
		$html .= '</div>';
		echo creator_elated_get_module_part( $html );
	}

}