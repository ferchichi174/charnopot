<?php

if (!function_exists('creator_elated_side_area_slide_from_right_type_style')) {

	function creator_elated_side_area_slide_from_right_type_style()
	{

		if (creator_elated_options()->getOptionValue('side_area_type') == 'side-menu-slide-from-right') {

			if (creator_elated_options()->getOptionValue('side_area_width') !== '' && creator_elated_options()->getOptionValue('side_area_width') >= 30) {
				echo creator_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-side-menu', array(
					'right' => '-'.creator_elated_options()->getOptionValue('side_area_width') . '%',
					'width' => creator_elated_options()->getOptionValue('side_area_width') . '%'
				));
			}

			if (creator_elated_options()->getOptionValue('side_area_content_overlay_color') !== '') {

				echo creator_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-wrapper .eltd-cover', array(
					'background-color' => creator_elated_options()->getOptionValue('side_area_content_overlay_color')
				));

			}
			if (creator_elated_options()->getOptionValue('side_area_content_overlay_opacity') !== '') {

				echo creator_elated_dynamic_css('.eltd-side-menu-slide-from-right.eltd-right-side-menu-opened .eltd-wrapper .eltd-cover', array(
					'opacity' => creator_elated_options()->getOptionValue('side_area_content_overlay_opacity')
				));

			}
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_slide_from_right_type_style');

}

if (!function_exists('creator_elated_side_area_icon_color_styles')) {

	function creator_elated_side_area_icon_color_styles()
	{

		if (creator_elated_options()->getOptionValue('side_area_icon_font_size') !== '') {

			echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener', array(
				'font-size' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_font_size')) . 'px'
			));

			if (creator_elated_options()->getOptionValue('side_area_icon_font_size') > 30) {
				echo '@media only screen and (max-width: 480px) {
						a.eltd-side-menu-button-opener {
						font-size: 30px;
						}
					}';
			}

		}

		if (creator_elated_options()->getOptionValue('side_area_icon_color') !== '') {

			echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener', array(
				'color' => creator_elated_options()->getOptionValue('side_area_icon_color')
			));

		}
		if (creator_elated_options()->getOptionValue('side_area_icon_hover_color') !== '') {

			echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('side_area_icon_hover_color')
			));

		}
		if (creator_elated_options()->getOptionValue('side_area_light_icon_color') !== '') {

			echo creator_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener,
			.eltd-light-header .eltd-top-bar .eltd-side-menu-button-opener', array(
				'color' => creator_elated_options()->getOptionValue('side_area_light_icon_color') . ' !important'
			));

		}
		if (creator_elated_options()->getOptionValue('side_area_light_icon_hover_color') !== '') {

			echo creator_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener:hover,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener:hover,
			.eltd-light-header .eltd-top-bar .eltd-side-menu-button-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('side_area_light_icon_hover_color') . ' !important'
			));

		}
		if (creator_elated_options()->getOptionValue('side_area_dark_icon_color') !== '') {

			echo creator_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener,
			.eltd-dark-header .eltd-top-bar .eltd-side-menu-button-opener', array(
				'color' => creator_elated_options()->getOptionValue('side_area_dark_icon_color') . ' !important'
			));

		}
		if (creator_elated_options()->getOptionValue('side_area_dark_icon_hover_color') !== '') {

			echo creator_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener:hover,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener:hover,
			.eltd-dark-header .eltd-top-bar .eltd-side-menu-button-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('side_area_dark_icon_hover_color') . ' !important'
			));

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_icon_color_styles');

}

if (!function_exists('creator_elated_side_area_icon_spacing_styles')) {

	function creator_elated_side_area_icon_spacing_styles()
	{
		$icon_spacing = array();

		if (creator_elated_options()->getOptionValue('side_area_icon_padding_left') !== '') {
			$icon_spacing['padding-left'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_padding_left')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_icon_padding_right') !== '') {
			$icon_spacing['padding-right'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_padding_right')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_icon_margin_left') !== '') {
			$icon_spacing['margin-left'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_margin_left')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_icon_margin_right') !== '') {
			$icon_spacing['margin-right'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_margin_right')) . 'px';
		}

		if (!empty($icon_spacing)) {

			echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener', $icon_spacing);

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_icon_spacing_styles');
}

if (!function_exists('creator_elated_side_area_icon_border_styles')) {

	function creator_elated_side_area_icon_border_styles()
	{
		if (creator_elated_options()->getOptionValue('side_area_icon_border_yesno') == 'yes') {

			$side_area_icon_border = array();

			if (creator_elated_options()->getOptionValue('side_area_icon_border_color') !== '') {
				$side_area_icon_border['border-color'] = creator_elated_options()->getOptionValue('side_area_icon_border_color');
			}

			if (creator_elated_options()->getOptionValue('side_area_icon_border_width') !== '') {
				$side_area_icon_border['border-width'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_border_width')) . 'px';
			} else {
				$side_area_icon_border['border-width'] = '1px';
			}

			if (creator_elated_options()->getOptionValue('side_area_icon_border_radius') !== '') {
				$side_area_icon_border['border-radius'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_icon_border_radius')) . 'px';
			}

			if (creator_elated_options()->getOptionValue('side_area_icon_border_style') !== '') {
				$side_area_icon_border['border-style'] = creator_elated_options()->getOptionValue('side_area_icon_border_style');
			} else {
				$side_area_icon_border['border-style'] = 'solid';
			}

			if (!empty($side_area_icon_border)) {
				$side_area_icon_border['-webkit-transition'] = 'all 0.15s ease-out';
				$side_area_icon_border['transition'] = 'all 0.15s ease-out';
				echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener', $side_area_icon_border);
			}

			if (creator_elated_options()->getOptionValue('side_area_icon_border_hover_color') !== '') {
				$side_area_icon_border_hover['border-color'] = creator_elated_options()->getOptionValue('side_area_icon_border_hover_color');
                echo creator_elated_dynamic_css('a.eltd-side-menu-button-opener:hover', $side_area_icon_border_hover);
			}


		}
	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_icon_border_styles');

}

if (!function_exists('creator_elated_side_area_alignment')) {

	function creator_elated_side_area_alignment()
	{

		if (creator_elated_options()->getOptionValue('side_area_aligment')) {

			echo creator_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-side-menu, .eltd-side-menu-slide-with-content .eltd-side-menu, .eltd-side-area-uncovered-from-content .eltd-side-menu', array(
				'text-align' => creator_elated_options()->getOptionValue('side_area_aligment')
			));

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_alignment');

}

if (!function_exists('creator_elated_side_area_styles')) {

	function creator_elated_side_area_styles()
	{

		$side_area_styles = array();

		if (creator_elated_options()->getOptionValue('side_area_background_color') !== '') {
			$side_area_styles['background-color'] = creator_elated_options()->getOptionValue('side_area_background_color');
		}

		if (creator_elated_options()->getOptionValue('side_area_padding_top') !== '') {
			$side_area_styles['padding-top'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_padding_top')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_padding_right') !== '') {
			$side_area_styles['padding-right'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_padding_right')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_padding_bottom') !== '') {
			$side_area_styles['padding-bottom'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_padding_bottom')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_padding_left') !== '') {
			$side_area_styles['padding-left'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_padding_left')) . 'px';
		}

		if (!empty($side_area_styles)) {
			echo creator_elated_dynamic_css('.eltd-side-menu, .eltd-side-area-uncovered-from-content .eltd-side-menu, .eltd-side-menu-slide-from-right .eltd-side-menu', $side_area_styles);
		}

		if (creator_elated_options()->getOptionValue('side_area_close_icon') == 'dark') {
			echo creator_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu span, .eltd-side-menu a.eltd-close-side-menu i', array(
				'color' => '#000000'
			));
		}

		if (creator_elated_options()->getOptionValue('side_area_close_icon_size') !== '') {
			echo creator_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu', array(
				'height' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'width' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'line-height' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'padding' => 0,
			));
			echo creator_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu span, .eltd-side-menu a.eltd-close-side-menu i', array(
				'font-size' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'height' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'width' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
				'line-height' => creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_close_icon_size')) . 'px',
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_styles');

}

if (!function_exists('creator_elated_side_area_title_styles')) {

	function creator_elated_side_area_title_styles()
	{

		$title_styles = array();

		if (creator_elated_options()->getOptionValue('side_area_title_color') !== '') {
			$title_styles['color'] = creator_elated_options()->getOptionValue('side_area_title_color');
		}

		if (creator_elated_options()->getOptionValue('side_area_title_fontsize') !== '') {
			$title_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_title_fontsize')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_title_lineheight') !== '') {
			$title_styles['line-height'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_title_lineheight')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_title_texttransform') !== '') {
			$title_styles['text-transform'] = creator_elated_options()->getOptionValue('side_area_title_texttransform');
		}

		if (creator_elated_options()->getOptionValue('side_area_title_google_fonts') !== '-1') {
			$title_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('side_area_title_google_fonts')) . ', sans-serif';
		}

		if (creator_elated_options()->getOptionValue('side_area_title_fontstyle') !== '') {
			$title_styles['font-style'] = creator_elated_options()->getOptionValue('side_area_title_fontstyle');
		}

		if (creator_elated_options()->getOptionValue('side_area_title_fontweight') !== '') {
			$title_styles['font-weight'] = creator_elated_options()->getOptionValue('side_area_title_fontweight');
		}

		if (creator_elated_options()->getOptionValue('side_area_title_letterspacing') !== '') {
			$title_styles['letter-spacing'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_title_letterspacing')) . 'px';
		}

		if (!empty($title_styles)) {

			echo creator_elated_dynamic_css('.eltd-side-menu-title h4, .eltd-side-menu-title h5', $title_styles);

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_title_styles');

}

if (!function_exists('creator_elated_side_area_text_styles')) {

	function creator_elated_side_area_text_styles()
	{
		$text_styles = array();

		if (creator_elated_options()->getOptionValue('side_area_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('side_area_text_google_fonts')) . ', sans-serif';
		}

		if (creator_elated_options()->getOptionValue('side_area_text_fontsize') !== '') {
			$text_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_text_fontsize')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_text_lineheight') !== '') {
			$text_styles['line-height'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_text_lineheight')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('side_area_text_letterspacing')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('side_area_text_fontweight') !== '') {
			$text_styles['font-weight'] = creator_elated_options()->getOptionValue('side_area_text_fontweight');
		}

		if (creator_elated_options()->getOptionValue('side_area_text_fontstyle') !== '') {
			$text_styles['font-style'] = creator_elated_options()->getOptionValue('side_area_text_fontstyle');
		}

		if (creator_elated_options()->getOptionValue('side_area_text_texttransform') !== '') {
			$text_styles['text-transform'] = creator_elated_options()->getOptionValue('side_area_text_texttransform');
		}

		if (creator_elated_options()->getOptionValue('side_area_text_color') !== '') {
			$text_styles['color'] = creator_elated_options()->getOptionValue('side_area_text_color');
		}

		if (!empty($text_styles)) {

			echo creator_elated_dynamic_css('.eltd-side-menu .widget, .eltd-side-menu .widget.widget_search form, .eltd-side-menu .widget.widget_search form input[type="text"], .eltd-side-menu .widget.widget_search form input[type="submit"], .eltd-side-menu .widget h6, .eltd-side-menu .widget h6 a, .eltd-side-menu .widget p, .eltd-side-menu .widget li a, .eltd-side-menu .widget.widget_rss li a.rsswidget, .eltd-side-menu #wp-calendar caption,.eltd-side-menu .widget li, .eltd-side-menu h3, .eltd-side-menu .widget.widget_archive select, .eltd-side-menu .widget.widget_categories select, .eltd-side-menu .widget.widget_text select, .eltd-side-menu .widget.widget_search form input[type="submit"], .eltd-side-menu #wp-calendar th, .eltd-side-menu #wp-calendar td, .eltd-side-menu .eltd_social_icon_holder i.simple_social', $text_styles);

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_text_styles');

}

if (!function_exists('creator_elated_side_area_link_styles')) {

	function creator_elated_side_area_link_styles()
	{
		$link_styles = array();

		if (creator_elated_options()->getOptionValue('sidearea_link_font_family') !== '-1') {
			$link_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('sidearea_link_font_family')) . ',sans-serif';
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_font_size') !== '') {
			$link_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('sidearea_link_font_size')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_line_height') !== '') {
			$link_styles['line-height'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('sidearea_link_line_height')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_letter_spacing') !== '') {
			$link_styles['letter-spacing'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('sidearea_link_letter_spacing')) . 'px';
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_font_weight') !== '') {
			$link_styles['font-weight'] = creator_elated_options()->getOptionValue('sidearea_link_font_weight');
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_font_style') !== '') {
			$link_styles['font-style'] = creator_elated_options()->getOptionValue('sidearea_link_font_style');
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_text_transform') !== '') {
			$link_styles['text-transform'] = creator_elated_options()->getOptionValue('sidearea_link_text_transform');
		}

		if (creator_elated_options()->getOptionValue('sidearea_link_color') !== '') {
			$link_styles['color'] = creator_elated_options()->getOptionValue('sidearea_link_color');
		}

		if (!empty($link_styles)) {

			echo creator_elated_dynamic_css('.eltd-side-menu .widget li a, .eltd-side-menu .widget a:not(.qbutton)', $link_styles);

		}

		if (creator_elated_options()->getOptionValue('sidearea_link_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-side-menu .widget a:hover, .eltd-side-menu .widget.widget_archive li:hover, .eltd-side-menu .widget.widget_categories li:hover,  .eltd-side-menu .widget_rss li a.rsswidget:hover', array(
				'color' => creator_elated_options()->getOptionValue('sidearea_link_hover_color')
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_link_styles');

}

if (!function_exists('creator_elated_side_area_border_styles')) {

	function creator_elated_side_area_border_styles()
	{

		if (creator_elated_options()->getOptionValue('side_area_enable_bottom_border') == 'yes') {

			if (creator_elated_options()->getOptionValue('side_area_bottom_border_color') !== '') {

				echo creator_elated_dynamic_css('.eltd-side-menu .widget', array(
					'border-bottom' => '1px solid ' . creator_elated_options()->getOptionValue('side_area_bottom_border_color'),
					'margin-bottom' => '10px',
					'padding-bottom' => '10px',
				));

			}

		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_side_area_border_styles');

}