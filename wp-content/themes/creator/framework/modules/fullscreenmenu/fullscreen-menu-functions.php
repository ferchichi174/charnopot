<?php

if(!function_exists('creator_elated_register_full_screen_menu_nav')) {
    function creator_elated_register_full_screen_menu_nav() {
	    register_nav_menus(
		    array(
			    'popup-navigation' => esc_html__('Fullscreen Navigation', 'creator')
		    )
	    );
    }

	add_action('after_setup_theme', 'creator_elated_register_full_screen_menu_nav');
}

if ( !function_exists('creator_elated_register_full_screen_menu_sidebars') ) {

	function creator_elated_register_full_screen_menu_sidebars() {

		register_sidebar(array(
			'name' => esc_html__('Fullscreen Menu Top','creator'),
			'id' => 'fullscreen_menu_above',
			'description' => esc_html__('This widget area is rendered above fullscreen menu','creator'),
			'before_widget' => '<div class="%2$s eltd-fullscreen-menu-above-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="eltd-fullscreen-widget-title">',
			'after_title' => '</h4>'
		));

		register_sidebar(array(
			'name' => esc_html__('Fullscreen Menu Bottom', 'creator'),
			'id' => esc_html__('fullscreen_menu_below','creator'),
			'description' => 'This widget area is rendered below fullscreen menu',
			'before_widget' => '<div class="%2$s eltd-fullscreen-menu-below-widget">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="eltd-fullscreen-widget-title">',
			'after_title' => '</h4>'
		));

	}

	add_action('widgets_init', 'creator_elated_register_full_screen_menu_sidebars');

}

if(!function_exists('creator_elated_fullscreen_menu_body_class')) {
	/**
	 * Function that adds body classes for different full screen menu types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function creator_elated_fullscreen_menu_body_class($classes) {

		$classes[] = 'eltd-' . creator_elated_options()->getOptionValue('fullscreen_menu_animation_style');

		return $classes;
	}

	add_filter('body_class', 'creator_elated_fullscreen_menu_body_class');
}

if ( !function_exists('creator_elated_get_full_screen_menu') ) {
	/**
	 * Loads fullscreen menu HTML template
	 */
	function creator_elated_get_full_screen_menu() {

			$parameters = array(
				'fullscreen_menu_in_grid' => creator_elated_options()->getOptionValue('fullscreen_in_grid') === 'yes' ? true : false
			);

			creator_elated_get_module_template_part('templates/fullscreen-menu', 'fullscreenmenu', '', $parameters);

	}

}

if ( !function_exists('creator_elated_get_full_screen_menu_navigation') ) {
	/**
	 * Loads fullscreen menu navigation HTML template
	 */
	function creator_elated_get_full_screen_menu_navigation() {

		creator_elated_get_module_template_part('templates/parts/navigation', 'fullscreenmenu');

	}

}