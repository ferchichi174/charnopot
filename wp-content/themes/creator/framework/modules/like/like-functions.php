<?php

if ( ! function_exists('creator_elated_like') ) {
	/**
	 * Returns CreatorElatedLike instance
	 *
	 * @return CreatorElatedLike
	 */
	function creator_elated_like() {
		return CreatorElatedLike::get_instance();
	}

}

function creator_elated_get_like() {

	return wp_kses(creator_elated_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}

if ( ! function_exists('creator_elated_like_latest_posts') ) {
	/**
	 * Add like to latest post
	 *
	 * @return string
	 */
	function creator_elated_like_latest_posts() {
		return creator_elated_like()->add_like();
	}

}

if ( ! function_exists('creator_elated_like_portfolio_list') ) {
	/**
	 * Add like to portfolio project
	 *
	 * @param $portfolio_project_id
	 * @return string
	 */
	function creator_elated_like_portfolio_list($portfolio_project_id) {
		return creator_elated_like()->add_like_portfolio_list($portfolio_project_id);
	}

}