<div class="eltd-portfolio-info-item">
    <h4 class="eltd-portfolio-item-title">
        <?php the_title(); ?>
    </h4>
    <div class="eltd-portfolio-content">
        <?php the_content(); ?>
    </div>
</div>