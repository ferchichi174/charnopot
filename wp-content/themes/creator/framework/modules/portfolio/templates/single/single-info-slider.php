<div class="eltd-info-slider-holder">
	<?php
	$media = creator_elated_get_portfolio_single_media();

	if(is_array($media) && count($media)) : ?>
		<div class="eltd-portfolio-media eltd-owl-slider">
			<?php foreach($media as $single_media) : ?>
				<div class="eltd-portfolio-single-media">
					<?php creator_elated_portfolio_get_media_html($single_media); ?>
					<div class="eltd-portfolio-info-holder">

						<?php
							creator_elated_portfolio_get_info_part('categories');
							creator_elated_portfolio_get_info_part('content');
						?>
						<div class="eltd-portfolio-info-wrapper">
							<div class="eltd-portfolio-info-inner">
								<?php
								//get portfolio date section
								creator_elated_portfolio_get_info_part('date');

								//get portfolio tags section
								creator_elated_portfolio_get_info_part('tags');

								//get portfolio custom fields section
								creator_elated_portfolio_get_info_part('custom-fields');
								?>
							</div>
							<div class="eltd-ptf-social-holder clearfix">
								<div class="eltd-ptf-social-holder-inner clearfix">
									<?php
										creator_elated_portfolio_get_info_part('social');
										creator_elated_like_portfolio_single(get_the_ID());
									?>
								</div>
							</div>
						</div>
					</div>

				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

</div>