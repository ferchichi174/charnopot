<div class="eltd-portfolio-info-holder">
    <?php
    //get portfolio categories section
    creator_elated_portfolio_get_info_part('categories');

    //get portfolio content section
    creator_elated_portfolio_get_info_part('content');

    //get portfolio date section
    creator_elated_portfolio_get_info_part('date');

    //get portfolio tags section
    creator_elated_portfolio_get_info_part('tags');

    //get portfolio custom fields section
    creator_elated_portfolio_get_info_part('custom-fields');

    //get portfolio share section
    ?>
    <div class="eltd-ptf-social-holder clearfix">
        <?php
            creator_elated_portfolio_get_info_part('social');
            creator_elated_like_portfolio_single(get_the_ID());
        ?>
    </div>
</div>

<div class="eltd-portfolio-single-holder">
    <?php
        echo creator_elated_get_ptf_masonry_layout();
    ?>
</div>