<div class="eltd-two-columns-66-33 clearfix">
	<div class="eltd-column1">
		<div class="eltd-column-inner">
			<?php
			$media = creator_elated_get_portfolio_single_media();

			if(is_array($media) && count($media)) : ?>
				<div class="eltd-portfolio-media eltd-owl-slider">
					<?php foreach($media as $single_media) : ?>
						<div class="eltd-portfolio-single-media">
							<?php creator_elated_portfolio_get_media_html($single_media); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="eltd-column2">
		<div class="eltd-column-inner">
			<div class="eltd-portfolio-info-holder">
				<?php
				//get portfolio categories section
				creator_elated_portfolio_get_info_part('categories');

				//get portfolio content section
				creator_elated_portfolio_get_info_part('content');

				//get portfolio custom fields section
				creator_elated_portfolio_get_info_part('custom-fields');

				//get portfolio date section
				creator_elated_portfolio_get_info_part('date');

				//get portfolio tags section
				creator_elated_portfolio_get_info_part('tags');

				//get portfolio share section
				?>
				<div class="eltd-ptf-social-holder clearfix">
					<?php
						creator_elated_portfolio_get_info_part('social');
						creator_elated_like_portfolio_single(get_the_ID());
					?>
				</div>
			</div>
		</div>
	</div>
</div>