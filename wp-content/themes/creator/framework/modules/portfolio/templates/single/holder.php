<?php if($fullwidth) : ?>
<div class="eltd-full-width">
    <div class="eltd-full-width-inner">
<?php else: ?>
<div class="eltd-container">
    <div class="eltd-container-inner clearfix">
<?php endif; ?>
        <div <?php creator_elated_class_attribute($holder_class); ?>>
            <?php if(post_password_required()) {
                echo get_the_password_form();
            } else {
                //load proper portfolio template
                creator_elated_get_module_template_part('templates/single/single', 'portfolio', $portfolio_template);

                if($portfolio_template !== 'info-slider'){
                    //load portfolio navigation
                    creator_elated_get_module_template_part('templates/single/parts/navigation', 'portfolio');

                    //load portfolio comments
                    creator_elated_get_module_template_part('templates/single/parts/comments', 'portfolio');
                }
            } ?>
        </div>
    </div>
</div>