<?php

if ( ! function_exists('creator_elated_portfolio_options_map') ) {

	function creator_elated_portfolio_options_map() {

		creator_elated_add_admin_page(array(
			'slug'  => '_portfolio',
			'title' => esc_html__('Portfolio','creator'),
			'icon'  => 'fa fa-camera-retro'
		));

		$panel = creator_elated_add_admin_panel(array(
			'title' =>  esc_html__('Portfolio Single','creator'),
			'name'  => 'panel_portfolio_single',
			'page'  => '_portfolio'
		));

		creator_elated_add_admin_field(array(
			'name'        => 'portfolio_single_template',
			'type'        => 'select',
			'label'       =>  esc_html__('Portfolio Type','creator'),
			'default_value'	=> 'small-images',
			'description' =>  esc_html__('Choose a default type for Single Project pages','creator'),
			'parent'      => $panel,
			'options'     => array(
				'small-images' => esc_html__( 'Portfolio small images','creator'),
				'small-slider' => esc_html__( 'Portfolio small slider','creator'),
				'info-slider' => esc_html__( 'Portfolio info slider','creator'),
				'big-images' => esc_html__( 'Portfolio big images','creator'),
				'big-slider' => esc_html__( 'Portfolio big slider','creator'),
				'custom' =>  esc_html__('Portfolio custom','creator'),
				'full-width-custom' => esc_html__( 'Portfolio full width custom','creator'),
				'gallery' =>  esc_html__('Portfolio gallery','creator'),
				'masonry'   =>  esc_html__('Portfolio masonry','creator'),
				'masonry-wide'   =>  esc_html__('Portfolio masonry wide','creator'),
			)
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_padding_meta',
			'type'          => 'text',
			'label'         =>  esc_html__('Padding on Portfolio Single Pages','creator'),
			'description'   =>  esc_html__('Insert padding in format 10px 10px 10px 10px','creator'),
			'parent'        => $panel,
			'default_value' => ''
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_images',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Lightbox for Images','creator'),
			'description'   => esc_html__( 'Enabling this option will turn on lightbox functionality for projects with images.','creator'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_videos',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Lightbox for Videos','creator'),
			'description'   =>  esc_html__('Enabling this option will turn on lightbox functionality for YouTube/Vimeo projects.','creator'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_categories',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Hide Categories','creator'),
			'description'   =>  esc_html__('Enabling this option will disable category meta description on Single Projects.','creator'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_date',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Hide Date','creator'),
			'description'   =>  esc_html__('Enabling this option will disable date meta on Single Projects.','creator'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_comments',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Show Comments','creator'),
			'description'   =>  esc_html__('Enabling this option will show comments on your page.','creator'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_sticky_sidebar',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Sticky Side Text','creator'),
			'description'   =>  esc_html__('Enabling this option will make side text sticky on Single Project pages','creator'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_pagination',
			'type'          => 'yesno',
			'label'         =>  esc_html__('Hide Pagination','creator'),
			'description'   =>  esc_html__('Enabling this option will turn off portfolio pagination functionality.','creator'),
			'parent'        => $panel,
			'default_value' => 'no',
			'args' => array(
				'dependence' => true,
				'dependence_hide_on_yes' => '#eltd_navigate_same_category_container'
			)
		));

		$container_navigate_category = creator_elated_add_admin_container(array(
			'name'            => 'navigate_same_category_container',
			'parent'          => $panel,
			'hidden_property' => 'portfolio_single_hide_pagination',
			'hidden_value'    => 'yes'
		));

		creator_elated_add_admin_field(array(
			'name'            => 'portfolio_single_nav_same_category',
			'type'            => 'yesno',
			'label'           =>  esc_html__('Enable Pagination Through Same Category','creator'),
			'description'     =>  esc_html__('Enabling this option will make portfolio pagination sort through current category.','creator'),
			'parent'          => $container_navigate_category,
			'default_value'   => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'        => 'portfolio_single_numb_columns',
			'type'        => 'select',
			'label'       => esc_html__( 'Number of Columns','creator'),
			'default_value' => 'three-columns',
			'description' =>  esc_html__('Enter the number of columns for Portfolio Gallery type','creator'),
			'parent'      => $panel,
			'options'     => array(
				'two-columns' =>  esc_html__('2 columns','creator'),
				'three-columns' =>  esc_html__('3 columns','creator'),
				'four-columns' => esc_html__( '4 columns','creator')
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'portfolio_single_slug',
			'type'        => 'text',
			'label'       =>  esc_html__('Portfolio Single Slug','creator'),
			'description' =>  esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)','creator'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_portfolio_options_map', 19);

}