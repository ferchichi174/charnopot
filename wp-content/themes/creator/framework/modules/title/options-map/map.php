<?php

if ( ! function_exists('creator_elated_title_options_map') ) {

	function creator_elated_title_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_title_page',
				'title' =>esc_html__( 'Title','creator'),
				'icon' => 'fa fa-list-alt'
			)
		);

		$panel_title = creator_elated_add_admin_panel(
			array(
				'page' => '_title_page',
				'name' => 'panel_title',
				'title' => esc_html__( 'Title Settings','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'show_title_area',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__( 'Show Title Area','creator'),
				'description' => esc_html__( 'This option will enable/disable Title Area','creator'),
				'parent' => $panel_title,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#eltd_show_title_area_container"
				)
			)
		);

		$show_title_area_container = creator_elated_add_admin_container(
			array(
				'parent' => $panel_title,
				'name' => 'show_title_area_container',
				'hidden_property' => 'show_title_area',
				'hidden_value' => 'no'
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_type',
				'type' => 'select',
				'default_value' => 'standard',
				'label' => esc_html__( 'Title Area Type','creator'),
				'description' => esc_html__( 'Choose title type','creator'),
				'parent' => $show_title_area_container,
				'options' => array(
					'standard' =>esc_html__(  'Standard','creator'),
					'breadcrumb' => esc_html__( 'Breadcrumb','creator')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"standard" => "",
						"breadcrumb" => "#eltd_title_area_type_container"
					),
					"show" => array(
						"standard" => "#eltd_title_area_type_container",
						"breadcrumb" => ""
					)
				)
			)
		);

		$title_area_type_container = creator_elated_add_admin_container(
			array(
				'parent' => $show_title_area_container,
				'name' => 'title_area_type_container',
				'hidden_property' => 'title_area_type',
				'hidden_value' => '',
				'hidden_values' => array('breadcrumb'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_enable_breadcrumbs',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__( 'Enable Breadcrumbs','creator'),
				'description' => esc_html__( 'This option will display Breadcrumbs in Title Area','creator'),
				'parent' => $title_area_type_container,
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_animation',
				'type' => 'select',
				'default_value' => 'no',
				'label' => esc_html__( 'Animations','creator'),
				'description' => esc_html__( 'Choose an animation for Title Area','creator'),
				'parent' => $show_title_area_container,
				'options' => array(
					'no' => esc_html__( 'No Animation','creator'),
					'right-left' => esc_html__( 'Text right to left','creator'),
					'left-right' => esc_html__( 'Text left to right','creator')
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_vertial_alignment',
				'type' => 'select',
				'default_value' => 'header_bottom',
				'label' => esc_html__( 'Vertical Alignment','creator'),
				'description' => 'Specify title vertical alignment',
				'parent' => $show_title_area_container,
				'options' => array(
					'header_bottom' => esc_html__( 'From Bottom of Header','creator'),
					'window_top' =>esc_html__(  'From Window Top','creator')
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_size',
				'type' => 'select',
				'default_value' => 'small',
				'label' => esc_html__( 'Title Size','creator'),
				'description' => esc_html__( 'Define title size','creator'),
				'parent' => $show_title_area_container,
				'options' => array(
					'small' =>esc_html__(  'Small','creator'),
					'medium' =>esc_html__(  'Medium','creator'),
					'large' => esc_html__( 'Large','creator')
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_content_alignment',
				'type' => 'select',
				'default_value' => 'left',
				'label' => esc_html__( 'Horizontal Alignment','creator'),
				'description' => esc_html__( 'Specify title horizontal alignment','creator'),
				'parent' => $show_title_area_container,
				'options' => array(
					'left' =>esc_html__(  'Left','creator'),
					'center' => esc_html__( 'Center','creator'),
					'right' => esc_html__( 'Right','creator')
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_background_color',
				'type' => 'color',
				'label' => esc_html__( 'Background Color','creator'),
				'description' => esc_html__( 'Choose a background color for Title Area','creator'),
				'parent' => $show_title_area_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_background_image',
				'type' => 'image',
				'label' => esc_html__( 'Background Image','creator'),
				'description' => esc_html__( 'Choose an Image for Title Area','creator'),
				'parent' => $show_title_area_container
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_background_image_responsive',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__( 'Background Responsive Image','creator'),
				'description' => esc_html__( 'Enabling this option will make Title background image responsive','creator'),
				'parent' => $show_title_area_container,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "#eltd_title_area_background_image_responsive_container",
					"dependence_show_on_yes" => ""
				)
			)
		);

		$title_area_background_image_responsive_container = creator_elated_add_admin_container(
			array(
				'parent' => $show_title_area_container,
				'name' => 'title_area_background_image_responsive_container',
				'hidden_property' => 'title_area_background_image_responsive',
				'hidden_value' => 'yes'
			)
		);

		creator_elated_add_admin_field(
			array(
				'name' => 'title_area_background_image_parallax',
				'type' => 'select',
				'default_value' => 'no',
				'label' => esc_html__( 'Background Image in Parallax','creator'),
				'description' =>esc_html__(  'Enabling this option will make Title background image parallax','creator'),
				'parent' => $title_area_background_image_responsive_container,
				'options' => array(
					'no' =>esc_html__(  'No','creator'),
					'yes' => esc_html__( 'Yes','creator'),
					'yes_zoom' => esc_html__( 'Yes, with zoom out','creator'),
				)
			)
		);

		creator_elated_add_admin_field(array(
			'name' => 'title_area_height',
			'type' => 'text',
			'label' => esc_html__( 'Height','creator'),
			'description' => esc_html__( 'Set a height for Title Area','creator'),
			'parent' => $title_area_background_image_responsive_container,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));


		$panel_typography = creator_elated_add_admin_panel(
			array(
				'page' => '_title_page',
				'name' => 'panel_title_typography',
				'title' =>esc_html__(  'Typography','creator')
			)
		);

		$group_page_title_styles = creator_elated_add_admin_group(array(
			'name'			=> 'group_page_title_styles',
			'title'			=> esc_html__( 'Title','creator'),
			'description'	=> esc_html__( 'Define styles for page title','creator'),
			'parent'		=> $panel_typography
		));

		$row_page_title_styles_1 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_title_styles_1',
			'parent'	=> $group_page_title_styles
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'colorsimple',
			'name'			=> 'page_title_color',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Text Color','creator'),
			'parent'		=> $row_page_title_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_title_fontsize',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Font Size','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_title_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_title_lineheight',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Line Height','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_title_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_title_texttransform',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Text Transform','creator'),
			'options'		=> creator_elated_get_text_transform_array(),
			'parent'		=> $row_page_title_styles_1
		));

		$row_page_title_styles_2 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_title_styles_2',
			'parent'	=> $group_page_title_styles,
			'next'		=> true
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'fontsimple',
			'name'			=> 'page_title_google_fonts',
			'default_value'	=> '-1',
			'label'			=> esc_html__( 'Font Family','creator'),
			'parent'		=> $row_page_title_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_title_fontstyle',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Font Style','creator'),
			'options'		=> creator_elated_get_font_style_array(),
			'parent'		=> $row_page_title_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_title_fontweight',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Weight','creator'),
			'options'		=> creator_elated_get_font_weight_array(),
			'parent'		=> $row_page_title_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_title_letter_spacing',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Letter Spacing','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_title_styles_2
		));

		$group_page_subtitle_styles = creator_elated_add_admin_group(array(
			'name'			=> 'group_page_subtitle_styles',
			'title'			=> esc_html__( 'Subtitle','creator'),
			'description'	=> esc_html__( 'Define styles for page subtitle','creator'),
			'parent'		=> $panel_typography
		));

		$row_page_subtitle_styles_1 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_subtitle_styles_1',
			'parent'	=> $group_page_subtitle_styles
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'colorsimple',
			'name'			=> 'page_subtitle_color',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Text Color','creator'),
			'parent'		=> $row_page_subtitle_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_subtitle_fontsize',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Size','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_subtitle_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_subtitle_lineheight',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Line Height','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_subtitle_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_subtitle_texttransform',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Text Transform','creator'),
			'options'		=> creator_elated_get_text_transform_array(),
			'parent'		=> $row_page_subtitle_styles_1
		));

		$row_page_subtitle_styles_2 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_subtitle_styles_2',
			'parent'	=> $group_page_subtitle_styles,
			'next'		=> true
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'fontsimple',
			'name'			=> 'page_subtitle_google_fonts',
			'default_value'	=> '-1',
			'label'			=> esc_html__( 'Font Family','creator'),
			'parent'		=> $row_page_subtitle_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_subtitle_fontstyle',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Style','creator'),
			'options'		=> creator_elated_get_font_style_array(),
			'parent'		=> $row_page_subtitle_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_subtitle_fontweight',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Weight','creator'),
			'options'		=> creator_elated_get_font_weight_array(),
			'parent'		=> $row_page_subtitle_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_subtitle_letter_spacing',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Letter Spacing','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_subtitle_styles_2
		));

		$group_page_breadcrumbs_styles = creator_elated_add_admin_group(array(
			'name'			=> 'group_page_breadcrumbs_styles',
			'title'			=> esc_html__( 'Breadcrumbs','creator'),
			'description'	=> esc_html__( 'Define styles for page breadcrumbs','creator'),
			'parent'		=> $panel_typography
		));

		$row_page_breadcrumbs_styles_1 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_breadcrumbs_styles_1',
			'parent'	=> $group_page_breadcrumbs_styles
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'colorsimple',
			'name'			=> 'page_breadcrumb_color',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Text Color','creator'),
			'parent'		=> $row_page_breadcrumbs_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_breadcrumb_fontsize',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Font Size','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_breadcrumbs_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_breadcrumb_lineheight',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Line Height','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_breadcrumbs_styles_1
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_breadcrumb_texttransform',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Text Transform','creator'),
			'options'		=> creator_elated_get_text_transform_array(),
			'parent'		=> $row_page_breadcrumbs_styles_1
		));

		$row_page_breadcrumbs_styles_2 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_breadcrumbs_styles_2',
			'parent'	=> $group_page_breadcrumbs_styles,
			'next'		=> true
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'fontsimple',
			'name'			=> 'page_breadcrumb_google_fonts',
			'default_value'	=> '-1',
			'label'			=>esc_html__(  'Font Family','creator'),
			'parent'		=> $row_page_breadcrumbs_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_breadcrumb_fontstyle',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Style','creator'),
			'options'		=> creator_elated_get_font_style_array(),
			'parent'		=> $row_page_breadcrumbs_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'selectblanksimple',
			'name'			=> 'page_breadcrumb_fontweight',
			'default_value'	=> '',
			'label'			=>esc_html__(  'Font Weight','creator'),
			'options'		=> creator_elated_get_font_weight_array(),
			'parent'		=> $row_page_breadcrumbs_styles_2
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'page_breadcrumb_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Letter Spacing','creator'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_page_breadcrumbs_styles_2
		));

		$row_page_breadcrumbs_styles_3 = creator_elated_add_admin_row(array(
			'name'		=> 'row_page_breadcrumbs_styles_3',
			'parent'	=> $group_page_breadcrumbs_styles,
			'next'		=> true
		));

		creator_elated_add_admin_field(array(
			'type'			=> 'colorsimple',
			'name'			=> 'page_breadcrumb_hovercolor',
			'default_value'	=> '',
			'label'			=> esc_html__( 'Hover/Active Color','creator'),
			'parent'		=> $row_page_breadcrumbs_styles_3
		));

	}

	add_action( 'creator_elated_options_map', 'creator_elated_title_options_map', 8);

}