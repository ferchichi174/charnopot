<div class="eltd-product eltd-product-image-on-left">

	<?php if ( has_post_thumbnail() ) { ?>

		<div class="eltd-product-image">
			<div class="eltd-product-image-inner" <?php echo creator_elated_get_inline_style($product_bck_image_style) ?>>
				<a href="<?php the_permalink(); ?>"></a>
			</div>
		</div>

	<?php } ?>

	<div class="eltd-product-content-holder">

		<h5 class="eltd-product-title">
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</h5>

		<div class="eltd-product-content-inner">
			<?php do_action( 'woocommerce_after_shop_loop_item_title' ) ?>
		</div>

	</div>

</div>