<div class="eltd-product eltd-product-standard-type">
	<h3 class="eltd-product-title">
		<a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
		</a>
	</h3>
	<div class="eltd-product-price">
		<?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
	</div>
	<div class="eltd-product-excerpt">
		<?php the_excerpt(); ?>
	</div>
	<?php if(isset($product_subtitle)) { ?>
		<div class="eltd-product-subtitle">
			<?php echo esc_html($product_subtitle) ?>
		</div>
	<?php } ?>
	<div class="eltd-product-add-to-cart-holder">
		<?php
		echo apply_filters( 'woocommerce_loop_add_to_cart_link',
			sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
				esc_url( $product->add_to_cart_url() ),
				esc_attr( isset( $quantity ) ? $quantity : 1 ),
				esc_attr( $product->get_id() ),
				esc_attr( $product->get_sku() ),
				esc_attr( isset( $class ) ? $class : 'button' ),
				esc_html( $product->add_to_cart_text() )
			),
			$product );
		?>
	</div>
</div>