<div <?php creator_elated_class_attribute($holder_classes); ?> <?php echo creator_elated_get_inline_attrs($infobox_data); ?> >
	<div class="eltd-info-box-inner">
		<div class="eltd-ib-front-holder">
			<div class="eltd-ib-front-holder-inner">
				<div class="eltd-ib-top-holder">
					<?php if($show_icon) : ?>
						<div class="eltd-ib-icon-holder">
							<?php echo creator_elated_icon_collections()->renderIcon($icon, $icon_pack, array(
								'icon_attributes' => array(
									'style' => $icon_styles
								)
							)); ?>
						</div>
					<?php endif; ?>

					<?php if(!empty($title)) : ?>
						<div class="eltd-ib-title-holder">
							<h5 class="eltd-ib-title">
								<?php echo esc_html($title); ?>
							</h5>
						</div>
					<?php endif; ?>
				</div>

				<div class="eltd-ib-bottom-holder">
					<?php if(!empty($text)) : ?>
						<div class="eltd-ib-text-holder">
							<p><?php echo esc_html($text); ?></p>
						</div>
					<?php endif; ?>
				</div>
			</div>

		</div>

		<?php if(!(empty($link))){ ?>
			<a  href="<?php echo esc_url($link) ?>" target="<?php echo esc_attr($target) ?>">
		<?php } ?>

		<div class="eltd-ib-overlay" <?php creator_elated_inline_style($holder_styles); ?>></div>

		<?php if(!(empty($link))){ ?>
			</a>
		<?php } ?>

	</div>
</div>