<?php
namespace CreatorElated\Modules\Shortcodes\InfoBox;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class InfoBox implements ShortcodeInterface {
	private $base;

	public function __construct() {
		$this->base = 'eltd_info_box';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map( array(
			'name'                      => esc_html__('Info Box','creator'),
			'base'                      => $this->base,
			'category'                  => 'by ELATED',
			'icon'                      => 'icon-wpb-info-box extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Layout','creator'),
						'param_name'  => 'layout',
						'value'       => array(
							esc_html__('Detailed','creator') => 'detailed',
							esc_html__('Simple','creator')   => 'simple'
						),
						'save_always' => true,
						'admin_label' => true
					)
				),
				creator_elated_icon_collections()->getVCParamsArray( array(
					'element' => 'layout',
					'value'   => array( 'detailed' )
				), '', true ),
				array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Overlay Type','creator'),
						'param_name'  => 'overlay_type',
						'value'       => array(
							esc_html__('Border Overlay','creator') => 'border_overlay',
							esc_html__('Background Overlay','creator')   => 'background_overlay'
						),
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'layout', 'value' => array( 'detailed' ) )
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Overlay Color','creator'),
						'param_name'  => 'overlay_color',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'layout', 'value' => array( 'detailed' ) )
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Title','creator'),
						'param_name'  => 'title',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Title Link','creator'),
						'param_name'  => 'title_link',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'layout', 'value' => array( 'simple' ) )
					),
					array(
						'type'        => 'textarea',
						'heading'     => esc_html__('Text','creator'),
						'param_name'  => 'text',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'layout', 'value' => array( 'detailed' ) )
					),

					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Link','creator'),
						'param_name'  => 'link',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'layout', 'value' => array( 'detailed' ) )
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Target','creator'),
						'param_name'  => 'target',
						'value'       => array(
							esc_html__('Same Window','creator') => '',
							esc_html__('New Window' ,'creator') => '_blank'
						),
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'link', 'not_empty' => true )
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Interactivity','creator'),
						'param_name'  => 'interactivity',
						'value'       => array(
							esc_html__('Yes','creator') => 'yes',
							esc_html__('No','creator')  => 'no'
						),
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'link', 'not_empty' => true ),
						'group'       => esc_html__('Behavior','creator')
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Skin','creator'),
						'param_name'  => 'skin',
						'value'       => array(
							esc_html__('Standard','creator') => '',
							esc_html__('Light','creator')    => 'light'
						),
						'save_always' => true,
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator')
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Background Color','creator'),
						'param_name'  => 'background_color',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator')
					),
					array(
						'type'        => 'attach_image',
						'heading'     =>esc_html__( 'Background Image','creator'),
						'param_name'  => 'background_image',
						'value'       => '',
						'save_always' => true,
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator')
					),
					array(
						'type'        => 'dropdown',
						'heading'     =>esc_html__( 'Box Border','creator'),
						'param_name'  => 'info_box_border',
						'value'       => array(
							esc_html__('Transparent','creator') => 'transparent_border',
							esc_html__('Solid'  ,'creator')   => 'solid_border'
						),
						'save_always' => true,
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator')
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Icon Color','creator'),
						'param_name'  => 'icon_color',
						'value'       => '',
						'save_always' => true,
						'dependency'  => array( 'element' => 'icon_pack', 'not_empty' => true ),
						'group'       =>esc_html__( 'Design Options','creator')
					)
				)
			)
		) );
	}

	public function render( $atts, $content = null ) {
		$defaultAtts = array(
			'layout'                    => 'detailed',
			'background_color'          => '',
			'background_image'          => '',
			'interactivity'             => '',
			'title'                     => '',
			'title_link'                => '',
			'link'               		=> '',
			'target'             		=> '',
			'text'                      => '',
			'icon_color'                => '',
			'skin'                      => '',
			'overlay_type'				=> '',
			'info_box_border'			=> '',
			'overlay_color'				=>''
		);

		$defaultAtts = array_merge( $defaultAtts, creator_elated_icon_collections()->getShortcodeParams() );
		$params      = shortcode_atts( $defaultAtts, $atts );

		$params['holder_styles']  = $this->getHolderStyles( $params );
		$params['link_params']  = $this->getLinkParams( $params );
		$params['holder_classes'] = $this->getHolderClasses( $params );
		$params['infobox_data']   = $this->getInfoBoxDataAttr( $params );

		$iconPackName          = creator_elated_icon_collections()->getIconCollectionParamNameByKey( $params['icon_pack'] );
		$params['icon']        = $iconPackName ? $params[ $iconPackName ] : '';
		$params['show_icon']   = $params['icon'] !== '';
		$params['icon_styles'] = $this->getIconStyles( $params );

		return creator_elated_get_shortcode_module_template_part( 'templates/info-box-template', 'info-box', $params['layout'], $params );
	}

	private function getHolderStyles( $params ) {
		$styles = array();

		if ( $params['background_color'] !== '' ) {
			$styles[] = 'background-color: ' . $params['background_color'];
		} elseif ( $params['background_image'] ) {
			$styles[] = 'background-image: url(' . wp_get_attachment_url( $params['background_image'] ) . ')';
		}

		return $styles;
	}

	/**
	 *
	 * Returns array of info box data attr
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getInfoBoxDataAttr( $params ) {
		$data = array();

		if ( ! empty( $params['overlay_type'] ) ) {
			$data['data-overlay-type'] = $params['overlay_type'];
		}

		if ( ! empty( $params['overlay_color'] ) ) {
			$data['data-overlay-color'] = $params['overlay_color'];
		}

		if ( ! empty( $params['background_color'] ) ) {
			$data['data-background-color'] = $params['background_color'];
		}

		return $data;
	}

	private function getLInkParams( $params ) {
		$linkParams = array();

		if ( ! empty( $params['link'] ) ) {
			$linkParams['link'] = $params['link'];
		}

		if ( ! empty( $params['target'] ) ) {
			$linkParams['target'] = $params['target'];
		}

		return $linkParams;
	}

	private function getHolderClasses( $params ) {
		$classes   = array( 'eltd-info-box-holder' );
		$classes[] = 'eltd-' . $params['layout'];

		if ( $params['interactivity'] !== 'no' ) {
			$classes[] = 'eltd-interactive';
		}

		if ( $params['skin'] !== '' ) {
			$classes[] = 'eltd-' . $params['skin'];
		}

		if ( $params['overlay_type'] !== '' ) {
			$type = $params['overlay_type'];

			switch ($type){
				case 'background_overlay':
					$classes [] .='eltd-info-box-background-overlay';
					break;
				case 'border_overlay':
					$classes [] .='eltd-info-box-border-overlay';
					break;
				default:
					break;
			}
		}

		if ( ! empty( $params['background_image'] ) && empty( $params['background_color'] ) ) {
			$classes[] = 'eltd-info-box-with-image';
		}

		if($params['info_box_border']==='solid_border'){
			$classes[] = 'eltd-info-box-with-border';
		}

		return $classes;
	}

	private function getIconStyles( $params ) {
		$styles = array();

		if ( ! empty( $params['show_icon'] ) && $params['show_icon'] ) {
			if ( ! empty( $params['icon_color'] ) ) {
				$styles[] = 'color: ' . $params['icon_color'];
			}
		}

		return implode( ', ', $styles );
	}

}