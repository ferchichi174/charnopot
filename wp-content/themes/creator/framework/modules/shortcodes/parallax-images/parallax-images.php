<?php
namespace CreatorElated\Modules\Shortcodes\ParallaxImages;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class ParallaxImages implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_parallax_images';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Parallax Images', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-parallax-images extended-custom-icon',
			'category' => 'by ELATED',
			'params' => array(
				array(
					'type' => 'attach_image',
					'heading' => esc_html__('Top Image', 'creator'),
					'param_name' => 'top_image',
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Top Image Link', 'creator'),
					'param_name' => 'top_image_link',
					'description' => esc_html__('Set an external URL to link to.', 'creator'),
				    'dependency' => array('element' => 'top_image', 'not_empty' => true),
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Top Image Link Target', 'creator'),
					'param_name' => 'top_image_link_target',
					'value' => array(
						esc_html__('Same Window', 'creator')    => '_self',
						esc_html__('New Window', 'creator')    => '_blank',
					),
				    'dependency' => array('element' => 'top_image_link', 'not_empty' => true),
					'save_always' => true,
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'attach_image',
					'heading' => esc_html__('Right Image', 'creator'),
					'param_name' => 'right_image',
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Right Image Link', 'creator'),
					'param_name' => 'right_image_link',
					'description' => esc_html__('Set an external URL to link to.',  'creator'),
				    'dependency' => array('element' => 'right_image', 'not_empty' => true),
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Right Image Link Target', 'creator'),
					'param_name' => 'right_image_link_target',
					'value' => array(
						esc_html__('Same Window' , 'creator')   => '_self',
						esc_html__('New Window', 'creator')   => '_blank',
					),
				    'dependency' => array('element' => 'right_image_link', 'not_empty' => true),
					'save_always' => true,
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'attach_image',
					'heading' => esc_html__('Bottom Image', 'creator'),
					'param_name' => 'bottom_image',
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Bottom Image Link', 'creator'),
					'param_name' => 'bottom_image_link',
					'description' => esc_html__('Set an external URL to link to.', 'creator'),
				    'dependency' => array('element' => 'bottom_image', 'not_empty' => true),
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Bottom Image Link Target', 'creator'),
					'param_name' => 'bottom_image_link_target',
					'value' => array(
						esc_html__('Same Window', 'creator')    => '_self',
						esc_html__('New Window', 'creator')    => '_blank',
					),
				    'dependency' => array('element' => 'bottom_image_link', 'not_empty' => true),
					'save_always' => true,
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'attach_image',
					'heading' => esc_html__('Left Image', 'creator'),
					'param_name' => 'left_image',
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Left Image Link', 'creator'),
					'param_name' => 'left_image_link',
					'description' => esc_html__('Set an external URL to link to.', 'creator'),
				    'dependency' => array('element' => 'left_image', 'not_empty' => true),
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Left Image Link Target', 'creator'),
					'param_name' => 'left_image_link_target',
					'value' => array(
						esc_html__('Same Window', 'creator')    => '_self',
						esc_html__('New Window', 'creator')    => '_blank',
					),
				    'dependency' => array('element' => 'left_image_link', 'not_empty' => true),
					'save_always' => true,
					'group' => esc_html__('Images', 'creator')
				),
				array(
					'type' => 'textarea_html',
					'heading' =>esc_html__( 'Content','creator'),
					'param_name' => 'content',
					'value' => '<h2>Awesome Portfolio</h2><p>Awesome shortcode</p><ul><li>My first item</li></ul>',
					'description' => '',
					'group' => esc_html__('Content', 'creator')
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Enable Image Fade Effect', 'creator'),
					'param_name' => 'image_fade_effect', 
					'value' => array(
						esc_html__('Yes', 'creator')    => 'yes',
						esc_html__('No', 'creator')    => 'no',
					),
					'save_always' => true,
					'admin_label' => true,
					'group' => esc_html__('Behavior', 'creator')
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Enable Image Parallax Hover Effect', 'creator'),
					'param_name' => 'image_parallax_hover_effect',
					'value' => array(
						esc_html__('Yes', 'creator')    => 'yes',
						esc_html__('No', 'creator')    => 'no',
					),
					'save_always' => true,
					'admin_label' => true,
					'group' => esc_html__('Behavior', 'creator')
				),
				array(
					'type' => 'dropdown',
					'heading' => 'Enable Image Parallax Scroll Effect',
					'param_name' => 'image_parallax_scroll_effect',
					'value' => array(
						esc_html__('Yes', 'creator')    => 'yes',
						esc_html__('No', 'creator')    => 'no',
					),
					'save_always' => true,
					'admin_label' => true,
					'group' => esc_html__('Behavior', 'creator')
				)
			)
		));
	}

	public function render($atts, $content = null) {
		$args = array(
			'top_image' => '',
			'top_image_link' => '',
			'top_image_link_target' => '_self',
			'right_image' => '',
			'right_image_link' => '',
			'right_image_link_target' => '_self',
			'bottom_image' => '',
			'bottom_image_link' => '',
			'bottom_image_link_target' => '_self',
			'left_image' => '',
			'left_image_link' => '',
			'left_image_link_target' => '_self',
			'image_fade_effect'	=> 'yes',
			'image_parallax_hover_effect' => 'yes',
			'image_parallax_scroll_effect' => 'yes',
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['image_params'] = $this->getImageParams($params);
		$params['content'] = preg_replace('#^<\/p>|<p>$#', '', $content);

		$html = creator_elated_get_shortcode_module_template_part('templates/parallax-images-template' , 'parallax-images', '', $params);

		return $html;
	}

	/**
	 * Return Parallax Image classes
	 *
	 * @param $params
	 * @return array
	 */
	private function getHolderClasses($params) {

		$holder_classes = array();

		$holder_classes[] = 'eltd-parallax-images';

		if ($params['image_fade_effect'] == 'yes') {
			$holder_classes[] = 'eltd-image-fade-effect';
		}

		if ($params['image_parallax_hover_effect'] == 'yes') {
			$holder_classes[] = 'eltd-image-parallax-hover-effect';
		}

		if ($params['image_parallax_scroll_effect'] == 'yes') {
			$holder_classes[] = 'eltd-image-parallax-scroll-effect';
		}

		return implode(' ', $holder_classes);

	}

    /**
     * Return Images params
     *
     * @param $params
     *
     * @return array
     */
    private function getImageParams($params) {
        $image_ids = array();
        $images_params    = array();
        $images = $params['top_image'].' '.$params['right_image'].' '.$params['bottom_image'].' '.$params['left_image'];
        $i  = 0;

	    if($images !== ''){
		    $image_ids = explode(' ', $images);
	    }

        foreach($image_ids as $id) {

            $image['image_id'] = $id;
            $image_original    = wp_get_attachment_image_src($id, 'full');
            $image['url']      = $image_original[0];
            $image['title']    = get_the_title($id);
            $image['image_link'] = get_post_meta($id, 'attachment_image_link', true);
            $image['image_target'] = get_post_meta($id, 'attachment_image_target', true);

            $image_dimensions = creator_elated_get_image_dimensions($image['url']);
            if(is_array($image_dimensions) && array_key_exists('height', $image_dimensions)) {

                if(!empty($image_dimensions['height']) && $image_dimensions['width']) {
                    $image['height'] = $image_dimensions['height'];
                    $image['width']  = $image_dimensions['width'];
                }
            }

            $images_params[$i] = $image;
            $i++;
        }

        return $images_params;
    }

}
