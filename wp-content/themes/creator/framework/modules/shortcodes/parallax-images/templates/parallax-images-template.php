<div <?php echo creator_elated_get_class_attribute($holder_classes); ?>>
	<div class="eltd-parallax-images-holder-inner">
		<div class="eltd-pi-top-row">
			<div class="eltd-top-img-wrapper">
				<div class="eltd-img-wrapper-inner" 
					<?php if ($image_parallax_scroll_effect == 'yes') {
						echo (
							'data--100-top="transform:translate3d(0,-30%,0)rotate(0.02deg)"
							data-100-top="transform:translate3d(0,0,0)rotate(0.02deg)"
							data-center-top="transform:translate3d(0,5%,0)rotate(0.02deg)"
							data-bottom="transform:translate3d(0,-5%,0)rotate(0.02deg)"
							data-bottom-top="transform:translate3d(0,-5%,0)rotate(0.02deg)"'
						);
					} ?>
					>
					<?php if ($top_image_link != '') { ?> 
						<a href="<?php echo esc_url($top_image_link) ?>" target="<?php echo esc_attr($top_image_link_target) ?>"></a>
					<?php } ?>
					<img src="#" alt="<?php echo esc_attr($image_params[0]['title']); ?>" 
								class="eltd-lazy-image" 
								data-image="<?php echo esc_url($image_params[0]['url']);?>" 
								<?php creator_elated_inline_style(array('width:'.$image_params[0]['width'].'px','height:1px')); ?>
								data-ratio="<?php echo esc_attr($image_params[0]['height']/$image_params[0]['width']); ?>" 
								data-lazy="true" />
				</div>
			</div>
			<div class="eltd-right-img-wrapper">
				<div class="eltd-img-wrapper-inner" 
					<?php if ($image_parallax_scroll_effect == 'yes') {
						echo (
							'data--100-top="transform:translate3d(0,-30%,0)rotate(0.02deg)" 
							data-100-top="transform:translate3d(0,-10%,0)rotate(0.02deg)"
							data-center-top="transform:translate3d(0,-10%,0)rotate(0.02deg)" 
							data-bottom="transform:translate3d(0,30%,0)rotate(0.02deg)"
							data-bottom-top="transform:translate3d(0,50%,0)rotate(0.02deg)"'
						);
					} ?>
					>
					<?php if ($right_image_link != '') { ?> 
						<a href="<?php echo esc_url($right_image_link) ?>" target="<?php echo esc_attr($right_image_link_target) ?>"></a>
					<?php } ?>
					<img src="#" alt="<?php echo esc_attr($image_params[1]['title']); ?>"
								class="eltd-lazy-image" 
								data-image="<?php echo esc_url($image_params[1]['url']);?>" 
								<?php creator_elated_inline_style(array('width:'.$image_params[1]['width'].'px','height:1px')); ?>
								data-ratio="<?php echo esc_attr($image_params[1]['height']/$image_params[1]['width']); ?>" 
								data-lazy="true" />
				</div>
			</div>
		</div>
		<div class="eltd-pi-bottom-row">
			<div class="eltd-bottom-img-wrapper">
				<div class="eltd-img-wrapper-inner" 
						<?php if ($image_parallax_scroll_effect == 'yes') {
							echo (
							'data--100-top="transform:translate3d(0,-50%,0)rotate(0.02deg)" 
							data-100-top="transform:translate3d(0,-20%,0)rotate(0.02deg)"
							data-center-top="transform:translate3d(0,-5%,0)rotate(0.02deg)" 
							data-bottom="transform:translate3d(0,0%,0)rotate(0.02deg)"
							data-bottom-top="transform:translate3d(0,10%,0)rotate(0.02deg)"'
							);
						} ?>
					>
					<?php if ($bottom_image_link != '') { ?> 
						<a href="<?php echo esc_url($bottom_image_link) ?>" target="<?php echo esc_attr($bottom_image_link_target) ?>"></a>
					<?php } ?>
					<img src="#" alt="<?php echo esc_attr($image_params[2]['title']); ?>" 
								class="eltd-lazy-image" 
								data-image="<?php echo esc_url($image_params[2]['url']);?>" 
								<?php creator_elated_inline_style(array('width:'.$image_params[2]['width'].'px','height:2px')); ?>
								data-ratio="<?php echo esc_attr($image_params[2]['height']/$image_params[2]['width']); ?>" 
								data-lazy="true" />
				</div>
			</div>
			<div class="eltd-left-img-wrapper">
				<div class="eltd-img-wrapper-inner" 
					<?php if ($image_parallax_scroll_effect == 'yes') {
						echo (
						'data--100-top="transform:translate3d(0,-30%,0)rotate(0.02deg)" 
						data-100-top="transform:translate3d(0,0,0)rotate(0.02deg)"
						data-center-top="transform:translate3d(0,20%,0)rotate(0.02deg)" 
						data-bottom="transform:translate3d(0,0%,0)rotate(0.02deg)"
						data-bottom-top="transform:translate3d(0,-10%,0)rotate(0.02deg)"'
							);
						} ?>
					>
					<?php if ($left_image_link != '') { ?> 
						<a href="<?php echo esc_url($left_image_link) ?>" target="<?php echo esc_attr($left_image_link_target) ?>"></a>
					<?php } ?>
					<img src="#" alt="<?php echo esc_attr($image_params[3]['title']); ?>" 
								class="eltd-lazy-image" 
								data-image="<?php echo esc_url($image_params[3]['url']);?>" 
								<?php creator_elated_inline_style(array('width:'.$image_params[3]['width'].'px','height:3px')); ?>
								data-ratio="<?php echo esc_attr($image_params[3]['height']/$image_params[3]['width']); ?>" 
								data-lazy="true" />
				</div>
			</div>
		</div>
	</div>
	<div class="eltd-parallax-images-content" 
		<?php if ($image_parallax_scroll_effect == 'yes') {
			echo (
				'data-100-top="transform:translate3d(0,-10%,0)rotate(0.02deg)" 
				data-bottom="transform:translate3d(0,10%,0)rotate(0.02deg)"'
			);
		} ?>
		>
		<?php echo do_shortcode($content); ?>
	</div>
</div>