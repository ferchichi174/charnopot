<li class="eltd-<?php echo esc_html($name) ?>-share">
	<a class="eltd-share-link" href="#" onclick="<?php echo creator_elated_get_module_part( $link ); ?>">
		<?php if ($custom_icon !== '') { ?>
			<img src="<?php echo esc_url($custom_icon); ?>" alt="<?php echo esc_attr($name); ?>" />
		<?php } else {
			 ?> <span aria-hidden="true" class="eltd-social-share-icon <?php echo esc_html( $label );?> "></span><?php
		} ?>
	</a>
</li>