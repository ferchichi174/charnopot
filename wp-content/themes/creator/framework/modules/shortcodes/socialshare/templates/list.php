<div class="eltd-social-share-holder eltd-list">

	<span class="eltd-social-share-text">
		<?php esc_html_e('Share:', 'creator'); ?>
	</span>

	<ul>
		<?php foreach ($networks as $net) {
			echo creator_elated_get_module_part( $net );
		} ?>
	</ul>

</div>