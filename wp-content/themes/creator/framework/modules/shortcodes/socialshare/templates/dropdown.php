<div class="eltd-social-share-holder eltd-dropdown">
	<a href="javascript:void(0)" target="_self" class="eltd-social-share-dropdown-opener">
		<i class="social_share"></i>
		<span class="eltd-social-share-text">
			<?php esc_html_e('Share', 'creator'); ?>
		</span>
	</a>
	<div class="eltd-social-share-dropdown">
		<ul>
			<?php foreach ($networks as $net) {
				echo creator_elated_get_module_part( $net );
			} ?>
		</ul>
	</div>
</div>