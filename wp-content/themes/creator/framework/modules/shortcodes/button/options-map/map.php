<?php

if(!function_exists('creator_elated_button_map')) {
    function creator_elated_button_map() {
        $panel = creator_elated_add_admin_panel(array(
            'title' =>esc_html__( 'Button','creator'),
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        //Typography options
        creator_elated_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => esc_html__('Typography','creator'),
            'parent' => $panel
        ));

        $typography_group = creator_elated_add_admin_group(array(
            'name' => 'typography_group',
            'title' =>esc_html__('Typography','creator'),
            'description' =>esc_html__( 'Setup typography for all button types','creator'),
            'parent' => $panel
        ));

        $typography_row = creator_elated_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family','creator'),
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform','creator'),
            'options'       => creator_elated_get_text_transform_array()
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style','creator'),
            'options'       => creator_elated_get_font_style_array()
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing','creator'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = creator_elated_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         =>esc_html__( 'Font Weight','creator'),
            'options'       => creator_elated_get_font_weight_array()
        ));

        //Outline type options
        creator_elated_add_admin_section_title(array(
            'name' => 'type_section_title',
            'title' =>esc_html__( 'Types','creator'),
            'parent' => $panel
        ));

        $outline_group = creator_elated_add_admin_group(array(
            'name' => 'outline_group',
            'title' => esc_html__('Outline Type','creator'),
            'description' => esc_html__('Setup outline button type','creator'),
            'parent' => $panel
        ));

        $outline_row = creator_elated_add_admin_row(array(
            'name' => 'outline_row',
            'next' => true,
            'parent' => $outline_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_bg_color',
            'default_value' => '',
            'label'         =>esc_html__( 'Hover Background Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color','creator'),
            'description'   => ''
        ));

        $outline_row2 = creator_elated_add_admin_row(array(
            'name' => 'outline_row2',
            'next' => true,
            'parent' => $outline_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $outline_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color','creator'),
            'description'   => ''
        ));

        //Solid type options
        $solid_group = creator_elated_add_admin_group(array(
            'name' => 'solid_group',
            'title' =>esc_html__( 'Solid Type','creator'),
            'description' =>esc_html__( 'Setup solid button type','creator'),
            'parent' => $panel
        ));

        $solid_row = creator_elated_add_admin_row(array(
            'name' => 'solid_row',
            'next' => true,
            'parent' => $solid_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         =>esc_html__( 'Text Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         =>esc_html__( 'Text Hover Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         =>esc_html__( 'Hover Background Color','creator'),
            'description'   => ''
        ));

        $solid_row2 = creator_elated_add_admin_row(array(
            'name' => 'solid_row2',
            'next' => true,
            'parent' => $solid_group
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color','creator'),
            'description'   => ''
        ));

        creator_elated_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color','creator'),
            'description'   => ''
        ));
    }

    add_action('creator_elated_options_elements_map', 'creator_elated_button_map');
}