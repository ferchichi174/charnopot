<?php
namespace CreatorElated\Modules\Shortcodes\Button;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;


/**
 * Class Button that represents button shortcode
 * @package CreatorElated\Modules\Shortcodes\Button
 */
class Button implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	/**
	 * Sets base attribute and registers shortcode with Visual Composer
	 */
	public function __construct() {
		$this->base = 'eltd_button';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	/**
	 * Returns base attribute
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function vcMap() {
		vc_map( array(
			'name'                      => esc_html__( 'Elated Button', 'creator' ),
			'base'                      => $this->base,
			'category'                  => 'by ELATED',
			'icon'                      => 'icon-wpb-button extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Size','creator' ),
						'param_name'  => 'size',
						'value'       => array(
							esc_html__('Default'     ,'creator' )           => '',
							esc_html__('Small','creator' )                  => 'small',
							esc_html__('Medium','creator' )                 => 'medium',
							esc_html__('Large','creator' )                  => 'large',
							esc_html__('Extra Large','creator' )            => 'huge',
							esc_html__('Extra Large Full Width','creator' ) => 'huge-full-width'
						),
						'save_always' => true,
						'admin_label' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Type','creator' ),
						'param_name'  => 'type',
						'value'       => array(
							esc_html__('Default','creator' )     => '',
							esc_html__('Outline','creator' )     => 'outline',
							esc_html__('Solid','creator' )       => 'solid',
							esc_html__('Transparent','creator' ) => 'transparent'
						),
						'save_always' => true,
						'admin_label' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Text','creator' ),
						'param_name'  => 'text',
						'admin_label' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Link','creator' ),
						'param_name'  => 'link',
						'admin_label' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Link Target','creator' ),
						'param_name'  => 'target',
						'value'       => array(
			esc_html__('Self','creator' )  => '_self',
			esc_html__('Blank','creator' ) => '_blank'
						),
						'save_always' => true,
						'admin_label' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Custom CSS class','creator' ),
						'param_name'  => 'custom_class',
						'admin_label' => true
					)
				),
				creator_elated_icon_collections()->getVCParamsArray( array(), '', true ),
				array(
					array(
						'type'        => 'colorpicker',
						'heading'     =>esc_html__( 'Color','creator' ),
						'param_name'  => 'color',
						'group'       => esc_html__('Design Options','creator' ),
						'admin_label' => true
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Color','creator' ),
						'param_name'  => 'hover_color',
						'group'       =>esc_html__( 'Design Options','creator' ),
						'admin_label' => true
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Background Color','creator' ),
						'param_name'  => 'background_color',
						'admin_label' => true,
						'dependency'  => array( 'element' => 'type', 'value' => array( 'solid' ) ),
						'group'       => esc_html__('Design Options','creator' )
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Background Color','creator' ),
						'param_name'  => 'hover_background_color',
						'admin_label' => true,
						'group'       =>esc_html__( 'Design Options','creator' )
					),
					array(
						'type'        => 'colorpicker',
						'heading'     =>esc_html__( 'Border Color','creator' ),
						'param_name'  => 'border_color',
						'admin_label' => true,
						'group'       =>esc_html__( 'Design Options','creator' )
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__('Hover Border Color','creator' ),
						'param_name'  => 'hover_border_color',
						'admin_label' => true,
						'group'       =>esc_html__( 'Design Options','creator' )
					),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Border Overlay Color','creator' ),
                        'param_name'  => 'border_overlay_color',
                        'admin_label' => true,
                        'dependency'  => array( 'element' => 'type', 'value' => array( 'solid' ) ),
                        'group'       =>esc_html__( 'Design Options','creator' )
                    ),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Font Size (px)','creator' ),
						'param_name'  => 'font_size',
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator' )
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__('Font Weight','creator' ),
						'param_name'  => 'font_weight',
						'value'       => creator_elated_get_font_weight_array( true ),
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator' ),
						'save_always' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Margin','creator' ),
						'param_name'  => 'margin',
						'description' => esc_html__( 'Insert margin in format: 0px 0px 1px 0px', 'creator' ),
						'admin_label' => true,
						'group'       => esc_html__('Design Options','creator' ),
					)
				)
			) //close array_merge
		) );
	}

	/**
	 * Renders HTML for button shortcode
	 *
	 * @param array $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {
		$default_atts = array(
			'size'                   => '',
			'type'                   => '',
			'text'                   => '',
			'link'                   => '',
			'target'                 => '',
			'color'                  => '',
			'hover_color'            => '',
			'background_color'       => '',
			'hover_background_color' => '',
			'border_color'           => '',
			'hover_border_color'     => '',
			'font_size'              => '',
			'font_weight'            => '',
			'margin'                 => '',
			'custom_class'           => '',
			'html_type'              => 'anchor',
			'input_name'             => '',
			'hover_animation'        => '',
            'border_overlay_color'   =>'',
			'custom_attrs'           => array()
		);

		$default_atts = array_merge( $default_atts, creator_elated_icon_collections()->getShortcodeParams() );
		$params       = shortcode_atts( $default_atts, $atts );

		if ( $params['html_type'] !== 'input' ) {
			$iconPackName   = creator_elated_icon_collections()->getIconCollectionParamNameByKey( $params['icon_pack'] );
			$params['icon'] = $iconPackName ? $params[ $iconPackName ] : '';
		}

		$params['size'] = ! empty( $params['size'] ) ? $params['size'] : 'medium';
		$params['type'] = ! empty( $params['type'] ) ? $params['type'] : 'outline';


		$params['link']   = ! empty( $params['link'] ) ? $params['link'] : '#';
		$params['target'] = ! empty( $params['target'] ) ? $params['target'] : '_self';

		//prepare params for template
		$params['button_classes']      = $this->getButtonClasses( $params );
		$params['button_custom_attrs'] = ! empty( $params['custom_attrs'] ) ? $params['custom_attrs'] : array();
		$params['button_styles']       = $this->getButtonStyles( $params );
		$params['button_data']         = $this->getButtonDataAttr( $params );
        $params['overlay_style']       = $this->getButtonOverlayColor($params);

		return creator_elated_get_shortcode_module_template_part( 'templates/' . $params['html_type'], 'button', $params['hover_animation'], $params );
	}

	/**
	 * Returns array of button styles
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getButtonStyles( $params ) {
		$styles = array();

		if ( ! empty( $params['color'] ) ) {
			$styles[] = 'color: ' . $params['color'];
		}

		if ( ! empty( $params['background_color'] ) && $params['type'] !== 'outline' ) {
			$styles[] = 'background-color: ' . $params['background_color'];
		}

		if ( ! empty( $params['border_color'] ) ) {
			$styles[] = 'border-color: ' . $params['border_color'];
		}

		if ( ! empty( $params['font_size'] ) ) {
			$styles[] = 'font-size: ' . creator_elated_filter_px( $params['font_size'] ) . 'px';
		}

		if ( ! empty( $params['font_weight'] ) ) {
			$styles[] = 'font-weight: ' . $params['font_weight'];
		}

		if ( ! empty( $params['margin'] ) ) {
			$styles[] = 'margin: ' . $params['margin'];
		}

        if ( ! empty( $params['border_overlay_color'] ) ) {
            $styles[] = 'border-bottom-color: ' . $params['border_overlay_color'];
        }

		return $styles;
	}

	/**
	 *
	 * Returns array of button data attr
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getButtonDataAttr( $params ) {
		$data = array();

		if ( ! empty( $params['hover_background_color'] ) ) {
			$data['data-hover-bg-color'] = $params['hover_background_color'];
		}

		if ( ! empty( $params['hover_color'] ) ) {
			$data['data-hover-color'] = $params['hover_color'];
		}

		if ( ! empty( $params['hover_color'] ) ) {
			$data['data-hover-color'] = $params['hover_color'];
		}

		if ( ! empty( $params['hover_border_color'] ) ) {
			$data['data-hover-border-color'] = $params['hover_border_color'];
		}


		return $data;
	}


	private function getButtonOverlayColor( $params ){
        $style="";

        if(!empty ($params['border_overlay_color'])){
            $style.='background-color: '.$params['border_overlay_color'];
        }

        return $style;
    }

	/**
	 * Returns array of HTML classes for button
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getButtonClasses( $params ) {
		$buttonClasses = array(
			'eltd-btn',
			'eltd-btn-' . $params['size'],
			'eltd-btn-' . $params['type']
		);

		if ( ! empty( $params['hover_background_color'] ) ) {
			$buttonClasses[] = 'eltd-btn-custom-hover-bg';
		}

		if ( ! empty( $params['hover_border_color'] ) ) {
			$buttonClasses[] = 'eltd-btn-custom-border-hover';
		}

		if ( ! empty( $params['hover_color'] ) ) {
			$buttonClasses[] = 'eltd-btn-custom-hover-color';
		}

		if ( ! empty( $params['icon'] ) ) {
			$buttonClasses[] = 'eltd-btn-icon';
		}

		if ( ! empty( $params['custom_class'] ) ) {
			$buttonClasses[] = $params['custom_class'];
		}

		if ( ! empty( $params['hover_animation'] ) ) {
			$buttonClasses[] = 'eltd-btn-' . $params['hover_animation'];
		}

		return $buttonClasses;
	}
}