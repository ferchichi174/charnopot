<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php creator_elated_inline_style($button_styles); ?> <?php creator_elated_class_attribute($button_classes); ?> <?php echo creator_elated_get_inline_attrs($button_data); ?> <?php echo creator_elated_get_inline_attrs($button_custom_attrs); ?>>
    <span class="eltd-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo creator_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
    <span class="eltd-btn-overlay" <?php creator_elated_inline_style($overlay_style); ?>  ></span>
</a>