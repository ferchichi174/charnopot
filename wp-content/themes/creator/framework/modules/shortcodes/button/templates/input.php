<div class="eltd-btn eltd-btn-solid eltd-btn-input" <?php creator_elated_inline_style($button_styles); ?> <?php creator_elated_class_attribute($button_classes); ?> <?php echo creator_elated_get_inline_attrs($button_data); ?> <?php echo creator_elated_get_inline_attrs($button_custom_attrs); ?>>
	<input type="submit" name="<?php echo esc_attr($input_name); ?>" value="<?php echo esc_attr($text); ?>" />
	<span class="eltd-btn-overlay"></span>
</div>