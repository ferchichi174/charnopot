<?php
namespace CreatorElated\Modules\Shortcodes\SectionTitle;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class SectionTitle implements ShortcodeInterface{

	private $base;

	public function __construct() {
		$this->base = 'eltd_section_title';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see eltd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map(
			array(
				'name' =>esc_html__( 'Section Title','creator'),
				'base' => $this->getBase(),
				'category' => 'by ELATED',
				'icon' => 'icon-wpb-section-title extended-custom-icon',
				'params' => array(
					array(
						'type' => 'textfield',
						'param_name' => 'title',
						'heading' => esc_html__( 'Section Title','creator'),
						'description' => '',
						'admin_label' => true
					),
					array(
						'type' => 'textfield',
						'param_name' => 'subtitle',
						'heading' => esc_html__( 'Section Subtitle','creator'),
						'description' => '',
						'admin_label' => true
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'type_out',
						'heading' => esc_html__( 'Enable Type Out Effect','creator'),
						'value' => array(
								esc_html__( 'No','creator') => 'no',
								esc_html__( 'Yes','creator') => 'yes',
						),
						'save_always' => true,
						'admin_label' => true,
						'group' => esc_html__( 'Advanced Options','creator'),
						'description' => esc_html__( 'If set to Yes, you can enter two more Section Titles to follow the original one in a typing-like effect.','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'title_2',
						'heading' => esc_html__( 'Section Title 2','creator'),
						'admin_label' => true,
						'group' =>esc_html__(  'Advanced Options','creator'),
						'dependency' => array('element' => 'type_out', 'value' => array('yes'))
					),
					array(
						'type' => 'textfield',
						'param_name' => 'title_3',
						'heading' => esc_html__( 'Section Title 3','creator'),
						'admin_label' => true,
						'group' => esc_html__( 'Advanced Options','creator'),
						'dependency' => array('element' => 'title_2', 'not_empty' => true)
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'loop',
						'heading' =>esc_html__(  'Loop Titles','creator'),
						'value' => array(
								esc_html__( 'No','creator') => 'no',
								esc_html__( 'Yes','creator') => 'yes',
						),
						'save_always' => true,
						'admin_label' => true,
						'group' => esc_html__( 'Advanced Options','creator'),
						'dependency' => array('element' => 'type_out', 'value' => array('yes'))
					),

                    array(
                        'type' => 'dropdown',
                        'param_name' => 'display_separator',
                        'heading' => esc_html__( 'Display Separator','creator'),
                        'description' => '',
                        'value' => array(
                            esc_html__( 'Yes','creator') => 'yes',
                            esc_html__( 'No','creator') => 'no'
                        ),
                        'admin_label' => true,
                        'save_always' => true,
                        'group' => esc_html__( 'Design Options','creator')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'separator_color',
                        'heading' => esc_html__( 'Separator Color','creator'),
                        'description' => '',
                        'admin_label' => true,
                        'dependency' => array('element' => 'display_separator', 'value' => array('yes')),
                        'group' => esc_html__( 'Design Options','creator')
                    ),


					array(
						'type' => 'dropdown',
						'param_name' => 'alignment',
						'heading' => esc_html__( 'Alignment','creator'),
						'description' => '',
						'value' => array(
								esc_html__( 'Center','creator') => '',
								esc_html__( 'Left','creator') => 'left',
								esc_html__( 'Right','creator') => 'right'
						),
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'title_font_family',
						'heading' => esc_html__( 'Title Font Family','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'title_font_size',
						'heading' => esc_html__( 'Title Font Size','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'colorpicker',
						'param_name' => 'title_color',
						'heading' => esc_html__( 'Title Color','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'title_font_weight',
						'heading' => esc_html__( 'Title Font Weight','creator'),
						'description' => '',
						'value' => creator_elated_get_font_weight_array(true),
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'title_line_height',
						'heading' =>esc_html__(  'Title Line Height','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'title_text_transform',
						'heading' => esc_html__('Title Text Transform', 'creator'),
						'description' => '',
						'admin_label' => '',
						'value' => creator_elated_get_text_transform_array(true),
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'subtitle_font_family',
						'heading' => esc_html__( 'Subtitle Font Family','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'subtitle_font_size',
						'heading' =>esc_html__(  'Subitle Font Size','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'colorpicker',
						'param_name' => 'subtitle_color',
						'heading' => esc_html__( 'Subitle Color','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'subtitle_font_weight',
						'heading' => esc_html__( 'Subtitle Font Weight','creator'),
						'description' => '',
						'value' => creator_elated_get_font_weight_array(true),
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'textfield',
						'param_name' => 'subtitle_line_height',
						'heading' =>esc_html__(  'Subtitle Line Height','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'subtitle_text_transform',
						'heading' => esc_html__('Subtitle Text Transform', 'creator'),
						'description' => '',
						'admin_label' => '',
						'value' => creator_elated_get_text_transform_array(true),
						'group' => esc_html__( 'Design Options','creator')
					),
					array(
						'type' => 'colorpicker',
						'param_name' => 'type_out_background_color',
						'heading' =>esc_html__(  'Type Out Background Color','creator'),
						'description' => '',
						'admin_label' => true,
						'group' => esc_html__( 'Design Options','creator'),
						'dependency' => array('element' => 'type_out', 'value' => array('yes'))
					),
				)
			)
		);

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'title' => '',
			'subtitle' => '',
			'alignment' => '',
			'title_font_size' => '',
			'title_font_family' => '',
			'title_color'	=> '',
			'title_font_weight' => '',
			'title_line_height' => '',
			'title_text_transform' => '',
			'subtitle_font_family' => '',
			'subtitle_font_size' => '',
			'subtitle_color'	=> '',
			'subtitle_font_weight' => '',
			'subtitle_line_height' => '',
			'subtitle_text_transform' => '',
			'type_out' => '',
			'title_2' => '',
			'title_3' => '',
            'display_separator' => '',
			'type_out_background_color' => '',
			'loop' => '',
            'separator_color'   =>''
		);

		$params = shortcode_atts($args, $atts);
		$params['section_title_holder_style'] = $this->getSectionHolderStyle($params);
		$params['section_title_style'] = $this->getSectionTitleStyle($params);
		$params['section_subtitle_style'] = $this->getSectionSubtitleStyle($params);
		$params['type_out_style'] = $this->getTypeOutStyle($params);
		$params['type_out_data'] = $this->getTypeOutData($params);
        $params['separator_class']= $this->getSeparatorClass($params);
        $params['separator_style'] = $this->getSeparatorStyle($params);

		$html = creator_elated_get_shortcode_module_template_part('templates/section-title', 'sectiontitle', '', $params);

		return $html;

	}

	/**
	 * Section Holder Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getSectionHolderStyle($params) {

		$holder_styles = array();

		$holder_styles[] = ($params['alignment'] !== '') ? 'text-align: ' . $params['alignment'] : 'text-align: center';

		return $holder_styles;

	}

	/**
	 * Title Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getSectionTitleStyle($params) {

		$title_styles = array();
		$title_styles[] = ($params['title_font_family'] !== '') ? 'font-family: '.$params['title_font_family'] : '';
		$title_styles[] = ($params['title_font_size'] !== '') ? 'font-size: ' . creator_elated_filter_px($params['title_font_size']) . 'px' : '';
		$title_styles[] = ($params['title_color'] !== '') ? 'color: ' . $params['title_color'] : '';
		$title_styles[] = ($params['title_font_weight'] !== '') ? 'font-weight: ' . $params['title_font_weight'] : '';
		$title_styles[] = ($params['title_line_height'] !== '') ? 'line-height: ' .creator_elated_filter_px($params['title_line_height']).'px'  : '';
		$title_styles[] = ($params['title_text_transform'] !== '') ? 'text-transform: ' .$params['title_text_transform']  : '';

		return $title_styles;

	}

	/**
	 * Subtitle styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getSectionSubtitleStyle($params) {

		$subtitle_styles = array();

		$subtitle_styles[] = ($params['subtitle_font_family'] !== '') ? 'font-family: '.$params['subtitle_font_family'] : '';
		$subtitle_styles[] = ($params['subtitle_font_size'] !== '') ? 'font-size: ' . creator_elated_filter_px($params['subtitle_font_size']) . 'px' : '';
		$subtitle_styles[] = ($params['subtitle_color'] !== '') ? 'color: ' . $params['subtitle_color'] : '';
		$subtitle_styles[] = ($params['subtitle_font_weight'] !== '') ? 'font-weight: ' . $params['subtitle_font_weight'] : '';
		$subtitle_styles[] = ($params['subtitle_line_height'] !== '') ? 'line-height: ' .creator_elated_filter_px($params['subtitle_line_height']).'px'  : '';
		$subtitle_styles[] = ($params['subtitle_text_transform'] !== '') ? 'text-transform: ' .$params['subtitle_text_transform'] : '';

		return $subtitle_styles;

	}

	/**
	 * Return Type Out style
	 *
	 * @param $params
	 * @return string
	 */
	private function getTypeOutStyle($params) {
		$type_out_style = array();

		if ($params['type_out_background_color'] !== '') {
			$type_out_style[] = 'background-color: '.$params['type_out_background_color'];
		}

		return implode(';', $type_out_style);
	}

	/**
	 * Return Type Out data
	 *
	 * @param $params
	 * @return string
	 */
	private function getTypeOutData($params) {
		$type_out_data = array();

		if (! empty( $params['loop'] )) {
			$type_out_data['data-loop'] = $params['loop'];
		}
		return $type_out_data;
	}

    private function getSeparatorClass($params){
        $class='';

        if($params['display_separator']!=='' && isset($params['display_separator'])){

            switch($params['display_separator']){

                case 'yes':
                    $class = 'eltd-enable-separator';
                    break;
                default:
                    $class = 'eltd-disable-separator';
                    break;
            }

        }
        return $class;
    }


    private function getSeparatorStyle($params){
        $style='';

        if(($params['separator_color']!=='') && (isset($params['separator_color']))){
            $style.='background-color:'.$params['separator_color'];
        }

        return $style;
    }

}