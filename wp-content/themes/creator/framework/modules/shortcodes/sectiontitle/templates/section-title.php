<div class="eltd-section-title-outer-holder" <?php echo creator_elated_inline_style($section_title_holder_style); ?>>
	<div class="eltd-section-title-title-holder">
		<span class="eltd-section-title" <?php echo creator_elated_inline_style($section_title_style); ?>>
			<?php if($type_out !== 'yes') { 
				echo esc_html($title); 
			} else if($type_out == 'yes') { ?>
				<span class="eltd-typed-wrap" <?php echo creator_elated_inline_style($type_out_style);?> <?php echo creator_elated_get_inline_attrs($type_out_data); ?>>
					<span class="eltd-typed">
							<span class="eltd-typed-strings"><?php echo esc_html($title); ?></span>
						<?php if($title_2 != '') { ?>
							<span class="eltd-typed-strings"><?php echo esc_html($title_2); ?></span>
						<?php } if($title_3 != '') { ?>
							<span class="eltd-typed-strings"><?php echo esc_html($title_3); ?></span>
						<?php } ?>
					</span>
				</span>
			<?php } ?>
		</span>
	</div>
    <span class="eltd-title-separator <?php  echo esc_attr($separator_class); ?>" <?php echo creator_elated_inline_style($separator_style); ?>></span>

	<div class="eltd-section-subtitle-holder">
		<span class="eltd-section-subtitle" <?php echo creator_elated_inline_style($section_subtitle_style); ?>>
			<?php echo esc_html($subtitle); ?>
		</span>
	</div>

</div>