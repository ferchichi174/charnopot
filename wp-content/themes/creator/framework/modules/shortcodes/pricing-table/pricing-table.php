<?php
namespace CreatorElated\Modules\Shortcodes\PricingTable;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTable implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_pricing_table';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Pricing Table', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-pricing-table extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'as_child' => array('only' => 'eltd_pricing_tables'),
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' =>  esc_html__('Title', 'creator'),
					'param_name' => 'title',
					'value' =>  esc_html__('Basic Plan', 'creator'),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' =>  esc_html__('Price', 'creator'),
					'param_name' => 'price',
					'description' => esc_html__( 'Default value is 100', 'creator')
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' =>  esc_html__('Currency', 'creator'),
					'param_name' => 'currency',
					'description' => esc_html__( 'Default mark is $', 'creator')
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' =>  esc_html__('Price Period', 'creator'),
					'param_name' => 'price_period',
					'description' =>  esc_html__('Default label is monthly', 'creator')
				),
				array(
					'type' => 'attach_image',
					'param_name' => 'background_image',
					'heading' =>  esc_html__('Price Holder Background Image', 'creator'),
					'admin_label' => true
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__( 'Show Button', 'creator'),
					'param_name' => 'show_button',
					'value' => array(
							esc_html__('Default', 'creator') => '',
							esc_html__('Yes', 'creator') => 'yes',
							esc_html__('No', 'creator') => 'no'
					),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__( 'Button Text', 'creator'),
					'param_name' => 'button_text',
					'dependency' => array('element' => 'show_button',  'value' => 'yes') 
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' =>  esc_html__('Button Link', 'creator'),
					'param_name' => 'link',
					'dependency' => array('element' => 'show_button',  'value' => 'yes')
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' =>  esc_html__('Featured', 'creator'),
					'param_name' => 'featured',
					'value' => array(
						esc_html__('No', 'creator') => 'no',
						esc_html__('Yes', 'creator') => 'yes'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'textarea_html',
					'holder' => 'div',
					'class' => '',
					'heading' =>  esc_html__('Content', 'creator'),
					'param_name' => 'content',
					'value' => '<li>content content content</li><li>content content content</li><li>content content content</li>',
					'description' => ''
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'title'         			   => 'Basic Plan',
			'price'         			   => '100',
			'currency'      			   => '$',
			'price_period'  			   => 'per month',
			'featured'        			   => 'no',
			'show_button'				   => 'yes',
			'link'          			   => '',
			'button_text'   			   => 'button',
			'background_image'			   => ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';
		$pricing_table_clasess		= 'eltd-price-table';
		
		if($featured == 'yes') {
			$pricing_table_clasess .= ' eltd-featured';
		}
		
		$params['pricing_table_classes'] = $pricing_table_clasess;
        $params['content'] = preg_replace('#^<\/p>|<p>$#', '', $content);
		$params['button_params'] = $this->getButtonParams($params);
		$params['price_holder_style']= $this->getPriceHolderStyle($params);

		$html .= creator_elated_get_shortcode_module_template_part('templates/pricing-table-template','pricing-table', '', $params);
		return $html;

	}

	private function getButtonParams($params){

		$button_params = array(
			'link' => $params['link'],
			'text' => $params['button_text'],
			'type' => 'solid'
		);




		return $button_params;

	}

	private function getPriceHolderStyle($params){
		$background_image_style = '';
		$background_image = wp_get_attachment_image_src( $params['background_image'], 'full' );
		if($background_image && $background_image !==''){
			$background_image_style = 'background-image: url('.$background_image[0].')';
		}
		return $background_image_style;

	}

}
