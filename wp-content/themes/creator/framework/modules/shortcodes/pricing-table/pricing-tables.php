<?php
namespace CreatorElated\Modules\Shortcodes\PricingTables;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTables implements ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'eltd_pricing_tables';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {

		vc_map( array(
			'name'                    => esc_html__( 'Elated Pricing Tables', 'creator' ),
			'base'                    => $this->base,
			'as_parent'               => array( 'only' => 'eltd_pricing_table' ),
			'content_element'         => true,
			'category'                => 'by ELATED',
			'icon'                    => 'icon-wpb-pricing-tables extended-custom-icon',
			'show_settings_on_create' => true,
			'params'                  => array(
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Enlarge featured','creator' ),
					'param_name'  => 'enlarge_featured',
					'value'       => array(
						esc_html__( 'No' ,'creator' ) => 'no',
						esc_html__( 'Yes','creator' ) => 'yes'
					),
					'save_always' => true,
					'admin_label' => true
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Enable Appear Effect','creator' ),
					'param_name'  => 'appear',
					'value'       => array(
						esc_html__( 'Yes','creator' ) => 'yes',
						esc_html__( 'No' ,'creator' ) => 'no'
					),
					'save_always' => true,
					'admin_label' => true
				)

			),
			'js_view' => 'VcColumnView'
		) );
	}

	public function render( $atts, $content = null ) {
		$args = array(
			'enlarge_featured' 	=> 'no',
			'appear'			=> ''
		);

		$params = shortcode_atts( $args, $atts );
		extract( $params );

		$class =  array();
		if ( $enlarge_featured == 'yes' ) {
			$class[] = 'eltd-bigger-featured';
		}
		else {
			$class[] = 'eltd-standard-featured';
		}

		if ( $appear == 'yes'){
			$class[] = 'eltd-pricing-table-appear';
		}

		$classes = implode(' ', $class);

		$html = '<div class="eltd-pricing-tables ' . $classes . '">';
		$html .= do_shortcode( $content );
		$html .= '</div>';

		return $html;
	}

}