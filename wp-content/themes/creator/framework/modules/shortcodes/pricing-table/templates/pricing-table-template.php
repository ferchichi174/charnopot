<div <?php creator_elated_class_attribute($pricing_table_classes)?>>
	<span aria-hidden="true" class="icon_star_alt eltd-pricing-star"></span>
	<div class="eltd-price-table-inner">
		<ul>
			<li class="eltd-table-price" <?php echo creator_elated_get_inline_style($price_holder_style) ?> >
				<div class="eltd-price-holder">
					<sup class="eltd-value"><?php echo esc_attr($currency) ?></sup>
					<span class="eltd-price"><?php echo esc_attr($price)?></span>
				</div>
				<div>
					<p class="eltd-mark"><?php echo esc_attr($price_period)?></p>
				</div>
			</li>
			<li class="eltd-table-title">
				<h6><?php echo esc_html($title) ?></h6>
			</li>
			<li class="eltd-table-content">
				<?php
					echo do_shortcode($content);
				?>
			</li>
			<?php
			if($show_button == "yes" && $button_text !== ''){
				
				?>
				<li class="eltd-table-button">
					<?php echo creator_elated_get_button_html($button_params); ?>
				</li>				
			<?php } ?>
		</ul>
	</div>
</div>