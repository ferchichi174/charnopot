<?php

if ( ! function_exists('creator_elated_pricing_table_options_map') ) {
	/**
	 * Add Pricing Table options to elements page
	 */
	function creator_elated_pricing_table_options_map() {

		$panel_pricing_table = creator_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_pricing_table',
				'title' => esc_html__('Pricing Table','creator')
			)
		);

	}

	add_action( 'creator_elated_options_elements_map', 'creator_elated_pricing_table_options_map');

}