<div class="eltd-blog-carousel-item clearfix <?php echo esc_attr($post_class) ?>">
	<a  class="eltd-blog-carousel-item-overlay"  href="<?php echo esc_url(get_the_permalink()) ?>"></a>

	<div class="eltd-blog-carousel-image-holder">
		<?php echo creator_elated_kses_img($post_image);?>
	</div>

	<div class="eltd-blog-carousel-content-holder">

		<h2 class="eltd-item-title">
			<a href="<?php echo esc_url(get_permalink()) ?>" >
				<?php echo esc_attr(get_the_title()) ?>
			</a>
		</h2>

		<?php if ($text_length != '0') {

			$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
			<p class="eltd-excerpt">
				<?php echo esc_html($excerpt)?>...
			</p>

		<?php } ?>

	</div>

</div>
