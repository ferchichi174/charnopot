<div class="eltd-blog-carousel-item clearfix">

	<div class="eltd-item-info-section eltd-large-info-section">
		<?php creator_elated_post_info(array(
			'date' => 'yes',
			'category' => 'yes'
		)) ?>
	</div>
	
	<h4 class="eltd-item-title">
		<a href="<?php echo esc_url(get_permalink()) ?>" >
			<?php echo esc_attr(get_the_title()) ?>
		</a>
	</h4>

	<?php if ($text_length != '0') {

		$excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
		<p class="eltd-excerpt">
			<?php echo esc_html($excerpt)?>...
		</p>

	<?php } ?>

	<div class="eltd-item-info-section eltd-small-info-section">
		<?php creator_elated_post_info(array(
			'comments' => 'yes'
		)) ?>
	</div>

</div>
