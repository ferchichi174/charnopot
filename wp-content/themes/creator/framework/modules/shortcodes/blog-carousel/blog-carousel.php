<?php

namespace CreatorElated\Modules\Shortcodes\BlogCarousel;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class BlogCarousel
 */
class BlogCarousel implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'eltd_blog_carousel';

		add_action('vc_before_init', array($this,'vcMap'));

		//Category filter
		add_filter( 'vc_autocomplete_eltd_blog_carousel_category_callback', array( &$this, 'blogListCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

		//Category render
		add_filter( 'vc_autocomplete_eltd_blog_carousel_category_render', array( &$this, 'blogListCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array

		//post filter
		add_filter( 'vc_autocomplete_eltd_blog_carousel_selected_posts_callback', array( &$this, 'blogPostsAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

		//post render
		add_filter( 'vc_autocomplete_eltd_blog_carousel_selected_posts_render', array( &$this, 'blogPostsAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array
	}

	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Elated Blog Carousel', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-blog-list-carousel extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' =>esc_html__( 'Carousel Type', 'creator'),
					'param_name' => 'type',
					'value' => array(
						esc_html__('Cards', 'creator') => 'cards',
						esc_html__('Variable Image Size', 'creator') => 'variable_image_size'
					),
					'description' => '',
					'save_always' => true
				),
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Number of Posts', 'creator'),
					'param_name' => 'number_of_posts',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' =>esc_html__( 'Number of Columns', 'creator'),
					'param_name' => 'number_of_columns',
					'value' => array(
							esc_html__('Two', 'creator') => '2',
							esc_html__('Three', 'creator') => '3',
							esc_html__('Four', 'creator') => '4'
					),
					'description' => '',
					'dependency' => array(
						'element' => 'type',
						'value' => array('cards')
					),
					'save_always' => true
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' =>esc_html__( 'Order By', 'creator'),
					'param_name' => 'order_by',
					'value' => array(
							esc_html__('Title', 'creator') => 'title',
							esc_html__('Date', 'creator') => 'date',
							esc_html__('ID', 'creator') => 'ID'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Order', 'creator'),
					'param_name' => 'order',
					'value' => array(
							esc_html__('ASC', 'creator') => 'ASC',
							esc_html__('DESC', 'creator') => 'DESC'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'autocomplete',
					'heading' => esc_html__('Category Slug', 'creator'),
					'param_name' => 'category',
					'description' =>esc_html__( 'Leave empty for all or use comma for list', 'creator'),
					'admin_label' => true
				),
				array(
					'type' => 'autocomplete',
					'heading' => esc_html__('Selected Posts', 'creator'),
					'param_name' => 'selected_posts',
					'settings'    => array(
						'multiple'      => true,
						'sortable'      => true,
						'unique_values' => true,
						// In UI show results except selected. NB! You should manually check values in backend
					),
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Enable Pagination', 'creator'),
					'param_name' => 'enable_pagination',
					'value' => array(
							esc_html__('No', 'creator') => 'no',
							esc_html__('Yes', 'creator') => 'yes'
					),
					'description' => '',
					'save_always' => true
				),
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'class' => '',
					'heading' =>esc_html__( 'Text length', 'creator'),
					'param_name' => 'text_length',
					'description' =>esc_html__( 'Number of characters', 'creator')
				)
			)
		) );

	}
	public function render($atts, $content = null) {

		$default_atts = array(
			'type' => '',
			'number_of_posts' => '-1',
			'number_of_columns' => 4,
			'order_by' => '',
			'order' => '',
			'category' => '',
			'selected_posts' => '',
			'enable_pagination' => '',
			'text_length' => '90'
		);

		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		$holder_classes = $this->getHolderClass($params);
		$queryArray = $this->generateBlogQueryArray($params);
		$query_result = new \WP_Query($queryArray);

		$html = '<div class="eltd-blog-carousel-holder '.esc_attr($holder_classes).'">';
		$html .= '<div class="eltd-blog-carousel-wrapper">';
		$post_count = 0;

		//if is set number_of_posts, compare with that value, in other cases take all posts
		$max_items = $query_result->found_posts;
		if($number_of_posts !== '' && $number_of_posts !== '-1'){
			$max_items = $number_of_posts;
		}

		if($query_result->have_posts()){


			switch($type){

				case 'cards':
					$html .= '<div class = "eltd-blog-carousel">';
					while($query_result->have_posts()){

						$query_result->the_post();
						$post_count++;

						if($post_count <= $max_items){

							$html .= creator_elated_get_shortcode_module_template_part('templates/blog-cards-item', 'blog-carousel', '', $params);

							if( $post_count % $number_of_columns === 0){

								if($post_count < $max_items){
									$html .= '</div>';
									$html .= '<div class = "eltd-blog-carousel">';
								}

							}
						}

					}

					break;
				case 'variable_image_size':
					while($query_result->have_posts()){
						$query_result->the_post();
						$params['post_image'] = $this->getPostImage(get_the_ID(), $type);
						$params['post_class'] = $this->getPostClass(get_the_ID(), $type);
						$html .= creator_elated_get_shortcode_module_template_part('templates/blog-var-img-size-item', 'blog-carousel', '', $params);
					}


					break;
			}

			wp_reset_postdata();
		}else{
			$html .= '<p>'. esc_html_e( 'Sorry, no posts matched your criteria.', 'creator' ) .'</p>';
		}
		$html .= '</div>'; //close eltd-blog-carousel-holder
		$html .= '</div>'; //close eltd-blog-carousel-wrapper
		$html .= '</div>'; //close eltd-blog-carousel
		return $html;

	}

	/**
	 * Generates query array
	 *
	 * @param $params
	 *
	 * @return array
	 */
	public function generateBlogQueryArray($params){

		$queryArray = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'category_name' => $params['category']
		);
		if ( ! empty( $params['selected_posts'] ) ) {
			$post_ids             = explode( ',', $params['selected_posts'] );
			$queryArray['post__in'] = $post_ids;
		}
		return $queryArray;
	}

	/**
	 * Get post featured image in landscape od portrait size
	 *
	 * @param $get_the_ID
	 */
	private function getPostImage($id, $type) {

		if ( $type == 'variable_image_size' ) {
			$orientation = get_post_meta($id, 'eltd_post_featured_blog_slider', true);
			$size = $orientation == 'portrait' ? 'creator_elated_blog_slider_portrait' : 'creator_elated_blog_slider_landscape';
			$image = get_the_post_thumbnail($id, $size);
		} else {
			$image = get_the_post_thumbnail($id, 'full');
		}

		return $image;

	}


	/**
	 * Get post featured image in landscape od portrait size
	 *
	 * @param $get_the_ID
	 */
	private function getPostClass($id, $type) {
		$class = '';
		if ( $type == 'variable_image_size' ) {
			$orientation = get_post_meta($id, 'eltd_post_featured_blog_slider', true);
			$class = $orientation == 'portrait' ? 'portrait' : 'landscape';
		}

		return $class;

	}

	/**
	 * Generates column classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClass($params){

		$columns_number = array();
		$type = $params['type'];
		$enable_pagination=$params['enable_pagination'];
		$columns = $params['number_of_columns'];
		switch($enable_pagination){
			case 'yes':
				$columns_number[] = 'enable-carousel-pagination';
				break;
			case 'no':
				$columns_number[] = 'disable-carousel-pagination';
				break;
			default:
				break;
		}

		switch($type){
			case 'cards':
				$columns_number[] = 'eltd-carousel-cards';

				//get column number class
				switch ($columns) {
					case 2:
						$columns_number[] = 'eltd-two-columns';
						break;
					case 3:
						$columns_number[] = 'eltd-three-columns';
						break;
					case 4:
						$columns_number[] = 'eltd-four-columns';
						break;
				}

				break;
			case 'variable_image_size':
				$columns_number[] = 'eltd-carousel-var-image-size';
				break;
		}

		return implode(' ',$columns_number);
	}

	/**
	 * Filter categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogListCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'creator' ) . ': ' . $value['category_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find categories by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogListCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get category
			$category = get_term_by( 'slug', $query, 'category' );
			if ( is_object( $category ) ) {

				$category_slug = $category->slug;
				$category_title = $category->name;

				$category_title_display = '';
				if ( ! empty( $category_title ) ) {
					$category_title_display = esc_html__( 'Category', 'creator' ) . ': ' . $category_title;
				}

				$data          = array();
				$data['value'] = $category_slug;
				$data['label'] = $category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

	/**
	 * Filter posts
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogPostsAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_id    = (int) $query;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT ID AS id, post_title AS title
					FROM {$wpdb->posts}
					WHERE post_type = 'post' AND ( ID = '%d' OR post_title LIKE '%%%s%%' )", $post_id > 0 ? $post_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['id'];  //Param that will be saved in shortcode
				$data['label'] = esc_html__( 'Id', 'creator' ) . ': ' . $value['id'] . ( ( strlen( $value['title'] ) > 0 ) ? ' - ' . esc_html__( 'Title', 'creator' ) . ': ' . $value['title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find categories by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogPostsAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get portfolio
			$post = get_post( (int) $query );
			if ( ! is_wp_error( $post ) ) {

				$post_id    = $post->ID;
				$post_title = $post->post_title;

				$post_title_display = '';
				if ( ! empty( $post_title ) ) {
					$post_title_display = ' - ' . esc_html__( 'Title', 'creator' ) . ': ' . $post_title;
				}

				$post_id_display = esc_html__( 'Id', 'creator' ) . ': ' . $post_id;

				$data          = array();
				$data['value'] = $post_id;
				$data['label'] = $post_id_display . $post_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

}
