<?php

namespace CreatorElated\Modules\Shortcodes\ProductSlider;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class ProductList
 */
class ProductSlider implements ShortcodeInterface {

    private $base;

    function __construct() {

        $this->base = 'eltd_product_slider';

        add_action( 'vc_before_init', array( $this, 'vcMap' ) );

        //Portfolio category filter
        add_filter( 'vc_autocomplete_eltd_product_slider_category_callback', array(
            &$this,
            'productCategoryAutocompleteSuggester',
        ), 10, 1 ); // Get suggestion(find). Must return an array

        //Portfolio category render
        add_filter( 'vc_autocomplete_eltd_product_slider_category_render', array(
            &$this,
            'productCategoryAutocompleteRender',
        ), 10, 1 ); // Get suggestion(find). Must return an array


    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map( array(
            'name'                      => esc_html__( 'Elated Product Slider', 'creator' ),
            'base'                      => $this->base,
            'category'                  => 'by ELATED',
            'icon'                      => 'icon-wpb-product-slider extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type' => 'textfield',
                    'param_name' => 'products_per_page',
                    'heading' => esc_html__( 'Products per Page','creator' ),
                    'description' => '',
                    'admin_label' => true
                ),
                array(
                    'type' => 'dropdown',
                    'param_name' => 'order_by',
                    'heading' => esc_html__( 'Order by','creator' ),
                    'value' => array(
                            esc_html__( 'Date','creator' ) => 'date',
                            esc_html__( 'Author' ,'creator' )=> 'author',
                            esc_html__( 'Title','creator' ) => 'title'
                    ),
                    'save_always'  => true,
                    'description' => '',
                    'admin_label' => true
                ),
                array(
                    'type' => 'dropdown',
                    'param_name' => 'order',
                    'heading' => esc_html__( 'Order','creator' ),
                    'value' => array(
                            esc_html__( 'Asc','creator' ) => 'asc',
                            esc_html__( 'Desc','creator' ) => 'desc'
                    ),
                    'save_always'  => true,
                    'description' => '',
                    'admin_label' => true
                ),
                array(
                    'type'        => 'autocomplete',
                    'heading'     => esc_html__( 'One-Category Product List','creator' ),
                    'param_name'  => 'category',
                    'admin_label' => true,
                    'description' => ''
                ),
                array(
                    'type' => 'textfield',
                    'param_name' => 'bottom_text',
                    'heading' => esc_html__( 'Bottom Text','creator' ),
                    'description' => '',
                    'admin_label' => true
                ),


            )
        ) );
    }

    public function render( $atts, $content = null ) {

        $args = array(
            'products_per_page' => '-1',
            'order_by'          => '',
            'order'             => '',
            'category'          => '',
            'bottom_text'      =>'Made With Love'
        );

        $params = shortcode_atts( $args, $atts );
        extract($params);
        $args = array(
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'posts_per_page'      => $products_per_page,
            'order_by'            => $order_by,
            'order'               => $order,
            'ignore_sticky_posts' => true,
        );

        if(!empty($category)){
            $args['product_cat'] = $category;
        }

        $query = new \WP_Query( $args );

        $html = '';

        if ( $query->have_posts() ) {
            $html .= '<div class="eltd-product-slider-holder eltd-woocommerce woocommerce clearfix">';
            $html .= '<div class = "eltd-product-slider">';
            while ( $query->have_posts() ) {
                $query->the_post();
                $params['product_slider_style'] = $this->getProductSliderStyle(get_the_ID());

                $html .= creator_elated_get_shortcode_module_template_part( 'templates/product-slider-item', 'product-slider', '', $params);

            }
            $html .= '</div>';
            $html .= '<div class="eltd-bottom-text"><p>'.$params['bottom_text'].'</p></div>';
            $html .= '</div>';
        } else {
            $html .= esc_html__( 'No products found', 'creator' );
        }
        return $html;
    }
    private function getProductSliderStyle($id){
        $holderStyle  = array();
        $image = get_post_meta($id, 'eltd_overlay_type_meta', true);
        if( !$image || $image === ''){
            $image = wp_get_attachment_url( get_post_thumbnail_id($id) );
        }
        $holderStyle [] = "background-image:url('".$image."')";

        return implode(";",$holderStyle);
    }

    public function productCategoryAutocompleteSuggester( $query ) {
        global $wpdb;
        $post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS product_category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'product_cat' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

        $results = array();
        if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
            foreach ( $post_meta_infos as $value ) {
                $data          = array();
                $data['value'] = $value['slug'];
                $data['label'] = ( ( strlen( $value['product_category_title'] ) > 0 ) ? esc_html__( 'Category', 'creator' ) . ': ' . $value['product_category_title'] : '' );
                $results[]     = $data;
            }
        }

        return $results;
    }

    /**
     * Find product by id
     * @since 4.4
     *
     * @param $query
     *
     * @return bool|array
     */
    public function productCategoryAutocompleteRender( $query ) {
        $query = trim( $query['value'] ); // get value from requested
        if ( ! empty( $query ) ) {
            // get product category
            $product_category = get_term_by( 'slug', $query, 'product_cat' );
            if ( is_object( $product_category ) ) {

                $product_category_slug = $product_category->slug;
                $product_category_title = $product_category->name;

                $product_category_title_display = '';
                if ( ! empty( $product_category_title ) ) {
                    $product_category_title_display = esc_html__( 'Category', 'creator' ) . ': ' . $product_category_title;
                }

                $data          = array();
                $data['value'] = $product_category_slug;
                $data['label'] = $product_category_title_display;

                return ! empty( $data ) ? $data : false;
            }

            return false;
        }

        return false;
    }


}