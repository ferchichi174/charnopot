<?php $product = creator_elated_woocommerce_global_product();?>
<div class="eltd-product-slider-item-holder product"  <?php echo creator_elated_get_inline_style($product_slider_style);?> >
    <div class = "eltd-product-slider-caption">
        <div class="eltd-product-slider-title">
            <h2><a href="<?php echo esc_url(get_permalink()) ?>" >
                    <?php echo esc_attr(get_the_title()) ?>
                </a>
            </h2>
        </div>
        <div class="eltd-product-slider-price">
            <?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
        </div>
        <div class="eltd-product-slider-description">
             <p> <?php the_content(); ?> </p>
        </div>
        <div class="eltd-product-slider-add-to-cart">
            <?php echo creator_elated_woocommerce_loop_add_to_cart_link(); ?>
        </div>
    </div>
</div>