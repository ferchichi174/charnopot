<?php

if ( ! function_exists('creator_elated_call_to_action_options_map') ) {
	/**
	 * Add Call to Action options to elements page
	 */
	function creator_elated_call_to_action_options_map() {

		$panel_call_to_action = creator_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_call_to_action',
				'title' =>esc_html__( 'Call To Action','creator')
			)
		);

	}

	add_action( 'creator_elated_options_elements_map', 'creator_elated_call_to_action_options_map');

}