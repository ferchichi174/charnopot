<?php
namespace CreatorElated\Modules\Shortcodes\InteractiveImage;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class InteractiveImage implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_interactive_image';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Elated Interactive Image', 'creator'),
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_interactive_images'),
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-interactive-image extended-custom-icon',
					'show_settings_on_create' => true,
					'params' => array(
						array(
							'type' => 'attach_image',
							'heading' => esc_html__('Image', 'creator'),
							'param_name' => 'image',
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Title', 'creator'),
							'param_name' => 'title',
							'admin_label' => true
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__('Tagline', 'creator'),
							'param_name' => 'tagline',
						),
						array(
							'type' => 'colorpicker',
							'heading' =>esc_html__('Title Color', 'creator'),
							'param_name' => 'title_color',
							'group'	=> esc_html__('Design Options', 'creator')
						),
						array(
							'type' => 'colorpicker',
							'heading' => esc_html__('Tagline Color', 'creator'),
							'param_name' => 'tagline_color',
							'group'	=> esc_html__('Design Options', 'creator')
						),
						array(
							'type' => 'colorpicker',
							'class' => '',
							'heading' => esc_html__('Background Color', 'creator'),
							'param_name' => 'background_color',
							'group'	=> esc_html__('Design Options', 'creator')
						),
						array(
							'type' => 'textfield',
							'heading' =>esc_html__('Link', 'creator'),
							'param_name' => 'link',
							'admin_label' => true
						),
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Target','creator'),
							'param_name' => 'target',
							'value' => array(
								esc_html__('New Window','creator')     => '_blank',
								esc_html__('Same Window','creator')     => '_self',
							),
							'save_always' => true,
	                        'dependency'  => array('element' => 'link', 'not_empty' => true)
						),
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'title'	=> '',
			'tagline' => '',
			'image' => '',
			'link'	=> '',
			'target'	=> '',
			'title_color' => '',
			'tagline_color' => '',
			'background_color' => '',
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['interactive_image_holder_styles'] = $this->getInteractiveImageHolderStyles($params);
		$params['interactive_image_title_styles'] = $this->getInteractiveImageTitleStyles($params);
		$params['interactive_image_tagline_styles'] = $this->getInteractiveImageTaglineStyles($params);
        $params['image'] = $this->getImageParams($params);

		$html = creator_elated_get_shortcode_module_template_part('templates/interactive-image-template', 'interactive-images', '', $params);

		return $html;
	}


	/**
	 * Return Interactive Image Holder Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveImageHolderStyles($params) {

		$interactive_image_holder_styles = array();

		if ($params['background_color'] !== '') {
			$interactive_image_holder_styles[] = 'background-color: ' . $params['background_color'];
		}

		return implode(';', $interactive_image_holder_styles);

	}


	/**
	 * Return Interactive Image Title Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveImageTitleStyles($params) {

		$interactive_image_title_styles = array();

		if ($params['title_color'] !== '') {
			$interactive_image_title_styles[] = 'color: ' . $params['title_color'];
		}

		return implode(';', $interactive_image_title_styles);

	}

	/**
	 * Return Interactive Image Tagline Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveImageTaglineStyles($params) {

		$interactive_image_tagline_styles = array();

		if ($params['tagline_color'] !== '') {
			$interactive_image_tagline_styles[] = 'color: ' . $params['tagline_color'];
		}

		return implode(';', $interactive_image_tagline_styles);

	}


	/**
     * Return image params
     *
     * @param $params
     *
     * @return array
     */
    private function getImageParams($params) {
	    $image_params = array();

	    $image_params['image_id'] = $params['image'];
	    $image_original = wp_get_attachment_image_src($params['image'], 'full');
	    $image_params['url'] = $image_original[0];
	    $image_params['title'] = get_the_title($params['image']);
	    $image_dimensions = creator_elated_get_image_dimensions($image_params['url']);

	    if(is_array($image_dimensions) && array_key_exists('height', $image_dimensions)) {
	        if(!empty($image_dimensions['height']) && $image_dimensions['width']) {
	            $image_params['height'] = $image_dimensions['height'];
	            $image_params['width']  = $image_dimensions['width'];
	        }
	    }

	    return $image_params;
    }
}
