<?php
namespace CreatorElated\Modules\Shortcodes\InteractiveImages;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class InteractiveImages implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_interactive_images';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Interactive Images', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-interactive-images extended-custom-icon',
			'category' => 'by ELATED',
			'as_parent' => array('only' => 'eltd_interactive_image'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'heading' =>esc_html__( 'Columns','creator'),
					'admin_label' => true,
					'save_always' => true,
					'param_name' => 'number_of_columns',
					'value' => array(
						esc_html__('3 Columns','creator')     => 'three-columns',
						esc_html__('4 Columns' ,'creator')    => 'four-columns',
						esc_html__('5 Columns','creator')     => 'five-columns',
					)
				),
				array(
					'type' => 'dropdown',
					'heading' =>esc_html__( 'Tile hover effect','creator'),
					'admin_label' => true,
					'save_always' => true,
					'param_name' => 'tile_effect',
					'value' => array(
						esc_html__('Yes','creator')   => 'yes',
						esc_html__('No' ,'creator')  	=> 'no',
					),
				),
				array(
					'type' => 'dropdown',
					'heading' =>esc_html__( 'Appear effect','creator'),
					'admin_label' => true,
					'save_always' => true,
					'param_name' => 'appear_effect',
					'value' => array(
						esc_html__('Randomize','creator')   => 'randomize',
						esc_html__('One by One','creator')   => 'one_by_one',
						esc_html__('None' ,'creator')  	=> 'none',
					),
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'number_of_columns' => '',
			'appear_effect' 	=> 'randomize',
			'tile_effect' 		=> 'yes',
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html	= '';

		$interactive_images_classes = array();
		$interactive_images_classes[] = 'eltd-interactive-images';

		if($number_of_columns != ''){
			$interactive_images_classes[] .= 'eltd-ii-'.$number_of_columns ;
		}

		if($tile_effect == 'yes'){
			$interactive_images_classes[] .= 'eltd-tile-hover-effect' ;
		}

		if($appear_effect != 'none'){
			$interactive_images_classes[] .= 'eltd-appear-effect';

			if ($appear_effect == 'randomize') {
				$interactive_images_classes[] .= ' eltd-randomize' ;
			} else if ($appear_effect == 'one_by_one') {

				$interactive_images_classes[] .= ' eltd-one-by-one' ;
			}

		}

		$interactive_images_class = implode(' ', $interactive_images_classes);

		$html .= '<div ' . creator_elated_get_class_attribute($interactive_images_class) . ' >';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}

}
