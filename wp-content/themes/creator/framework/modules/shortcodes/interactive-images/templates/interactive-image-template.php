<div class="eltd-interactive-image-holder">
	<div class="eltd-interactive-image">
		<?php if ($link != '') { ?>
			<a href="<?php echo esc_url($link) ?>" target="<?php echo esc_attr($target) ?>"></a>
		<?php } ?>
		<div class="eltd-interactive-image-inner">
			<div class="eltd-ii-front-holder">
                <img 	class="eltd-lazy-image" 
                		src="#" 
                		alt="<?php echo esc_url($image['title']);?>" 
                		data-image="<?php echo esc_url($image['url']);?>" <?php creator_elated_inline_style(array('width:'.$image['width'].'px','height:1px')); ?>
                		data-ratio="<?php echo esc_attr($image['height']/$image['width']); ?>" 
                		data-lazy="true" />
			</div>
			<div class="eltd-ii-back-holder" <?php creator_elated_inline_style($interactive_image_holder_styles); ?>>
				<div class="eltd-ii-back-holder-inner">
					<h3 class="eltd-ii-title" <?php creator_elated_inline_style($interactive_image_title_styles); ?>><?php echo esc_attr($title); ?></h3>
					<p class="eltd-ii-tagline" <?php creator_elated_inline_style($interactive_image_tagline_styles); ?>><?php echo esc_attr($tagline); ?></p>
				</div>
				<div class="eltd-ii-back-holder-image eltd-lazy-image" data-image="<?php echo esc_url($image['url']);?>" data-lazy='true'></div>
			</div>
		</div>
	</div>
</div>