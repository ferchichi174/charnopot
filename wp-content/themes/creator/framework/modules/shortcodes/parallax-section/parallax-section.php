<?php
namespace CreatorElated\Modules\Shortcodes\ParallaxSection;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class ParallaxSection implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_parallax_section';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Parallax Section', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-parallax-section extended-custom-icon',
			'category' => 'by ELATED',
			'show_settings_on_create' => false,
			'as_parent' => array('only' => 'eltd_parallax_section_item'),
			'js_view' => 'VcColumnView',
		));
	}

	public function render($atts, $content = null) {
	
		$html	= '';

		$parallax_section_classes = array();
		$parallax_section_classes[] = 'eltd-parallax-section';

		$parallax_section_class_attribute = implode(' ', $parallax_section_classes);

		$html .= '<div ' . creator_elated_get_class_attribute($parallax_section_classes) . ' >';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}

}
