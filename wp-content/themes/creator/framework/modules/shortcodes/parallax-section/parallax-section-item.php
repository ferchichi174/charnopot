<?php
namespace CreatorElated\Modules\Shortcodes\ParallaxSectionItem;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class ParallaxSectionItem implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_parallax_section_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Elated Parallax Section Item', 'creator'),
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_parallax_section'),
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-parallax-section-item extended-custom-icon',
					'show_settings_on_create' => true,
					'params' => array(
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Layout','creator'),
							'param_name' => 'layout',
							'value' => array(
								esc_html__('Horizontal','creator')     => 'horizontal',
								esc_html__('Vertical','creator')     => 'vertical',
							),
							'save_always' => true,
							'admin_label' => true,
							'group'	=> esc_html__('Layout Options', 'creator')
						),
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Item Position','creator'),
							'param_name' => 'item_position',
							'value' => array(
								esc_html__('Left','creator')     => 'left',
								esc_html__('Right','creator')     => 'right',
							),
							'save_always' => true,
							'admin_label' => true,
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'attach_image',
							'heading' =>esc_html__( 'Narrow Image', 'creator'),
							'param_name' => 'narrow_image',
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'attach_image',
							'heading' =>esc_html__( 'Wide Image', 'creator'),
							'param_name' => 'wide_image',
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'textfield',
							'heading' =>esc_html__( 'Title', 'creator'),
							'param_name' => 'title',
							'admin_label' => true,
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'textfield',
							'heading' =>esc_html__( 'Tagline', 'creator'),
							'param_name' => 'tagline',
							'admin_label' => true,
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Title and Tagline position','creator'),
							'param_name' => 'horizontal_text_position',
							'value' => array(
								esc_html__('Left','creator')     => 'left',
								esc_html__('Center','creator')     => 'center',
								esc_html__('Right','creator')     => 'right'
							),
							'save_always' => true,							
	                        'dependency'  => array('element' => 'layout', 'value' => array('horizontal')),
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Title and Tagline position','creator'),
							'param_name' => 'vertical_text_position',
							'value' => array(
								esc_html__('Top','creator')     => 'top',
								esc_html__('Center','creator')     => 'center',
								esc_html__('Bottom','creator')     => 'bottom'
							),
							'save_always' => true,
	                        'dependency'  => array('element' => 'layout', 'value' => array('vertical')),
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'colorpicker',
							'heading' =>esc_html__( 'Title Color', 'creator'),
							'param_name' => 'title_color',
							'group'	=> esc_html__('Design Options','creator')
						),
						array(
							'type' => 'colorpicker',
							'heading' =>esc_html__( 'Tagline Color', 'creator'),
							'param_name' => 'tagline_color',
							'group'	=> esc_html__('Design Options','creator')
						),
						array(
							'type' => 'textfield',
							'heading' =>esc_html__( 'Link', 'creator'),
							'param_name' => 'link',
							'admin_label' => true,
							'group'	=> esc_html__('Layout Options','creator')
						),
						array(
							'type' => 'dropdown',
							'heading' =>esc_html__( 'Target','creator'),
							'param_name' => 'target',
							'value' => array(
								esc_html__('New Window','creator')     => '_blank',
								esc_html__('Same Window','creator')     => '_self',
							),
							'save_always' => true,
	                        'dependency'  => array('element' => 'link', 'not_empty' => true),
							'group'	=> esc_html__('Layout Options','creator')
						),
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'layout' => '',
			'item_position' => '',
			'narrow_image' => '',
			'wide_image' => '',
			'title'	=> '',
			'tagline' => '',
			'horizontal_text_position' => '',
			'vertical_text_position' => '',
			'link'	=> '',
			'target'	=> '',
			'title_color' => '',
			'tagline_color' => '',
		);
		
		$params = shortcode_atts($args, $atts);
		extract($params);

		$params['psi_holder_classes'] = $this->getParallaxSectionItemHolderClasses($params);
		$params['psi_title_styles'] = $this->getParallaxSectionItemTitleStyles($params);
		$params['psi_tagline_styles'] = $this->getParallaxSectionItemTaglineStyles($params);
		$params['psi_image_params'] = $this->getParallaxSectionItemImageParams($params);
		$params['psi_image_data'] = $this -> getParallaxSectionItemWideImageDataParams($params);

		$html = creator_elated_get_shortcode_module_template_part('templates/parallax-section-item-template', 'parallax-section', '', $params);

		return $html;
	}


	/**
	 * Return Parallax Section Item Holder Classes
	 *
	 * @param $params
	 * @return array
	 */
	private function getParallaxSectionItemHolderClasses($params) {

		$holder_classes = array();

		$holder_classes[] = 'eltd-parallax-section-item';

		if ($params['layout'] == 'horizontal') {
			$holder_classes[] = 'eltd-psi-horizontal';

			if ($params['horizontal_text_position'] !== '') {
				$holder_classes[] = 'eltd-psi-text-'.$params['horizontal_text_position'];
			}
		} 

		if ($params['layout'] == 'vertical') {
			$holder_classes[] = 'eltd-psi-vertical';

			if ($params['vertical_text_position'] !== '') {
				$holder_classes[] = 'eltd-psi-text-'.$params['vertical_text_position'];
			}
		}


		if ($params['item_position'] !== '') { 
			$holder_classes[] = 'eltd-psi-item-'.$params['item_position'];
		}

		return implode(' ', $holder_classes);

	}



	/**
	 * Return Parallax Section Item Wide Image Data Params
	 *
	 * @param $params
	 * @return array
	 */
	private function getParallaxSectionItemWideImageDataParams($params) {

		$data_params = array();
		$narrow_image = $params['psi_image_params'][0];
		$wide_image = $params['psi_image_params'][1];

		if ($params['layout'] == 'vertical') { 
			$data_params['start'] = 'data-100-bottom = transform:translate3d(0,0,0)rotate(0.02deg);';
			$data_params['middle'] = 'data--100-bottom = transform:translate3d(0,-80px,0)rotate(0.02deg);';
			$data_params['end'] = 'data-center-top = transform:translate3d(0,-'.($narrow_image['height'] - $wide_image['height']).'px,0)rotate(0.02deg);';
		}

		if ($params['layout'] == 'horizontal') {
			if ($params['item_position'] == 'left') {
				$data_params['start'] = 'data-100-bottom = transform:translate3d(0,0,0)rotate(0.02deg);';
				$data_params['middle'] = 'data--100-bottom = transform:translate3d(80px,0,0)rotate(0.02deg);';
				$data_params['end'] = 'data-center-top = transform:translate3d('.($narrow_image['width'] - $wide_image['width']).'px,0,0)rotate(0.02deg);';
			}

			if ($params['item_position'] == 'right') {
				$data_params['start'] = 'data-100-bottom = transform:translate3d(0,0,0)rotate(0.02deg);';
				$data_params['middle'] = 'data--100-bottom = transform:translate3d(-80px,0,0)rotate(0.02deg);';
				$data_params['end'] = 'data-center-top = transform:translate3d(-'.($narrow_image['width'] - $wide_image['width']).'px,0,0)rotate(0.02deg);';
			}
		}
		//rotate 0.02deg, fix shake in moz

		return implode(' ', $data_params);
	}

    /**
     * Return Parallax Section Item Image Params
     *
     * @param $params
     *
     * @return array
     */
    private function getParallaxSectionItemImageParams($params) {
        $image_ids = array();
        $images_params    = array();
        $images = $params['narrow_image'].' '.$params['wide_image'];
        $i  = 0;

	    if($images !== ''){
		    $image_ids = explode(' ', $images);
	    }

        foreach($image_ids as $id) {

            $image['image_id'] = $id;
            $image_original    = wp_get_attachment_image_src($id, 'full');
            $image['url']      = $image_original[0];
            $image['title']    = get_the_title($id);
            $image['image_link'] = get_post_meta($id, 'attachment_image_link', true);
            $image['image_target'] = get_post_meta($id, 'attachment_image_target', true);

            $image_dimensions = creator_elated_get_image_dimensions($image['url']);
            if(is_array($image_dimensions) && array_key_exists('height', $image_dimensions)) {

                if(!empty($image_dimensions['height']) && $image_dimensions['width']) {
                    $image['height'] = $image_dimensions['height'];
                    $image['width']  = $image_dimensions['width'];
                }
            }

            $images_params[$i] = $image;
            $i++;
        }

        return $images_params;
    }

	/**
	 * Return Parallax Section Item Title Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getParallaxSectionItemTitleStyles($params) {

		$title_styles = array();

		if ($params['title_color'] !== '') {
			$title_styles[] = 'color: ' . $params['title_color'];
		}

		return implode(';', $title_styles);

	}

	/**
	 * Return Parallax Section Item Tagline Styles
	 *
	 * @param $params
	 * @return array
	 */
	private function getParallaxSectionItemTaglineStyles($params) {

		$tagline_styles = array();

		if ($params['tagline_color'] !== '') {
			$tagline_styles[] = 'color: ' . $params['tagline_color'];
		}

		return implode(';', $tagline_styles);

	}
}
