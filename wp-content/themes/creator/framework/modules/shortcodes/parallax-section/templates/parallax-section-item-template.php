<div <?php echo creator_elated_get_class_attribute($psi_holder_classes); ?>
	data-bottom = 'transform:translateY(20%);'
	data-top = 'transform:translateY(-20%);'
	>
	<div class="eltd-psi-inner">
		<?php if ($title != '') { ?>
		<div class="eltd-psi-text-holder clearfix">
			<div class="eltd-psi-text-holder-inner">
				<h5 class="eltd-psi-title" <?php creator_elated_inline_style($psi_title_styles); ?>><?php echo esc_attr($title); ?></h5>
				<p class="eltd-psi-tagline" <?php creator_elated_inline_style($psi_tagline_styles); ?>><?php echo esc_attr($tagline);?></p>
			</div>
			<div class="eltd-dotted-line-holder">
				<img class="eltd-dotted-line" src="<?php echo esc_url(ELATED_ASSETS_ROOT.'/css/img/dotted-border.png') ?>" alt="<?php _e('dotted line','creator'); ?>" />
			</div>
		</div>
		<?php } ?>
		<div class="eltd-psi-images-holder">
			<?php if ($link != '') { ?>
				<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>"></a>
			<?php } ?>
			<img class="eltd-psi-narrow-image" 
					src="<?php echo esc_url($psi_image_params[0]['url']);?>" 
					alt="<?php echo esc_attr($psi_image_params[0]['title']); ?>" />
			<img class="eltd-psi-wide-image" 
					src="<?php echo esc_url($psi_image_params[1]['url']);?>" 
					alt="<?php echo esc_attr($psi_image_params[0]['title']); ?>" 
					<?php echo esc_attr($psi_image_data); ?> />			
		</div>
	</div>
</div>