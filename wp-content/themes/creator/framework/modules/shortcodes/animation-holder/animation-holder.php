<?php
namespace CreatorElated\Modules\Shortcodes\AnimationHolder;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class AnimationHolder implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_animation_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Animation Holder', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-animation-holder extended-custom-icon',
			'category' => 'by ELATED',
			'as_parent' => array('except' => 'eltd_elements_holder_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' =>esc_html__( 'Animation Name', 'creator'),
					'param_name' => 'animation_name',
					'value' => array(
						esc_html__('No Animation', 'creator')		=> '',
						esc_html__('Grow In', 'creator')			=> 'grow-in',
						esc_html__('Fade In', 'creator')			=> 'fade-in',
						esc_html__('Fade In Up', 'creator')		=> 'fade-in-up',
						esc_html__('Fade In Down', 'creator')		=> 'fade-in-down',
						esc_html__('Fade In Left', 'creator')		=> 'fade-in-left',
						esc_html__('Fade In Right', 'creator')	=> 'fade-in-right'
					),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'class' => '',
					'heading' => esc_html__('Animation Delay (ms)', 'creator'),
					'param_name' => 'animation_delay',
					'value' => '',
					'description' => '',
					'dependency' => array('element' => 'animation_name', 'not_empty' => true)
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'animation_name' => '',
			'animation_delay' => ''
		);

		$params = shortcode_atts($args, $atts);
		extract($params);

		$html	= '';

		$animation_name 	= $this->getAnimationName($params);
		$animation_data 	= $this->getAnimationData($params);

		$html .= '<div ' . creator_elated_get_class_attribute($animation_name) .' '.creator_elated_get_inline_attrs($animation_data). '>';
		$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}

	private function getAnimationName($params) {
		$animation = "";

		if($params['animation_name'] !== '') {
			$animation = 'eltd-animation-holder eltd-animation-'.$params['animation_name'];
		}

		return $animation;
	}

	private function getAnimationData($params) {
		$data = array();

		if ($params['animation_delay'] !== '') {
			$data['data-animation-delay'] = $params['animation_delay'];
		}

		return $data;
	}

}
