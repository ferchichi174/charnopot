<?php

if(!function_exists('creator_elated_tabs_map')) {
	function creator_elated_tabs_map() {

		$panel = creator_elated_add_admin_panel(array(
			'title' => esc_html__('Tabs','creator'),
			'name'  => 'panel_tabs',
			'page'  => '_elements_page'
		));

		//Typography options
		creator_elated_add_admin_section_title(array(
			'name' => 'typography_section_title',
			'title' => esc_html__('Tabs Navigation Typography','creator'),
			'parent' => $panel
		));

		$typography_group = creator_elated_add_admin_group(array(
			'name' => 'typography_group',
			'title' =>esc_html__( 'Tabs Navigation Typography','creator'),
			'description' => esc_html__('Setup typography for tabs navigation','creator'),
			'parent' => $panel
		));

		$typography_row = creator_elated_add_admin_row(array(
			'name' => 'typography_row',
			'next' => true,
			'parent' => $typography_group
		));

		creator_elated_add_admin_field(array(
			'parent'        => $typography_row,
			'type'          => 'fontsimple',
			'name'          => 'tabs_font_family',
			'default_value' => '',
			'label'         => esc_html__('Font Family','creator'),
		));

		creator_elated_add_admin_field(array(
			'parent'        => $typography_row,
			'type'          => 'selectsimple',
			'name'          => 'tabs_text_transform',
			'default_value' => '',
			'label'         => esc_html__('Text Transform','creator'),
			'options'       => creator_elated_get_text_transform_array()
		));

		creator_elated_add_admin_field(array(
			'parent'        => $typography_row,
			'type'          => 'selectsimple',
			'name'          => 'tabs_font_style',
			'default_value' => '',
			'label'         => esc_html__('Font Style','creator'),
			'options'       => creator_elated_get_font_style_array()
		));

		creator_elated_add_admin_field(array(
			'parent'        => $typography_row,
			'type'          => 'textsimple',
			'name'          => 'tabs_letter_spacing',
			'default_value' => '',
			'label'         => esc_html__('Letter Spacing','creator'),
			'args'          => array(
				'suffix' => 'px'
			)
		));

		$typography_row2 = creator_elated_add_admin_row(array(
			'name' => 'typography_row2',
			'next' => true,
			'parent' => $typography_group
		));

		creator_elated_add_admin_field(array(
			'parent'        => $typography_row2,
			'type'          => 'selectsimple',
			'name'          => 'tabs_font_weight',
			'default_value' => '',
			'label'         => esc_html__('Font Weight','creator'),
			'options'       => creator_elated_get_font_weight_array()
		));

		//Initial Tab Color Styles

		creator_elated_add_admin_section_title(array(
			'name' => 'tab_color_section_title',
			'title' => esc_html__('Tab Navigation Color Styles','creator'),
			'parent' => $panel
		));
		$tabs_color_group = creator_elated_add_admin_group(array(
			'name' => 'tabs_color_group',
			'title' => esc_html__('Tab Navigation Color Styles','creator'),
			'description' => esc_html__('Set color styles for tab navigation','creator'),
			'parent' => $panel
		));
		$tabs_color_row = creator_elated_add_admin_row(array(
			'name' => 'tabs_color_row',
			'next' => true,
			'parent' => $tabs_color_group
		));

		creator_elated_add_admin_field(array(
			'parent'        => $tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_color',
			'default_value' => '',
			'label'         => esc_html__('Color','creator')
		));
		creator_elated_add_admin_field(array(
			'parent'        => $tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_back_color',
			'default_value' => '',
			'label'         => esc_html__('Background Color','creator')
		));
		creator_elated_add_admin_field(array(
			'parent'        => $tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_border_color',
			'default_value' => '',
			'label'         =>esc_html__( 'Border Color','creator')
		));

		//Active Tab Color Styles

		$active_tabs_color_group = creator_elated_add_admin_group(array(
			'name' => 'active_tabs_color_group',
			'title' =>esc_html__( 'Active and Hover Navigation Color Styles','creator'),
			'description' => esc_html__('Set color styles for active and hover tabs','creator'),
			'parent' => $panel
		));
		$active_tabs_color_row = creator_elated_add_admin_row(array(
			'name' => 'active_tabs_color_row',
			'next' => true,
			'parent' => $active_tabs_color_group
		));

		creator_elated_add_admin_field(array(
			'parent'        => $active_tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_color_active',
			'default_value' => '',
			'label'         => esc_html__('Color','creator')
		));
		creator_elated_add_admin_field(array(
			'parent'        => $active_tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_back_color_active',
			'default_value' => '',
			'label'         => esc_html__('Background Color','creator')
		));
		creator_elated_add_admin_field(array(
			'parent'        => $active_tabs_color_row,
			'type'          => 'colorsimple',
			'name'          => 'tabs_border_color_active',
			'default_value' => '',
			'label'         => esc_html__('Border Color','creator')
		));

	}

	add_action('creator_elated_options_elements_map', 'creator_elated_tabs_map');
}