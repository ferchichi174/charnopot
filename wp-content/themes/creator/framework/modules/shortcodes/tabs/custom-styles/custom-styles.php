<?php
if(!function_exists('creator_elated_tabs_typography_styles')){
	function creator_elated_tabs_typography_styles(){
		$selector = '.eltd-tabs .eltd-tabs-nav li a';
		$tabs_tipography_array = array();
		$font_family = creator_elated_options()->getOptionValue('tabs_font_family');

		if(creator_elated_is_font_option_valid($font_family)){
			$tabs_tipography_array['font-family'] = creator_elated_get_font_option_val($font_family);
		}

		$text_transform = creator_elated_options()->getOptionValue('tabs_text_transform');
		if(!empty($text_transform)) {
			$tabs_tipography_array['text-transform'] = $text_transform;
		}

		$font_style = creator_elated_options()->getOptionValue('tabs_font_style');
		if(!empty($font_style)) {
			$tabs_tipography_array['font-style'] = $font_style;
		}

		$letter_spacing = creator_elated_options()->getOptionValue('tabs_letter_spacing');
		if($letter_spacing !== '') {
			$tabs_tipography_array['letter-spacing'] = creator_elated_filter_px($letter_spacing).'px';
		}

		$font_weight = creator_elated_options()->getOptionValue('tabs_font_weight');
		if(!empty($font_weight)) {
			$tabs_tipography_array['font-weight'] = $font_weight;
		}
		$tabs_color = creator_elated_options()->getOptionValue('tabs_color');
		if($tabs_color !== '') {
			$tabs_tipography_array['color'] = creator_elated_options()->getOptionValue('tabs_color');
		}

		echo creator_elated_dynamic_css($selector, $tabs_tipography_array);
	}
	add_action('creator_elated_style_dynamic', 'creator_elated_tabs_typography_styles');
}

if(!function_exists('creator_elated_tabs_inital_color_styles')){
	function creator_elated_tabs_inital_color_styles(){
		$selector = array(
			'.eltd-tabs.eltd-horizontal-tab.eltd-color-tabs li a',
			'.eltd-tabs.eltd-vertical-tab.eltd-color-tabs li a'
		);
		$styles = array();

		if(creator_elated_options()->getOptionValue('tabs_back_color')) {
			$styles['background-color'] = creator_elated_options()->getOptionValue('tabs_back_color');
		}
		if(creator_elated_options()->getOptionValue('tabs_border_color')) {
			$styles['border-color'] = creator_elated_options()->getOptionValue('tabs_border_color');
		}

		echo creator_elated_dynamic_css($selector, $styles);
	}
	add_action('creator_elated_style_dynamic', 'creator_elated_tabs_inital_color_styles');
}
if(!function_exists('creator_elated_tabs_active_color_styles')){
	function creator_elated_tabs_active_color_styles(){
		$selector = array(
			'.eltd-tabs.eltd-horizontal-tab.eltd-color-tabs li.ui-state-active a',
			'.eltd-tabs.eltd-horizontal-tab.eltd-color-tabs li.ui-state-hover a',
			'.eltd-tabs.eltd-vertical-tab.eltd-color-tabs li.ui-state-active a',
			'.eltd-tabs.eltd-vertical-tab.eltd-color-tabs li.ui-state-hover a',
			'.eltd-tabs.eltd-horizontal-tab.eltd-transparent-tabs li.ui-state-active a',
			'.eltd-tabs.eltd-horizontal-tab.eltd-transparent-tabs li.ui-state-hover a',
			'.eltd-tabs.eltd-vertical-tab.eltd-transparent-tabs li.ui-state-active a',
			'.eltd-tabs.eltd-vertical-tab.eltd-transparent-tabs li.ui-state-hover a',
		);
		$styles = array();

		if(creator_elated_options()->getOptionValue('tabs_color_active')) {
			$styles['color'] = creator_elated_options()->getOptionValue('tabs_color_active');
		}
		if(creator_elated_options()->getOptionValue('tabs_back_color_active')) {
			$styles['background-color'] = creator_elated_options()->getOptionValue('tabs_back_color_active');
		}
		if(creator_elated_options()->getOptionValue('tabs_border_color_active')) {
			$styles['border-color'] = creator_elated_options()->getOptionValue('tabs_border_color_active');
		}

		echo creator_elated_dynamic_css($selector, $styles);
	}
	add_action('creator_elated_style_dynamic', 'creator_elated_tabs_active_color_styles');
}