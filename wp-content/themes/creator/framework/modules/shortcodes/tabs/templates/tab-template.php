<div class="eltd-tabs <?php echo esc_attr($tab_class); ?> <?php echo esc_attr($tab_style_class); ?> clearfix">
	<ul class="eltd-tabs-nav">
		<?php foreach ($tabs_titles as $tab_title) { ?>
			<li>
				<a href="#tab-<?php echo sanitize_title($tab_title)?>">
					<?php if($tab_title_layout === 'eltd-tab-with-icon' || $tab_title_layout === 'eltd-tab-only-icon') { ?>
						<span class="eltd-icon-frame"></span>
					<?php } ?>

					<?php if($tab_title !== '' && $tab_title_layout !== 'eltd-tab-only-icon') { ?>
						<span class="eltd-tab-text-after-icon">
							<?php echo esc_attr($tab_title)?>
						</span>
					<?php } ?>
				</a>
			 </li>
		<?php } ?>
	</ul> 
	<?php echo do_shortcode($content); ?>
</div>