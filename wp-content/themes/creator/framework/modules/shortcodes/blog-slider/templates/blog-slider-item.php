<div class="eltd-blog-slider-item clearfix">

	<div class="eltd-blog-slider-top-section">

		<?php if ( has_post_thumbnail() ) { ?>
			<div class="eltd-post-image">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail('full'); ?>
				</a>
			</div>
		<?php } ?>
		<div class="eltd-blog-slider-caption-holder">
			<div class = "eltd-blog-slider-caption">
				<div class="eltd-item-info-section eltd-large-info-section">
					<?php creator_elated_post_info(array(
						'date' => 'yes',
						'category' => 'yes'
					)) ?>
				</div>
				<h2 class="eltd-section-title">
					<a href="<?php echo esc_url(get_permalink()) ?>" >
						<?php echo esc_attr(get_the_title()) ?>
					</a>
				</h2>
			</div>
		</div>

	</div>
</div>
