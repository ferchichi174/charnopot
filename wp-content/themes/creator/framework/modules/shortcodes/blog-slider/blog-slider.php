<?php

namespace CreatorElated\Modules\Shortcodes\BlogSlider;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class BlogSlider
 */
class BlogSlider implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	function __construct() {
		$this->base = 'eltd_blog_slider';

		add_action('vc_before_init', array($this,'vcMap'));

		//Category filter
		add_filter( 'vc_autocomplete_eltd_blog_slider_category_callback', array( &$this, 'blogListCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

		//Category render
		add_filter( 'vc_autocomplete_eltd_blog_slider_category_render', array( &$this, 'blogListCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array

		//post filter
		add_filter( 'vc_autocomplete_eltd_blog_slider_selected_posts_callback', array( &$this, 'blogPostsAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

		//post render
		add_filter( 'vc_autocomplete_eltd_blog_slider_selected_posts_render', array( &$this, 'blogPostsAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array
	}

	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Elated Blog Slider', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-blog-list-slider extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' =>esc_html__( 'Type','creator'),
					'param_name' => 'type',
					'value' => array(
						esc_html__('Post Info On Right','creator') => 'eltd_post_info_right',
						esc_html__('Post Info On Left' ,'creator')=> 'eltd_post_info_left'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Number of Posts','creator'),
					'param_name' => 'number_of_posts',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Order By','creator'),
					'param_name' => 'order_by',
					'value' => array(
						esc_html__('Title','creator') => 'title',
						esc_html__('Date','creator') => 'date',
						esc_html__('ID' ,'creator')=> 'ID'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' =>esc_html__( 'Order','creator'),
					'param_name' => 'order',
					'value' => array(
			esc_html__('ASC' ,'creator')=> 'ASC',
			esc_html__('DESC','creator') => 'DESC'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('In Grid?','creator'),
					'param_name' => 'grid',
					'value' => array(
			esc_html__('No','creator') => 'no',
			esc_html__('Yes','creator') => 'yes'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Slider Padding Top','creator'),
					'param_name' => 'slider_padding_top',
					'dependency' => array(
						'element' => 'grid',
						'value' => 'yes'
					)
				),
				array(
					'type' => 'dropdown',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Display Navigation','creator'),
					'param_name' => 'display_nav',
					'value' => array(
						esc_html__('No','creator') => 'no',
						esc_html__('Yes','creator') => 'yes'
					),
					'save_always' => true,
					'description' => ''
				),

				array(
					'type' => 'autocomplete',
					'heading' =>esc_html__( 'Category Slug','creator'),
					'param_name' => 'category',
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'autocomplete',
					'heading' => esc_html__('Selected Posts','creator'),
					'param_name' => 'selected_posts',
					'settings'    => array(
						'multiple'      => true,
						'sortable'      => true,
						'unique_values' => true,
						// In UI show results except selected. NB! You should manually check values in backend
					),
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'textfield',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Text length','creator'),
					'param_name' => 'text_length',
					'description' => esc_html__('Number of characters','creator')
				)
			)
		) );

	}
	public function render($atts, $content = null) {

		$default_atts = array(
			'number_of_posts' => '-1',
			'order_by' => '',
			'order' => '',
			'grid'  => '',
			'slider_padding_top' => '',
			'category' => '',
			'selected_posts' => '',
			'text_length' => '90',
			'display_nav' =>'',
			'type'=>''
		);

		$params = shortcode_atts($default_atts, $atts);
		extract($params);

		$queryArray = $this->generateBlogQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$html = '';

		$holderClasess=$this->getsliderHolderClasses($params);

		$html.= '<div class="eltd-blog-slider-holder '.esc_attr($holderClasess).'">';
		if($grid === 'yes'){
			$grid_style = $this->sliderGridStyle($params);
			$html .= '<div class="eltd-grid" '.creator_elated_get_inline_style($grid_style).'>';
		}
		if($query_result->have_posts()){
			$html .= '<div class = "eltd-blog-slider">';

			while($query_result->have_posts()){
				$query_result->the_post();
					$html .= creator_elated_get_shortcode_module_template_part('templates/blog-slider-item', 'blog-slider', '', $params);
			}

			$html .= '</div>';
			wp_reset_postdata();

		}else{
			$html .= '<p>'. esc_html_e( 'Sorry, no posts matched your criteria.', 'creator' ) .'</p>';
		}
		if($grid === 'yes'){
			$html .= '</div>'; //close eltd-grid
		}
		$html .= '</div>'; //close eltd-blog-slider

		return $html;

	}

	private function getsliderHolderClasses($params){
		$holderClasses= array();


		switch($params['type']){
			case 'eltd_post_info_left':
				$holderClasses [] = 'eltd-blog-slider-left-type';
				break;
			default:
				break;
		}

		switch($params['display_nav']){
			case 'yes':
				$holderClasses [] = 'eltd-blog-slider-display-nav';
				break;
			default:
				break;
		}

		return implode(' ', $holderClasses);


	}

	/**
	 * Generates query array
	 *
	 * @param $params
	 *
	 * @return array
	 */
	public function generateBlogQueryArray($params){

		$queryArray = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'category_name' => $params['category']
		);
		$post_ids = null;
		if ( ! empty( $params['selected_posts'] ) ) {
			$post_ids             = explode( ',', $params['selected_posts'] );
			$queryArray['post__in'] = $post_ids;
		}
		return $queryArray;
	}

	function sliderGridStyle($params){

		$style = array();
		if($params['grid'] === 'yes'){
			if($params['slider_padding_top'] !== ''){
				$style[] = 'padding-top: '.$params['slider_padding_top'].'px';
			}
		}
		return implode(' ', $style);
	}
	/**
	 * Filter categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogListCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'creator' ) . ': ' . $value['category_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find categories by slug
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogListCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get category
			$category = get_term_by( 'slug', $query, 'category' );
			if ( is_object( $category ) ) {

				$category_slug = $category->slug;
				$category_title = $category->name;

				$category_title_display = '';
				if ( ! empty( $category_title ) ) {
					$category_title_display = esc_html__( 'Category', 'creator' ) . ': ' . $category_title;
				}

				$data          = array();
				$data['value'] = $category_slug;
				$data['label'] = $category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

	/**
	 * Filter posts
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogPostsAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_id    = (int) $query;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT ID AS id, post_title AS title
					FROM {$wpdb->posts}
					WHERE post_type = 'post' AND ( ID = '%d' OR post_title LIKE '%%%s%%' )", $post_id > 0 ? $post_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['id'];  //Param that will be saved in shortcode
				$data['label'] = esc_html__( 'Id', 'creator' ) . ': ' . $value['id'] . ( ( strlen( $value['title'] ) > 0 ) ? ' - ' . esc_html__( 'Title', 'creator' ) . ': ' . $value['title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find categories by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogPostsAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get portfolio
			$post = get_post( (int) $query );
			if ( ! is_wp_error( $post ) ) {

				$post_id    = $post->ID;
				$post_title = $post->post_title;

				$post_title_display = '';
				if ( ! empty( $post_title ) ) {
					$post_title_display = ' - ' . esc_html__( 'Title', 'creator' ) . ': ' . $post_title;
				}

				$post_id_display = esc_html__( 'Id', 'creator' ) . ': ' . $post_id;

				$data          = array();
				$data['value'] = $post_id;
				$data['label'] = $post_id_display . $post_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

}
