<?php

namespace CreatorElated\Modules\Shortcodes\BlogList;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class BlogList
 */
class BlogList implements ShortcodeInterface {
	/**
	* @var string
	*/
	private $base;
	
	function __construct() {
		$this->base = 'eltd_blog_list';
		
		add_action('vc_before_init', array($this,'vcMap'));

		//Category filter
		add_filter( 'vc_autocomplete_eltd_blog_list_category_callback', array( &$this, 'blogListCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array

		//Category render
		add_filter( 'vc_autocomplete_eltd_blog_list_category_render', array( &$this, 'blogListCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array

	}
	
	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Elated Blog List', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-blog-list extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params' => array(
					array(
						'type' => 'dropdown',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Type', 'creator'),
						'param_name' => 'type',
						'value' => array(
							esc_html__('Standard','creator') => 'standard',
							esc_html__('Masonry','creator') => 'masonry',
							esc_html__('Simple','creator') => 'simple',
							esc_html__('Author on Top','creator') => 'author_on_top',
							esc_html__('Gallery','creator') =>'gallery',
							esc_html__('Overlay','creator') =>'overlay',
							esc_html__('Split Column','creator') =>'split_column',
							esc_html__('Minimal','creator')   => 'minimal',
							esc_html__('Pinboard','creator')   => 'pinboard'
						),
						'description' => '',
                        'save_always' => true
					),
					array(
						'type' => 'textfield',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Number of Posts','creator'),
						'param_name' => 'number_of_posts',
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Number of Columns','creator'),
						'param_name' => 'number_of_columns',
						'value' => array(
							esc_html__('One','creator') => '1',
							esc_html__('Two','creator') => '2',
							esc_html__('Three' ,'creator')=> '3',
							esc_html__('Four','creator') => '4'
						),
						'description' => '',
						'dependency' => array(
							'element' => 'type',
							'value' => array('standard', 'masonry', 'simple', 'author_on_top','gallery','overlay')
						),
                        'save_always' => true
					),
					array(
						'type' => 'dropdown',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Number of Columns','creator'),
						'param_name' => 'number_of_columns2',
						'value' => array(
								esc_html__('Two' ,'creator')=> '2',
								esc_html__('Three','creator') => '3'
						),
						'description' => '',
						'dependency' => array(
							'element' => 'type',
							'value' => array('split_column')
						),
						'save_always' => true
					),
					array(
						'type' => 'dropdown',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Order By','creator'),
						'param_name' => 'order_by',
						'value' => array(
							esc_html__('Title','creator') => 'title',
							esc_html__('Date','creator') => 'date'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'holder' => 'div',
						'class' => '',
						'heading' => 'Order',
						'param_name' => 'order',
						'value' => array(
							esc_html__('ASC','creator') => 'ASC',
							esc_html__('DESC','creator') => 'DESC'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'heading' => 'Skin',
						'param_name' => 'skin',
						'value' => array(
							esc_html__('Dark','creator') => 'dark',
							esc_html__('Light','creator') => 'light'
						),
						'save_always' => true,
						'dependency' => Array('element' => 'type', 'value' => array('simple')),
						'description' => ''
					),
					array(
						'type' => 'autocomplete',
						'heading' => esc_html__('Category Slug','creator'),
						'param_name' => 'category',
						'description' => esc_html__('Leave empty for all or use comma for list','creator'),
						'admin_label' => true
					),
					array(
						'type' => 'dropdown',
						'param_name' => 'title_text_transform',
						'heading' => esc_html__('Title Text Transform', 'creator'),
						'description' => '',
						'admin_label' => '',
						'value' => creator_elated_get_text_transform_array(true)
					),
					array(
						'type' => 'textfield',
						'holder' => 'div',
						'class' => '',
						'heading' => esc_html__('Text length','creator'),
						'param_name' => 'text_length',
						'description' => esc_html__('Number of characters','creator')
					)
				)
		) );

	}
	public function render($atts, $content = null) {
		
		$default_atts = array(
			'type' => '',
            'number_of_posts' => '',
            'number_of_columns' => 3,
			'number_of_columns2'=>3,
            'image_size' => 'original',
            'order_by' => '',
            'order' => '',
            'category' => '',
            'title_text_transform' => '',
			'text_length' => '90',
			'skin'	=>''
        );
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		$params['holder_classes'] = $this->getBlogHolderClasses($params);
	
		$queryArray = $this->generateBlogQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$params['query_result'] = $query_result;


        $thumbImageSize = $this->generateImageSize($params);
		$params['thumb_image_size'] = $thumbImageSize;
		$params['button_params'] = $this->getButtonParams($params);
		$params['title_style'] = $this->getTitleStyle($params);

		$params['blog_list_object'] = $this;

		$html ='';
        $html .= creator_elated_get_shortcode_module_template_part('templates/blog-list-holder', 'blog-list', '', $params);
		return $html;
		
	}

	/**
	   * Generates holder classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getBlogHolderClasses($params){
		$holderClasses = '';

		$columns_number = $this->getColumnNumberClass($params);

		if(!empty($params['type'])){
			switch($params['type']){
				case 'author_on_top':
					$holderClasses = 'eltd-blog-author-top';
				break;	
				case 'masonry' : 
					$holderClasses = 'eltd-masonry';
				break;
				case 'simple' :
					$holderClasses = 'eltd-blog-simple';
					$holderClasses .=' '.$this->getSimpleBlogSkin($params);
				break;
				case 'overlay' :
					$holderClasses = 'eltd-blog-overlay';
					break;
				case 'split_column' :
					$holderClasses = 'eltd-blog-split-column';
					break;
				case 'gallery':
					$holderClasses = 'eltd-blog-gallery';
					break;
				case 'minimal' :
					$holderClasses = 'eltd-blog-minimal';
				break;
				case 'pinboard' :
					$holderClasses = 'eltd-blog-pinboard';
					break;
				default: 
					$holderClasses = 'eltd-blog-standard';
			}
		}
		
		$holderClasses .= ' '.$columns_number;
		
		return $holderClasses;
		
	}



	/**
	 * Generates skin classes for simple blog type
	 *
	 * @param $params
	 *
	 * @return string
	 */

	private function getSimpleBlogSkin($params){
		$skin_class='';

		if($params['type']==='simple'){
			if($params['skin']!==''){
				switch ($params['skin']){
					case 'light':
						$skin_class='eltd-simple-blog-light';
						break;
					default:
						$skin_class='eltd-simple-blog-dark';
						break;
				}
			}
		}
		return $skin_class;

	}

	/**
	 * Generates column classes for blog list minimal and split_column type
	 *
	 * @param $params
	 *
	 * @return string
	 */

	private function getColumnNumberClass($params){
		
		$columns_number = '';
		$columns = $params['number_of_columns'];
		if($params['type'] !== 'minimal' && $params['type'] !== 'pinboard'){
			switch ($columns) {
				case 1:
					$columns_number = 'eltd-one-column';
					break;
				case 2:
					$columns_number = 'eltd-two-columns';
					break;
				case 3:
					$columns_number = 'eltd-three-columns';
					break;
				case 4:
					$columns_number = 'eltd-four-columns';
					break;
				default:
					$columns_number = 'eltd-one-column';
					break;
			}
		}

		if($params['type'] === 'split_column'){
			$columns = $params['number_of_columns2'];
			switch ($columns) {
				case 2:
					$columns_number = 'eltd-two-columns';
					break;
				case 3:
					$columns_number = 'eltd-three-columns';
					break;
				default:
					$columns_number = 'eltd-one-column';
					break;
			}
		}


		return $columns_number;
	}

	/**
	   * Generates query array
	   *
	   * @param $params
	   *
	   * @return array
	*/
	public function generateBlogQueryArray($params){
		
		$queryArray = array(
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'category_name' => $params['category']
		);
		return $queryArray;
	}

	/**
	   * Generates image size option
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function generateImageSize($params){
		$thumbImageSize = '';
		$imageSize = $params['image_size'];

		if ($imageSize !== '' && $imageSize == 'landscape') {
            $thumbImageSize .= 'creator_elated_landscape';
        } else if($imageSize === 'square'){
			$thumbImageSize .= 'creator_elated_square';
		} else if ($imageSize !== '' && $imageSize == 'original') {
            $thumbImageSize .= 'full';
        }

		return $thumbImageSize;
	}

	public function generateBackgroundImageStyle($id){

		$style_attr = '';
		if ( has_post_thumbnail($id) ) {
			$background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( $id ,'full'));
			$background_image_src = $background_image_object[0];
			$style_attr  = 'background-image:url('.  esc_url($background_image_src) .')';
		}

		return $style_attr;

	}

	public function generateOverlayBlogHoverImage($id){
	
		$image = get_post_meta($id, 'eltd_overlay_type_meta', true);
		if( !$image || $image === ''){
			$image = wp_get_attachment_url( get_post_thumbnail_id($id) );
		}


		return $image;
	}

	private function getButtonParams($params){

		$first_color = '#27d7ab';
		$first_color_options = creator_elated_options()->getOptionValue('first_color');
		if(isset($first_color_options) && $first_color_options !== ''){
			$first_color = $first_color_options;
		}
		$button_params = array(
			'link' => get_permalink(),
			'custom_class' => 'eltd-view-more',
			'text' => esc_html__('View More', 'creator'),
			'color' => '#333333',
			'type' => 'transparent',
			'hover_color' => $first_color
		);
		return $button_params;

	}

	private function getTitleStyle($params){

		$title_styles[] = ($params['title_text_transform'] !== '') ? 'text-transform: ' .$params['title_text_transform']  : '';

		return $title_styles;
	}



	/**
	 * Filter categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogListCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'creator' ) . ': ' . $value['category_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find categories by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogListCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get category
			$category = get_term_by( 'slug', $query, 'category' );
			if ( is_object( $category ) ) {

				$category_slug = $category->slug;
				$category_title = $category->name;

				$category_title_display = '';
				if ( ! empty( $category_title ) ) {
					$category_title_display = esc_html__( 'Category', 'creator' ) . ': ' . $category_title;
				}

				$data          = array();
				$data['value'] = $category_slug;
				$data['label'] = $category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}
	
}
