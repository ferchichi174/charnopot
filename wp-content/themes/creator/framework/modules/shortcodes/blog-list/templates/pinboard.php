<li class="eltd-blog-list-item clearfix <?php echo esc_attr($pinboard_css_class); ?>">

	<?php if(has_post_thumbnail()){ ?>
		<div class="eltd-item-image" <?php echo creator_elated_get_inline_style($bck_image_style);  ?>>
			<a href="<?php echo get_the_permalink(); ?>"></a>
		</div>
	<?php } ?>

	<a class="eltd-blog-pinboard-overlay" href="<?php echo get_the_permalink(); ?>"></a>

	<div class="eltd-item-text-holder">
		<div class="eltd-item-text-holder-inner">
			<div class="eltd-item-info-section">
				<?php creator_elated_post_info(array(
					'author' => 'yes',
				)) ?>
			</div>

			<h4 class="eltd-item-title" <?php echo creator_elated_inline_style($title_style); ?>>
				<a href="<?php echo esc_url(get_permalink()) ?>" >
					<?php echo esc_attr(get_the_title()) ?>
				</a>
			</h4>

			<?php creator_elated_read_more_button('' , 'eltd-pinboard-read-more-button'); ?>
		</div>
	</div>
</li>