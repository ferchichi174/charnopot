<?php   $style_attr = '';
if ( has_post_thumbnail() ) {
    $background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID()),'full');
    $background_image_src = $background_image_object[0];
    $style_attr  = 'background-image:url('.  esc_url($background_image_src) .')';
} ?>

<li class="eltd-blog-list-item eltd-blog-list-gallery-item">

    <div class="eltd-blog-list-item-holder"  <?php echo creator_elated_get_inline_style($style_attr);  ?>>

        <a class="eltd-blog-list-block-link" href="<?php echo esc_url(get_permalink()) ?>"></a>

        <div class="eltd-item-image">
            <?php
            echo get_the_post_thumbnail(get_the_ID(), $thumb_image_size);
            ?>
        </div>
        <div class="eltd-item-text-holder">
            <?php creator_elated_post_info(array(
                'author' => 'yes'
            )); ?>

            <h4 class="eltd-item-title" <?php echo creator_elated_inline_style($title_style); ?>>
                <a href="<?php echo esc_url(get_permalink()) ?>" >
                    <?php echo esc_attr(get_the_title()) ?>
                </a>
            </h4>
        </div>
        <a href="<?php echo esc_url(get_permalink()) ?>" class="eltd-view-more">
            <?php esc_html_e('View More', 'creator'); ?>
        </a>
    </div>

</li>