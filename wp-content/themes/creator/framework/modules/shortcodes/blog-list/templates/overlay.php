<li class="eltd-blog-list-item eltd-blog-list-overlay-item">

    <div class="eltd-item-image">

        <a href="<?php echo esc_url(get_permalink()) ?>">
            <?php
            echo get_the_post_thumbnail(get_the_ID(), $thumb_image_size);
            ?>

            <div class="eltd-item-text-holder">
                <?php
                    $imgSrc=$blog_list_object->generateOverlayBlogHoverImage(get_the_ID());
                ?>
                <img src="<?php echo esc_url($imgSrc)?>" class="eltd-blog-overlay-hover-image" alt="<?php esc_attr_e( 'eltd-blog-overlay-hover-image', 'creator' ); ?>">
                <div class="eltd-item-text-holder-inner">
                    <?php creator_elated_post_info(array(
                        'date' => 'yes'
                    )); ?>

                    <h5 class="eltd-item-title" <?php echo creator_elated_inline_style($title_style); ?>>
                        <?php echo esc_attr(get_the_title()) ?>
                    </h5>

                    <p class="eltd-read-more"><?php esc_html_e('Read More' , 'creator'); ?></p>
                </div>
            </div>
        </a>


    </div>

</li>