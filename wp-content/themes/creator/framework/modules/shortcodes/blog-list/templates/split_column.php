<?php   $style_attr = '';
if ( has_post_thumbnail() ) {
    $background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID()),'full');
    $background_image_src = $background_image_object[0];
    $style_attr  = 'background-image:url('.  esc_url($background_image_src) .')';
} ?>

<li class="eltd-blog-list-item eltd-blog-list-split-column-item">
    <div class="eltd-blog-list-item-holder" >
        <div class="eltd-item-image-holder" <?php echo creator_elated_get_inline_style($style_attr);  ?>>
            <a href="<?php echo esc_url(get_permalink()) ?>">
                <div class="eltd-item-image">
                    <?php
                        echo get_the_post_thumbnail(get_the_ID(), $thumb_image_size);
                    ?>
                </div>
            </a>
        </div>

        <div class="eltd-item-text-holder">
            <?php creator_elated_post_info(array(
                'author' => 'yes'
            )); ?>

            <h5 class="eltd-item-title" <?php echo creator_elated_inline_style($title_style); ?>>
                <a href="<?php echo esc_url(get_permalink()) ?>" >
                    <?php echo esc_attr(get_the_title()) ?>
                </a>
            </h5>
            <?php if ($text_length != '0') {

                $excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>

                <p class="eltd-excerpt">
                    <?php echo esc_html($excerpt)?>...
                </p>

            <?php }
                echo creator_elated_get_button_html($button_params);
            ?>
        </div>
    </div>


</li>