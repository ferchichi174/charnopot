<?php if(isset($embedded_link_url) && $embedded_link_url !== ''){ ?>

	<div class="eltd-embedded-link">
		<?php
			$embed = wp_oembed_get( $embedded_link_url, $embedded_params );
			echo creator_elated_get_module_part( $embed );
		?>
	</div>

<?php } ?>