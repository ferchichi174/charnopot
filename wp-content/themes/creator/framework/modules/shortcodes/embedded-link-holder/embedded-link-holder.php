<?php
namespace CreatorElated\Modules\Shortcodes\EmbeddedLinkHolder;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class Highlight
 */
class EmbeddedLinkHolder implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_embedded_link_holder';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/*
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 */

	public function vcMap() {
		vc_map(
			array(
				'name'                      => esc_html__('Embedded Link Holder', 'creator'),
				'base'                      => $this->base,
				'category'                  => 'by ELATED',
				'icon'                      => 'icon-wpb-embedded-link-holder extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' =>esc_html__( 'Embedded Link Url', 'creator'),
						'holder' => 'div',
						'class' => '',
						'param_name' => 'embedded_link_url',
						'value' => ''
					),
					array(
						'type' => 'textfield',
						'heading' =>esc_html__( 'Embedded Link Height(px)', 'creator'),
						'holder' => 'div',
						'class' => '',
						'param_name' => 'embedded_link_height',
						'value' => ''
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Embedded Link Width(px)', 'creator'),
						'holder' => 'div',
						'class' => '',
						'param_name' => 'embedded_link_width',
						'value' => ''
					)
				)
			)
		);
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'embedded_link_url' => '',
			'embedded_link_height' => '',
		    'embedded_link_width' => ''
		);

		$params = shortcode_atts($args, $atts);
		$html = '';
		extract ($params);
		$embedded_params = array();
		if($embedded_link_height !== ''){
			$embedded_params['height'] = $embedded_link_height;
		}
		if($embedded_link_width !== ''){
			$embedded_params['width'] = $embedded_link_width;
		}
		$params['embedded_params'] = $embedded_params;

		$html .= '<div class="eltd-embedded-link-holder">';
		//Get HTML from template
		$html .= creator_elated_get_shortcode_module_template_part('templates/embedded-template', 'embedded-link-holder', '', $params);
		$html .= '</div>';

		return $html;

	}
}