<?php
namespace CreatorElated\Modules\Shortcodes\Process;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Tabs
 * @package CreatorElated\Modules\Process
 */
class ProcessHolder implements ShortcodeInterface {

	private $base;

	function __construct() {
		$this->base = 'eltd_process_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see eltd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map(array(
			'name' =>esc_html__( 'Process Holder','creator'),
			'base' => $this->getBase(),
			'as_parent' => array('only' => 'eltd_process'),
			'content_element' => true,
			'category' => 'by ELATED',
			'icon' => 'icon-wpb-process-holder extended-custom-icon',
			'show_settings_on_create' => true,
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'heading' =>esc_html__( 'Columns','creator'),
					'param_name' => 'columns',
					'value' => array(
						esc_html__('Four'   ,'creator')   => 'four',
						esc_html__('Five' ,'creator')     => 'five',
						esc_html__('Six' ,'creator')     =>  'six'
					),
					'save_always' => true,
					'admin_label' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Skin','creator'),
					'param_name' => 'skin',
					'value' => array(
						esc_html__('Dark' ,'creator')     => 'dark',
						esc_html__('Light','creator')      => 'light'
					),
					'save_always' => true,
					'admin_label' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Appear Effect','creator'),
					'param_name' => 'appear_effect',
					'value' => array(
						esc_html__('Yes' ,'creator')     => 'yes',
						esc_html__('No','creator')      => 'no'
					),
					'save_always' => true,
					'admin_label' => true,
					'group' => esc_html__('Behavior','creator')
				)
			)
		));
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'columns' => '',
			'skin' => '',
			'appear_effect' => 'yes',
		);

		$params = shortcode_atts($args, $atts);
		$params['content'] = $content;
		$params['classes'] = 'eltd-processes-holder eltd-' . $params['columns'] . '-columns eltd-process-'.$params['skin'].' clearfix eltd-appear-effect-' . $params['appear_effect'] ;

		$html = creator_elated_get_shortcode_module_template_part('templates/process-holder', 'process', '', $params);

		return $html;


	}

}