<div class="eltd-process <?php echo esc_attr($process_classes) ?>" <?php echo creator_elated_get_inline_attrs($process_data)?> >

	<div class="eltd-process-content-wrapper">
		<div class="eltd-process-content-holder">
			<div class="eltd-process-content-overlay"></div>

			<?php if($link != '') { ?>
				<a class="eltd-process-link" href="<?php echo esc_url($link) ?>" target="<?php echo esc_attr($target) ?>"></a>
			<?php } ?>
			<?php if($background_image != '') { ?>
				<div class="eltd-process-bgrnd" <?php echo creator_elated_get_inline_style($background_image_style)?>></div>
			<?php } ?>
			<div class="eltd-process-content-holder-inner">

				<?php if($type === 'process_icons'){ ?>

					<?php echo creator_elated_get_module_part( $icon ); ?>

				<?php } elseif($type === 'process_text'){ ?>
					<span class="eltd-process-inner-text">
					<?php echo esc_html($text_in_process, 'creator') ?>
				</span>

				<?php } ?>

			</div>
		</div>
	</div>
	<span aria-hidden="true" class="arrow_right eltd-process-arrow-right"></span>
	<div class="eltd-process-text-holder">
		<h5 class="eltd-process-title">
			<?php echo esc_html($title); ?>
		</h5>
		<p class="eltd-process-text">
			<?php echo esc_html($text); ?>
		</p>
	</div>


</div>