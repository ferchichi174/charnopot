<?php
namespace CreatorElated\Modules\Shortcodes\ProgressBar;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProgressBar implements ShortcodeInterface{
	private $base;
	
	function __construct() {
		$this->base = 'eltd_progress_bar';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Elated Progress Bar', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-progress-bar extended-custom-icon',
			'category' => 'by ELATED',
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Title','creator'),
					'param_name' => 'title',
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Percentage','creator'),
					'param_name' => 'percent',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__('Percentage Type','creator'),
					'param_name' => 'percentage_type',
					'value' => array(
						esc_html__('Floating','creator')  => 'floating',
						esc_html__('Static','creator') => 'static'
					),
					'dependency' => Array('element' => 'percent', 'not_empty' => true)
				),
			    array(
				    'type' => 'dropdown',
				    'admin_label' => true,
				    'heading' => esc_html__('Skin','creator'),
				    'param_name' => 'skin',
				    'save_always' => true,
				    'value' => array(
						esc_html__('Default','creator') => 'default',
						esc_html__('Dark','creator')  => 'dark',
						esc_html__('Light','creator') => 'light'
				    )
			    ),
			)
		) );

	}

	public function render($atts, $content = null) {
		$args = array(
			'skin' => '',
            'title' => '',
            'percent' => '100',
            'percentage_type' => 'floating'
        );
		$params = shortcode_atts($args, $atts);
		
		//Extract params for use in method
		extract($params);
		
		$params['percentage_classes'] = $this->getPercentageClasses($params);
		if(!empty($percentage_type)){
			$params['skin_class'] = $params['skin'];
		}
		else {
			$params['skin_class']="";
		}

        //init variables
		$html = creator_elated_get_shortcode_module_template_part('templates/progress-bar-template', 'progress-bar', '', $params);
		
        return $html;
		
	}
	/**
    * Generates css classes for progress bar
    *
    * @param $params
    *
    * @return array
    */
	private function getPercentageClasses($params){
		
		$percentClassesArray = array();
		
		if(!empty($params['percentage_type']) !=''){
			
			if($params['percentage_type'] == 'floating'){
				
				$percentClassesArray[]= 'eltd-floating eltd-floating-outside';

			}
			elseif($params['percentage_type'] == 'static'){
				
				$percentClassesArray[] = 'eltd-static';
				
			}
		}

		return implode(' ', $percentClassesArray);
	}
}