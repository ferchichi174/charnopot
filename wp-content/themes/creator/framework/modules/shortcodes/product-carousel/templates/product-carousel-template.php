<?php $product = creator_elated_woocommerce_global_product();?>
<div class="eltd-product-carousel-item-holder product">
	<div class="eltd-product-carousel-image-holder">
		<img src="<?php echo esc_url($product_carousel_image) ?>" alt="<?php esc_attr_e( 'product_carousel_image', 'creator' ); ?>">
	</div>

	<div class = "eltd-product-carousel-caption">
		<div class = "eltd-product-carousel-holder">
			<div class="eltd-product-carousel-inner">
				<div class="eltd-product-carousel-title">
					<h4><a href="<?php echo esc_url(get_permalink()) ?>" >
							<?php echo esc_attr(get_the_title()) ?>
						</a>
					</h4>
				</div>
				<div class="eltd-product-carousel-price">
					<?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
				</div>
				<div class="eltd-product-carousel-add-to-cart">
					<?php echo creator_elated_woocommerce_loop_add_to_cart_link(); ?>
				</div>
			</div>
		</div>
	</div>
</div>