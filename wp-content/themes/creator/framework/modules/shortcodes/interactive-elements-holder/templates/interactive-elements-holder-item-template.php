<div class="eltd-interactive-elements-holder-item <?php echo esc_attr($interactive_elements_holder_item_class); ?>" <?php echo creator_elated_get_inline_style($interactive_elements_holder_item_style); ?> <?php echo creator_elated_get_inline_attrs($elements_holder_item_data); ?>>
	<div class="eltd-interactive-elements-holder-item-inner">
		<div class="eltd-interactive-elements-holder-item-content <?php echo esc_attr($interactive_elements_holder_item_content_class); ?>" <?php echo creator_elated_get_inline_style($interactive_elements_holder_item_content_style); ?>>
			<div class="eltd-interactive-elements-holder-item-content-inner">
				<?php echo do_shortcode($content); ?>
			</div>
		</div>
	</div>
</div>