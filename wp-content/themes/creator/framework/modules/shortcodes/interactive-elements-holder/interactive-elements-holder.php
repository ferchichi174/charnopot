<?php
namespace CreatorElated\Modules\Shortcodes\InteractiveElementsHolder;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class InteractiveElementsHolder implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'eltd_interactive_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elated Interactive Elements Holder', 'creator'),
			'base' => $this->base,
			'icon' => 'icon-wpb-interactive-elements-holder extended-custom-icon',
			'category' => 'by ELATED',
			'as_parent' => array('only' => 'eltd_interactive_elements_holder_left_item, eltd_interactive_elements_holder_right_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' =>esc_html__( 'Interactivity', 'creator'),
					'param_name' => 'interactivity',
					'value' => array(
						esc_html__('Animation to Left', 'creator') 	=> 'left',
						esc_html__('Animation to Right', 'creator')    	=> 'right',
					),
					'save_always' => true,
					'description' => esc_html__('Set hover animation direction', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Clickable', 'creator'),
					'param_name' => 'clickable',
					'value' => array(
						esc_html__('Yes' , 'creator')	=> 'yes',
						esc_html__('No' , 'creator')   => 'no',
					),
					'save_always' => true,
					'description' => esc_html__('Make whole item clickable', 'creator')
				),
				array(
					'type' => 'textfield',
					'class' => '',
					'heading' =>esc_html__( 'Link', 'creator'),
					'param_name' => 'link',
					'value' => '',
					'save_always' => true,
					'description' =>esc_html__( 'Set an external URL to link to', 'creator'),
					'dependency' => array('element' => 'clickable',  'value' => 'yes')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Link Target', 'creator'),
					'param_name' => 'link_target',
					'value' => array(
							esc_html__('Blank', 'creator') => '_blank',
							esc_html__('Self', 'creator')  => '_self',
					),
					'save_always' => true,
					'description' => esc_html__('Link target', 'creator'),
					'dependency' => array('element' => 'link',  'not_empty' => true)
				),
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' =>esc_html__( 'Background Color', 'creator'),
					'param_name' => 'background_color',
					'value' => '',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' =>esc_html__( 'Width & Responsiveness', 'creator'),
					'heading' =>esc_html__( 'Switch to One Column', 'creator'),
					'param_name' => 'switch_to_one_column',
					'value' => array(
								esc_html__('Default' , 'creator')   		=> '',
								esc_html__('Below 1280px' , 'creator')		=> '1280',
								esc_html__('Below 1024px' , 'creator')   	=> '1024',
								esc_html__('Below 768px'  , 'creator')   	=> '768',
								esc_html__(	'Below 600px' , 'creator')   	=> '600',
								esc_html__('Below 480px' , 'creator')   	=> '480',
								esc_html__(	'Never'    	, 'creator')		=> 'never'
					),
					'description' => esc_html__('Choose on which stage item will be in one column', 'creator')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness', 'creator'),
					'heading' => esc_html__('Choose Alignment In Responsive Mode', 'creator'),
					'param_name' => 'alignment_one_column',
					'value' => array(
							esc_html__('Default' , 'creator')   	=> '',
							esc_html__('Left' , 'creator')			=> 'left',
							esc_html__(	'Center'    , 'creator')	=> 'center',
							esc_html__('Right' , 'creator')    	=> 'right'
					),
					'description' =>esc_html__( 'Alignment When Items are in One Column', 'creator')
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'switch_to_one_column'		=> '',
			'alignment_one_column' 		=> '',
			'interactivity'	=> '',
			'clickable'	=> '',
			'number_of_columns'=> '',
			'link'	=> '',
			'link_target' => '',
			'background_color' => ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html = '';
		$interactive_elements_holder_style = '';
		$interactive_elements_holder_classes = array();
		$interactive_elements_holder_classes[] = 'eltd-interactive-elements-holder';


		if($number_of_columns != ''){
			$interactive_elements_holder_classes[] .= 'eltd-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$interactive_elements_holder_classes[] = 'eltd-responsive-mode-' . $switch_to_one_column ;
		} else {
			$interactive_elements_holder_classes[] = 'eltd-responsive-mode-1024' ;
		}

		if($alignment_one_column != ''){
			$interactive_elements_holder_classes[] = 'eltd-one-column-alignment-' . $alignment_one_column ;
		}

		if($interactivity != ''){
			$interactive_elements_holder_classes[] = 'eltd-animation-to-' . $interactivity ;
		}

		if($background_color != ''){
			$interactive_elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$interactive_elements_holder_class = implode(' ', $interactive_elements_holder_classes);
		$html = '';
		$html .= '<div class="eltd-interactive-elements-holder-wrapper">';
		$html .= '<div ' . creator_elated_get_class_attribute($interactive_elements_holder_class) . ' ' . creator_elated_get_inline_attr($interactive_elements_holder_style, 'style'). '>';
		$html .= do_shortcode($content);
		if ($link != '') {
			$html .= '<a href=" '.$link.'" target="'.$link_target.'"></a>';
		}
		$html .= '</div>';
		$html .= '</div>';

		return $html;

	}

}
	