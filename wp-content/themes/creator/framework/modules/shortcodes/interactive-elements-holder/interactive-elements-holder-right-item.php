<?php
namespace CreatorElated\Modules\Shortcodes\InteractiveElementsHolderRightItem;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class InteractiveElementsHolderRightItem implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_interactive_elements_holder_right_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Elated Interactive Elements Holder Right Item', 'creator'),
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_interactive_elements_holder'),
					'as_parent' => array('except' => 'vc_row, vc_accordion, no_cover_boxes, no_portfolio_list, no_portfolio_slider'),
					'content_element' => true,
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-interactive-elements-holder-right-item extended-custom-icon',
					'show_settings_on_create' => true,
					'js_view' => 'VcColumnView',
					'params' => array(
						array(
							'type' => 'colorpicker',
							'class' => '',
							'heading' =>  esc_html__('Background Color', 'creator'),
							'param_name' => 'background_color',
							'value' => '',
							'description' => ''
						),
						array(
							'type' => 'attach_image',
							'class' => '',
							'heading' =>  esc_html__('Background Image', 'creator'),
							'param_name' => 'background_image',
							'value' => '',
							'description' => ''
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'heading' =>  esc_html__('Padding', 'creator'),
							'param_name' => 'item_padding',
							'value' => '',
							'description' =>  esc_html__('Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'dropdown',
							'class' => '',
							'heading' =>  esc_html__('Horizontal Alignment', 'creator'),
							'param_name' => 'horizontal_aligment',
							'value' => array(
									esc_html__('Left' , 'creator')   	=> 'left',
									esc_html__('Right', 'creator')     => 'right',
									esc_html__('Center', 'creator')    => 'center'
							),
							'description' => ''
						),
						array(
							'type' => 'dropdown',
							'class' => '',
							'heading' =>  esc_html__('Vertical Alignment', 'creator'),
							'param_name' => 'vertical_alignment',
							'value' => array(
									esc_html__('Middle' , 'creator')   => 'middle',
									esc_html__(	'Top'  , 'creator')  	=> 'top',
									esc_html__(	'Bottom' , 'creator')   => 'bottom'
							),
							'description' => ''
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' =>  esc_html__('Width & Responsiveness', 'creator'),
							'heading' =>  esc_html__('Padding on screen size between 1280px-1600px', 'creator'),
							'param_name' => 'item_padding_1280_1600',
							'value' => '',
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' =>  esc_html__('Width & Responsiveness', 'creator'),
							'heading' =>  esc_html__('Padding on screen size between 1024px-1280px', 'creator'),
							'param_name' => 'item_padding_1024_1280',
							'value' => '',
							'description' =>  esc_html__('Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' =>  esc_html__('Width & Responsiveness', 'creator'),
							'heading' =>  esc_html__('Padding on screen size between 768px-1024px', 'creator'),
							'param_name' => 'item_padding_768_1024',
							'value' => '',
							'description' =>  esc_html__('Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' => esc_html__( 'Width & Responsiveness', 'creator'),
							'heading' =>  esc_html__('Padding on screen size between 600px-768px', 'creator'),
							'param_name' => 'item_padding_600_768',
							'value' => '',
							'description' =>  esc_html__('Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' =>  esc_html__('Width & Responsiveness', 'creator'),
							'heading' => esc_html__( 'Padding on screen size between 480px-600px', 'creator'),
							'param_name' => 'item_padding_480_600',
							'value' => '',
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'creator')
						),
						array(
							'type' => 'textfield',
							'class' => '',
							'group' =>  esc_html__('Width & Responsiveness', 'creator'),
							'heading' => esc_html__( 'Padding on Screen Size Bellow 480px', 'creator'),
							'param_name' => 'item_padding_480',
							'value' => '',
							'description' =>  esc_html__('Please insert padding in format 0px 10px 0px 10px', 'creator')
						)
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'background_color' => '',
			'background_image' => '',
			'item_padding' => '',
			'horizontal_aligment' => '',
			'vertical_alignment' => '',
			'item_padding_1280_1600' => '',
			'item_padding_1024_1280' => '',
			'item_padding_768_1024' => '',
			'item_padding_600_768' => '',
			'item_padding_480_600' => '',
			'item_padding_480' => ''
		);

		$params = shortcode_atts($args, $atts);
		extract($params);
		$params['content']= $content;

		$rand_class = 'eltd-interactive-elements-holder-custom-' . mt_rand(100000,1000000);

		$params['interactive_elements_holder_item_style'] = $this->getInteractiveElementsHolderItemStyle($params);
		$params['interactive_elements_holder_item_content_style'] = $this->getInteractiveElementsHolderItemContentStyle($params);
		$params['interactive_elements_holder_item_content_responsive_style'] = $this->getInteractiveElementsHolderItemContentResponsiveStyle($params);
		$params['interactive_elements_holder_item_class'] = $this->getInteractiveElementsHolderItemClass($params);
		$params['interactive_elements_holder_item_content_class'] = $rand_class;
		$params['elements_holder_item_data'] = $this->getData($params);

		$html = '';
		$html .= creator_elated_get_shortcode_module_template_part('templates/interactive-elements-holder-item-template', 'interactive-elements-holder', '', $params);
		return $html;
	}


	/**
	 * Return Interactive Elements Holder Item style
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveElementsHolderItemStyle($params) {

		$interactive_elements_holder_item_style = array();

		if ($params['background_color'] !== '') {
			$interactive_elements_holder_item_style[] = 'background-color: ' . $params['background_color'];
		}
		if ($params['background_image'] !== '') {
			$interactive_elements_holder_item_style[] = 'background-image: url(' . wp_get_attachment_url($params['background_image']) . ')';
		}
		return implode(';', $interactive_elements_holder_item_style);

	}

	/**
	 * Return Interactive Elements Holder Item Content style
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveElementsHolderItemContentStyle($params) {

		$interactive_elements_holder_item_content_style = array();

		if ($params['item_padding'] !== '') {
			$interactive_elements_holder_item_content_style[] = 'padding: ' . $params['item_padding'];
		}

		return implode(';', $interactive_elements_holder_item_content_style);

	}

	/**
	 * Return Interactive Elements Holder Item Content Responsive style
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveElementsHolderItemContentResponsiveStyle($params) {

		$interactive_elements_holder_item_responsive_style = array();

		if ($params['item_padding_1280_1600'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_1280_1600'] = $params['item_padding_1280_1600'];
		}
		if ($params['item_padding_1024_1280'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_1024_1280'] = $params['item_padding_1024_1280'];
		}
		if ($params['item_padding_768_1024'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_768_1024'] = $params['item_padding_768_1024'];
		}
		if ($params['item_padding_600_768'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_600_768'] = $params['item_padding_600_768'];
		}
		if ($params['item_padding_480_600'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_480_600'] = $params['item_padding_480_600'];
		}
		if ($params['item_padding_480'] !== '') {
			$interactive_elements_holder_item_responsive_style['item_padding_480'] = $params['item_padding_480'];
		}

		return $interactive_elements_holder_item_responsive_style;

	}

	/**
	 * Return Interactive Elements Holder Item classes
	 *
	 * @param $params
	 * @return array
	 */
	private function getInteractiveElementsHolderItemClass($params) {

		$interactive_elements_holder_item_class = array();

		$interactive_elements_holder_item_class[] = 'eltd-interactive-elements-holder-right-item';

		if ($params['vertical_alignment'] !== '') {
			$interactive_elements_holder_item_class[] = 'eltd-vertical-alignment-'. $params['vertical_alignment'];
		}

		if ($params['horizontal_aligment'] !== '') {
			$interactive_elements_holder_item_class[] = 'eltd-horizontal-alignment-'. $params['horizontal_aligment'];
		}


		return implode(' ', $interactive_elements_holder_item_class);

	}

	private function getData($params) {
		$data = array();

		$data['data-item-class'] = $params['interactive_elements_holder_item_content_class'];

		if ($params['item_padding_1280_1600'] !== '') {
			$data['data-1280-1600'] = $params['item_padding_1280_1600'];
		}

		if ($params['item_padding_1024_1280'] !== '') {
			$data['data-1024-1280'] = $params['item_padding_1024_1280'];
		}

		if ($params['item_padding_768_1024'] !== '') {
			$data['data-768-1024'] = $params['item_padding_768_1024'];
		}

		if ($params['item_padding_600_768'] !== '') {
			$data['data-600-768'] = $params['item_padding_600_768'];
		}

		if ($params['item_padding_480_600'] !== '') {
			$data['data-480-600'] = $params['item_padding_480_600'];
		}

		if ($params['item_padding_480'] !== '') {
			$data['data-480'] = $params['item_padding_480'];
		}

		return $data;
	}

}
