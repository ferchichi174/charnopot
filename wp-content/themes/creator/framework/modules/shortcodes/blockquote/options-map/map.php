<?php

if ( ! function_exists('creator_elated_blockquote_options_map') ) {
	/**
	 * Add Blockquote options to elements page
	 */
	function creator_elated_blockquote_options_map() {

		$panel_blockquote = creator_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_blockquote',
				'title' =>esc_html__( 'Blockquote','creator')
			)
		);

	}

	add_action( 'creator_elated_options_elements_map', 'creator_elated_blockquote_options_map');

}