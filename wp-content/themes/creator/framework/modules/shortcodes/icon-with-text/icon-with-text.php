<?php
namespace CreatorElated\Modules\Shortcodes\IconWithText;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class IconWithText
 * @package CreatorElated\Modules\Shortcodes\IconWithText
 */
class IconWithText implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     *
     */
    public function __construct() {
        $this->base = 'eltd_icon_with_text';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     *
     */
    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Elated Icon With Text', 'creator'),
            'base'                      => $this->base,
            'icon'                      => 'icon-wpb-icon-with-text extended-custom-icon',
            'category'                  => 'by ELATED',
            'allowed_container_element' => 'vc_row',
            'params'                    => array_merge(
                creator_elated_icon_collections()->getVCParamsArray(),
                array(
                    array(
                        'type'       => 'attach_image',
                        'heading'    => esc_html__('Custom Icon', 'creator'),
                        'param_name' => 'custom_icon'
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Change Icon on Hover', 'creator'),
                        'param_name'  => 'change_icon_on_hover',
                        'value'       => array(
                                esc_html__('No', 'creator') => 'no',
                                esc_html__( 'Yes', 'creator')  => 'yes'
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'       => 'attach_image',
                        'heading'    => esc_html__('Hover Image', 'creator'),
                        'param_name' => 'icon_hover_image',
                        'description' => esc_html__('This option works only if custom icon is attached', 'creator'),
                        'dependency'  => array(
                            'element' => 'change_icon_on_hover',
                            'value'   => 'yes'
                        )
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Icon Position', 'creator'),
                        'param_name'  => 'icon_position',
                        'value'       => array(
                                esc_html__(  'Top'    , 'creator')         => 'top',
                                esc_html__(  'Left',  'creator')           => 'left',
                                esc_html__(  'Left From Title', 'creator') => 'left-from-title',
                                esc_html__(  'Right' , 'creator')          => 'right'
                        ),
                        'description' =>esc_html__( 'Icon Position', 'creator'),
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     =>esc_html__( 'Icon Layout', 'creator'),
                        'param_name'  => 'icon_layout',
                        'value'       => array(
                                esc_html__(  'Standard' , 'creator')            => 'standard',
                                esc_html__(  'Icon In Background', 'creator')   => 'icon_in_background',
                        ),
                        'description'=>'',
                        'save_always' => true,
                        'admin_label' => true,
                        'dependency' => array(
                            'element' => 'icon_position',
                            'value' => 'top'
                        ),
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Icon Type', 'creator'),
                        'param_name'  => 'icon_type',
                        'value'       => array(
                                esc_html__( 'Normal', 'creator') => 'normal',
                                esc_html__( 'Circle', 'creator') => 'circle',
                                esc_html__( 'Square', 'creator') => 'square'
                        ),
                        'save_always' => true,
                        'admin_label' => true,
                        'group'       => esc_html__('Icon Settings', 'creator'),
                        'description' => esc_html__('This attribute doesn\'t work when Icon Position is Top. In This case Icon Type is Normal', 'creator'),
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Icon Size', 'creator'),
                        'param_name'  => 'icon_size',
                        'value'       => array(
                                esc_html__(    'Tiny' , 'creator')      => 'eltd-icon-tiny',
                                esc_html__(    'Small' , 'creator')     => 'eltd-icon-small',
                                esc_html__(    'Medium', 'creator')     => 'eltd-icon-medium',
                                esc_html__(    'Large'  , 'creator')    => 'eltd-icon-large',
                                esc_html__(    'Very Large', 'creator') => 'eltd-icon-huge'
                        ),
                        'admin_label' => true,
                        'save_always' => true,
                        'group'       => esc_html__('Icon Settings', 'creator'),
                        'description' =>esc_html__( 'This attribute doesn\'t work when Icon Position is Top', 'creator')
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Custom Icon Size (px)', 'creator'),
                        'param_name' => 'custom_icon_size',
                        'group'      => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     =>esc_html__( 'Icon Animation', 'creator'),
                        'param_name'  => 'icon_animation',
                        'value'       => array(
                                esc_html__( 'No', 'creator')  => '',
                                esc_html__( 'Yes', 'creator') => 'yes'
                        ),
                        'group'       =>esc_html__( 'Icon Settings', 'creator'),
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    =>esc_html__( 'Icon Animation Delay (ms)', 'creator'),
                        'param_name' => 'icon_animation_delay',
                        'group'      => esc_html__('Icon Settings', 'creator'),
                        'dependency' => array('element' => 'icon_animation', 'value' => array('yes'))
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     =>esc_html__( 'Icon Margin', 'creator'),
                        'param_name'  => 'icon_margin',
                        'value'       => '',
                        'description' => esc_html__('Margin should be set in a top right bottom left format', 'creator'),
                        'admin_label' => true,
                        'group'       => esc_html__('Icon Settings', 'creator'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     =>esc_html__( 'Shape Size (px)', 'creator'),
                        'param_name'  => 'shape_size',
                        'description' => '',
                        'admin_label' => true,
                        'group'       => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'heading'    => esc_html__('Icon Color', 'creator'),
                        'param_name' => 'icon_color',
                        'group'      => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'heading'    => esc_html__('Icon Hover Color', 'creator'),
                        'param_name' => 'icon_hover_color',
                        'group'      => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     =>esc_html__( 'Icon Background Color', 'creator'),
                        'param_name'  => 'icon_background_color',
                        'description' => esc_html__('Icon Background Color (only for square and circle icon type)', 'creator'),
                        'dependency'  => array('element' => 'icon_type', 'value' => array('square', 'circle')),
                        'group'       => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     =>esc_html__( 'Icon Hover Background Color', 'creator'),
                        'param_name'  => 'icon_hover_background_color',
                        'description' => esc_html__('Icon Hover Background Color (only for square and circle icon type)', 'creator'),
                        'dependency'  => array('element' => 'icon_type', 'value' => array('square', 'circle')),
                        'group'       =>esc_html__( 'Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Icon Border Color', 'creator'),
                        'param_name'  => 'icon_border_color',
                        'description' => esc_html__('Only for Square and Circle Icon type', 'creator'),
                        'dependency'  => array('element' => 'icon_type', 'value' => array('square', 'circle')),
                        'group'       =>esc_html__( 'Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Icon Border Hover Color', 'creator'),
                        'param_name'  => 'icon_border_hover_color',
                        'description' => esc_html__('Only for Square and Circle Icon type', 'creator'),
                        'dependency'  => array('element' => 'icon_type', 'value' => array('square', 'circle')),
                        'group'       => esc_html__('Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     =>esc_html__( 'Border Width', 'creator'),
                        'param_name'  => 'icon_border_width',
                        'description' =>esc_html__( 'Only for Square and Circle Icon type', 'creator'),
                        'dependency'  => array('element' => 'icon_type', 'value' => array('square', 'circle')),
                        'group'       =>esc_html__( 'Icon Settings', 'creator')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title', 'creator'),
                        'param_name'  => 'title',
                        'value'       => '',
                        'admin_label' => true
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => esc_html__('Title Tag', 'creator'),
                        'param_name' => 'title_tag',
                        'value'      => array(
                            ''   => '',
                            'h2' => 'h2',
                            'h3' => 'h3',
                            'h4' => 'h4',
                            'h5' => 'h5',
                            'h6' => 'h6',
                        ),
                        'dependency' => array('element' => 'title', 'not_empty' => true),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'heading'    => esc_html__('Title Color', 'creator'),
                        'param_name' => 'title_color',
                        'dependency' => array('element' => 'title', 'not_empty' => true),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    =>esc_html__( 'Title Text Transform', 'creator'),
                        'param_name' => 'title_text_transform',
                        'default'    =>'uppercase',
                        'save_always'=>true,
                        'value' => creator_elated_get_text_transform_array(true),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title Font Size (px)', 'creator'),
                        'param_name'  => 'title_font_size',
                        'value'       => '',
                        'admin_label' => true,
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    =>esc_html__( 'Title Font Weight', 'creator'),
                        'param_name' => 'title_font_weight',
                        'value'=> creator_elated_get_font_weight_array(),
                        'save_always' => true,
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Title Font Family', 'creator'),
                        'param_name' => 'title_font_family',
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'textarea',
                        'heading'    => esc_html__('Text', 'creator'),
                        'param_name' => 'text'
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'heading'    => esc_html__('Text Color', 'creator'),
                        'param_name' => 'text_color',
                        'dependency' => array('element' => 'text', 'not_empty' => true),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     =>esc_html__( 'Link', 'creator'),
                        'param_name'  => 'link',
                        'value'       => '',
                        'admin_label' => true
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Link Text', 'creator'),
                        'param_name' => 'link_text',
                        'dependency' => array('element' => 'link', 'not_empty' => true)
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    =>esc_html__( 'Target', 'creator'),
                        'param_name' => 'target',
                        'value'      => array(
                            ''      => '',
            esc_html__(  'Self' , 'creator') => '_self',
            esc_html__( 'Blank' ,'creator') => '_blank'
                        ),
                        'dependency' => array('element' => 'link', 'not_empty' => true),
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Text Left Padding (px)', 'creator'),
                        'param_name' => 'text_left_padding',
                        'dependency' => array('element' => 'icon_position', 'value' => array('left')),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Text Padding (px)', 'creator'),
                        'param_name' => 'text_padding',
                        'dependency' => array('element' => 'icon_position', 'value' => array('top')),
                        'group'      => esc_html__('Text Settings', 'creator')
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => esc_html__('Text Right Padding (px)', 'creator'),
                        'param_name' => 'text_right_padding',
                        'dependency' => array('element' => 'icon_position', 'value' => array('right')),
                        'group'      =>esc_html__( 'Text Settings', 'creator')
                    )
                )
            )
        ));
    }

    /**
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $default_atts = array(
            'custom_icon'                 => '',
            'icon_position'               => '',
            'icon_type'                   => '',
            'icon_size'                   => '',
            'custom_icon_size'            => '',
            'icon_animation'              => '',
            'icon_animation_delay'        => '',
            'icon_margin'                 => '',
            'shape_size'                  => '',
            'icon_color'                  => '',
            'icon_hover_color'            => '',
            'icon_background_color'       => '',
            'icon_hover_background_color' => '',
            'icon_border_color'           => '',
            'icon_border_hover_color'     => '',
            'icon_border_width'           => '',
            'title'                       => '',
            'title_tag'                   => 'h5',
            'title_color'                 => '',
            'text'                        => '',
            'text_color'                  => '',
            'link'                        => '',
            'link_text'                   => '',
            'target'                      => '_self',
            'text_left_padding'           => '',
            'text_padding'                => '',
            'text_right_padding'          => '',
            'icon_layout'                 => '',
            'change_icon_on_hover'        => '',
            'icon_hover_image'            => '',
            'title_font_family'           => '',
            'title_text_transform'        => '',
            'title_font_weight'           => '',
            'title_font_size'             => ''
        );

        $default_atts = array_merge($default_atts, creator_elated_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        $params['icon_parameters'] = $this->getIconParameters($params);
        $params['holder_classes']  = $this->getHolderClasses($params);
        $params['title_styles']    = $this->getTitleStyles($params);
        $params['content_styles']  = $this->getContentStyles($params);
        $params['text_styles']     = $this->getTextStyles($params);

        return creator_elated_get_shortcode_module_template_part('templates/iwt', 'icon-with-text', $params['icon_position'], $params);
    }

    /**
     * Returns parameters for icon shortcode as a string
     *
     * @param $params
     *
     * @return array
     */
    private function getIconParameters($params) {
        $params_array = array();

        if(empty($params['custom_icon'])) {
            $iconPackName = creator_elated_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

            $params_array['icon_pack']   = $params['icon_pack'];
            $params_array[$iconPackName] = $params[$iconPackName];

            if(!empty($params['icon_size'])) {
                $params_array['size'] = $params['icon_size'];
            }

            if(!empty($params['custom_icon_size'])) {
                $params_array['custom_size'] = $params['custom_icon_size'];
            }

            if(!empty($params['icon_type'])) {
                $params_array['type'] = $params['icon_type'];
            }

            $params_array['shape_size'] = $params['shape_size'];

            if(!empty($params['icon_border_color'])) {
                $params_array['border_color'] = $params['icon_border_color'];
            }

            if(!empty($params['icon_border_hover_color'])) {
                $params_array['hover_border_color'] = $params['icon_border_hover_color'];
            }

            if(!empty($params['icon_border_width'])) {
                $params_array['border_width'] = $params['icon_border_width'];
            }

            if(!empty($params['icon_background_color'])) {
                $params_array['background_color'] = $params['icon_background_color'];
            }

            if(!empty($params['icon_hover_background_color'])) {
                $params_array['hover_background_color'] = $params['icon_hover_background_color'];
            }

            $params_array['icon_color'] = $params['icon_color'];

            if(!empty($params['icon_hover_color'])) {
                $params_array['hover_icon_color'] = $params['icon_hover_color'];
            }

            if(!empty($params['link'])) {
                $params_array['link'] = $params['link'];
            }

            if(!empty($params['target'])) {
                $params_array['target'] = $params['target'];
            }

            $params_array['icon_animation']       = $params['icon_animation'];
            $params_array['icon_animation_delay'] = $params['icon_animation_delay'];
            $params_array['margin']               = $params['icon_margin'];
        }

        return $params_array;
    }

    /**
     * Returns array of holder classes
     *
     * @param $params
     *
     * @return array
     */
    private function getHolderClasses($params) {
        $classes = array('eltd-iwt');

        if(!empty($params['icon_position'])) {
            switch($params['icon_position']) {
                case 'top':
                    $classes[] = 'eltd-iwt-icon-top';
                    break;
                case 'left':
                    $classes[] = 'eltd-iwt-icon-left';
                    break;
                case 'right':
                    $classes[] = 'eltd-iwt-icon-right';
                    break;
                case 'left-from-title':
                    $classes[] = 'eltd-iwt-left-from-title';
                    break;
                default:
                    break;
            }
        }

        if(!empty($params['icon_hover_image'])){
            $classes[]='eltd-iwt-icon-image-hover';
        }

        if(!empty($params['icon_type'])){
            switch($params['icon_type']){
                case 'circle':
                    $classes[]='eltd-iwt-circle-holder';
                    break;
                case 'square':
                    $classes[]='eltd-iwt-square-holder';
                    break;
                default :
                    break;
            }
        }

        if(!empty($params['icon_hover_image'])){

        }


        if(isset($params['icon_layout']) && $params['icon_layout']!==''){
                switch($params['icon_layout']){
                    case 'icon_in_background':
                        $classes [] = 'eltd-iwt-icon-background-layout';
                        break;
                    default:
                        $classes [] = 'eltd-iwt-standard-layout';
                        break;

                }
        }

        if(!empty($params['icon_size'])) {
            $classes[] = 'eltd-iwt-'.str_replace('eltd-', '', $params['icon_size']);
        }

        return $classes;
    }

    private function getTitleStyles($params) {
        $styles = array();

        if(!empty($params['title_color'])) {
            $styles[] = 'color: '.$params['title_color'];
        }
        if(!empty($params['title_font_family'])) {
            $styles[] = 'font-family: '.$params['title_font_family'];
        }
        if(!empty($params['title_text_transform'])) {
            $styles[] = 'text-transform: '.$params['title_text_transform'];
        }
        if(!empty($params['title_font_size'])) {
            $styles[] = 'font-size: '.creator_elated_filter_px($params['title_font_size']).'px';
        }
        if(!empty($params['title_font_weight'])) {
            $styles[] = 'font-weight: '.$params['title_font_weight'];
        }


        return $styles;
    }

    private function getTextStyles($params) {
        $styles = array();

        if(!empty($params['text_color'])) {
            $styles[] = 'color: '.$params['text_color'];
        }

        return $styles;
    }

    private function getContentStyles($params) {
        $styles = array();

        if($params['icon_position'] == 'left' && !empty($params['text_left_padding'])) {
            $styles[] = 'padding-left: '.creator_elated_filter_px($params['text_left_padding']).'px';
        }

        if($params['icon_position'] == 'right' && !empty($params['text_right_padding'])) {
            $styles[] = 'padding-right: '.creator_elated_filter_px($params['text_right_padding']).'px';
        }
        if($params['icon_position'] == 'top' && !empty($params['text_padding'])) {
            $styles[] = 'padding: '.($params['text_padding']);
        }

        return implode(';',$styles);
    }
}