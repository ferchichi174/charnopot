<?php
namespace CreatorElated\Modules\Shortcodes\Workflow;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * class Workflow
 */
class Workflow implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_workflow';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                    => esc_html__('Workflow', 'creator'),
            'base'                    => $this->base,
            'as_parent'               => array('only' => 'eltd_workflow_item'),
            'content_element'         => true,
            'category'                => 'by ELATED',
            'icon'                    => 'icon-wpb-workflow extended-custom-icon',
            'show_settings_on_create' => true,
            'js_view'                 => 'VcColumnView',
            'params'                  => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Extra class name', 'creator'),
                    'param_name'  => 'el_class',
                    'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'creator')
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__('Workflow line color', 'creator'),
                    'param_name'  => 'line_color',
                    'description' => esc_html__('Pick a color for the workflow line.', 'creator')
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => esc_html__('Animate Workflow', 'creator'),
                    'param_name'  => 'animate',
                    'value'       => array(
                        esc_html__('Yes','creator') => 'yes',
                        esc_html__('No' ,'creator')  => 'no',
                    ),
                    'description' => esc_html__('Animate Workflow shortcode when it comes into viewport', 'creator'),
                    'save_always' => true,
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = (array(
            'el_class' => '',
            'line_color' => '',
            'circle_color' => '',
            'animate' => 'yes',
        ));

        $params = shortcode_atts($default_atts, $atts);
        extract($params);

        $params['workflow_data']   = $this->getWorkflowDataAttr($params);

        $params['el_class']   = $this->getWorkflowClasses($params);
        $params['content']   = $content;
        $output = '';

        $output .= creator_elated_get_shortcode_module_template_part('templates/workflow-holder-template', 'workflow', '', $params);

        return $output;
    }

    /**
     * Generates workflow extra classes
     *
     * @param $params
     *
     * @return string
     */
    private function getWorkflowClasses($params) {

        $el_class = '';
        $class = $params['el_class'];

        if($class !== ''){
            $el_class = $class;
        }

        if($params['animate'] == 'yes'){
            $el_class = 'eltd-workflow-animate';
        }

        return $el_class;
    }


    private function getWorkflowDataAttr($params){
        $data = array();

        if ( ! empty( $params['line_color'] ) ) {
            $data['data-line-color'] = $params['line_color'];
        }

        return $data;
    }



}
