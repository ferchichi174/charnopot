<?php
namespace CreatorElated\Modules\Shortcodes\WorkflowItem;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * class Workflow
 */
class WorkflowItem implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_workflow_item';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            "name"                    => esc_html__('Workflow Item', 'creator'),
            "base"                    => $this->base,
            "as_child"                => array('only' => 'eltd_workflow'),
            "category"                => 'by ELATED',
            "icon"                    => "icon-wpb-workflow-item extended-custom-icon",
            "show_settings_on_create" => true,
            'params'                  => array_merge(
                array(
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title', 'creator'),
                        'param_name'  => 'title',
                        'admin_label' => true,
                        'description' => esc_html__('Enter workflow item title.', 'creator')
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Date', 'creator'),
                        'param_name'  => 'date',
                        'admin_label' => true,
                        'description' => esc_html__('Enter date for workflow item', 'creator')
                    ),
                    array(
                        'type'        => 'textarea',
                        'heading'     => esc_html__('Text', 'creator'),
                        'param_name'  => 'text',
                        'description' => esc_html__('Enter workflow item text.', 'creator')
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => esc_html__('Image', 'creator'),
                        'param_name'  => 'image',
                        'description' => esc_html__('Insert workflow item image.', 'creator')
                    ),
                    array(
                        'type'        => 'checkbox',
                        'heading'     => esc_html__('Set item on right side', 'creator'),
                        'param_name'  => 'image_float',
                        'value'       => array( esc_html__('Make Item Float Right?','creator' )=> 'yes'),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Circle border color', 'creator'),
                        'param_name'  => 'circle_border_color',
                        'description' => esc_html__('Pick a color for the circle border color.', 'creator')
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__('Circle background color', 'creator'),
                        'param_name'  => 'circle_background_color',
                        'description' => esc_html__('Pick a color for the circle background color.', 'creator')
                    ),
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = (array(
            'title' => '',
            'date' => '',
            'text' => '',
            'image' => '',
            'image_float' => '',
            'circle_border_color' => '',
            'circle_background_color' => '',
        ));

        $params       = shortcode_atts($default_atts, $atts);
        $style_params = $this->getStyleProperties($params);
        $params = array_merge($params, $style_params);
        extract($params);

        $params['image_on_right_class'] = $this ->imageOnRightSideClass($params);

        $output = '';
        $output .= creator_elated_get_shortcode_module_template_part('templates/workflow-item-template', 'workflow', '', $params);

        return $output;
    }

    /**
     * Checks if image is set to be on right and set class
     *
     * @param $params
     *
     * @return string
     */
    private function imageOnRightSideClass($params) {

        $class = '';

        if($params['image_float'] == 'yes'){
            $class .= 'reverse';
        }

        return $class;
    }

    /**
     * Generates circle line color
     *
     * @param $params
     *
     * @return array
     */

    private function getStyleProperties($params) {

        $style = array();
        $style['circle_border_color'] = '';
        $style['circle_background_color'] = '';

        if($params['circle_border_color'] !== ''){
            $style['circle_border_color'] = 'border-color:'.$params['circle_border_color'].';';
        }
        if($params['circle_background_color'] !== ''){
            $style['circle_background_color'] = 'background-color:'.$params['circle_background_color'].';';
        }

        return $style;
    }
}
