<div class="eltd-workflow-item">
    <div class="eltd-workflow-item-inner <?php echo esc_attr($image_on_right_class) ?>">
        <span class="circle" style="<?php echo esc_attr($circle_border_color.$circle_background_color); ?>"></span>
        <span class="eltd-workflow-line"></span>
        <div class="eltd-workflow-image">
            <?php if(!empty($image)){
                echo wp_get_attachment_image($image, 'full');
            } ?>
        </div>
        <div class="eltd-workflow-text">
            <?php if(!empty($date)){ ?>
                <p class="date"><?php echo esc_attr($date) ?></p>
            <?php } ?>
            <?php if(!empty($title)){ ?>
                <h5><?php echo esc_attr($title) ?></h5>
            <?php } ?>
            <?php if(!empty($text)){ ?>
                <p class="text"><?php echo esc_attr($text) ?></p>
            <?php } ?>
        </div>
    </div>
</div>