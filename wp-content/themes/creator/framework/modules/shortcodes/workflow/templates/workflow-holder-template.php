<div class="eltd-workflow <?php echo esc_attr($el_class) ?>" <?php echo creator_elated_get_inline_attrs($workflow_data); ?>>
    <?php echo do_shortcode($content) ?>
</div>