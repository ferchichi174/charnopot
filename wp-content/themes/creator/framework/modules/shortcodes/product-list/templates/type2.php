<?php
global $product;
?>
<li class="product ">
	<div class="eltd-product-inner">
		<div class="eltd-product-image-holder">
			<?php
			do_action( 'woocommerce_before_shop_loop_item' );
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			<div class="eltd-product-image-overlay">
				<a class="eltd-product-link" href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>">
					<div class="eltd-product-image-overlay-inner">
						<?php
						do_action( 'creator_elated_woocommerce_product_overlay' );
						?>
					</div>
				</a>
			</div>
		</div>
		<div class="eltd-product-content-holder">
			<div class="eltd-product-title-holder">
				<div class="eltd-product-price">
					<?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
				</div>
				<h5 class="eltd-product-list-product-title"><a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"><?php echo get_the_title();?></a></h5>
			</div>
			<?php
			do_action( 'woocommerce_after_shop_loop_item_title' );
			do_action( 'woocommerce_after_shop_loop_item' );
			?>
		</div>
	</div>
</li>