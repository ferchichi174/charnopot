<?php
$rand_number = rand();
if(is_array($filter_categories) && count($filter_categories)){ ?>

	<div class="eltd-product-list-filter-outer">
		<div class="eltd-product-list-filter-inner">

			<ul class="eltd-product-list-filter">

				<li data-class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" data-filter="*">
					<span>
						<?php echo esc_html__('All', 'creator')?>
					</span>
				</li>

				<?php foreach($filter_categories as $cat){ ?>

					<li data-class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" class="filter filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" data-filter = ".product_cat-<?php echo creator_elated_get_module_part( $cat->slug );  ?>">
						<span>
							<?php echo creator_elated_get_module_part( $cat->name ); ?>
						</span>
					</li>

				<?php } ?>

			</ul>

		</div>
	</div>

<?php }