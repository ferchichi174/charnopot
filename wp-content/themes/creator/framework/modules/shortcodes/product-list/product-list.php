<?php
namespace CreatorElated\Modules\Shortcodes\ProductList;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class ProductList
 */
class ProductList implements ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'eltd_product_list';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );


		//Portfolio category filter
		add_filter( 'vc_autocomplete_eltd_product_list_category_callback', array(
			&$this,
			'productCategoryAutocompleteSuggester',
		), 10, 1 ); // Get suggestion(find). Must return an array

		//Portfolio category render
		add_filter( 'vc_autocomplete_eltd_product_list_category_render', array(
			&$this,
			'productCategoryAutocompleteRender',
		), 10, 1 ); // Get suggestion(find). Must return an array

	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {

		vc_map( array(
			'name'                      => esc_html__( 'Elated Product List', 'creator' ),
			'base'                      => $this->base,
			'category'                  => 'by ELATED',
			'icon'                      => 'icon-wpb-product-list extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array(
				array(
					'type' => 'dropdown',
					'param_name' => esc_html__( 'type','creator' ),
					'heading' => 'Type',
					'value' => array(
						esc_html__( 'Type 1','creator' ) => 'type1',
						esc_html__( 'Type 2','creator' ) => 'type2'
					),
					'save_always'  => true,
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'textfield',
					'param_name' => 'products_per_page',
					'heading' => esc_html__( 'Products per Page','creator' ),
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'column_number',
					'heading' => esc_html__( 'Column Number','creator' ),
					'value' => array(
							esc_html__( 'Two','creator' ) => '2',
							esc_html__( 'Three','creator' ) => '3',
							esc_html__( 'Four','creator' )=> '4',
							esc_html__( 'Five','creator' ) => '5',
							esc_html__( 'Six','creator' ) => '6'
					),
					'save_always'  => true,
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'order_by',
					'heading' => esc_html__( 'Order by','creator' ),
					'value' => array(
							esc_html__( 'Date','creator' ) => 'date',
							esc_html__( 'Author','creator' ) => 'author',
							esc_html__( 'Title','creator' ) => 'title'
					),
					'save_always'  => true,
					'description' => '',
					'admin_label' => true
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'order',
					'heading' =>esc_html__(  'Order','creator' ),
					'value' => array(
						esc_html__( 'Asc','creator' ) => 'asc',
						esc_html__( 'Desc' ,'creator' )=> 'desc'
					),
					'save_always'  => true,
					'description' => '',
					'admin_label' => true
				),
				array(
					'type'        => 'autocomplete',
					'heading'     =>esc_html__(  'One-Category Product List','creator' ),
					'param_name'  => 'category',
					'admin_label' => true,
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'param_name' => 'enable_filter',
					'heading' => esc_html__( 'Enable Filter','creator' ),
					'value'   => array(
							esc_html__( 'No','creator' ) => 'no',
							esc_html__( 'Yes' ,'creator' )=> 'yes'
					),
					'description' => '',
					'admin_label' => true
				)
			)
		) );

	}

	public function render( $atts, $content = null ) {

		$args = array(
			'type'              => '',
			'products_per_page' => '-1',
			'column_number'     => '4',
			'enable_filter'     => '',
			'order_by'          => '',
			'order'             => '',
			'category'          => ''
		);

		$params = shortcode_atts( $args, $atts );
		extract($params);
		$args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'posts_per_page'      => $products_per_page,
			'order_by'            => $order_by,
			'order'               => $order,
			'ignore_sticky_posts' => true
		);

		if(!empty($category)){
			$args['product_cat'] = $category;
		}

		$query = new \WP_Query( $args );

		$classes = $this->getHolderClasses($params);
		$filter_params['filter_categories'] = $this->getFilterCategories($params);
		$inner_classes = $this->getHolderInnerClasses($params);

		$html = '<div class="eltd-product-list-holder woocommerce '.esc_attr($classes).'">';

		if($enable_filter == 'yes'){
			$html .= creator_elated_get_shortcode_module_template_part( 'templates/product-list-filter', 'product-list', '', $filter_params );
		}

		if ( $query->have_posts() ) {
			$html .= '<ul class="eltd-product-list-items products clearfix '.$inner_classes.' ">';

			while ( $query->have_posts() ) {
				$query->the_post();
				$params['product'] = creator_elated_woocommerce_global_product();

				$html .= creator_elated_get_shortcode_module_template_part( 'templates/'.$params['type'] , 'product-list', '', $params);

			}
			wp_reset_postdata();
			$html .= '</ul>';
		} else {

			$html .= esc_html__( 'No products found', 'creator' );

		}
		$html .= '</div>'; //close eltd-product-list-holder

		return $html;
	}

	function getHolderInnerClasses($params){

		$class = 'eltd-type-1';

		if(isset($params['type'])){
			if($params['type'] === 'type2'){
				$class = 'eltd-type-2';
			}
		}

		return $class;

	}

	/**
	 * Generate Column Number css class
	 *
	 * @param $params
	 * @return string
	 */
	private function getHolderClasses($params){

		$class = array();

		if(!empty($params['column_number'])){

			$class[] = 'columns-'.$params['column_number'];

		}

		if(!empty($params['enable_filter']) && $params['enable_filter'] == 'yes'){
			$class[] = 'eltd-product-list-with-filter';
		}

		return implode(' ', $class);

	}

	/**
	 * Generates filter categories array
	 *
	 * @param $params
	 *
	 *
	 *
	 *
	 * * @return array
	 */
	public function getFilterCategories( $params ) {

		$cat_id       = 0;
		$top_category = '';

		if ( ! empty( $params['category'] ) ) {

			$top_category = get_term_by( 'slug', $params['category'], 'product_cat' );
			if ( isset( $top_category->term_id ) ) {
				$cat_id = $top_category->term_id;
			}

		}

		$args = array(
			'child_of' => $cat_id
		);

		$filter_categories = get_terms( 'product_cat', $args );

		return $filter_categories;

	}

	/**
	 * Filter product categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function productCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS product_category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'product_cat' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['product_category_title'] ) > 0 ) ? esc_html__( 'Category', 'creator' ) . ': ' . $value['product_category_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find product by id
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function productCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get product category
			$product_category = get_term_by( 'slug', $query, 'product_cat' );
			if ( is_object( $product_category ) ) {

				$product_category_slug = $product_category->slug;
				$product_category_title = $product_category->name;

				$product_category_title_display = '';
				if ( ! empty( $product_category_title ) ) {
					$product_category_title_display = esc_html__( 'Category', 'creator' ) . ': ' . $product_category_title;
				}

				$data          = array();
				$data['value'] = $product_category_slug;
				$data['label'] = $product_category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}
}