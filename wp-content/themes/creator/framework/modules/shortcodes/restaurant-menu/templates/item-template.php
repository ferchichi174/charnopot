<div class="eltd-rstrnt-item">

	<?php
	if($item_image !== ''){ ?>

		<div class="eltd-rstrnt-item-image">
			<?php echo wp_get_attachment_image( $item_image, array(52,52) ); ?>
		</div>

	<?php } ?>

	<div class="eltd-rstrnt-item-inner">

		<?php if ($title !== '' || $price !== '' ) {?>

			<div class="eltd-rstrnt-title-price-holder">

				<?php if ($title !== '') { ?>

					<<?php echo esc_attr($title_tag);?> class="eltd-rstrnt-title">
						<?php echo esc_html($title); ?>
					</<?php echo esc_attr($title_tag);?>>

				<?php }

				if ($price !== '') { ?>

					<div class="eltd-rstrnt-price-holder">
						<h5 class="eltd-rstrnt-price">
							<span class="eltd-rstrnt-currency"><?php echo esc_html($currency); ?></span>
							<span class="eltd-rstrnt-number"><?php echo esc_html($price); ?></span>
						</h5>
					</div>

				<?php } ?>

			</div>

		<?php } ?>

		<?php if ($description !== '') { ?>
			<p class="eltd-rstrnt-desc"><?php echo esc_html($description); ?></p>
		<?php } ?>
	</div>
</div>