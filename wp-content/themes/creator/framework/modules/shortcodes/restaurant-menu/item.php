<?php
namespace CreatorElated\Modules\Shortcodes\RestaurantItem;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

class RestaurantItem implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'eltd_restaurant_item';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map( 
				array(
					'name' => esc_html__('Elated Price List Item', 'creator'),
					'base' => $this->base,
					'as_child' => array('only' => 'eltd_restaurant_menu'),
					'category' => 'by ELATED',
					'icon' => 'icon-wpb-restaurant-item extended-custom-icon',
					'params' =>	array(
							array(
								'type' => 'textfield',
								'class' => '',
								'heading' =>  esc_html__('Item Title','creator'),
								'param_name' => 'title',
								'value' => '',
								'description' => ''
							),
							array(
								'type'       => 'dropdown',
								'heading'    =>  esc_html__('Title Tag','creator'),
								'param_name' => 'title_tag',
								'value'      => array(
									''   => '',
									'h2' => 'h2',
									'h3' => 'h3',
									'h4' => 'h4',
									'h5' => 'h5',
									'h6' => 'h6',
								),
								'dependency' => array('element' => 'title', 'not_empty' => true)
							),
							array(
								'type' => 'textfield',
								'class' => '',
								'heading' => esc_html__( 'Currency','creator'),
								'param_name' => 'currency',
								'value' => '',
								'description' =>  esc_html__('Default is "$"','creator'),
							),
							array(
								'type' => 'textfield',
								'class' => '',
								'heading' =>  esc_html__('Price','creator'),
								'param_name' => 'price',
								'value' => '',
								'description' => ''
							),
							array(
								'type' => 'attach_image',
								'class' => '',
								'heading' => esc_html__( 'Item Image','creator'),
								'param_name' => 'item_image',
								'value' => '',
								'description' => ''
							),
							array(
								'type' => 'textfield',
								'class' => '',
								'heading' => esc_html__( 'Description','creator'),
								'param_name' => 'description',
								'value' => '',
								'description' => ''
							)
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'title' => '',
			'title_tag' => 'h4',
			'currency' => '$',
			'item_image' => '',
			'price' => '',
			'description' => ''
		);

		$params = shortcode_atts($args, $atts);
		$params['content'] = $content;

		$html = creator_elated_get_shortcode_module_template_part('templates/item-template', 'restaurant-menu', '', $params);

		return $html;
	}
}
