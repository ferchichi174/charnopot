<?php

if ( ! function_exists('creator_elated_team_options_map') ) {
	/**
	 * Add Team options to elements page
	 */
	function creator_elated_team_options_map() {

		$panel_team = creator_elated_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_team',
				'title' =>esc_html__( 'Team','creator')
			)
		);

	}

	add_action( 'creator_elated_options_elements_map', 'creator_elated_team_options_map');

}