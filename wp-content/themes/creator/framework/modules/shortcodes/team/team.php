<?php
namespace CreatorElated\Modules\Shortcodes\Team;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Team
 */
class Team implements ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_team';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see eltd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		$team_social_icons_array = array();
		for ( $x = 1; $x < 6; $x ++ ) {
			$teamIconCollections = creator_elated_icon_collections()->getCollectionsWithSocialIcons();
			foreach ( $teamIconCollections as $collection_key => $collection ) {

				$team_social_icons_array[] = array(
					'type'       => 'dropdown',
					'heading'    =>esc_html__( 'Social Icon ','creator') . $x,
					'param_name' => 'team_social_' . $collection->param . '_' . $x,
					'value'      => $collection->getSocialIconsArrayVC(),
					'dependency' => Array( 'element' => 'team_social_icon_pack', 'value' => array( $collection_key ) )
				);

			}

			$team_social_icons_array[] = array(
				'type'       => 'textfield',
				'heading'    =>esc_html__(  'Social Icon ','creator') . $x . ' Link',
				'param_name' => 'team_social_icon_' . $x . '_link',
				'dependency' => array(
					'element' => 'team_social_icon_pack',
					'value'   => creator_elated_icon_collections()->getIconCollectionsKeys()
				)
			);

			$team_social_icons_array[] = array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Social Icon ','creator') . $x . ' Target',
				'param_name' => 'team_social_icon_' . $x . '_target',
				'value'      => array(
					''      => '',
				esc_html__( 'Self','creator')  => '_self',
				esc_html__( 'Blank' ,'creator')=> '_blank'
				),
				'dependency' => Array( 'element' => 'team_social_icon_' . $x . '_link', 'not_empty' => true )
			);

		}

		vc_map( array(
			'name'                      => esc_html__( 'Elated Team', 'creator' ),
			'base'                      => $this->base,
			'category'                  => 'by ELATED',
			'icon'                      => 'icon-wpb-team extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__( 'Type', 'creator' ),
						'param_name'  => 'team_type',
						'value'       => array(
							esc_html__( 'Main Info on Hover' , 'creator' )   => 'main-info-on-hover',
							esc_html__( 'Main Info Below Image' , 'creator' )=> 'main-info-below-image',
							esc_html__( 'Social Icons On Hover' , 'creator' )=> 'social-icons-on-hover'
						),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Hover Type', 'creator' ),
						'param_name'  => 'hover_type',
						'value'       => array(
							esc_html__( 'Color Overlay', 'creator' ) => 'color_overlay',
							esc_html__( 'Bottom Bar' , 'creator' ) => 'bottom_bar',
							esc_html__( 'Transparent Overlay', 'creator' )  => 'transparent_overlay'
						),
						'save_always' => true,
						'dependency'  => array(
							'element' => 'team_type',
							'value'   => 'main-info-on-hover'
						)
					),
					array(
						'type'        => 'dropdown',
						'heading'     =>esc_html__(  'Change Image on Hover', 'creator' ),
						'param_name'  => 'change_image_on_hover',
						'value'       => array(
							esc_html__( 'No', 'creator' ) => 'no',
							esc_html__( 'Yes', 'creator' )  => 'yes'
						),
						'save_always' => true,
						'dependency'  => array(
							'element' => 'team_type',
							'value'   => 'main-info-below-image'
						)
					),
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Image', 'creator' ),
						'param_name' => 'team_image'
					),
					array(
						'type'       => 'attach_image',
						'heading'    =>esc_html__(  'Hover Image', 'creator' ),
						'param_name' => 'team_hover_image',
						'dependency'  => array(
							'element' => 'change_image_on_hover',
							'value'   => 'yes'
						)
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Image Style', 'creator' ),
						'param_name'  => 'image_style',
						 'value'       => array(
							esc_html__( 'Square', 'creator' ) => 'square',
							esc_html__( 'Round', 'creator' )  => 'round'
						),
						'save_always' => true,
						'dependency'  => array(
							'element' => 'team_type',
							'value'   => 'main-info-below-image'
						)
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Name', 'creator' ),
						'admin_label' => true,
						'param_name'  => 'team_name'
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Position', 'creator' ),
						'admin_label' => true,
						'param_name'  => 'team_position'
					),
					array(
						'type'        => 'textarea',
						'heading'     => esc_html__( 'Description', 'creator' ),
						'admin_label' => true,
						'param_name'  => 'team_description'
					),
					array(
						'type'        => 'textfield',
						'heading'     =>esc_html__(  'Text Padding', 'creator' ),
						'admin_label' => true,
						'param_name'  => 'team_info_padding',
						'dependency'  => array(
							'element' => 'team_type',
							'value'   => 'main-info-below-image'
						)
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Social Icon Pack', 'creator' ),
						'param_name'  => 'team_social_icon_pack',
						'admin_label' => true,
						'value'       => array_merge( array( '' => '' ), creator_elated_icon_collections()->getIconCollectionsVCExclude( 'linea_icons' ) ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Social Icons Type', 'creator' ),
						'param_name'  => 'team_social_icon_type',
						'value'       => array(
							esc_html__( 'Normal', 'creator' ) => 'normal',
							esc_html__( 'Circle', 'creator' ) => 'circle',
							esc_html__( 'Square', 'creator' ) => 'square'
						),
						'save_always' => true,
						'dependency'  => array(
							'element' => 'team_social_icon_pack',
							'value'   => creator_elated_icon_collections()->getIconCollectionsKeys()
						)
					),
				),
				$team_social_icons_array
			)
		) );

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$args = array(
			'team_image'            => '',
			'team_type'             => 'main-info-on-hover',
			'team_name'             => '',
			'team_position'         => '',
			'team_description'      => '',
			'team_social_icon_pack' => '',
			'team_social_icon_type' => 'normal_social',
			'image_style'           => 'square',
			'hover_type'           	=> 'color_overlay',
			'change_image_on_hover' => 'no',
			'team_hover_image'		=> '',
			'team_info_padding'=>''
		);

		$team_social_icons_form_fields = array();
		$number_of_social_icons        = 5;


		for ( $x = 1; $x <= $number_of_social_icons; $x ++ ) {

			foreach ( creator_elated_icon_collections()->iconCollections as $collection_key => $collection ) {
				$team_social_icons_form_fields[ 'team_social_' . $collection->param . '_' . $x ] = '';
			}

			$team_social_icons_form_fields[ 'team_social_icon_' . $x . '_link' ]   = '';
			$team_social_icons_form_fields[ 'team_social_icon_' . $x . '_target' ] = '';

		}

		$args = array_merge( $args, $team_social_icons_form_fields );

		$params = shortcode_atts( $args, $atts );

		$params['number_of_social_icons'] = 5;
		$params['team_social_icons']      = $this->getTeamSocialIcons( $params );
		$params['holder_classes'] = $this->getHolderClasses($params);
		$params['team_info_style'] = $this->getTeamInfoStyle($params);

		//Get HTML from template based on type of team
		$html = creator_elated_get_shortcode_module_template_part( 'templates/' . $params['team_type'], 'team', '', $params );

		return $html;

	}

	private function getHolderClasses($params){
		$classes = array();

		$classes[] = '';

		$teamType = $params['team_type'];

		switch($teamType) {

			case 'main-info-on-hover':
				$classes[] = 'main-info-on-hover';
				$hoverType=$params['hover_type'];
				switch($hoverType) {

					case 'color_overlay':
						$classes[] = 'color-overlay';
						break;
					case 'bottom_bar':
						$classes[] = 'bottom-bar';
						break;
					case 'transparent_overlay':
						$classes[] = 'transparent-overlay';
						break;
					default:
						break;
				}
				break;

			case 'main-info-below-image':
				$classes[] = 'main-info-below-image';
				break;
			case 'social-icons-on-hover':
				$classes [] = 'social-icons-on-hover';
				break;
			default:
				break;
		}

		return implode(' ', $classes);

	}

	private function getTeamInfoStyle($params){
		$style="";

		if (isset($params['team_info_padding']) && $params['team_info_padding']!=='' ){
			$style.='padding:'.$params['team_info_padding'];
		}
		return $style;
	}



	private function getTeamSocialIcons( $params ) {

		extract( $params );
		$social_icons = array();

		if ( $team_social_icon_pack !== '' ) {

			$icon_pack                    = creator_elated_icon_collections()->getIconCollection( $team_social_icon_pack );
			$team_social_icon_type_label  = 'team_social_' . $icon_pack->param;
			$team_social_icon_param_label = $icon_pack->param;

			for ( $i = 1; $i <= $number_of_social_icons; $i ++ ) {

				$team_social_icon   = ${$team_social_icon_type_label . '_' . $i};
				$team_social_link   = ${'team_social_icon_' . $i . '_link'};
				$team_social_target = ${'team_social_icon_' . $i . '_target'};

				if ( $team_social_icon !== '' ) {

					if($team_social_target === ''){
						//validation reasons
						$team_social_target = '_self';
					}

					$team_icon_params                                  = array();
					$team_icon_params['icon_pack']                     = $team_social_icon_pack;
					$team_icon_params[ $team_social_icon_param_label ] = $team_social_icon;
					$team_icon_params['link']                          = ( $team_social_link !== '' ) ? $team_social_link : '';
					$team_icon_params['target']                        = ( $team_social_target !== '' ) ? $team_social_target : '';
					$team_icon_params['type']                          = ( $team_social_icon_type !== '' ) ? $team_social_icon_type : '';

					$social_icons[] = creator_elated_execute_shortcode( 'eltd_icon', $team_icon_params );
				}

			}

		}

		return $social_icons;

	}

}