<?php
/**
 * Team info on hover shortcode template
 */
?>
<div class="eltd-team <?php echo esc_attr( $holder_classes ) ?>">
	<div class="eltd-team-inner">
		<?php if ( $team_image !== '' ) { ?>
			<div class="eltd-team-image">
				<?php echo wp_get_attachment_image( $team_image, 'full' ); ?>
				<div class="eltd-team-info-over">
					<div class="eltd-team-table">
						<div class="eltd-team-cell">
							<?php if ( $team_name !== '' ) { ?>
								<h5 class="eltd-team-name">
									<?php echo esc_html( $team_name ); ?>
								</h5>
							<?php }
							if ( $team_position !== '' ) { ?>
								<h6 class="eltd-team-position"><?php echo esc_html( $team_position ) ?></h6>
							<?php } ?>
							<?php if ( $team_description !== '' ) { ?>
								<div class="eltd-team-description">
									<p><?php echo esc_html( $team_description ) ?></p>
								</div>
							<?php } ?>
							<?php if (!empty( $team_social_icons ) ) { ?>
								<div class="eltd-team-social <?php echo esc_attr( $team_social_icon_type ) ?> ">
									<div class="eltd-team-social-icon-holder clearfix">
									<?php foreach ( $team_social_icons as $team_social_icon ) {
										echo creator_elated_get_module_part( $team_social_icon );
									} ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>