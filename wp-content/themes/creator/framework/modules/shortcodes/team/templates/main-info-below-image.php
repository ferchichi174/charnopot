<?php
/**
 * Team info on hover shortcode template
 */
?>
<div class="eltd-team <?php echo esc_attr( $holder_classes ) ?>">
	<div class="eltd-team-inner">
		<?php if ( $team_image !== '' ) { ?>
			<div class="eltd-team-image <?php echo esc_attr( $image_style ); ?>">
				<?php echo wp_get_attachment_image( $team_image, 'full' ); ?>
			</div>
			<?php if (($change_image_on_hover == 'yes' ) && ($team_hover_image != '')) { ?>
				<div class="eltd-team-image eltd-team-hover-image <?php echo esc_attr( $image_style ); ?>">
					<?php echo wp_get_attachment_image( $team_hover_image, 'full' ); ?>
				</div>
			<?php } ?>
		<?php } ?>

		<div class="eltd-team-info" <?php if($team_info_style!==""){ echo creator_elated_get_inline_style($team_info_style); } ?> >
			<?php if ( $team_name !== '' ) { ?>
				<h5 class="eltd-team-name">
					<?php echo esc_html( $team_name ); ?>
				</h5>
			<?php }
			if ( $team_position !== '' ) { ?>
				<h6 class="eltd-team-position"><?php echo esc_html( $team_position ) ?></h6>
			<?php }
			if ( $team_description !== '' ) { ?>
				<div class="eltd-team-description">
					<p><?php echo esc_html( $team_description ) ?></p>
				</div>
			<?php }

			if(is_array($team_social_icons) && count($team_social_icons)){?>

				<div class="eltd-team-social <?php echo esc_attr( $team_social_icon_type ) ?> ">
					<div class="eltd-team-social-icon-holder clearfix">
						<?php foreach ( $team_social_icons as $team_social_icon ) { ?>
							<span class="eltd-team-icon-holder-inner">
								<span class="eltd-team-icon-item">
									<?php
									echo creator_elated_get_module_part( $team_social_icon );
									?>
								</span>
							</span>

						<?php  } ?>
					</div>
				</div>
			<?php } ?>

		</div>

	</div>
</div>