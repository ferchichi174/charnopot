<?php
/**
 * Counter shortcode template
 */
?>
<div
	class="eltd-counter-holder <?php echo esc_attr( $counter_classes ); ?> " <?php echo creator_elated_get_inline_style( $counter_holder_styles ); ?>>
	<?php if ( $with_icon == 'yes' ) { ?>
		<?php if(!empty($custom_icon)) { ?>
			<div class="eltd-counter-custom-icon"><?php echo wp_get_attachment_image($custom_icon, 'full'); ?></div>
			<?php }
			else { ?>
		<div class="eltd-counter-icon" <?php echo creator_elated_get_inline_style( $icon_style ); ?>>
			<?php echo creator_elated_get_module_part( $icon ); ?>
		</div>
	<?php } 
	} ?>
	<div class="eltd-counter-details">
		<span
			class="eltd-counter <?php echo esc_attr( $type ) ?>" <?php echo creator_elated_get_inline_style( $counter_styles ); ?>>
			<?php echo esc_attr( $digit ); ?>
		</span>
		<h6 class="eltd-counter-title" <?php echo creator_elated_get_inline_style( $title_style ); ?>>
			<?php echo esc_html( $title ); ?>
		</h6>
		<?php if ( $text != "" ) { ?>
			<p class="eltd-counter-text" <?php echo creator_elated_get_inline_style( $text_style ); ?>><?php echo esc_html( $text ); ?></p>
		<?php } ?>
	</div>

</div>