<?php
namespace CreatorElated\Modules\Shortcodes\Counter;

use CreatorElated\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Counter
 */
class Counter implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_counter';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see eltd_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		vc_map( array(
			'name'                      => esc_html__( 'Elated Counter', 'creator' ),
			'base'                      => $this->getBase(),
			'category'                  => 'by ELATED',
			'admin_enqueue_css'         => array( creator_elated_get_skin_uri() . '/assets/css/eltd-vc-extend.css' ),
			'icon'                      => 'icon-wpb-counter extended-custom-icon',
			'allowed_container_element' => 'vc_row',
			'params'                    => array_merge(
				array(
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__( 'Type', 'creator' ),
						'param_name'  => 'type',
						'value'       => array(
							esc_html__( 'Zero Counter', 'creator' )   => 'zero',
							esc_html__( 'Random Counter', 'creator' ) => 'random'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type'        => 'dropdown',
						'heading'     => esc_html__( 'Position', 'creator' ),
						'param_name'  => 'position',
						'value'       => array(
							esc_html__( 'Left'  , 'creator' ) => 'left',
							esc_html__( 'Right', 'creator' )  => 'right',
							esc_html__( 	'Center', 'creator' ) => 'center'
						),
						'save_always' => true,
						'description' => ''
					),

					array(
						'type' => 'dropdown',
						'heading' =>esc_html__(  'Skin', 'creator' ),
						'param_name' => 'skin',
						'value' => array(
							'' => '',
							esc_html__( 'Light' , 'creator' )=> 'light',
							esc_html__( 'Dark', 'creator' ) => 'dark',
						),
						'admin_label' => true,
						'save_always' => true,
					),
					array(
						'type'        => 'dropdown',
						'heading'     =>esc_html__(  'With Icon', 'creator' ),
						'param_name'  => 'with_icon',
						'value'       => array(
							esc_html__( 'Yes', 'creator' ) => 'yes',
							esc_html__( 'No', 'creator' )  => 'no'
						),
						'save_always' => true,
						'admin_label' => true
					),
					array(
						'type'       => 'attach_image',
						'heading'    => esc_html__( 'Custom Icon', 'creator' ),
						'param_name' => 'custom_icon',
						'dependency'  => array( 'element' => 'with_icon', 'value' => array( 'yes' ) ),
					),
					array(
						'type'        => 'dropdown',
						'heading'     =>esc_html__(  'Icon Position', 'creator' ),
						'param_name'  => 'icon_position',
						'value'       => array(
								esc_html__( 'Top', 'creator' ) => 'top',
								esc_html__( 'Left', 'creator' )  => 'left'
						),
						'save_always' => true,
						'admin_label' => true,
						'dependency'  => array( 'element' => 'with_icon', 'value' => array( 'yes' ) ),
					),

				),

				creator_elated_icon_collections()->getVCParamsArray( array(
					'element' => 'with_icon',
					'value'   => array( 'yes' )
				) ),
				array(
					array(
						'type'        => 'textfield',
						'admin_label' => true,
						'heading'     => esc_html__( 'Digit', 'creator' ),
						'param_name'  => 'digit',
						'description' => ''
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Title', 'creator' ),
						'param_name'  => 'title',
						'admin_label' => true,
						'description' => ''
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Text', 'creator' ),
						'param_name'  => 'text',
						'admin_label' => true,
						'description' => ''
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Padding Bottom(px)', 'creator' ),
						'param_name'  => 'padding_bottom',
						'admin_label' => true,
						'description' => '',
						'group'       =>esc_html__(  'Design Options', 'creator' ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Digit Font Size (px)', 'creator' ),
						'param_name'  => 'font_size',
						'description' => '',
						'group'       => esc_html__( 'Design Options', 'creator' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Counter Color', 'creator' ),
						'param_name'  => 'counter_color',
						'admin_label' => true,
						'description' => '',
						'group'       => esc_html__( 'Design Options', 'creator' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Title Color', 'creator' ),
						'param_name'  => 'title_color',
						'admin_label' => true,
						'description' => '',
						'group'       => esc_html__( 'Design Options', 'creator' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     =>esc_html__(  'Icon Color', 'creator' ),
						'param_name'  => 'icon_color',
						'admin_label' => true,
						'description' => '',
						'group'       => esc_html__( 'Design Options', 'creator' ),
					),
					array(
						'type'        => 'colorpicker',
						'heading'     => esc_html__( 'Text Color', 'creator' ),
						'param_name'  => 'text_color',
						'description' => '',
						'admin_label' => true,
						'group'       => esc_html__( 'Design Options', 'creator' ),
					)
				)
			)
		) );

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$args = array(
			'type'            => '',
			'position'        => '',
			'custom_icon'     => '',
			'digit'           => '',
			'underline_digit' => '',
			'title'           => '',
			'font_size'       => '',
			'text'            => '',
			'padding_bottom'  => '',
			'with_icon'       => 'no',
			'skin'       	  => '',
			'counter_color'   => '',
			'title_color'     => '',
			'icon_color'      => '',
			'text_color'      => '',
			'icon_position'=>''


		);

		$args   = array_merge( $args, creator_elated_icon_collections()->getShortcodeParams() );
		$params = shortcode_atts( $args, $atts );

		$params['counter_holder_styles'] = $this->getCounterHolderStyle( $params );
		$params['counter_styles']        = $this->getCounterStyle( $params );
		$params['icon']                  = $this->getCounterIcon( $params );
		$params['icon_style']            = $this->getCounterIconStyle( $params );
		$params['title_style']           = $this->getCounterTitleStyle( $params );
		$params['text_style']            = $this->getCounterTextStyle( $params );
		$params['counter_classes']		= $this->getCounterClasses($params);

		//Get HTML from template
		$html = creator_elated_get_shortcode_module_template_part( 'templates/counter-template', 'counter', '', $params );

		return $html;

	}
	/**
	 * Return Counter holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCounterClasses( $params ) {
		$counterHolderClass = array();

		switch($params['skin']) {
			case 'light':
				$counterHolderClass[] = 'eltd-counter-light';
			break;
			case 'dark':
				$counterHolderClass[] = 'eltd-counter-dark';
				break;
			default:
				break;
		}

		switch($params['position']) {
			case 'left':
				$counterHolderClass[] = 'eltd-counter-left';
				break;
			case 'right':
				$counterHolderClass[] = 'eltd-counter-right';
				break;
			case 'center':
				$counterHolderClass[] = 'eltd-counter-center';
				break;
		}
		switch($params['icon_position']) {
			case 'top':
				$counterHolderClass[] = 'eltd-counter-icon-top';
				break;
			case 'left':
				$counterHolderClass[] = 'eltd-counter-icon-left';
				break;
			default:
				break;
		}



		return implode(' ', $counterHolderClass);
	}

	/**
	 * Return Counter holder styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCounterHolderStyle( $params ) {
		$counterHolderStyle = array();

		if ( $params['padding_bottom'] !== '' ) {

			$counterHolderStyle[] = 'padding-bottom: ' . $params['padding_bottom'] . 'px';

		}

		return implode( ';', $counterHolderStyle );
	}

	/**
	 * Return Counter styles
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getCounterStyle( $params ) {
		$counterStyle = array();

		if ( $params['font_size'] !== '' ) {
			$counterStyle[] = 'font-size: ' . $params['font_size'] . 'px';
		}
		if ( $params['counter_color'] !== '' ) {
			$counterStyle[] = 'color: ' . $params['counter_color'];
		}

		return implode( ';', $counterStyle );
	}

	/**
	 * Return Counter Icon
	 *
	 * @param $params
	 *
	 * @return mixed
	 */
	private function getCounterIcon( $params ) {

			$icon = creator_elated_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

			if ($params['with_icon'] == 'yes') {
				return creator_elated_icon_collections()->renderIcon($params[$icon], $params['icon_pack']);
			}


	}

	private function getCounterIconStyle( $params ) {

		$counterIconStyle = array();

		if ( $params['icon_color'] !== '' ) {
			$counterIconStyle[] = 'color: ' . $params['icon_color'];
		}

		return implode( ';', $counterIconStyle );
	}

	private function getCounterTitleStyle( $params ) {
		$counterTitleStyle = array();

		if ( $params['title_color'] !== '' ) {
			$counterTitleStyle[] = 'color: ' . $params['title_color'];
		}

		return implode( ';', $counterTitleStyle );
	}

	private function getCounterTextStyle( $params ) {
		$counterTextStyle = array();

		if ( $params['text_color'] !== '' ) {
			$counterTextStyle[] = 'color: ' . $params['text_color'];
		}

		return implode( ';', $counterTextStyle );
	}

}