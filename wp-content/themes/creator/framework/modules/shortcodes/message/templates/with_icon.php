<?php
$icon_html = creator_elated_icon_collections()->renderIcon($icon, $icon_pack);
?>

<div class="eltd-message-icon-holder">
	<div class="eltd-message-icon">
		<div class="eltd-message-icon-inner">
			<?php
			echo creator_elated_get_module_part( $icon_html );
			?>			
		</div> 
	</div>	 
</div>

