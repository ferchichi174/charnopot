<?php
/**
 * Custom Font shortcode template
 */
?>

<<?php echo esc_attr($custom_font_tag);?> class="eltd-custom-font-holder" <?php creator_elated_inline_style($custom_font_style);?> <?php echo esc_attr($custom_font_data);?>>
	<?php if($link !== ''){ ?>
		<a href="<?php echo esc_url($link) ?>">
	<?php }
	echo esc_html($content_custom_font);
	if($link !== ''){ ?>
		</a>
	<?php } ?>
</<?php echo esc_attr($custom_font_tag);?>>