<?php

if ( ! function_exists('creator_elated_search_options_map') ) {

	function creator_elated_search_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_search_page',
				'title' =>esc_html__( 'Search','creator'),
				'icon' => 'fa fa-search'
			)
		);

		$search_panel = creator_elated_add_admin_panel(
			array(
				'title' =>esc_html__( 'Search','creator'),
				'name' => 'search',
				'page' => '_search_page'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_icon_pack',
				'default_value'	=> 'font_elegant',
				'label'			=>esc_html__( 'Search Icon Pack','creator'),
				'description'	=>esc_html__( 'Choose icon pack for search icon','creator'),
				'options'		=> creator_elated_icon_collections()->getIconCollections()
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'search_in_grid',
				'default_value'	=> 'yes',
				'label'			=> esc_html__('Search area in grid','creator'),
				'description'	=> esc_html__('Set search area to be in grid','creator'),
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'initial_header_icon_title',
				'title'		=> esc_html__('Initial Search Icon in Header','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'text',
				'name'			=> 'header_search_icon_size',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Icon Size','creator'),
				'description'	=>esc_html__( 'Set size for icon','creator'),
				'args'			=> array(
					'col_width' => 3,
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_color_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=>esc_html__( 'Icon Colors','creator'),
				'description'	=> esc_html__('Define color style for icon','creator'),
				'name'		=> 'search_icon_color_group'
			)
		);

		$search_icon_color_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'	=> $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_color',
				'label'		=> esc_html__('Color','creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_hover_color',
				'label'		=>esc_html__( 'Hover Color','creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_light_search_icon_color',
				'label'		=>esc_html__( 'Light Header Icon Color','creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_light_search_icon_hover_color',
				'label'		=> esc_html__('Light Header Icon Hover Color','creator')
			)
		);

		$search_icon_color_row2 = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row2',
				'next'		=> true
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $search_icon_color_row2,
				'type'		=> 'colorsimple',
				'name'		=> 'header_dark_search_icon_color',
				'label'		=> esc_html__('Dark Header Icon Color','creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'parent' => $search_icon_color_row2,
				'type'		=> 'colorsimple',
				'name'		=> 'header_dark_search_icon_hover_color',
				'label'		=> esc_html__('Dark Header Icon Hover Color','creator')
			)
		);


		$search_icon_background_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=>esc_html__( 'Icon Background Style','creator'),
				'description'	=>esc_html__( 'Define background style for icon','creator'),
				'name'		=> 'search_icon_background_group'
			)
		);

		$search_icon_background_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_icon_background_group,
				'name'		=> 'search_icon_background_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_background_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_background_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_background_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_background_hover_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Hover Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'enable_search_icon_text',
				'default_value'	=> 'yes',
				'label'			=>esc_html__( 'Enable Search Icon Text','creator'),
				'description'	=> esc_html__("Enable this option to show 'Search' text next to search icon in header",'creator'),
				'args'			=> array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_enable_search_icon_text_container'
				)
			)
		);

		$enable_search_icon_text_container = creator_elated_add_admin_container(
			array(
				'parent'			=> $search_panel,
				'name'				=> 'enable_search_icon_text_container',
				'hidden_property'	=> 'enable_search_icon_text',
				'hidden_value'		=> 'no'
			)
		);

		$enable_search_icon_text_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $enable_search_icon_text_container,
				'title'		=> esc_html__('Search Icon Text','creator'),
				'name'		=> 'enable_search_icon_text_group',
				'description'	=> esc_html__('Define Style for Search Icon Text','creator')
			)
		);

		$enable_search_icon_text_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color',
				'label'			=>esc_html__( 'Text Color','creator'),
				'default_value'	=> ''
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color_hover',
				'label'			=> esc_html__('Text Hover Color','creator'),
				'default_value'	=> ''
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_fontsize',
				'label'			=> esc_html__('Font Size','creator'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_lineheight',
				'label'			=> esc_html__('Line Height','creator'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$enable_search_icon_text_row2 = creator_elated_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row2',
				'next'		=> true
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_texttransform',
				'label'			=> esc_html__('Text Transform','creator'),
				'default_value'	=> '',
				'options'		=> creator_elated_get_text_transform_array()
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_icon_text_google_fonts',
				'label'			=> esc_html__('Font Family','creator'),
				'default_value'	=> '-1',
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_fontstyle',
				'label'			=> esc_html__('Font Style','creator'),
				'default_value'	=> '',
				'options'		=> creator_elated_get_font_style_array(),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_fontweight',
				'label'			=> esc_html__('Font Weight','creator'),
				'default_value'	=> '',
				'options'		=> creator_elated_get_font_weight_array(),
			)
		);

		$enable_search_icon_text_row3 = creator_elated_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row3',
				'next'		=> true
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row3,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_letterspacing',
				'label'			=>esc_html__( 'Letter Spacing','creator'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_spacing_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Spacing','creator'),
				'description'	=>esc_html__( 'Define padding and margins for Search icon','creator'),
				'name'		=> 'search_icon_spacing_group'
			)
		);

		$search_icon_spacing_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_icon_spacing_group,
				'name'		=> 'search_icon_spacing_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_padding_left',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Padding Left','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_padding_right',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Padding Right','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_margin_left',
				'default_value'	=> '',
				'label'			=> esc_html__('Margin Left','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_spacing_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_margin_right',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Margin Right','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'search_form_title',
				'title'		=>esc_html__( 'Search Bar','creator')
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'color',
				'name'			=> 'search_background_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Background Color','creator'),
				'description'	=> esc_html__('Choose a background color for Select search bar','creator')
			)
		);

		$search_input_text_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Input Text','creator'),
				'description'	=> esc_html__('Define style for search text','creator'),
				'name'		=> 'search_input_text_group'
			)
		);

		$search_input_text_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_input_text_group,
				'name'		=> 'search_input_text_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_text_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Text Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_text_disabled_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Disabled Text Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_text_fontsize',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Size','creator'),
				'args'			=> array(
					'suffix' => 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_texttransform',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Text Transform','creator'),
				'options'		=> creator_elated_get_text_transform_array()
			)
		);

		$search_input_text_row2 = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_input_text_group,
				'name'		=> 'search_input_text_row2'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_text_google_fonts',
				'default_value'	=> '-1',
				'label'			=>esc_html__( 'Font Family','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_fontstyle',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Style','creator'),
				'options'		=> creator_elated_get_font_style_array(),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_text_fontweight',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Weight','creator'),
				'options'		=> creator_elated_get_font_weight_array()
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_input_text_row2,
				'type'			=> 'textsimple',
				'name'			=> 'search_text_letterspacing',
				'default_value'	=> '',
				'label'			=> esc_html__('Letter Spacing','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_label_text_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Label Text','creator'),
				'description'	=>esc_html__( 'Define style for search label text','creator'),
				'name'		=> 'search_label_text_group'
			)
		);

		$search_label_text_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_label_text_group,
				'name'		=> 'search_label_text_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_label_text_color',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Text Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_label_text_fontsize',
				'default_value'	=> '',
				'label'			=>esc_html__( 'Font Size','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_label_text_texttransform',
				'default_value'	=> '',
				'label'			=> esc_html__('Text Transform','creator'),
				'options'		=> creator_elated_get_text_transform_array()
			)
		);

		$search_label_text_row2 = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_label_text_group,
				'name'		=> 'search_label_text_row2',
				'next'		=> true
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_label_text_google_fonts',
				'default_value'	=> '-1',
				'label'			=>esc_html__( 'Font Family','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_label_text_fontstyle',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Style','creator'),
				'options'		=> creator_elated_get_font_style_array()
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_label_text_fontweight',
				'default_value'	=> '',
				'label'			=> esc_html__('Font Weight','creator'),
				'options'		=> creator_elated_get_font_weight_array()
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_label_text_row2,
				'type'			=> 'textsimple',
				'name'			=> 'search_label_text_letterspacing',
				'default_value'	=> '',
				'label'			=> esc_html__('Letter Spacing','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_icon_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=>esc_html__( 'Search Icon','creator'),
				'description'	=>esc_html__( 'Define style for search icon','creator'),
				'name'		=> 'search_icon_group'
			)
		);

		$search_icon_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_icon_group,
				'name'		=> 'search_icon_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_hover_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Hover Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_disabled_color',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Disabled Color','creator'),
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_icon_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_size',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Size','creator'),
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_close_icon_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Close','creator'),
				'description'	=> esc_html__('Define style for search close icon','creator'),
				'name'		=> 'search_close_icon_group'
			)
		);

		$search_close_icon_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_close_icon_group,
				'name'		=> 'search_icon_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_close_color',
				'label'			=> esc_html__('Icon Color','creator'),
				'default_value'	=> ''
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_close_hover_color',
				'label'			=>esc_html__( 'Icon Hover Color','creator'),
				'default_value'	=> ''
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_close_icon_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_close_size',
				'label'			=>esc_html__( 'Icon Size','creator'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);

		$search_bottom_border_group = creator_elated_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Search Bottom Border','creator'),
				'description'	=>esc_html__( 'Define style for Search text input bottom border (for Fullscreen search type)','creator'),
				'name'		=> 'search_bottom_border_group'
			)
		);

		$search_bottom_border_row = creator_elated_add_admin_row(
			array(
				'parent'	=> $search_bottom_border_group,
				'name'		=> 'search_icon_row'
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_bottom_border_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_border_color',
				'label'			=> esc_html__('Border Color','creator'),
				'default_value'	=> ''
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent'		=> $search_bottom_border_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_border_focus_color',
				'label'			=> esc_html__('Border Focus Color','creator'),
				'default_value'	=> ''
			)
		);

	}

	add_action('creator_elated_options_map', 'creator_elated_search_options_map', 12);

}