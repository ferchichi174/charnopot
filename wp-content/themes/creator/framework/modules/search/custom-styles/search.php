<?php

if (!function_exists('creator_elated_search_covers_header_style')) {

	function creator_elated_search_covers_header_style()
	{

		if (creator_elated_options()->getOptionValue('search_height') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-header-bottom.eltd-animated .eltd-form-holder-outer, .eltd-search-slide-header-bottom .eltd-form-holder-outer, .eltd-search-slide-header-bottom', array(
				'height' => creator_elated_filter_px(creator_elated_options()->getOptionValue('search_height')) . 'px'
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_covers_header_style');

}

if (!function_exists('creator_elated_search_opener_icon_size')) {

	function creator_elated_search_opener_icon_size()
	{

		if (creator_elated_options()->getOptionValue('header_search_icon_size')) {
			echo creator_elated_dynamic_css('.eltd-search-opener', array(
				'font-size' => creator_elated_filter_px(creator_elated_options()->getOptionValue('header_search_icon_size')) . 'px'
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_opener_icon_size');

}

if (!function_exists('creator_elated_search_opener_icon_colors')) {

	function creator_elated_search_opener_icon_colors()
	{

		if (creator_elated_options()->getOptionValue('header_search_icon_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-opener', array(
				'color' => creator_elated_options()->getOptionValue('header_search_icon_color')
			));
		}

		if (creator_elated_options()->getOptionValue('header_search_icon_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('header_search_icon_hover_color')
			));
		}

		if (creator_elated_options()->getOptionValue('header_light_search_icon_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener,
			.eltd-light-header .eltd-top-bar .eltd-search-opener', array(
				'color' => creator_elated_options()->getOptionValue('header_light_search_icon_color') . ' !important'
			));
		}

		if (creator_elated_options()->getOptionValue('header_light_search_icon_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener:hover,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener:hover,
			.eltd-light-header .eltd-top-bar .eltd-search-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('header_light_search_icon_hover_color') . ' !important'
			));
		}

		if (creator_elated_options()->getOptionValue('header_dark_search_icon_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener,
			.eltd-dark-header .eltd-top-bar .eltd-search-opener', array(
				'color' => creator_elated_options()->getOptionValue('header_dark_search_icon_color') . ' !important'
			));
		}
		if (creator_elated_options()->getOptionValue('header_dark_search_icon_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-search-opener:hover,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-search-opener:hover,
			.eltd-dark-header .eltd-top-bar .eltd-search-opener:hover', array(
				'color' => creator_elated_options()->getOptionValue('header_dark_search_icon_hover_color') . ' !important'
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_opener_icon_colors');

}

if (!function_exists('creator_elated_search_opener_icon_background_colors')) {

	function creator_elated_search_opener_icon_background_colors()
	{

		if (creator_elated_options()->getOptionValue('search_icon_background_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-opener', array(
				'background-color' => creator_elated_options()->getOptionValue('search_icon_background_color')
			));
		}

		if (creator_elated_options()->getOptionValue('search_icon_background_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-opener:hover', array(
				'background-color' => creator_elated_options()->getOptionValue('search_icon_background_hover_color')
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_opener_icon_background_colors');
}

if (!function_exists('creator_elated_search_opener_text_styles')) {

	function creator_elated_search_opener_text_styles()
	{
		$text_styles = array();

		if (creator_elated_options()->getOptionValue('search_icon_text_color') !== '') {
			$text_styles['color'] = creator_elated_options()->getOptionValue('search_icon_text_color');
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_fontsize') !== '') {
			$text_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_icon_text_fontsize')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_lineheight') !== '') {
			$text_styles['line-height'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_icon_text_lineheight')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_texttransform') !== '') {
			$text_styles['text-transform'] = creator_elated_options()->getOptionValue('search_icon_text_texttransform');
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('search_icon_text_google_fonts')) . ', sans-serif';
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_fontstyle') !== '') {
			$text_styles['font-style'] = creator_elated_options()->getOptionValue('search_icon_text_fontstyle');
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_fontweight') !== '') {
			$text_styles['font-weight'] = creator_elated_options()->getOptionValue('search_icon_text_fontweight');
		}

		if (!empty($text_styles)) {
			echo creator_elated_dynamic_css('.eltd-search-icon-text', $text_styles);
		}
		if (creator_elated_options()->getOptionValue('search_icon_text_color_hover') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-opener:hover .eltd-search-icon-text', array(
				'color' => creator_elated_options()->getOptionValue('search_icon_text_color_hover')
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_opener_text_styles');
}

if (!function_exists('creator_elated_search_opener_spacing')) {

	function creator_elated_search_opener_spacing()
	{
		$spacing_styles = array();

		if (creator_elated_options()->getOptionValue('search_padding_left') !== '') {
			$spacing_styles['padding-left'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_padding_left')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_padding_right') !== '') {
			$spacing_styles['padding-right'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_padding_right')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_margin_left') !== '') {
			$spacing_styles['margin-left'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_margin_left')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_margin_right') !== '') {
			$spacing_styles['margin-right'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_margin_right')) . 'px';
		}

		if (!empty($spacing_styles)) {
			echo creator_elated_dynamic_css('.eltd-search-opener', $spacing_styles);
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_opener_spacing');
}

if (!function_exists('creator_elated_search_bar_background')) {

	function creator_elated_search_bar_background()
	{

		if (creator_elated_options()->getOptionValue('search_background_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-header-bottom, .eltd-search-cover, .eltd-search-fade .eltd-fullscreen-search-holder .eltd-fullscreen-search-table, .eltd-fullscreen-search-overlay, .eltd-search-slide-window-top, .eltd-search-slide-window-top input[type="text"]', array(
				'background-color' => creator_elated_options()->getOptionValue('search_background_color')
			));
		}
	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_bar_background');
}

if (!function_exists('creator_elated_search_text_styles')) {

	function creator_elated_search_text_styles()
	{
		$text_styles = array();

		if (creator_elated_options()->getOptionValue('search_text_color') !== '') {
			$text_styles['color'] = creator_elated_options()->getOptionValue('search_text_color');
		}
		if (creator_elated_options()->getOptionValue('search_text_fontsize') !== '') {
			$text_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_text_fontsize')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_text_texttransform') !== '') {
			$text_styles['text-transform'] = creator_elated_options()->getOptionValue('search_text_texttransform');
		}
		if (creator_elated_options()->getOptionValue('search_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('search_text_google_fonts')) . ', sans-serif';
		}
		if (creator_elated_options()->getOptionValue('search_text_fontstyle') !== '') {
			$text_styles['font-style'] = creator_elated_options()->getOptionValue('search_text_fontstyle');
		}
		if (creator_elated_options()->getOptionValue('search_text_fontweight') !== '') {
			$text_styles['font-weight'] = creator_elated_options()->getOptionValue('search_text_fontweight');
		}
		if (creator_elated_options()->getOptionValue('search_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo creator_elated_dynamic_css('.eltd-search-slide-header-bottom input[type="text"], .eltd-search-cover input[type="text"], .eltd-fullscreen-search-holder .eltd-search-field, .eltd-search-slide-window-top input[type="text"]', $text_styles);
		}
		if (creator_elated_options()->getOptionValue('search_text_disabled_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-header-bottom.eltd-disabled input[type="text"]::-webkit-input-placeholder, .eltd-search-slide-header-bottom.eltd-disabled input[type="text"]::-moz-input-placeholder', array(
				'color' => creator_elated_options()->getOptionValue('search_text_disabled_color')
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_text_styles');
}

if (!function_exists('creator_elated_search_label_styles')) {

	function creator_elated_search_label_styles()
	{
		$text_styles = array();

		if (creator_elated_options()->getOptionValue('search_label_text_color') !== '') {
			$text_styles['color'] = creator_elated_options()->getOptionValue('search_label_text_color');
		}
		if (creator_elated_options()->getOptionValue('search_label_text_fontsize') !== '') {
			$text_styles['font-size'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_label_text_fontsize')) . 'px';
		}
		if (creator_elated_options()->getOptionValue('search_label_text_texttransform') !== '') {
			$text_styles['text-transform'] = creator_elated_options()->getOptionValue('search_label_text_texttransform');
		}
		if (creator_elated_options()->getOptionValue('search_label_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = creator_elated_get_formatted_font_family(creator_elated_options()->getOptionValue('search_label_text_google_fonts')) . ', sans-serif';
		}
		if (creator_elated_options()->getOptionValue('search_label_text_fontstyle') !== '') {
			$text_styles['font-style'] = creator_elated_options()->getOptionValue('search_label_text_fontstyle');
		}
		if (creator_elated_options()->getOptionValue('search_label_text_fontweight') !== '') {
			$text_styles['font-weight'] = creator_elated_options()->getOptionValue('search_label_text_fontweight');
		}
		if (creator_elated_options()->getOptionValue('search_label_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = creator_elated_filter_px(creator_elated_options()->getOptionValue('search_label_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo creator_elated_dynamic_css('.eltd-fullscreen-search-holder .eltd-search-label', $text_styles);
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_label_styles');
}

if (!function_exists('creator_elated_search_icon_styles')) {

	function creator_elated_search_icon_styles()
	{

		if (creator_elated_options()->getOptionValue('search_icon_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top > i, .eltd-search-slide-header-bottom .eltd-search-submit i, .eltd-fullscreen-search-holder .eltd-search-submit', array(
				'color' => creator_elated_options()->getOptionValue('search_icon_color')
			));
		}
		if (creator_elated_options()->getOptionValue('search_icon_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top > i:hover, .eltd-search-slide-header-bottom .eltd-search-submit i:hover, .eltd-fullscreen-search-holder .eltd-search-submit:hover', array(
				'color' => creator_elated_options()->getOptionValue('search_icon_hover_color')
			));
		}
		if (creator_elated_options()->getOptionValue('search_icon_disabled_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-header-bottom.eltd-disabled .eltd-search-submit i, .eltd-search-slide-header-bottom.eltd-disabled .eltd-search-submit i:hover', array(
				'color' => creator_elated_options()->getOptionValue('search_icon_disabled_color')
			));
		}
		if (creator_elated_options()->getOptionValue('search_icon_size') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top > i, .eltd-search-slide-header-bottom .eltd-search-submit i, .eltd-fullscreen-search-holder .eltd-search-submit', array(
				'font-size' => creator_elated_filter_px(creator_elated_options()->getOptionValue('search_icon_size')) . 'px'
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_icon_styles');
}

if (!function_exists('creator_elated_search_close_icon_styles')) {

	function creator_elated_search_close_icon_styles()
	{

		if (creator_elated_options()->getOptionValue('search_close_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top .eltd-search-close i, .eltd-search-cover .eltd-search-close i, .eltd-fullscreen-search-close i', array(
				'color' => creator_elated_options()->getOptionValue('search_close_color')
			));
		}
		if (creator_elated_options()->getOptionValue('search_close_hover_color') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top .eltd-search-close i:hover, .eltd-search-cover .eltd-search-close i:hover, .eltd-fullscreen-search-close i:hover', array(
				'color' => creator_elated_options()->getOptionValue('search_close_hover_color')
			));
		}
		if (creator_elated_options()->getOptionValue('search_close_size') !== '') {
			echo creator_elated_dynamic_css('.eltd-search-slide-window-top .eltd-search-close i, .eltd-search-cover .eltd-search-close i, .eltd-fullscreen-search-close i', array(
				'font-size' => creator_elated_filter_px(creator_elated_options()->getOptionValue('search_close_size')) . 'px'
			));
		}

	}

	add_action('creator_elated_style_dynamic', 'creator_elated_search_close_icon_styles');
}

?>
