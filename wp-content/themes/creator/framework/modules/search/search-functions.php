<?php

if( !function_exists('creator_elated_search_body_class') ) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function creator_elated_search_body_class($classes) {

		if ( is_active_widget( false, false, 'eltd_search_opener' ) ) {

			$classes[] = 'eltd-fullscreen-search eltd-search-fade';

		}
		return $classes;

	}

	add_filter('body_class', 'creator_elated_search_body_class');
}

if ( ! function_exists('creator_elated_get_search') ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function creator_elated_get_search() {

		if ( creator_elated_active_widget( false, false, 'eltd_search_opener' ) ) {

			$search_type = creator_elated_options()->getOptionValue('search_type');

			if ($search_type == 'search-covers-header') {
				creator_elated_set_position_for_covering_search();
				return;
			} 

			creator_elated_load_search_template();

		}
	}

}

if ( ! function_exists('creator_elated_set_position_for_covering_search') ) {
	/**
	 * Finds part of header where search template will be loaded
	 */
	function creator_elated_set_position_for_covering_search() {

		$containing_sidebar = creator_elated_active_widget( false, false, 'eltd_search_opener' );

		foreach ($containing_sidebar as $sidebar) {

			if ( strpos( $sidebar, 'top-bar' ) !== false ) {
				add_action( 'creator_elated_after_header_top_html_open', 'creator_elated_load_search_template');
			} else if ( strpos( $sidebar, 'main-menu' ) !== false ) {
				add_action( 'creator_elated_after_header_menu_area_html_open', 'creator_elated_load_search_template');
			} else if ( strpos( $sidebar, 'mobile-logo' ) !== false ) {
				add_action( 'creator_elated_after_mobile_header_html_open', 'creator_elated_load_search_template');
			} else if ( strpos( $sidebar, 'logo' ) !== false ) {
				add_action( 'creator_elated_after_header_logo_area_html_open', 'creator_elated_load_search_template');
			} else if ( strpos( $sidebar, 'sticky' ) !== false ) {
				add_action( 'creator_elated_after_sticky_menu_html_open', 'creator_elated_load_search_template');
			}

		}

	}

}

if ( ! function_exists('creator_elated_set_search_position_in_menu') ) {
	/**
	 * Finds part of header where search template will be loaded
	 */
	function creator_elated_set_search_position_in_menu( $type ) {

		add_action( 'creator_elated_after_header_menu_area_html_open', 'creator_elated_load_search_template');
		if ( $type == 'search-slides-from-header-bottom' ) {
			add_action( 'creator_elated_after_sticky_menu_html_open', 'creator_elated_load_search_template');
		}

	}
}

if ( ! function_exists('creator_elated_set_search_position_mobile') ) {
	/**
	 * Hooks search template to mobile header
	 */
	function creator_elated_set_search_position_mobile() {

		add_action( 'creator_elated_after_mobile_header_html_open', 'creator_elated_load_search_template');

	}

}

if ( ! function_exists('creator_elated_load_search_template') ) {
	/**
	 * Loads HTML template with parameters
	 */
	function creator_elated_load_search_template() {

		$search_type = 'fullscreen-search';

		$search_icon = '';
		$search_icon_close = '';
		if ( creator_elated_options()->getOptionValue('search_icon_pack') !== '' ) {
			$search_icon = creator_elated_icon_collections()->getSearchIcon( creator_elated_options()->getOptionValue('search_icon_pack'), true );
			$search_icon_close = creator_elated_icon_collections()->getSearchClose( creator_elated_options()->getOptionValue('search_icon_pack'), true );
		}

		$parameters = array(
			'search_in_grid'		=> creator_elated_options()->getOptionValue('search_in_grid') == 'yes' ? true : false,
			'search_icon'			=> $search_icon,
			'search_icon_close'		=> $search_icon_close
		);

		creator_elated_get_module_template_part( 'templates/types/'.$search_type, 'search', '', $parameters );

	}

}