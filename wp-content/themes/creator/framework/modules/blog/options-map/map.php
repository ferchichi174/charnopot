<?php

if ( ! function_exists('creator_elated_blog_options_map') ) {

	function creator_elated_blog_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_blog_page',
				'title' => esc_html__('Blog', 'creator'),
				'icon' => 'fa fa-files-o'
			)
		);

		/**
		 * Blog Lists
		 */

		$custom_sidebars = creator_elated_get_custom_sidebars();

		$panel_blog_lists = creator_elated_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_lists',
				'title' => esc_html__('Blog Lists', 'creator')
			)
		);

		creator_elated_add_admin_field(array(
			'name'        => 'blog_list_type',
			'type'        => 'select',
			'label'       => esc_html__('Blog Layout for Archive Pages', 'creator'),
			'description' => esc_html__('Choose a default blog layout', 'creator'),
			'default_value' => 'standard',
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'standard'				=> esc_html__('Blog: Standard', 'creator'),
				'two-columns'			=> esc_html__('Blog: Two Columns', 'creator'),
				'three-columns'			=> esc_html__('Blog: Three Columns', 'creator'),
				'pinboard' 				=> esc_html__('Blog: Pinboard', 'creator'),
				'masonry' 				=> esc_html__('Blog: Masonry', 'creator'),
				'masonry-full-width' 	=> esc_html__('Blog: Masonry Full Width', 'creator'),
				'masonry-gallery' 	    => esc_html__('Blog: Masonry Gallery', 'creator'),
				'chequered' 	        => esc_html__('Blog: Chequered', 'creator'),
				'masonry-gallery-full-width' => esc_html__('Blog: Masonry Gallery Full Width', 'creator'),
				'standard-whole-post' 	=> esc_html__('Blog: Standard Whole Post', 'creator')
			)
		));

		creator_elated_add_admin_field(array(
			'name'        => 'archive_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Archive and Category Sidebar', 'creator'),
			'description' => esc_html__('Choose a sidebar layout for archived Blog Post Lists and Category Blog Lists', 'creator'),
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar', 'creator'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'creator'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'creator'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'creator'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'creator')
			)
		));


		if(count($custom_sidebars) > 0) {
			creator_elated_add_admin_field(array(
				'name' => 'blog_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'creator'),
				'description' => esc_html__('Choose a sidebar to display on Blog Post Lists and Category Blog Lists. Default sidebar is "Sidebar Page"', 'creator'),
				'parent' => $panel_blog_lists,
				'options' => creator_elated_get_custom_sidebars()
			));
		}

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'pagination',
				'default_value' => 'yes',
				'label' => esc_html__('Pagination', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display pagination links on bottom of Blog Post List', 'creator'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_eltd_pagination_container'
				)
			)
		);

		$pagination_container = creator_elated_add_admin_container(
			array(
				'name' => 'eltd_pagination_container',
				'hidden_property' => 'pagination',
				'hidden_value' => 'no',
				'parent' => $panel_blog_lists,
			)
		);

		creator_elated_add_admin_field(
			array(
				'parent' => $pagination_container,
				'type' => 'text',
				'name' => 'blog_page_range',
				'default_value' => '',
				'label' => esc_html__('Pagination Range limit', 'creator'),
				'description' => esc_html__('Enter a number that will limit pagination to a certain range of links', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'selectblank',
				'name' => 'pagination_type',
				'default_value' => 'standard_pagination',
				'label' => esc_html__('Pagination Type', 'creator'),
				'parent' => $pagination_container,
				'description' => esc_html__('Choose Pagination Type', 'creator'),
				'options' => array(
					'standard_paginaton' => esc_html__('Standard Pagination', 'creator'),
					'load_more_pagination' => esc_html__('Load More', 'creator')
				),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'masonry_filter',
				'default_value' => 'no',
				'label' => esc_html__('Masonry Filter', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display category filter on Masonry and Masonry Full Width Templates', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_section_title(array(
			'name' => 'standard_list_section_title',
			'title' => esc_html__('Standard Type', 'creator'),
			'parent' => $panel_blog_lists
		));


		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'standard_default_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Default Post Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'standard_split_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Split Post Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'standard_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Standard List Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'standard-whole-post_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Standard Whole Post List Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);

		creator_elated_add_admin_section_title(array(
			'name' => 'two_columns_section_title',
			'title' => esc_html__('Two Columns Type', 'creator'),
			'parent' => $panel_blog_lists
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'two_columns_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'two-columns_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);
		creator_elated_add_admin_section_title(array(
			'name' => 'three_columns_section_title',
			'title' => esc_html__('Three Columns Type', 'creator'),
			'parent' => $panel_blog_lists
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'three_columns_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'three-columns_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);

		creator_elated_add_admin_section_title(array(
			'name' => 'masonry_section_title',
			'title' => esc_html__('Masonry Type', 'creator'),
			'parent' => $panel_blog_lists
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'masonry_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'masonry_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);
		creator_elated_add_admin_section_title(array(
			'name' => 'masonry_full_width_section_title',
			'title' => esc_html__('Masonry Full Width Type', 'creator'),
			'parent' => $panel_blog_lists
		));
		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'masonry_full_width_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'masonry-full-width_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);
		creator_elated_add_admin_section_title(array(
			'name' => 'masonry_gallery_section_title',
			'title' => esc_html__('Masonry Gallery Type', 'creator'),
			'parent' => $panel_blog_lists
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'masonry_gallery_number_of_chars',
				'default_value' => '30',
				'label' => esc_html__('Number of Words in Excerpt', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'creator'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_section_title(array(
			'name' => 'pinboard_section_title',
			'title' => esc_html__('Pinboard Type', 'creator'),
			'parent' => $panel_blog_lists
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'pinboard_list_enable_share',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);

		/**
		 * Blog Single
		 */
		$panel_blog_single = creator_elated_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_single',
				'title' => esc_html__('Blog Single', 'creator')
			)
		);

		creator_elated_add_admin_field(array(
			'name'          => 'blog_single_padding_meta',
			'type'          => 'text',
			'label'         => esc_html__('Padding on Single Pages', 'creator'),
			'description'   => esc_html__('Insert padding in format 10px 10px 10px 10px', 'creator'),
			'parent'        => $panel_blog_single,
			'default_value' => ''
		));
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_enable_share',
				'default_value' => 'no',
				'label' => esc_html__('Enable Social Share', 'creator'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enabling this option will display social share icons in each post.
								Note that you need to enable social share in Elated Options->Social Networks', 'creator')
			)
		);

		creator_elated_add_admin_field(array(
			'name'        => 'blog_single_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout', 'creator'),
			'description' => esc_html__('Choose a sidebar layout for Blog Single pages', 'creator'),
			'parent'      => $panel_blog_single,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar', 'creator'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'creator'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'creator'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'creator'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'creator')
			),
			'default_value'	=> 'default'
		));


		if(count($custom_sidebars) > 0) {
			creator_elated_add_admin_field(array(
				'name' => 'blog_single_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'creator'),
				'description' => esc_html__('Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"', 'creator'),
				'parent' => $panel_blog_single,
				'options' => creator_elated_get_custom_sidebars()
			));
		}

		creator_elated_add_admin_field(array(
			'name'          => 'blog_single_title_in_title_area',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Post Title in Title Area', 'creator'),
			'description'   => esc_html__('Enabling this option will show post title in title area on single post pages', 'creator'),
			'parent'        => $panel_blog_single,
			'default_value' => 'no'
		));

		creator_elated_add_admin_field(array(
			'name'          => 'blog_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'creator'),
			'description'   => esc_html__('Enabling this option will show comments on your page.', 'creator'),
			'parent'        => $panel_blog_single,
			'default_value' => 'yes'
		));

		creator_elated_add_admin_field(array(
			'name'			=> 'blog_single_related_posts',
			'type'			=> 'yesno',
			'label'			=> esc_html__('Show Related Posts', 'creator'),
			'description'   => esc_html__('Enabling this option will show related posts on your single post.', 'creator'),
			'parent'        => $panel_blog_single,
			'default_value' => 'yes'
		));

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_navigation',
				'default_value' => 'yes',
				'label' => esc_html__('Enable Prev/Next Single Post Navigation Links', 'creator'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enable navigation links through the blog posts (left and right arrows will appear)', 'creator'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_eltd_blog_single_navigation_container'
				)
			)
		);

		$blog_single_navigation_container = creator_elated_add_admin_container(
			array(
				'name' => 'eltd_blog_single_navigation_container',
				'hidden_property' => 'blog_single_navigation',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_navigation_through_same_category',
				'default_value' => 'no',
				'label'       => esc_html__('Enable Navigation Only in Current Category', 'creator'),
				'description' => esc_html__('Limit your navigation only through current category', 'creator'),
				'parent'      => $blog_single_navigation_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_author_info',
				'default_value' => 'yes',
				'label' => esc_html__('Show Author Info Box', 'creator'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enabling this option will display author name and descriptions on Blog Single pages', 'creator'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_eltd_blog_single_author_info_container'
				)
			)
		);

		$blog_single_author_info_container = creator_elated_add_admin_container(
			array(
				'name' => 'eltd_blog_single_author_info_container',
				'hidden_property' => 'blog_author_info',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_author_info_email',
				'default_value' => 'no',
				'label'       => esc_html__('Show Author Email', 'creator'),
				'description' => esc_html__('Enabling this option will show author email', 'creator'),
				'parent'      => $blog_single_author_info_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

	}

	add_action( 'creator_elated_options_map', 'creator_elated_blog_options_map', 18);

}