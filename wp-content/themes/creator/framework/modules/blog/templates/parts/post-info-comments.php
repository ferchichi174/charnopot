<div class="eltd-post-info-comments-holder">
	<span aria-hidden="true" class="icon_comment_alt eltd-blog-icon"></span>
	<a class="eltd-post-info-comments" href="<?php comments_link(); ?>" target="_self">
		<?php comments_number('0 ' . esc_html__('Comments','creator'), '1 '.esc_html__('Comment','creator'), '% '.esc_html__('Comments','creator') ); ?>
	</a>
</div>