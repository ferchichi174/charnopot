<div class="eltd-post-info-category">
	<?php
	$categories = get_the_category();
	foreach ( $categories as $cat ) {?>

		<?php echo creator_elated_get_single_category_html($cat->term_id) ?>
		<a href="<?php echo esc_url(get_category_link($cat->term_id)) ?>">
			<span>
				<?php echo esc_html($cat->name); ?>
			</span>
		</a>

	<?php }
	?>
</div>