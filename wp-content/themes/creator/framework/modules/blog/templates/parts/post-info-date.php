<div class="eltd-post-info-date">
	<span aria-hidden="true" class="icon_calendar eltd-blog-icon"></span>
	<?php if(!creator_elated_post_has_title()) { ?>
		<a href="<?php the_permalink() ?>">
	<?php }

		the_time(get_option('date_format'));

	if(!creator_elated_post_has_title()) { ?>
		</a>
	<?php } ?>
</div>