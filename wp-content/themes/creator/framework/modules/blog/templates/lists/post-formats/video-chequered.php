<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="eltd-post-image-holder">
        <?php creator_elated_get_module_template_part('templates/lists/parts/image', 'blog'); ?>
        <div class="eltd-play-btn"><i class="eltd-icon-linea-icon eltd-icon-linea-icon icon-music-play-button eltd-icon-element "></i></div>

        <div class="eltd-post-text">

            <div class="eltd-post-info eltd-top-section">
                <?php creator_elated_post_info(array(
                    'date' => 'yes',
                    'category' => 'yes'
                )) ?>
            </div>

            <?php

            creator_elated_get_module_template_part('templates/lists/parts/title', 'blog');

            $args_pages = array(
                'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
                'after'            => '</div></div>',
                'link_before'      => '<span>',
                'link_after'       => '</span>',
                'pagelink'         => '%'
            );

            wp_link_pages($args_pages);?>


        </div>
    </div>

</article>