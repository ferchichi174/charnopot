<?php $params = array(	'title_tag' => 'h4');?>

<article id="post-<?php the_ID(); ?>" <?php post_class($pinboard_class); ?>>

	<div class="eltd-post-image-holder" <?php echo creator_elated_get_inline_style($background_image_style) ?>>

		<div class="eltd-post-text">
			<div class="eltd-post-text-inner">

				<div class="eltd-post-info eltd-top-section">
					<?php creator_elated_post_info(array(
						'author' => 'yes',
						'share' => $social_share_flag,
					)) ?>
				</div>

				<?php
				creator_elated_get_module_template_part('templates/lists/parts/title', 'blog', '', $params);
				creator_elated_read_more_button('' , 'eltd-pinboard-read-more-button');
				?>

			</div>
		</div>
	</div>

	<a class = "eltd-blog-pinboard-overlay" href="<?php echo get_the_permalink(); ?>"></a>

</article>