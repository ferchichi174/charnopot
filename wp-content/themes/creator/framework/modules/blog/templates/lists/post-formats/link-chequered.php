<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="eltd-post-content" >

        <div class="eltd-post-text">

            <div class="eltd-post-mark">
                <span class="icon_link_alt eltd-link-quote-mark eltd-link-mark"></span>
            </div>

            <?php creator_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>

        </div>

    </div>
</article>