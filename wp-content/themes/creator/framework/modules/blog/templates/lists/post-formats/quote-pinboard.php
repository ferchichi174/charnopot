<?php $params = array(	'title_tag' => 'h4');?>

<article id="post-<?php the_ID(); ?>" <?php post_class($pinboard_class); ?>>

	<div class="eltd-post-image-holder" <?php echo creator_elated_get_inline_style($background_image_style) ?>>
		<div class="eltd-post-text-holder">
			<div class="eltd-post-text-holder-inner">
				<div class="eltd-post-text">

					<div class="eltd-post-text-inner">
						<div class="eltd-post-info eltd-top-section">
							<?php creator_elated_post_info(array(
								'author' => 'yes',
								'share' => $social_share_flag,
							)) ?>
						</div>

						<div class="eltd-quote-icon-post-mark eltd-icon-post-mark">
							<span class="icon_quotations eltd-link-quote-mark"></span>
						</div>

						<h4 class="eltd-post-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php echo esc_html($quote_text); ?>
							</a>
						</h4>

						<span class="eltd-quote-author"><?php the_title(); ?></span>

					</div>

				</div>
			</div>
		</div>
	</div>

	<a class = "eltd-blog-pinboard-overlay" href="<?php echo get_the_permalink(); ?>"></a>

</article>