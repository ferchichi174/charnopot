<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="eltd-post-image-holder">
		<?php creator_elated_get_module_template_part('templates/lists/parts/image', 'blog'); ?>
	</div>

	<div class="eltd-post-text">

		<div class="eltd-post-info eltd-top-section">
			<?php creator_elated_post_info(array(
				'date' => 'yes',
				'category' => 'yes'
			)) ?>
		</div>

		<?php

		creator_elated_get_module_template_part('templates/lists/parts/title', 'blog');
		creator_elated_excerpt($excerpt_length);

		$args_pages = array(
			'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
			'after'            => '</div></div>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'pagelink'         => '%'
		);

		wp_link_pages($args_pages);
		the_content();?>

		<div class="eltd-post-info eltd-bottom-section clearfix">

			<div class="eltd-left-section">
				<?php creator_elated_post_info(array(
					'author' => 'yes'
				)) ?>
			</div>

			<div class="eltd-right-section">
				<?php creator_elated_post_info(array(
					'comments' => 'yes',
					'share' => $social_share_flag
				)) ?>
			</div>

		</div>

	</div>
</article>