<?php
$style_attr = '';
if ( has_post_thumbnail() ) {
	$background_image_object = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID()),'full');
	$background_image_src = $background_image_object[0];
	$style_attr  = 'background-image:url('.  esc_url($background_image_src) .')';
} ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('eltd-split-post'); ?>>
	<div class="eltd-post-content">
		<div class="eltd-left-section" <?php echo creator_elated_get_inline_style($style_attr)?>>
			<a class="eltd-post-permalink" href="<?php echo get_the_permalink()?>"></a>
		</div>
		<div class="eltd-right-section">
			<div class="eltd-post-header">
				<div class="eltd-post-header-left">

					<div class="eltd-post-info eltd-top-section">
						<?php creator_elated_post_info(array(
							'date' => 'yes',
							'category' => 'yes'
						)) ?>
					</div>

				</div>
				<div class="eltd-post-header-right">
					<?php creator_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
				</div>
			</div>
			<div class="eltd-post-text">
				<div class="eltd-post-text-inner">
					<?php
					creator_elated_excerpt($split_post_excerpt);
					$args_pages = array(
						'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
						'after'            => '</div></div>',
						'link_before'      => '<span>',
						'link_after'       => '</span>',
						'pagelink'         => '%'
					);
					wp_link_pages($args_pages);
					creator_elated_read_more_button();
					?>
				</div>
				<div class="eltd-post-info-bottom clearfix">
					<?php creator_elated_post_info(array(
						'comments' => 'yes'
					)); ?>
				</div>
			</div>
		</div>
	</div>
</article>
