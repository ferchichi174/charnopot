<?php
$image_gallery_val = get_post_meta( get_the_ID(), 'eltd_post_gallery_images_meta' , true );
$image_gallery_type = get_post_meta( get_the_ID(), 'eltd_post_gallery_type', true);
?>
<?php if ($image_gallery_val !== "") {
	//Masonry type
	if ( $image_gallery_type == 'masonry' ) {

		echo creator_elated_get_blog_gallery_masonry_layout();

	} else { //Slider type ?>

		<div class="eltd-post-image">
			<div class="eltd-blog-gallery eltd-owl-slider">
				<?php
				$image_gallery_array = explode(',',$image_gallery_val);
				if(isset($image_gallery_array) && count($image_gallery_array)!= 0):
					foreach($image_gallery_array as $gimg_id): ?>
						<div>
							<a href="<?php the_permalink(); ?>">
								<?php echo wp_get_attachment_image( $gimg_id, 'full' ); ?>
							</a>
							<?php if(isset($blog_type) && $blog_type === 'chequered'){
								?>
								<div class="eltd-post-text">

									<div class="eltd-post-info eltd-top-section">
										<?php creator_elated_post_info(array(
											'date' => 'yes',
											'category' => 'yes'
										)) ?>
									</div>

									<?php

									creator_elated_get_module_template_part('templates/lists/parts/title', 'blog');

									$args_pages = array(
										'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
										'after'            => '</div></div>',
										'link_before'      => '<span>',
										'link_after'       => '</span>',
										'pagelink'         => '%'
									);

									wp_link_pages($args_pages); ?>

								</div>
							<?php }?>
						</div>
					<?php endforeach;
				endif;
				?>
			</div>
		</div>

	<?php } ?>

<?php }