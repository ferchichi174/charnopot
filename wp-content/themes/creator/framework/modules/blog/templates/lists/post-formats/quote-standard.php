<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="eltd-post-content"
	     style="background-image: url(' <?php echo esc_url( $params['quote_image'][0] ); ?> ')">

		<div class="eltd-post-text <?php echo esc_attr($quote_image_class) ?>">

			<div class="eltd-post-mark">
				<span class="icon_quotations eltd-link-quote-mark"></span>
			</div>

			<div class="eltd-post-info eltd-top-section">
				<?php creator_elated_post_info( array(
					'date'     => 'yes',
					'category' => 'yes'
				) ) ?>
			</div>

			<div class="eltd-quote-post-title">

				<div class="eltd-post-title">
					<?php if ( $quote_text !== '' ) { ?>
						<h4>
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php echo esc_html( $quote_text ); ?>
							</a>
						</h4>
					<?php } ?>
				</div>
			</div>

			<div class="eltd-post-info eltd-bottom-section clearfix">

                <div class="eltd-left-section">
                    <?php creator_elated_post_info(array(
                        'author' => 'yes'
                    )) ?>
                </div>

                <div class="eltd-right-section">
                    <?php creator_elated_post_info(array(
                        'share' => $social_share_flag,
                    )) ?>
                </div>

			</div>


		</div>

	</div>

</article>