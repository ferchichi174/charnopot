<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="eltd-post-content">

        <div class="eltd-post-text">

            <div class="eltd-post-mark">
                <span class="icon_quotations eltd-link-quote-mark"></span>
            </div>

            <div class="eltd-post-info eltd-top-section">
                <?php creator_elated_post_info( array(
                    'date'     => 'no',
                    'category' => 'no'
                ) ) ?>
            </div>

            <div class="eltd-quote-post-title">

                <div class="eltd-post-title">
                    <?php if ( $quote_text !== '' ) { ?>
                        <h4>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php echo esc_html( $quote_text ); ?>
                            </a>
                        </h4>
                    <?php } ?>
                </div>

                <span class="eltd-quote-author"><?php the_title(); ?></span>
            </div>
            


        </div>

    </div>

</article>