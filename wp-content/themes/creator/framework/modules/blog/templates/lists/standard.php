<?php
$chars_array = creator_elated_blog_lists_number_of_chars();

if(isset($chars_array['standard_split_post']) && $chars_array['standard_split_post'] !== ''){
	$params['split_post_excerpt'] = $chars_array['standard_split_post'];
}else{
	$params['split_post_excerpt'] = '30';
}

?>
<div class="<?php echo esc_attr($blog_classes)?>"   <?php echo esc_attr($blog_data_params) ?> >
	<?php

		if($blog_query->have_posts()) : while ( $blog_query->have_posts() ) : $blog_query->the_post();
			$post_layout = creator_elated_check_post_layout(get_the_ID());

			if($post_layout == 'split' ){
				creator_elated_get_module_template_part('templates/lists/post-formats/split-post', 'blog', '', $params);
			}else{
				creator_elated_get_post_format_html($blog_type);
			}
		endwhile;
		wp_reset_postdata();
		else:
			creator_elated_get_module_template_part('templates/parts/no-posts', 'blog');
		endif;

		if(creator_elated_options()->getOptionValue('pagination') == 'yes') {
			creator_elated_pagination($blog_query->max_num_pages, $blog_page_range, $paged);
		}
	?>
</div>
