<div class="<?php echo esc_attr($blog_classes)?>"  <?php echo esc_attr($blog_data_params) ?> >

	<div class="eltd-blog-pinboard-grid-sizer"></div>

	<?php
	if($blog_query->have_posts()) : while ( $blog_query->have_posts() ) : $blog_query->the_post();
		creator_elated_get_post_format_html($blog_type);
	endwhile;
	wp_reset_postdata();
	else:
		creator_elated_get_module_template_part('templates/parts/no-posts', 'blog');
	endif;

	?>
</div>

<?php
if(creator_elated_options()->getOptionValue('pagination') == 'yes') {
	creator_elated_pagination($blog_query->max_num_pages, $blog_page_range, $paged, $blog_type);
}