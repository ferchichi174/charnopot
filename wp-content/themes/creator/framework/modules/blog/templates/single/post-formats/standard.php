<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if(has_post_thumbnail()){?>
		<div class="eltd-post-image-holder">
			<?php creator_elated_get_module_template_part('templates/single/parts/image', 'blog'); ?>
		</div>
	<?php } ?>

	<div class="eltd-post-text">

		<div class="eltd-post-info eltd-top-section">
			<?php creator_elated_post_info(array(
				'date' => 'yes',
				'category' => 'yes'
			)) ?>
		</div>

		<?php
		creator_elated_get_module_template_part('templates/single/parts/title', 'blog');
		the_content();

		wp_link_pages(array(
			'before'           => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
			'after'            => '</div></div>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'pagelink'         => '%'
		));

		?>
		<div class="eltd-blog-tags-info-holder">

			<?php if( has_tag()){
				creator_elated_get_module_template_part('templates/single/parts/tags', 'blog');
			} ?>

			<div class="eltd-post-info clearfix">

				<?php creator_elated_post_info(array(
					'share' => $social_share_flag,
					'comments' => 'yes'
				)) ?>

			</div>
		</div>

	</div>
</article>