<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="eltd-post-content"  style="background-image: url(' <?php echo esc_url($params['quote_image'][0]); ?> ')">

		<div class="eltd-post-text">
			<div class="eltd-post-mark">
				<span class="icon_quotations eltd-link-quote-mark"></span>
			</div>

			<div class="eltd-post-info eltd-top-section">
				<?php creator_elated_post_info(array(
					'date' => 'yes',
					'category' => 'yes'
				)) ?>
			</div>

			<div class="eltd-quote-post-title">

				<h4 class="eltd-post-title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php echo esc_html($quote_text); ?>
					</a>
				</h4>

			</div>

			<div class="eltd-post-info eltd-bottom-section clearfix">

				<div class="eltd-left-section">
					<?php creator_elated_post_info(array(
						'author' => 'yes'
					)) ?>
				</div>

                <div class="eltd-right-section">
                    <?php creator_elated_post_info(array(
                        'share' => $social_share_flag,
                    )) ?>
                </div>

			</div>


		</div>

	</div>

	<div class="eltd-blog-single-content">
		<?php
		the_content();
		?>
		<div class="eltd-blog-tags-info-holder">
			<?php do_action('creator_elated_before_blog_article_closed_tag'); ?>
			<div class="eltd-post-info clearfix">
				<?php creator_elated_post_info(array(
					'share' => $social_share_flag,
					'comments' => 'yes'
				)) ?>

			</div>
		</div>
	</div>

</article>