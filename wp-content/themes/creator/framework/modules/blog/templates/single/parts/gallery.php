<?php
$image_gallery_val = get_post_meta( get_the_ID(), 'eltd_post_gallery_images_meta' , true );
$image_gallery_type = get_post_meta( get_the_ID(), 'eltd_post_gallery_type', true);
?>
<?php if ($image_gallery_val !== "") {
	//Masonry type
	if ( $image_gallery_type == 'masonry' ) {

		echo creator_elated_get_blog_gallery_masonry_layout();

	} else { //Slider type ?>

		<div class="eltd-post-image">
			<div class="eltd-blog-gallery eltd-owl-slider">
				<?php

				$image_gallery_array = explode(',',$image_gallery_val);
				if(isset($image_gallery_array) && count($image_gallery_array)!= 0):
					foreach($image_gallery_array as $gimg_id): ?>
						<div>
							<a href="<?php the_permalink(); ?>">
								<?php echo wp_get_attachment_image( $gimg_id, 'full' ); ?>
							</a>
						</div>
					<?php endforeach;
				endif;
				?>
			</div>
		</div>

	<?php } ?>

<?php } ?>