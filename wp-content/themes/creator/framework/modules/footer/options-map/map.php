<?php

if ( ! function_exists('creator_elated_footer_options_map') ) {
	/**
	 * Add footer options
	 */
	function creator_elated_footer_options_map() {

		creator_elated_add_admin_page(
			array(
				'slug' => '_footer_page',
				'title' => esc_html__('Footer', 'creator'),
				'icon' => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = creator_elated_add_admin_panel(
			array(
				'title' => esc_html__('Footer', 'creator'),
				'name' => 'footer',
				'page' => '_footer_page'
			)
		);
		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_image',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Background Image', 'creator'),
				'description' => esc_html__('Disabling this option will hide footer background image', 'creator'),
				'parent' => $footer_panel,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'image',
				'name' => 'footer_background_image',
				'default_value' => '',
				'label' => esc_html__('Footer Background Image', 'creator'),
				'description' => '',
				'parent' => $footer_panel
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'uncovering_footer',
				'default_value' => 'no',
				'label' => esc_html__('Uncovering Footer', 'creator'),
				'description' => esc_html__('Enabling this option will make Footer gradually appear on scroll', 'creator'),
				'parent' => $footer_panel,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'footer_in_grid',
				'default_value' => 'yes',
				'label' => esc_html__('Footer in Grid', 'creator'),
				'description' => esc_html__('Enabling this option will place Footer content in grid', 'creator'),
				'parent' => $footer_panel,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_top',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Top', 'creator'),
				'description' => esc_html__('Enabling this option will show Footer Top area', 'creator'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_show_footer_top_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_top_container = creator_elated_add_admin_container(
			array(
				'name' => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns',
				'default_value' => '4',
				'label' => esc_html__('Footer Top Columns', 'creator'),
				'description' => esc_html__('Choose number of columns for Footer Top area', 'creator'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'5' => '3(25%+25%+50%)',
					'6' => '3(50%+25%+25%)',
					'4' => '4'
				),
				'parent' => $show_footer_top_container,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns_alignment',
				'default_value' => '',
				'label' => esc_html__('Footer Top Columns Alignment', 'creator'),
				'description' => esc_html__('Text Alignment in Footer Columns', 'creator'),
				'options' => array(
					'left' => esc_html__('Left', 'creator'),
					'center' => esc_html__('Center', 'creator'),
					'right' => esc_html__('Right', 'creator')
				),
				'parent' => $show_footer_top_container,
			)
		);

		creator_elated_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_bottom',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Bottom', 'creator'),
				'description' => esc_html__('Enabling this option will show Footer Bottom area', 'creator'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#eltd_show_footer_bottom_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_bottom_container = creator_elated_add_admin_container(
			array(
				'name' => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);


		creator_elated_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_bottom_columns',
				'default_value' => '3',
				'label' => esc_html__('Footer Bottom Columns', 'creator'),
				'description' => esc_html__('Choose number of columns for Footer Bottom area', 'creator'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent' => $show_footer_bottom_container,
			)
		);

	}

	add_action( 'creator_elated_options_map', 'creator_elated_footer_options_map', 15);

}