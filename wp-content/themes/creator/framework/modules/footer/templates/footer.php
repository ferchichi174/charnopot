<?php
/**
 * Footer template part
 */
$footer_bck_image = '';
if(isset($show_footer_image) && $show_footer_image === 'yes'){
	if(isset($footer_background_image) && $footer_background_image !== ''){
		$footer_bck_image = 'background-image: url('.esc_url($footer_background_image).')';
	}
}

creator_elated_get_content_bottom_area();  ?>

</div> <!-- close div.content_inner -->
</div> <!-- close div.content -->

<?php if (!isset($_REQUEST["ajax_req"]) || $_REQUEST["ajax_req"] != 'yes') { ?>
<footer <?php creator_elated_class_attribute($footer_classes);?> <?php echo creator_elated_get_inline_style($footer_bck_image); ?>>
	<div class="eltd-footer-inner clearfix">

		<?php
		if($display_footer_top) {
			creator_elated_get_footer_top();
		}
		if($display_footer_bottom) {
			creator_elated_get_footer_bottom();
		}
		?>

	</div>
</footer>
<?php } ?>

</div> <!-- close div.eltd-wrapper-inner  -->
</div> <!-- close div.eltd-wrapper -->
<?php wp_footer(); ?>
</body>
</html>