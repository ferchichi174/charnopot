<?php

if(!function_exists('creator_elated_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function creator_elated_register_sidebars() {

        register_sidebar(array(
            'name' => 'Sidebar',
            'id' => 'sidebar',
            'description' => 'Default Sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="eltd-widget-title">',
            'after_title' => '</h5>'
        ));

    }

    add_action('widgets_init', 'creator_elated_register_sidebars');
}

if(!function_exists('creator_elated_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates CreatorElatedSidebar object
     */
    function creator_elated_add_support_custom_sidebar() {
        add_theme_support('CreatorElatedSidebar');
        if (get_theme_support('CreatorElatedSidebar')) new CreatorElatedSidebar();
    }

    add_action('after_setup_theme', 'creator_elated_add_support_custom_sidebar');
}
