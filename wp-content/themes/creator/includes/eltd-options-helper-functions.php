<?php

if(!function_exists('creator_elated_is_responsive_on')) {
    /**
     * Checks whether responsive mode is enabled in theme options
     * @return bool
     */
    function creator_elated_is_responsive_on() {
        return creator_elated_options()->getOptionValue('responsiveness') !== 'no';
    }
}