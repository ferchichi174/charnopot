<?php
/*
Template Name: Blog: Chequered
*/
?>
<?php get_header(); ?>

<?php creator_elated_get_title(); ?>
<?php get_template_part('slider'); ?>

    <div class="eltd-container">
        <div class="eltd-container-inner clearfix">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                the_content();
                creator_elated_get_blog('chequered');
                do_action('creator_elated_page_after_content');
            endwhile;
            endif;
            ?>
        </div>
    </div>

<?php get_footer(); ?>