<div class="eltd-comment-holder clearfix" id="comments">
	<div class="eltd-comment-number">
		<?php if(get_comments_number() > 0): ?>
			<h4><?php comments_number( esc_html__( '', 'creator' ), '1' . esc_html__( ' Comment ', 'creator' ), '% ' . esc_html__( ' Comments ', 'creator' ) ); ?></h4>
		<?php endif; ?>
	</div>
	<div class="eltd-comments">
		<?php if ( post_password_required() ) : ?>
		<p class="eltd-no-password"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'creator' ); ?></p>
	</div>
</div>
<?php
return;
endif;
?>
<?php if ( have_comments() ) : ?>

	<ul class="eltd-comment-list">
		<?php wp_list_comments( array( 'callback' => 'creator_elated_comment' ) ); ?>
	</ul>


	<?php // End Comments ?>

<?php else : // this is displayed if there are no comments so far

	if ( ! comments_open() ) :
		?>
		<!-- If comments are open, but there are no comments. -->


		<!-- If comments are closed. -->
		<p><?php esc_html_e( 'Sorry, the comment form is closed at this time.', 'creator' ); ?></p>

	<?php endif; ?>
<?php endif; ?>
</div></div>
<?php
$commenter = wp_get_current_commenter();
$req       = get_option( 'require_name_email' );
$aria_req  = ( $req ? " aria-required='true'" : '' );
$consent  = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

$args = array(
	'id_form'              => 'commentform',
	'id_submit'            => 'submit_comment',
	'title_reply'          => esc_html__( 'Leave a Reply', 'creator' ),
	'title_reply_to'       => esc_html__( 'Post a Reply to %s', 'creator' ),
	'cancel_reply_link'    => esc_html__( 'Cancel Reply', 'creator' ),
	'title_reply_before'   => '<h4 id="reply-title" class="comment-reply-title">',
	'title_reply_after'    => '</h4>',
	'label_submit'         => esc_html__( 'Submit', 'creator' ),
	'comment_field'        => '<textarea id="comment" placeholder="' . esc_attr__( 'Write your comment here...', 'creator' ) . '" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
	'comment_notes_before' => '',
	'comment_notes_after'  => '',
	'fields'               => apply_filters( 'comment_form_default_fields', array(
		'author' => '<input id="author" name="author" placeholder="' . esc_attr__( 'Your full name', 'creator' ) . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' />',
		'email'    => '<input id="email" name="email" placeholder="' . esc_attr__( 'E-mail address', 'creator' ) . '" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '"' . $aria_req . ' />',
		'cookies' => '<p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes" ' . $consent . ' />' .
			'<label for="wp-comment-cookies-consent">' . esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', 'creator' ) . '</label></p>',
	) )
);
?>
<?php if ( get_comment_pages_count() > 1 ) {
	?>
	<div class="eltd-comment-pager">
		<p><?php paginate_comments_links(); ?></p>
	</div>
<?php } ?>
<div class="eltd-comment-form">
	<?php comment_form( $args ); ?>
</div>
							


