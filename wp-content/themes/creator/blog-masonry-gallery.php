<?php
/*
Template Name: Blog: Masonry Gallery
*/
	get_header();
	creator_elated_get_title();
	get_template_part('slider');
?>

<div class="eltd-container">
	<?php do_action('creator_elated_after_container_open'); ?>
	<div class="eltd-container-inner">
		<?php
		if (have_posts()) : while (have_posts()) : the_post();
			the_content();
			creator_elated_get_blog('masonry-gallery');
			do_action('creator_elated_page_after_content');
		endwhile;
		endif;
		?>
	</div>
	<?php do_action('creator_elated_before_container_close'); ?>
</div>

<?php get_footer(); ?>