<?php
	$creator_elated_archive_pages_classes = creator_elated_blog_archive_pages_classes(creator_elated_get_default_blog_list());
	get_header();
	creator_elated_get_title();
?>

<div class="<?php echo esc_attr($creator_elated_archive_pages_classes['holder']); ?>">

	<?php do_action('creator_elated_after_container_open'); ?>
		<div class="<?php echo esc_attr($creator_elated_archive_pages_classes['inner']); ?>">
			<?php
				creator_elated_get_blog(creator_elated_get_default_blog_list());
			?>
		</div>
	<?php do_action('creator_elated_before_container_close'); ?>

</div>

<?php get_footer(); ?>