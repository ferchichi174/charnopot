<?php
include_once get_template_directory().'/theme-includes.php';

if(!function_exists('creator_elated_styles')) {
    /**
     * Function that includes theme's core styles
     */
    function creator_elated_styles() {

        //include theme's core styles
        wp_enqueue_style('creator-elated-default-style', ELATED_ROOT.'/style.css');
        wp_enqueue_style('creator-elated-modules-plugins', ELATED_ASSETS_ROOT.'/css/plugins.min.css');
        wp_enqueue_style('creator-elated-modules', ELATED_ASSETS_ROOT.'/css/modules.min.css');

        creator_elated_icon_collections()->enqueueStyles();

        if(creator_elated_load_blog_assets()) {
            wp_enqueue_style('creator-elated-blog', ELATED_ASSETS_ROOT.'/css/blog.min.css');
        }

        if(creator_elated_load_blog_assets() || is_singular('portfolio-item')) {
            wp_enqueue_style('wp-mediaelement');
        }

        //is woocommerce installed?
        if ( creator_elated_is_woocommerce_installed() ) {
            if ( creator_elated_load_woo_assets() || creator_elated_is_ajax_enabled() ) {

                //include theme's woocommerce styles
                wp_enqueue_style( 'creator-elated-woocommerce', ELATED_ASSETS_ROOT . '/css/woocommerce.min.css' );

            }
        }

        //define files afer which style dynamic needs to be included. It should be included last so it can override other files
        $style_dynamic_deps_array = array();
        if(creator_elated_load_woo_assets() || creator_elated_is_ajax_enabled()) {
            $style_dynamic_deps_array = array('creator-elated-woocommerce', 'creator-elated-woocommerce-responsive');
        }

        if(file_exists(ELATED_ROOT_DIR.'/assets/css/style_dynamic.css') && creator_elated_is_css_folder_writable() && !is_multisite()) {
            wp_enqueue_style('creator-elated-style-dynamic', ELATED_ASSETS_ROOT.'/css/style_dynamic.css', $style_dynamic_deps_array, filemtime(ELATED_ROOT_DIR.'/assets/css/style_dynamic.css')); //it must be included after woocommerce styles so it can override it
        }

        //is responsive option turned on?
        if(creator_elated_is_responsive_on()) {
            wp_enqueue_style('creator-elated-modules-responsive', ELATED_ASSETS_ROOT.'/css/modules-responsive.min.css');
            wp_enqueue_style('creator-elated-blog-responsive', ELATED_ASSETS_ROOT.'/css/blog-responsive.min.css');

            //is woocommerce installed?
            if(creator_elated_is_woocommerce_installed()) {
                if(creator_elated_load_woo_assets() || creator_elated_is_ajax_enabled()) {

                    //include theme's woocommerce responsive styles
                    wp_enqueue_style('creator-elated-woocommerce-responsive', ELATED_ASSETS_ROOT.'/css/woocommerce-responsive.min.css');
                }
            }

            //include proper styles
            if(file_exists(ELATED_ROOT_DIR.'/assets/css/style_dynamic_responsive.css') && creator_elated_is_css_folder_writable() && !is_multisite()) {
                wp_enqueue_style('creator-elated-style-dynamic-responsive', ELATED_ASSETS_ROOT.'/css/style_dynamic_responsive.css', array(), filemtime(ELATED_ROOT_DIR.'/assets/css/style_dynamic_responsive.css'));
            }
        }

        //include Visual Composer styles
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            wp_enqueue_style('js_composer_front');
        }
    }

    add_action('wp_enqueue_scripts', 'creator_elated_styles');
}

if(!function_exists('creator_elated_google_fonts_styles')) {
	/**
	 * Function that includes google fonts defined anywhere in the theme
	 */
    function creator_elated_google_fonts_styles() {
        $font_simple_field_array = creator_elated_options()->getOptionsByType('fontsimple');
        if(!(is_array($font_simple_field_array) && count($font_simple_field_array) > 0)) {
            $font_simple_field_array = array();
        }

        $font_field_array = creator_elated_options()->getOptionsByType('font');
        if(!(is_array($font_field_array) && count($font_field_array) > 0)) {
            $font_field_array = array();
        }

        $available_font_options = array_merge($font_simple_field_array, $font_field_array);

        $google_font_weight_array = creator_elated_options()->getOptionValue('google_font_weight');
        if(!empty($google_font_weight_array)) {
            $google_font_weight_array = array_slice(creator_elated_options()->getOptionValue('google_font_weight'), 1);
        }

        $font_weight_str = '100,300,400,500,700';
        if(!empty($google_font_weight_array) && $google_font_weight_array !== '') {
            $font_weight_str = implode(',',$google_font_weight_array);
        }

        $google_font_subset_array = creator_elated_options()->getOptionValue('google_font_subset');
        if(!empty($google_font_subset_array)) {
            $google_font_subset_array = array_slice(creator_elated_options()->getOptionValue('google_font_subset'), 1);
        }

        $font_subset_str = 'latin-ext';
        if(!empty($google_font_subset_array) && $google_font_subset_array !== '') {
            $font_subset_str = implode(',',$google_font_subset_array);
        }

        //define available font options array
        $fonts_array = array();
        foreach($available_font_options as $font_option) {
            //is font set and not set to default and not empty?
            $font_option_value = creator_elated_options()->getOptionValue($font_option);
            if(creator_elated_is_font_option_valid($font_option_value) && !creator_elated_is_native_font($font_option_value)) {
                $font_option_string = $font_option_value.':'.$font_weight_str;
                if(!in_array($font_option_string, $fonts_array)) {
                    $fonts_array[] = $font_option_string;
                }
            }
        }

        $fonts_array         = array_diff($fonts_array, array('-1:'.$font_weight_str));
        $google_fonts_string = implode('|', $fonts_array);

        //default fonts should be separated with %7C because of HTML validation
        $default_font_string = 'Open Sans:'.$font_weight_str.'|Rokkitt:'.$font_weight_str.'|Noto Serif:'.$font_weight_str;
        $protocol = is_ssl() ? 'https:' : 'http:';

        //is google font option checked anywhere in theme?
        if (count($fonts_array) > 0) {

            //include all checked fonts
            $fonts_full_list = $default_font_string . '|' . str_replace('+', ' ', $google_fonts_string);
            $fonts_full_list_args = array(
                'family' => urlencode($fonts_full_list),
                'subset' => urlencode($font_subset_str),
            );

            $creator_elated_fonts = add_query_arg( $fonts_full_list_args, $protocol.'//fonts.googleapis.com/css' );
            wp_enqueue_style( 'creator-elated-google-fonts', esc_url_raw($creator_elated_fonts), array(), '1.0.0' );

        } else {
            //include default google font that theme is using
            $default_fonts_args = array(
                'family' => urlencode($default_font_string),
                'subset' => urlencode($font_subset_str),
            );
            $creator_elated_fonts = add_query_arg( $default_fonts_args, $protocol.'//fonts.googleapis.com/css' );
            wp_enqueue_style( 'creator-elated-google-fonts', esc_url_raw($creator_elated_fonts), array(), '1.0.0' );
        }

    }

	add_action('wp_enqueue_scripts', 'creator_elated_google_fonts_styles');
}

if(!function_exists('creator_elated_scripts')) {
    /**
     * Function that includes all necessary scripts
     */
    function creator_elated_scripts() {
        global $wp_scripts;

        //init theme core scripts
		wp_enqueue_script( 'jquery-ui-core');
		wp_enqueue_script( 'jquery-ui-tabs');
		wp_enqueue_script( 'jquery-ui-accordion');
		wp_enqueue_script( 'wp-mediaelement');

		// 3rd party JavaScripts that we used in our theme
		foreach (glob(ELATED_ROOT_DIR.'/assets/js/modules/plugins/*.js') as $file) {
			$filename = substr($file, strrpos($file, '/') + 1);
			wp_enqueue_script( $filename, ELATED_ROOT.'/assets/js/modules/plugins/'.$filename , array('jquery'), false, true );
		}

        wp_enqueue_script('isotope', ELATED_ASSETS_ROOT.'/js/jquery.isotope.min.js', array('jquery'), false, true);

		if(creator_elated_is_smoth_scroll_enabled()) {
			wp_enqueue_script("creator-elated-smooth-page-scroll", ELATED_ASSETS_ROOT . "/js/smoothPageScroll.js", array(), false, true);
		}

        //include google map api script
        if(creator_elated_options()->getOptionValue('google_maps_api_key') != '') {
            $google_maps_api_key = creator_elated_options()->getOptionValue('google_maps_api_key');
            wp_enqueue_script('google-map-api', '//maps.googleapis.com/maps/api/js?key=' . $google_maps_api_key, array(), false, true);
        }

        wp_enqueue_script('creator-elated-modules', ELATED_ASSETS_ROOT.'/js/modules.min.js', array('jquery'), false, true);

        if(creator_elated_load_blog_assets()) {
            wp_enqueue_script('creator-elated-blog', ELATED_ASSETS_ROOT.'/js/blog.min.js', array('jquery'), false, true);
        }

        //include comment reply script
        $wp_scripts->add_data('comment-reply', 'group', 1);
        if(is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script("comment-reply");
        }

        //include Visual Composer script
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            wp_enqueue_script('wpb_composer_front_js');
        }
    }

    add_action('wp_enqueue_scripts', 'creator_elated_scripts');
}


if(!function_exists('creator_elated_is_ajax_request')) {
    /**
     * Function that checks if the incoming request is made by ajax function
     */
    function creator_elated_is_ajax_request() {

        return isset($_POST["ajaxReq"]) && $_POST["ajaxReq"] == 'yes';

    }
}

if(!function_exists('creator_elated_is_ajax_enabled')) {
    /**
     * Function that checks if ajax is enabled
     */
    function creator_elated_is_ajax_enabled() {

        return creator_elated_options()->getOptionValue('smooth_page_transitions') === 'yes';

    }
}

if(!function_exists('creator_elated_ajax_meta')) {
    /**
     * Function that echoes meta data for ajax
     *
     * @since 4.3
     * @version 0.2
     */
    function creator_elated_ajax_meta() {

        $id = creator_elated_get_page_id();

        $page_transition = get_post_meta($id, "eltd_page_transition_type", true);
        ?>

        <div class="eltd-seo-title"><?php echo wp_get_document_title(); ?></div>

        <?php if($page_transition !== ''){ ?>
            <div class="eltd-page-transition"><?php echo esc_html($page_transition); ?></div>
        <?php } else if(creator_elated_options()->getOptionValue('default_page_transition')) {?>
            <div class="eltd-page-transition"><?php echo esc_html(creator_elated_options()->getOptionValue('default_page_transition')); ?></div>
        <?php }
    }

    add_action('creator_elated_ajax_meta', 'creator_elated_ajax_meta');
}

if(!function_exists('creator_elated_no_ajax_pages')) {
    /**
     * Function that echoes pages on which ajax should not be applied
     *
     * @since 4.3
     * @version 0.2
     */
    function creator_elated_no_ajax_pages($global_variables) {

        //is ajax enabled?
        if(creator_elated_is_ajax_enabled()) {
            $no_ajax_pages = array();

            //get posts that have ajax disabled and merge with main array
            $no_ajax_pages = array_merge($no_ajax_pages, creator_elated_get_objects_without_ajax());

            //is wpml installed?
            if(creator_elated_is_wpml_installed()) {
                //get translation pages for current page and merge with main array
                $no_ajax_pages = array_merge($no_ajax_pages, creator_elated_get_wpml_pages_for_current_page());
            }

            //is woocommerce installed?
            if(creator_elated_is_woocommerce_installed()) {
                //get all woocommerce pages and products and merge with main array
                $no_ajax_pages = array_merge($no_ajax_pages, creator_elated_get_woocommerce_pages());
            }
            //do we have some internal pages that want to be without ajax?
            if ( creator_elated_options()->getOptionValue('internal_no_ajax_links') !== '' ) {
                //get array of those pages
                $options_no_ajax_pages_array = explode(',', creator_elated_options()->getOptionValue('internal_no_ajax_links'));

                if(is_array($options_no_ajax_pages_array) && count($options_no_ajax_pages_array)) {
                    $no_ajax_pages = array_merge($no_ajax_pages, $options_no_ajax_pages_array);
                }
            }

            //add logout url to main array
            $no_ajax_pages[] = wp_specialchars_decode(wp_logout_url());

            $global_variables['no_ajax_pages'] = $no_ajax_pages;
        }

        return $global_variables;

    }

    add_filter('creator_elated_js_global_variables', 'creator_elated_no_ajax_pages');
}

if(!function_exists('creator_elated_get_objects_without_ajax')) {
   /**
     * Function that returns urls of objects that have ajax disabled.
     * Works for posts, pages and portfolio pages.
     * @return array array of urls of posts that have ajax disabled
     *
     * @version 0.1
     */
    function creator_elated_get_objects_without_ajax() {
        $posts_without_ajax = array();

        $posts_args =  array(
            'post_type'  => array('post', 'portfolio-item', 'page'),
            'post_status' => 'publish',
            'meta_key' => 'eltd_page_transition_type',
            'meta_value' => 'no-animation'
        );

        $posts_query = new WP_Query($posts_args);

        if($posts_query->have_posts()) {
            while($posts_query->have_posts()) {
                $posts_query->the_post();
                $posts_without_ajax[] = get_permalink(get_the_ID());
            }
        }

        wp_reset_postdata();

        return $posts_without_ajax;
    }
}


//defined content width variable
if (!isset( $content_width )) $content_width = 1200;

if(!function_exists('creator_elated_theme_setup')) {
    /**
     * Function that adds various features to theme. Also defines image sizes that are used in a theme
     */
    function creator_elated_theme_setup() {
        //add support for feed links
        add_theme_support('automatic-feed-links');

        //add support for post formats
        add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));

        //add theme support for post thumbnails
        add_theme_support('post-thumbnails');

        //add theme support for title tag
        add_theme_support('title-tag');

        //define thumbnail sizes
        add_image_size('creator_elated_square', 600, 600, true);
        add_image_size('creator_elated_landscape', 800, 545, true);
        add_image_size('creator_elated_portrait', 600, 850, true);
        add_image_size('creator_elated_large_width', 1200, 600, true);
        add_image_size('creator_elated_large_height', 600, 1200, true);
        add_image_size('creator_elated_large_width_height', 1000, 1000, true);
        add_image_size('creator_elated_blog_slider_landscape', 860, 575, true);
        add_image_size('creator_elated_blog_slider_portrait', 382, 575, true);

        load_theme_textdomain( 'creator', get_template_directory().'/languages' );
    }

    add_action('after_setup_theme', 'creator_elated_theme_setup');
}


if ( ! function_exists( 'creator_elated_enqueue_editor_customizer_styles' ) ) {
	/**
	 * Enqueue supplemental block editor styles
	 */
	function creator_elated_enqueue_editor_customizer_styles() {
		wp_enqueue_style( 'creator-elated-admin-styles', ELATED_FRAMEWORK_ADMIN_ASSETS_ROOT . '/css/eltdf-modules-admin.css' );
		wp_enqueue_style( 'creator-elated-editor-customizer-styles', ELATED_FRAMEWORK_ADMIN_ASSETS_ROOT . '/css/editor-customizer-style.css' );
	}

	// add google font
	add_action( 'enqueue_block_editor_assets', 'creator_elated_google_fonts_styles' );
	// add action
	add_action( 'enqueue_block_editor_assets', 'creator_elated_enqueue_editor_customizer_styles' );
}

if(!function_exists('creator_elated_rgba_color')) {
    /**
     * Function that generates rgba part of css color property
     *
     * @param $color string hex color
     * @param $transparency float transparency value between 0 and 1
     *
     * @return string generated rgba string
     */
    function creator_elated_rgba_color($color, $transparency) {
        if($color !== '' && $transparency !== '') {
            $rgba_color = '';

            $rgb_color_array = creator_elated_hex2rgb($color);
            $rgba_color .= 'rgba('.implode(', ', $rgb_color_array).', '.$transparency.')';

            return $rgba_color;
        }
    }
}

if(!function_exists('creator_elated_header_meta')) {
    /**
     * Function that echoes meta data if our seo is enabled
     */
    function creator_elated_header_meta() { ?>

        <meta charset="<?php bloginfo('charset'); ?>"/>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php }

    add_action('creator_elated_header_meta', 'creator_elated_header_meta');
}

if(!function_exists('creator_elated_user_scalable_meta')) {
    /**
     * Function that outputs user scalable meta if responsiveness is turned on
     * Hooked to creator_elated_header_meta action
     */
    function creator_elated_user_scalable_meta() {
        //is responsiveness option is chosen?
        if(creator_elated_is_responsive_on()) { ?>
            <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <?php } else { ?>
            <meta name="viewport" content="width=1200,user-scalable=yes">
        <?php }
    }

    add_action('creator_elated_header_meta', 'creator_elated_user_scalable_meta');
}

if(!function_exists('creator_elated_get_page_id')) {
	/**
	 * Function that returns current page / post id.
	 * Checks if current page is woocommerce page and returns that id if it is.
	 * Checks if current page is any archive page (category, tag, date, author etc.) and returns -1 because that isn't
	 * page that is created in WP admin.
	 *
	 * @return int
	 *
	 * @version 0.1
	 *
	 * @see creator_elated_is_woocommerce_installed()
	 * @see creator_elated_is_woocommerce_shop()
	 */
	function creator_elated_get_page_id() {
		if(creator_elated_is_woocommerce_installed() && creator_elated_is_woocommerce_shop()) {
			return creator_elated_get_woo_shop_page_id();
		}

		if(is_archive() || is_search() || is_404() || (is_home() && is_front_page())) {
			return -1;
		}

		return get_queried_object_id();
	}
}


if(!function_exists('creator_elated_is_default_wp_template')) {
    /**
     * Function that checks if current page archive page, search, 404 or default home blog page
     * @return bool
     *
     * @see is_archive()
     * @see is_search()
     * @see is_404()
     * @see is_front_page()
     * @see is_home()
     */
    function creator_elated_is_default_wp_template() {
        return is_archive() || is_search() || is_404() || (is_front_page() && is_home());
    }
}

if(!function_exists('creator_elated_get_page_template_name')) {
    /**
     * Returns current template file name without extension
     * @return string name of current template file
     */
    function creator_elated_get_page_template_name() {
        $file_name = '';

        if(!creator_elated_is_default_wp_template()) {
            $file_name_without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', basename(get_page_template()));

            if($file_name_without_ext !== '') {
                $file_name = $file_name_without_ext;
            }
        }

        return $file_name;
    }
}

if(!function_exists('creator_elated_has_shortcode')) {
    /**
     * Function that checks whether shortcode exists on current page / post
     *
     * @param string shortcode to find
     * @param string content to check. If isn't passed current post content will be used
     *
     * @return bool whether content has shortcode or not
     */
    function creator_elated_has_shortcode($shortcode, $content = '') {
        $has_shortcode = false;

        if($shortcode) {
            //if content variable isn't past
            if($content == '') {
                //take content from current post
                $page_id = creator_elated_get_page_id();
                if(!empty($page_id)) {
                    $current_post = get_post($page_id);

                    if(is_object($current_post) && property_exists($current_post, 'post_content')) {
                        $content = $current_post->post_content;
                    }
                }
            }

            //does content has shortcode added?
            if(stripos($content, '['.$shortcode) !== false) {
                $has_shortcode = true;
            }
        }

        return $has_shortcode;
    }
}

if(!function_exists('creator_elated_get_dynamic_sidebar')) {
    /**
     * Return Custom Widget Area content
     *
     * @return string
     */
    function creator_elated_get_dynamic_sidebar($index = 1) {
        ob_start();
        dynamic_sidebar($index);
        $sidebar_contents = ob_get_clean();

        return $sidebar_contents;
    }
}

if(!function_exists('creator_elated_get_sidebar')) {
    /**
     * Return Sidebar
     *
     * @return string
     */
    function creator_elated_get_sidebar() {

        $id = creator_elated_get_page_id();

        $sidebar = "sidebar";

        if (get_post_meta($id, 'eltd_custom_sidebar_meta', true) != '') {
            $sidebar = get_post_meta($id, 'eltd_custom_sidebar_meta', true);
        } else {
            if (is_single() && creator_elated_options()->getOptionValue('blog_single_custom_sidebar') != '') {
                $sidebar = esc_attr(creator_elated_options()->getOptionValue('blog_single_custom_sidebar'));
            } elseif ((creator_elated_is_woocommerce_installed() && ( is_product_category() || is_product_tag() ) ) && get_post_meta(get_option('woocommerce_shop_page_id'), 'eltd_custom_sidebar_meta', true) != '' ){
				$sidebar = get_post_meta(get_option('woocommerce_shop_page_id'), 'eltd_custom_sidebar_meta', true);
			} elseif ((is_archive() || (is_home() && is_front_page())) && creator_elated_options()->getOptionValue('blog_custom_sidebar') != '') {
                $sidebar = esc_attr(creator_elated_options()->getOptionValue('blog_custom_sidebar'));
            } elseif (is_page() && creator_elated_options()->getOptionValue('page_custom_sidebar') != '') {
                $sidebar = esc_attr(creator_elated_options()->getOptionValue('page_custom_sidebar'));
            }
        }

        return $sidebar;
    }
}



if( !function_exists('creator_elated_sidebar_columns_class') ) {

    /**
     * Return classes for columns holder when sidebar is active
     *
     * @return array
     */

    function creator_elated_sidebar_columns_class() {

        $sidebar_class = array();
        $sidebar_layout = creator_elated_sidebar_layout();

        switch($sidebar_layout):
            case 'sidebar-33-right':
                $sidebar_class[] = 'eltd-two-columns-66-33';
                break;
            case 'sidebar-25-right':
                $sidebar_class[] = 'eltd-two-columns-75-25';
                break;
            case 'sidebar-33-left':
                $sidebar_class[] = 'eltd-two-columns-33-66';
                break;
            case 'sidebar-25-left':
                $sidebar_class[] = 'eltd-two-columns-25-75';
                break;

        endswitch;
        $sidebar_class[] = 'eltd-content-has-sidebar clearfix';

        return creator_elated_class_attribute($sidebar_class);

    }

}


if( !function_exists('creator_elated_sidebar_layout') ) {

    /**
     * Function that check is sidebar is enabled and return type of sidebar layout
     */

    function creator_elated_sidebar_layout() {

        $sidebar_layout = '';
        $page_id        = creator_elated_get_page_id();

        $page_sidebar_meta = get_post_meta($page_id, 'eltd_sidebar_meta', true);

        if(($page_sidebar_meta !== '') && $page_id !== -1) {
            if($page_sidebar_meta == 'no-sidebar') {
                $sidebar_layout = '';
            } else {
                $sidebar_layout = $page_sidebar_meta;
            }
        } else {
            if(is_single() && creator_elated_options()->getOptionValue('blog_single_sidebar_layout')) {
                $sidebar_layout = esc_attr(creator_elated_options()->getOptionValue('blog_single_sidebar_layout'));
            } elseif ( ( creator_elated_is_woocommerce_installed() && ( is_product_category() || is_product_tag() ) ) && get_post_meta(get_option('woocommerce_shop_page_id'), 'eltd_sidebar_meta', true) != '' ){
				$page_sidebar_meta = get_post_meta(get_option('woocommerce_shop_page_id'), 'eltd_sidebar_meta', true);
				if($page_sidebar_meta == 'no-sidebar') {
					$sidebar_layout = '';
				} else {
					$sidebar_layout = $page_sidebar_meta;
				}
			} elseif((is_archive() || (is_home() && is_front_page())) && creator_elated_options()->getOptionValue('archive_sidebar_layout')) {
                $sidebar_layout = esc_attr(creator_elated_options()->getOptionValue('archive_sidebar_layout'));
            } elseif(is_page() && creator_elated_options()->getOptionValue('page_sidebar_layout')) {
                $sidebar_layout = esc_attr(creator_elated_options()->getOptionValue('page_sidebar_layout'));
            }
        }

		if ( ! empty( $sidebar_layout ) && ! is_active_sidebar( creator_elated_get_sidebar() ) ) {
			$sidebar_layout = '';
		}

        return $sidebar_layout;

    }

}

if( !function_exists('creator_elated_page_custom_style') ) {
    /**
     * Function that print custom page style
     */
    function creator_elated_page_custom_style() {
        $style = '';
        $style = apply_filters('creator_elated_add_page_custom_style', $style);

        if($style !== '') {
            wp_add_inline_style( 'creator-elated-modules', $style);
        }
    }

    add_action('wp_enqueue_scripts', 'creator_elated_page_custom_style');
}

if( !function_exists('creator_elated_register_page_custom_style') ) {

    /**
     * Function that print custom page style
     */

    function creator_elated_register_page_custom_style() {
       add_action( (creator_elated_is_ajax_enabled() && creator_elated_is_ajax_request()) ? 'creator_elated_ajax_meta' : 'wp_head', 'creator_elated_page_custom_style' );
    }

    add_action( 'creator_elated_after_options_map', 'creator_elated_register_page_custom_style' );
}


if( !function_exists('creator_elated_vc_custom_style') ) {

    /**
     * Function that print custom page style
     */

    function creator_elated_vc_custom_style() {
        if(creator_elated_visual_composer_installed()) {
            $id = creator_elated_get_page_id();
            if(is_page() || is_single() || is_singular('portfolio-item')) {

                $shortcodes_custom_css = get_post_meta( $id, '_wpb_shortcodes_custom_css', true );
                if ( ! empty( $shortcodes_custom_css ) ) {
                    echo '<style type="text/css" data-type="vc_shortcodes-custom-css-'.esc_attr($id).'">';
                    echo get_post_meta( $id, '_wpb_shortcodes_custom_css', true );
                    echo '</style>';
                }

                $post_custom_css = get_post_meta( $id, '_wpb_post_custom_css', true );
                if ( ! empty( $post_custom_css ) ) {
                    echo '<style type="text/css" data-type="vc_custom-css-'.esc_attr($id).'">';
                    echo get_post_meta( $id, '_wpb_post_custom_css', true );
                    echo '</style>';
                }
            }
        }
    }

}


if( !function_exists('creator_elated_register_vc_custom_style') ) {

    /**
     * Function that print custom page style
     */

    function creator_elated_register_vc_custom_style() {
        if (creator_elated_is_ajax_enabled() && creator_elated_is_ajax_request()) {
            add_action( 'creator_elated_ajax_meta', 'creator_elated_vc_custom_style' );
        }

    }

    add_action( 'creator_elated_after_options_map', 'creator_elated_register_vc_custom_style' );
}



if( !function_exists('creator_elated_container_style') ) {

    /**
     * Function that return container style
     */

    function creator_elated_container_style($style) {
        $id = creator_elated_get_page_id();
        $class_prefix = creator_elated_get_unique_page_class();

        $container_selector = array(
            $class_prefix.' .eltd-page-header .eltd-menu-area'
        );

        $container_class = array();
        $meta_header_height = get_post_meta($id, "eltd_header_height_meta" , true);

        if($meta_header_height && $meta_header_height !== ''){
            $container_class['height'] = intval(creator_elated_filter_px($meta_header_height)).'px';
        }

        $current_style = creator_elated_dynamic_css($container_selector, $container_class);
        $current_style = $current_style . $style;

        return $current_style;

    }
    add_filter('creator_elated_add_page_custom_style', 'creator_elated_container_style');
}

if( !function_exists('creator_elated_logo_height_style') ) {

	/**
	 * Function that return container style
	 */

	function creator_elated_logo_height_style($style) {
		$id = creator_elated_get_page_id();
		$class_prefix = creator_elated_get_unique_page_class();

		$logo_height_selector = array(
			$class_prefix.' .eltd-page-header .eltd-logo-area'
		);
		$logo_max_height_selector = array(
			$class_prefix.' .eltd-page-header .eltd-logo-area .eltd-logo-wrapper a'
		);

		$logo_class = array();
		$logo_max_height_class = array();
		$meta_header_height = get_post_meta($id, "eltd_logo_height_meta" , true);

		if($meta_header_height && $meta_header_height !== ''){
			$logo_class['height'] = intval(creator_elated_filter_px($meta_header_height)).'px';
			$logo_max_height_class['max-height'] = intval(creator_elated_filter_px($meta_header_height))*0.9.'px';
		}

		$current_style = creator_elated_dynamic_css($logo_height_selector, $logo_class);
		$current_style .= creator_elated_dynamic_css($logo_max_height_selector, $logo_max_height_class);
		$current_style = $current_style . $style;

		return $current_style;

	}
	add_filter('creator_elated_add_page_custom_style', 'creator_elated_logo_height_style');
}

if( !function_exists('creator_elated_header_height_style') ) {

    /**
     * Function that return container style
     */

    function creator_elated_header_height_style($style) {
        $id = creator_elated_get_page_id();
        $class_prefix = creator_elated_get_unique_page_class();

        $container_selector = array(
            $class_prefix.' .eltd-content .eltd-content-inner > .eltd-container',
            $class_prefix.' .eltd-content .eltd-content-inner > .eltd-full-width',
        );

        $container_class = array();
        $page_backgorund_color = get_post_meta($id, "eltd_page_background_color_meta", true);

        if($page_backgorund_color){
            $container_class['background-color'] = $page_backgorund_color;
        }

        $current_style = creator_elated_dynamic_css($container_selector, $container_class);
        $current_style = $current_style . $style;

        return $current_style;

    }
    add_filter('creator_elated_add_page_custom_style', 'creator_elated_header_height_style');
}

if(!function_exists('creator_elated_get_unique_page_class')) {
    /**
     * Returns unique page class based on post type and page id
     *
     * @return string
     */
    function creator_elated_get_unique_page_class() {
        $id = creator_elated_get_page_id();
        $page_class = '';

        if(is_single()) {
            $page_class = 'body.postid-'.$id;
        } elseif($id === creator_elated_get_woo_shop_page_id()) {
            $page_class = 'body.archive';
        } elseif(is_home()) {
            $page_class .= ' .home';
        } else {
            $page_class .= ' body.page-id-'.$id;
        }

        return $page_class;
    }
}

if( !function_exists('creator_elated_page_padding') ) {

    /**
     * Function that return container style
     */

    function creator_elated_page_padding( $style ) {

		$id = creator_elated_get_page_id();

        $page_selector = array(
            '.page-id-' . $id . ' .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner',
			'.page-id-' . $id . ' .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner',
            '.postid-' . $id . ' .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner',
            '.postid-' . $id . ' .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner'
        );

        if( creator_elated_is_woocommerce_installed() && is_shop()){
            $page_selector = array(
                '.post-type-archive-product .eltd-content .eltd-content-inner .eltd-container > .eltd-container-inner',
                '.post-type-archive-product .eltd-content .eltd-content-inner .eltd-full-width > .eltd-full-width-inner'
            );
        }

        $page_css = array();
        $padding = '';
        $page_padding = '';

        $page_padding = get_post_meta($id, 'eltd_page_padding_meta', true);

        $portfolio_single_padding = creator_elated_options()->getOptionValue('portfolio_single_padding_meta');
        $blog_single_padding =  creator_elated_options()->getOptionValue('blog_single_padding_meta');
        $shop_single_padding = creator_elated_options()->getOptionValue('woo_product_single_padding_meta');

        if($page_padding !== '' && !is_singular('product')){
            $padding = $page_padding;
        }
        else{

            if(is_single()){


                if($portfolio_single_padding && $portfolio_single_padding !== '' && is_singular('portfolio-item')){

                    $page_selector = array(
                        '.single-portfolio-item .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner',
                        '.single-portfolio-item .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner'
                    );
                    $padding = $portfolio_single_padding;

                }
                elseif($blog_single_padding && $blog_single_padding !== '' && is_singular('post')){

                    $page_selector = array(
                        '.single-post .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner',
                        '.single-post .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner'
                    );
                    $padding = $blog_single_padding;

                }
                elseif($shop_single_padding && $shop_single_padding !== '' && is_singular('product')){


                    $page_selector = array(
                        '.single-product .eltd-content .eltd-content-inner > .eltd-container > .eltd-container-inner',
                        '.single-product .eltd-content .eltd-content-inner > .eltd-full-width > .eltd-full-width-inner'
                    );
                    $padding = $shop_single_padding;

                }

            }

        }
        if($padding != ''){
            $page_css['padding'] = $padding;
        }

        $current_style = creator_elated_dynamic_css($page_selector, $page_css);

        $current_style = $current_style . $style;
        return $current_style;

    }
    add_filter('creator_elated_add_page_custom_style', 'creator_elated_page_padding');
}

if(!function_exists('creator_elated_load_proper_main_style')){

    function creator_elated_load_proper_main_style($style){

        $id = creator_elated_get_page_id();
        $main_style = creator_elated_get_meta_field_intersect('main_style', $id);

        $selectors = array();
        $current_style = '';

        $oswald_style_css = array(
            'font-family' => 'Oswald'
        );
        $poppins_style_css = array(
            'font-family' => 'Poppins'
        );
        $raleway_style_css = array(
            'font-family' => 'Raleway'
        );
        $yesteryear_style_css = array(
            'font-family' => 'Yesteryear'
        );
        $greatvibes_css = array(
            'font-family' => 'Great Vibes'
        );


        switch($main_style){

            case 'oswald_style':
                $selectors = creator_elated_get_main_style_selectors('oswald_style') ;
                $current_style = creator_elated_dynamic_css($selectors, $oswald_style_css);
                break;

            case 'poppins_style':
                $selectors = creator_elated_get_main_style_selectors('poppins_style') ;
                $current_style = creator_elated_dynamic_css($selectors, $poppins_style_css);
                break;

            case 'raleway_style':
                $selectors = creator_elated_get_main_style_selectors('raleway_style') ;
                $current_style = creator_elated_dynamic_css($selectors, $raleway_style_css);
                break;

            case 'yesteryear_style':
                $selectors = creator_elated_get_main_style_selectors('yesteryear_style') ;
                $current_style = creator_elated_dynamic_css($selectors, $yesteryear_style_css);
                break;

            case 'greatvibes_style':
                $selectors = creator_elated_get_main_style_selectors('greatvibes_style') ;
                $current_style = creator_elated_dynamic_css($selectors, $greatvibes_css);
                break;

        }

        $current_style = $current_style . $style;
        return $current_style;


    }
    add_filter('creator_elated_add_page_custom_style', 'creator_elated_load_proper_main_style');
}

if(!function_exists('creator_elated_get_main_style_selectors')){

    function creator_elated_get_main_style_selectors($style){

        $return_selector_array = array();
        $style_class = '';

        switch($style){
            case 'oswald_style':
                $style_class = 'eltd-oswald-style';
                $selectors = array(
                    '.eltd-product.eltd-product-standard-type .eltd-product-price',
                    '.eltd-product.eltd-product-standard-type .eltd-product-title',
                    '.products.eltd-type-1 .eltd-product-list-product-title',
                    '.products.eltd-type-1 .product .price',
                    '.eltd-product.eltd-product-image-on-left .eltd-product-title',
                    '.eltd-section-title',
                    '.eltd-content h4',
                    '.countdown-amount'
                );
                break;
            case 'poppins_style':
                $style_class = 'eltd-poppins-style';
                $selectors = array(
                    '.eltd-section-title',
                    '.eltd-woocommerce-page .product .price',
                    '.eltd-woocommerce-page .product .eltd-product-price',
                    '.woocommerce .product .price',
                    '.woocommerce .product .eltd-product-price',
                    '.eltd-product-list-product-title',
                    '.eltd-content h4',
                    '.eltd-blog-list-holder.eltd-blog-standard .eltd-blog-list-item .eltd-item-text-holder .eltd-item-title'
                );
                break;
            case 'raleway_style':
                $style_class = 'eltd-raleway-style';
                $selectors = array(
                    '.eltd-section-title',
                    '.eltd-content h3',
                    '.eltd-team-name',
                    '.eltd-restaurant-menu .eltd-rstrnt-title',
                    '.eltd-restaurant-menu .eltd-rstrnt-price'
                );
                break;
            case 'yesteryear_style':
                $style_class = 'eltd-yesteryear-style';
                $selectors = array(
                    '.eltd-team-position',
                    '.eltd-section-title',
                    '.countdown-amount'
                );
                break;
            case 'greatvibes_style':
                $style_class = 'eltd-greatvibes-style';
                $selectors = array(
                    '.eltd-section-title',
                    '.eltd-content h3',
                );
                break;
        }

        foreach($selectors as $selector){
            $return_selector_array[] = 'body.'.$style_class.' '.$selector;
        }

        return $return_selector_array;

    }

}

if(!function_exists('creator_elated_get_paspartu_styles')){

        function creator_elated_get_paspartu_styles($style){

            $paspartu_flag = creator_elated_get_meta_field_intersect('enable_paspartu');
            $current_style = '';

            if($paspartu_flag === 'yes'){

                $paspartu_css = array();
                $paspartu_sticky_css = array();
                $paspartu_selectors  = array(
                    'body.eltd-paspartu-enabled .eltd-wrapper'
                );

                $paspartu_sticky_selectors = array(
                    'body.eltd-paspartu-enabled .eltd-page-header .eltd-sticky-header',
                );

                $paspartu_color = creator_elated_get_meta_field_intersect('paspartu_color');
                $paspartu_size = creator_elated_get_meta_field_intersect('paspartu_size');

                if($paspartu_color !== ''){
                    $paspartu_css['background-color'] = $paspartu_color;
                }

                if($paspartu_size !== ''){
                    $paspartu_css['padding'] = creator_elated_filter_px($paspartu_size).'px';
                    $paspartu_sticky_css['padding'] = creator_elated_filter_px($paspartu_size).'px';
                }

                $current_style = creator_elated_dynamic_css($paspartu_selectors, $paspartu_css);
                $current_style .= creator_elated_dynamic_css($paspartu_sticky_selectors, $paspartu_sticky_css);

            }

            $current_style = $current_style . $style;

            return $current_style;

        }
        add_filter('creator_elated_add_page_custom_style', 'creator_elated_get_paspartu_styles');
}

if(!function_exists('creator_elated_print_custom_css')) {
    /**
     * Prints out custom css from theme options
     */
    function creator_elated_print_custom_css() {
        $custom_css = creator_elated_options()->getOptionValue('custom_css');

        if($custom_css !== '') {
            wp_add_inline_style( 'creator-elated-modules', $custom_css);
        }
    }

    add_action('wp_enqueue_scripts', 'creator_elated_print_custom_css');
}

if(!function_exists('creator_elated_print_custom_js')) {
    /**
     * Prints out custom css from theme options
     */
    function creator_elated_print_custom_js() {
        $custom_js = creator_elated_options()->getOptionValue('custom_js');

        if($custom_js !== '') {
            wp_add_inline_script('creator-elated-modules', $custom_js);
        }
    }

    add_action('wp_enqueue_scripts', 'creator_elated_print_custom_js');
}



if(!function_exists('creator_elated_get_global_variables')) {
    /**
     * Function that generates global variables and put them in array so they could be used in the theme
     */
    function creator_elated_get_global_variables() {

        $global_variables = array();
        $element_appear_amount = -150;

        $global_variables['eltdAddForAdminBar'] = is_admin_bar_showing() ? 32 : 0;
        $global_variables['eltdElementAppearAmount'] = creator_elated_options()->getOptionValue('element_appear_amount') !== '' ? creator_elated_options()->getOptionValue('element_appear_amount') : $element_appear_amount;
        $global_variables['eltdFinishedMessage'] = esc_html__('No more posts', 'creator');
        $global_variables['eltdMessage'] = esc_html__('Loading new posts...', 'creator');
        $global_variables['eltdAddingToCart'] = esc_html__('Adding to Cart...', 'creator');

        $global_variables = apply_filters('creator_elated_js_global_variables', $global_variables);

        wp_localize_script('creator-elated-modules', 'eltdGlobalVars', array(
            'vars' => $global_variables
        ));

    }

    add_action('wp_enqueue_scripts', 'creator_elated_get_global_variables');
}

if(!function_exists('creator_elated_per_page_js_variables')) {
	/**
	 * Outputs global JS variable that holds page settings
	 */
	function creator_elated_per_page_js_variables() {
        $per_page_js_vars = apply_filters('creator_elated_per_page_js_vars', array());

        wp_localize_script('creator-elated-modules', 'eltdPerPageVars', array(
            'vars' => $per_page_js_vars
        ));
    }

    add_action('wp_enqueue_scripts', 'creator_elated_per_page_js_variables');
}

if(!function_exists('creator_elated_content_elem_style_attr')) {
    /**
     * Defines filter for adding custom styles to content HTML element
     */
    function creator_elated_content_elem_style_attr() {
        $styles = apply_filters('creator_elated_content_elem_style_attr', array());

        creator_elated_inline_style($styles);
    }
}

if(!function_exists('creator_elated_is_woocommerce_installed')) {
    /**
     * Function that checks if woocommerce is installed
     * @return bool
     */
    function creator_elated_is_woocommerce_installed() {
        return function_exists('is_woocommerce');
    }
}

if(!function_exists('creator_elated_visual_composer_installed')) {
    /**
     * Function that checks if visual composer installed
     * @return bool
     */
    function creator_elated_visual_composer_installed() {
        //is Visual Composer installed?
        if(class_exists('WPBakeryVisualComposerAbstract')) {
            return true;
        }

        return false;
    }
}

if(!function_exists('creator_elated_contact_form_7_installed')) {
    /**
     * Function that checks if contact form 7 installed
     * @return bool
     */
    function creator_elated_contact_form_7_installed() {
        //is Contact Form 7 installed?
        if(defined('WPCF7_VERSION')) {
            return true;
        }

        return false;
    }
}

if(!function_exists('creator_elated_is_wpml_installed')) {
    /**
     * Function that checks if WPML plugin is installed
     * @return bool
     *
     * @version 0.1
     */
    function creator_elated_is_wpml_installed() {
        return defined('ICL_SITEPRESS_VERSION');
    }
}

if (!function_exists('creator_elated_is_membership_installed')) {
    /**
     * Function that checks if Elated Membership Plugin installed
     *
     * @return bool
     */
    function creator_elated_is_membership_installed() {
        return defined('ELATED_MEMBERSHIP_VERSION');
    }
}

if(!function_exists('creator_elated_max_image_width_srcset')) {
	/**
	 * Set max width for srcset to 1920
	 *
	 * @return int
	 */
	function creator_elated_max_image_width_srcset() {
        return 1920;
    }

	add_filter('max_srcset_image_width', 'creator_elated_max_image_width_srcset');
}

if(! function_exists('creator_elated_get_user_custom_fields')){
    /**
     * Function returns links and icons for author social networks
     *
     * return array
     *
     */
    function creator_elated_get_user_custom_fields( $id ){

        $user_social_array = array();
        $social_network_array = array('instagram', 'twitter','facebook','googleplus');

        foreach($social_network_array as $network){

            $$network = array(
                'name' => esc_attr($network),
                'link' => get_the_author_meta($network, $id),
                'class' => 'social_'.$network
            );

            $user_social_array[$network] = $$network;

        }

        return $user_social_array;
    }
}

if ( ! function_exists( 'creator_elated_attachment_image_size_media' ) ) {
    /**
     * Add Attachment Image Sizes option
     *
     * @param $form_fields array, fields to include in attachment form
     * @param $post object, attachment record in database
     *
     * @return mixed
     */
    function creator_elated_attachment_image_size_media( $form_fields, $post ) {

        $options = array(
            'default'   => esc_html__('Default', 'creator'),
            'large_height' => esc_html__('Large Height', 'creator'),
            'large_width_height' => esc_html__('Large Width/Height', 'creator')
        );

        $html = '';
        $selected = get_post_meta( $post->ID, 'attachment_image_size', true );

        $html .= '<select name="attachments['. $post->ID .'][attachment-image-size]" class="attachment-image-size" data-setting="attachment-image-size">';
        // Browse and add the options
        foreach ( $options as $key => $value ) {
            if ( $key == $selected ) {
                $html .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $html .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }

        $html .= '</select>';

        $form_fields['attachment-image-size'] = array(
            'label' => esc_html__('Attachment Image Size', 'creator'),
            'input' => 'html',
            'html' => $html,
            'value' => get_post_meta( $post->ID, 'attachment_image_size', true )
        );

        return $form_fields;
    }

    add_filter( 'attachment_fields_to_edit', 'creator_elated_attachment_image_size_media', 10, 2 );

}

if ( ! function_exists( 'creator_elated_attachment_image_size_media_save' ) ) {
    /**
     * Save values of Attachment Image sizes in media uploader
     *
     * @param $post array, the post data for database
     * @param $attachment array, attachment fields from $_POST form
     *
     * @return mixed
     */
    function creator_elated_attachment_image_size_media_save( $post, $attachment ) {

        if( isset( $attachment['attachment-image-size'] ) ) {
            update_post_meta( $post['ID'], 'attachment_image_size', $attachment['attachment-image-size'] );
        }
        return $post;

    }

    add_filter( 'attachment_fields_to_save', 'creator_elated_attachment_image_size_media_save', 10, 2 );

}

if(!function_exists('creator_elated_excerpt_by_id')){

    function creator_elated_excerpt_by_id($excerpt_length = '', $id){
        $post = get_post($id);

        //is word count set to something different that 0?
        if($excerpt_length !== '0') {
            //if word count is set and different than empty take that value, else that general option from theme options
            $word_count = '10';

            if(isset($excerpt_length) && $excerpt_length != ""){
                $word_count = $excerpt_length;
            }

            //if post excerpt field is filled take that as post excerpt, else that content of the post
            $post_excerpt = $post->post_excerpt != "" ? $post->post_excerpt : strip_tags($post->post_content);

            //remove leading dots if those exists
            $clean_excerpt = strlen($post_excerpt) && strpos($post_excerpt, '...') ? strstr($post_excerpt, '...', true) : $post_excerpt;

            //if clean excerpt has text left
            if($clean_excerpt !== '') {
                //explode current excerpt to words
                $excerpt_word_array = explode (' ', $clean_excerpt);

                //cut down that array based on the number of the words option
                $excerpt_word_array = array_slice ($excerpt_word_array, 0, $word_count);

                //add exerpt postfix
                $excert_postfix		= apply_filters('creator_elated_excerpt_postfix', '...');

                //and finally implode words together
                $excerpt 			= implode (' ', $excerpt_word_array).$excert_postfix;

                //is excerpt different than empty string?
                if($excerpt !== '') {
                    echo '<p class="eltd-post-excerpt">'.do_shortcode($excerpt).'</p>';
                }
            }
        }
    }

}

if(!function_exists('creator_elated_get_first_main_color')){
    /**
     * Functiom generates first color which will be used in some other functions(creator_elated_get_button_html) where this value need to be generated
     *
     * @return string
     */
    function creator_elated_get_first_main_color(){

        $first_color = '#b79c7d';

        $first_color_options = creator_elated_options()->getOptionValue('first_color');
        if($first_color_options !== ''){
            $first_color = $first_color_options;
        }

        return $first_color;
    }

}

if(!function_exists('creator_elated_attachment_image_additional_fields')) {
    /**
     * Add Attachment Image Sizes option
     *
     * @param $form_fields array, fields to include in attachment form
     * @param $post object, attachment record in database
     *
     * @return mixed
     */
    function creator_elated_attachment_image_additional_fields($form_fields, $post) {

        // ADDING IMAGE LINK FILED - START //

        $form_fields['attachment-image-link'] = array(
            'label' => esc_html__('Image Link', 'creator'),
            'input' => 'text',
            'application' => 'image',
            'exclusions'  => array( 'audio', 'video' ),
            'value' => get_post_meta($post->ID, 'attachment_image_link', true)
        );

        // ADDING IMAGE LINK FILED - END //

        return $form_fields;
    }

    add_filter('attachment_fields_to_edit', 'creator_elated_attachment_image_additional_fields', 10, 2);

}

if(!function_exists('creator_elated_attachment_image_additional_fields_save')) {
    /**
     * Save values of Attachment Image sizes in media uploader
     *
     * @param $post array, the post data for database
     * @param $attachment array, attachment fields from $_POST form
     *
     * @return mixed
     */
    function creator_elated_attachment_image_additional_fields_save($post, $attachment) {

        if(isset($attachment['attachment-image-link'])) {
            update_post_meta($post['ID'], 'attachment_image_link', $attachment['attachment-image-link']);
        }

        return $post;

    }

    add_filter('attachment_fields_to_save', 'creator_elated_attachment_image_additional_fields_save', 10, 2);

}
if ( ! function_exists( 'creator_elated_is_gutenberg_installed' ) ) {
	/**
	 * Function that checks if Gutenberg plugin installed
	 * @return bool
	 */
	function creator_elated_is_gutenberg_installed() {
		return function_exists( 'is_gutenberg_page' ) && is_gutenberg_page();
	}
}

if ( ! function_exists( 'creator_elated_is_wp_gutenberg_installed' ) ) {
	/**
	 * Function that checks if WordPress 5.x with Gutenberg editor installed
	 * @return bool
	 */
	function creator_elated_is_wp_gutenberg_installed() {
		return class_exists( 'WP_Block_Type' );
	}
}

if ( ! function_exists( 'creator_elated_get_module_part' ) ) {
	function creator_elated_get_module_part( $module ) {
		return $module;
	}
}