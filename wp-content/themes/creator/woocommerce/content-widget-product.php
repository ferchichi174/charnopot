<?php
/**
 * The template for displaying product widget entries.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if ( ! is_a( $product, 'WC_Product' ) ) {
	return;
}

?>

<?php 

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

?>


<li>
	<div class="eltd-woocommerce-widget-product-image">
		<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"
		   title="<?php echo esc_attr( $product->get_title() ); ?>">
			<?php echo creator_elated_get_module_part( $product->get_image() ); ?>
		</a>
	</div>
	<div class="eltd-woocommerce-widget-product-details">
		<h5 class="product-title">
			<a	href="<?php echo esc_url( $product->get_permalink() ); ?>"><?php echo creator_elated_get_module_part( $product->get_title() ); ?></a></h5>
			<?php if ( ! empty( $show_rating ) ) {
				print wc_get_rating_html( $average, $rating_count ); 
			}
		?>
	</div>
	<div class="eltd-woocommerce-widget-product-price">
		<?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
	</div>
</li>
