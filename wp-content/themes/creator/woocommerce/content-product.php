<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$product_list_type = creator_elated_options()->getOptionValue('woo_product_list_type');
if($product_list_type === 'type2'){ ?>
	<li class="product ">
		<div class="eltd-product-inner">
			<div class="eltd-product-image-holder">
				<?php
				do_action( 'woocommerce_before_shop_loop_item' );
				do_action( 'woocommerce_before_shop_loop_item_title' );
				?>
				<div class="eltd-product-image-overlay">
					<a class="eltd-product-link" href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"></a>
					<div class="eltd-product-image-overlay-inner">
						<?php
						do_action( 'creator_elated_woocommerce_product_overlay' );
						?>
					</div>
				</div>
			</div>
			<div class="eltd-product-content-holder">
				<div class="eltd-product-title-holder">
					<div class="eltd-product-price">
						<?php echo creator_elated_get_module_part( $product->get_price_html() ); ?>
					</div>
					<h5 class="eltd-product-list-product-title"><a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"><?php echo get_the_title();?></a></h5>
				</div>
				<?php
				do_action( 'woocommerce_after_shop_loop_item_title' );
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
		</div>
	</li>
	<?php
}
else{
?>
<li <?php wc_product_class( '', $product );  ?> >
	<div class="eltd-product-inner">

		<div class="eltd-product-image-holder">

			<?php
			/**
			 * woocommerce_before_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_open - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item' );

			/**
			 * woocommerce_before_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			<div class="eltd-product-image-overlay">
				<a class="eltd-product-link" href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"></a>
				<div class="eltd-product-image-overlay-inner">
					<?php
					/**
					 * creator_elated_woocommerce_product_overlay hook
					 *
					 * @hooked woocommerce_template_loop_add_to_cart - 10
					 */
					do_action( 'creator_elated_woocommerce_product_overlay' );
					?>
				</div>
			</div>
		</div>

		<div class="eltd-product-content-holder">

			<div class="eltd-product-title-holder">
				<?php
				/**
				 * woocommerce_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_product_title - 10
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_shop_loop_item_title' );
				?>
			</div>
			<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );

			/**
			 * woocommerce_after_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_close - 5
			 */
			do_action( 'woocommerce_after_shop_loop_item' );
			?>

		</div>


	</div>
</li>



<?php } ?>