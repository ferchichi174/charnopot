<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

$class = '';
$product_list_type = creator_elated_options()->getOptionValue('woo_product_list_type');
if($product_list_type === 'type2'){
    $class = 'eltd-type-2';
}
else {
    $class = 'eltd-type-1';
}

?>
<ul class="products columns <?php echo esc_attr($class)?> ">
