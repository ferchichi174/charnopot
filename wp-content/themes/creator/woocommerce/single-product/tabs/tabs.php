<?php
/**
 * Single Product tabs
 * @see 	https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $product_tabs ) ) : ?>

	<div class="eltd-tabs woocommerce-tabs wc-tabs-wrapper eltd-horizontal-tab eltd-color-tabs clearfix">
		<ul class="eltd-tabs-nav tabs wc-tabs">
			<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab">
					<a href="#tab-<?php echo esc_attr( $key ); ?>">
						<span class="eltd-icon-frame">
							<span class = "eltd-woo-tab-icon"></span>
						</span>
						<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $product_tab['title'] ), $key ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
			<div class="eltd-tab-container woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>">
				<?php call_user_func( $product_tab['callback'], $key, $product_tab ); ?>
			</div>
		<?php endforeach; ?>

        <?php do_action( 'woocommerce_product_after_tabs' ); ?>
	</div>

<?php endif; ?>