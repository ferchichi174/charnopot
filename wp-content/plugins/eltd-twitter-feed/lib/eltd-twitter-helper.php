<?php

if(!defined('ABSPATH')) exit;

class ElatedTwitterHelper {
    public function getTweetText($tweet) {
        $protocol = is_ssl() ? 'https' : 'http';
        if(!empty($tweet['text'])) {
            //add links around https or http parts of text
            $tweet['text'] = preg_replace('/(https?)\:\/\/([a-z0-9\/\.\&\#\?\-\+\~\_\,]+)/i', '<a target="_blank" href="'.('$1://$2').'">$1://$2</a>', $tweet['text']);

            //add links around @mentions
            $tweet['text'] = preg_replace('/\@([a-aA-Z0-9\.\_\-]+)/i', '<a target="_blank" href="'.esc_url($protocol.'://twitter.com/$1').'">@$1</a>', $tweet['text']);

            //add links around #hastags
            $tweet['text'] = preg_replace('/\#([a-aA-Z0-9\.\_\-]+)/i', '<a target="_blank" href="'.esc_url($protocol.'://twitter.com/hashtag/$1').'">#$1</a>', $tweet['text']);

            return $tweet['text'];
        }

        return '';
    }

    public function getTweetUserName($tweet) {
        if(isset($tweet['user']['name'])) {
            return esc_attr($tweet['user']['name']);
        }

        return '';
    }

    public function getTweetTime($tweet) {
        if(!empty($tweet['created_at'])) {
            return human_time_diff(strtotime($tweet['created_at']), current_time('timestamp') ).' '.__('ago', 'eltd_twitter_feed');
        }

        return '';
    }

    public function getTweetCreateTime($tweet) {
        if(!empty($tweet['created_at'])) {
            return $tweet['created_at'];
        }

        return '';
    }

    public function getTweetUserURL($tweet) {

        if(isset($tweet['user']['entities']['url'])){
            $url_string = $tweet['user']['entities']['url']['urls'][0]['expanded_url'];
            if(isset($url_string)) {
                return esc_url($url_string);
            }
        }

        return '';
    }

    public function getTweetURL($tweet) {
        if(!empty($tweet['id_str']) && $tweet['user']['screen_name']) {
            return 'https://twitter.com/'.$tweet['user']['screen_name'].'/statuses/'.$tweet['id_str'];
        }

        return '#';
    }

    public function checkTweetImage($tweet){
        if(isset($tweet['user']['profile_image_url_https'])){
            return true;
        }

        return false;
    }

    public function getTweetImage($tweet){

        if(isset($tweet['user']['profile_image_url_https'])){
            return 'background-image: url( '.$tweet["user"]["profile_image_url_https"].')';
        }

        return '';
    }
}

if(!function_exists('eltd_twitter_feed_get_shortcode_module_template_part')) {
    /**
     * Loads module template part.
     *
     * @param string $shortcode name of the shortcode folder
     * @param string $template name of the template to load
     * @param string $slug
     * @param array $params array of parameters to pass to template
     *
     * @return string
     */
    function eltd_twitter_feed_get_shortcode_module_template_part($shortcode,$template, $slug = '', $params = array()) {

        //HTML Content from template
        $html = '';
        $template_path = ELATED_TWITTER_FEED_ABS_PATH.'/shortcodes/'.$shortcode.'/templates';

        $temp = $template_path.'/'.$template;
        if(is_array($params) && count($params)) {
            extract($params);
        }

        $template = '';

        if($temp !== '') {
            if($slug !== '') {
                $template = "{$temp}-{$slug}.php";
            }
            $template = $temp.'.php';
        }
        if($template) {
            ob_start();
            include($template);
            $html = ob_get_clean();
        }

        return $html;
    }
}