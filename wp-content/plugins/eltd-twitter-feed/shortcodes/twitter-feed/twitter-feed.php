<?php

namespace ElatedTwitterFeed\Shortcodes;

use ElatedTwitterFeed\Lib;

/**
 * Class Promotion
 * @package ElatedTwitterFeed\Shortcodes
 */
class TwitterFeed implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_twitter_feed';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
				'name'                      => esc_html__('Twitter Feed' , 'eltd_twitter_feed'),
				'base'                      => $this->base,
				'category'                  => esc_html__('by ELATED', 'eltd_twitter_feed'),
				'icon'                      => 'icon-wpb-twitter-feed extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type' => 'dropdown',
						'admin_label' => true,
						'heading' => esc_html__('Search by', 'eltd_twitter_feed'),
						'param_name' => 'type',
						'value' => array(
							esc_html__('User', 'eltd_twitter_feed') => 'user_id',
							esc_html__('Hashtag', 'eltd_twitter_feed') => 'hashtag'
						),
						'save_always' => true,
						'description' => '',
					),
					array(
						'type' => 'textfield',
						'admin_label' => true,
						'heading' => esc_html__('Enter Hashtag', 'eltd_twitter_feed'),
						'param_name' => 'hashtag',
						'description' => '',
						'dependency' => array(
							'element' => 'type',
							'value'  => 'hashtag'
						)
					),
					array(
						'type' => 'textfield',
						'admin_label' => true,
						'heading' => esc_html__('Enter User ID', 'eltd_twitter_feed'),
						'param_name' => 'user_id',
						'description' => '',
						'dependency' => array(
							'element' => 'type',
							'value'  => 'user_id'
						)
					),
					array(
						'type' => 'textfield',
						'admin_label' => true,
						'heading' => esc_html__('Number of Items', 'eltd_twitter_feed'),
						'param_name' => 'number',
						'description' => '',
					),
					array(
						'type' => 'dropdown',
						'admin_label' => true,
						'heading' => esc_html__('Number of Columns', 'eltd_twitter_feed'),
						'param_name' => esc_html__('number_columns', 'eltd_twitter_feed'),
						'value' => array(
							esc_html__('Two', 'eltd_twitter_feed') => '2',
							esc_html__('Three', 'eltd_twitter_feed') => '3',
						),
						'description' => '',
						'save_always' => true
					)
				)
			) );
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$args   = array(
			'type' => '',
			'hashtag' => '',
			'user_id' => '',
			'number' => '5',
			'number_columns' => '',
		);
		$params = shortcode_atts( $args, $atts );
		extract( $params );

		$html = '';
		$params['holderClasses'] = $this->getHolderClasses($params);

		$response = false;
		$twitter_api = \ElatedTwitterApi::getInstance();

		if($twitter_api->hasUserConnected()) {
			if($type === 'user_id'){
				$response = $twitter_api->fetchTweets($user_id, $number, array(
					'transient_time' => 0,
					'transient_id'   => 'eltd_twitter_'.$user_id
				));
			}
			elseif($type === 'hashtag'){
				$response = $twitter_api->fetchTweetsByHashTag($hashtag, $number, array(
					'transient_time' => 0,
					'transient_id'   => 'eltd_twitter_'.$hashtag
				));
			}

			if($response){
				if($response->status) {
					$params['twitter_api'] = $twitter_api;
					$html .= '<div class="eltd-twitter-feed-holder '.$params['holderClasses'].' clearfix">';

					if(is_array($response->data) && count($response->data)){

						if($type === 'user_id'){
							foreach($response->data as $tweet){
								$params['current_tweet']  = $tweet;
								$params['tweet_image_flag'] = $twitter_api->getHelper()->checkTweetImage($tweet);
								$params['tweet_image_style'] = $twitter_api->getHelper()->getTweetImage($tweet);
								$params['tweet_url'] = $twitter_api->getHelper()->getTweetURL($tweet);
								$params['tweet_date'] = $twitter_api->getHelper()->getTweetCreateTime($tweet);
								$params['tweet_user_name'] = $twitter_api->getHelper()->getTweetUserName($tweet);
								$params['tweet_text'] = $twitter_api->getHelper()->getTweetText($tweet);
								$params['tweet_user_url'] = $twitter_api->getHelper()->getTweetUserURL($tweet);
								$html .= eltd_twitter_feed_get_shortcode_module_template_part( 'twitter-feed', 'twitter-feed-template', '', $params );
						    }
						}elseif($type === 'hashtag'){
							if(is_array($response->data['statuses']) && count($response->data['statuses'])){

								foreach($response->data['statuses'] as $tweet){
									$params['current_tweet']  = $tweet;
									$params['current_tweet']  = $tweet;
									$params['tweet_image_flag'] = $twitter_api->getHelper()->checkTweetImage($tweet);
									$params['tweet_image_style'] = $twitter_api->getHelper()->getTweetImage($tweet);
									$params['tweet_url'] = $twitter_api->getHelper()->getTweetURL($tweet);
									$params['tweet_date'] = $twitter_api->getHelper()->getTweetCreateTime($tweet);
									$params['tweet_user_name'] = $twitter_api->getHelper()->getTweetUserName($tweet);
									$params['tweet_text'] = $twitter_api->getHelper()->getTweetText($tweet);
									$params['tweet_user_url'] = $twitter_api->getHelper()->getTweetUserURL($tweet);
									$html .= eltd_twitter_feed_get_shortcode_module_template_part( 'twitter-feed', 'twitter-feed-template', '', $params );
								}
							}
							else{
								$html .= esc_html__('Sorry, no posts matched your criteria.', 'eltd_twitter_feed');
							}
						}

					}
					else{
						$html .= esc_html__('Sorry, no posts matched your criteria.', 'eltd_twitter_feed');
					}


					$html .= '</div>'; //close eltd-twitter-feed-holder
				}
			}

		}else{
			$html .= esc_html__('It seams like you haven\t connected with your Twitter account', 'eltd_twitter_feed');
		}

		return $html;
	}

	private function getHolderClasses($params){
		$classes = array();
		$columnsNumber=$params['number_columns'];

		if(isset($columnsNumber) && $columnsNumber!==''){
			switch($columnsNumber){
				case '2':
					$classes[] = 'eltd-twitter-feed-two-columns';
					break;
				case '3':
					$classes[]  = 'eltd-twitter-feed-three-columns';
					break;
				default:
					break;
			}
		}
		if(isset($type)){
			if($type === 'user_id'){
				$classes[] = 'etld-twitter-search-by-user';
			}
			elseif($type === 'hashtag'){
				$classes[] = 'etld-twitter-search-by-hashtag';
			}
		}

		return implode('', $classes);
	}

}