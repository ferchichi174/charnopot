<div class="eltd-twitter-feed-item-holder">
	<?php if($tweet_image_flag){ ?>
		<div class="eltd-twitter-feed-image-holder">
			<div class="eltd-twitter-feed-image-holder-inner" <?php echo creator_elated_get_inline_style($tweet_image_style); ?>>
				<a class="eltd-twitter-feed-link"  target="_blank" href="<?php echo esc_url($tweet_url); ?>">
				</a>
			</div>
		</div>
	<?php } ?>

	<div class="eltd-twitter-feed-text-holder">
		<div class="eltd-twitter-feed-date">
			<a target="_blank" href="<?php echo esc_url($tweet_url); ?>">
				<?php echo wp_kses_post($tweet_date); ?>
			</a>
		</div>
		<h5 class="eltd-twitter-feed-username">
			<a target="_blank" href="<?php echo esc_url($tweet_url); ?>">
				<?php echo wp_kses_post($tweet_user_name); ?>
			</a>
		</h5>
		<div class = "eltd-twitter-feed-text">
			<?php
			echo wp_kses_post($tweet_text);

			if(isset ($tweet_user_url) && $tweet_user_url !==''){?>
				<div class="eltd-twitter-user-url">
					<a href="<?php echo creator_elated_get_module_part($tweet_user_url);  ?>">
						<?php echo creator_elated_get_module_part($tweet_user_url);  ?>
					</a>
				</div>
			<?php }?>
		</div>
	</div>
</div>