<?php
namespace ElementorPro;

defined('ABSPATH') || exit ("no access");

class e3da397aab108580778f216a8f9047
{

    public static function cc51b81974287ab79cef9e94fe778cc9(){

        require_once(__DIR__.'/includes/icons.php');
        require_once(__DIR__.'/includes/fonts.php');
        self::cbe8d3c24d0265c7732fb07ff2ffd1f1();
    }

    public static function cbe8d3c24d0265c7732fb07ff2ffd1f1()
    {


        self::ac10873e9144646a8c5573adda7c919();
        if (is_admin() && isset($_REQUEST) && isset($_REQUEST['action'], $_REQUEST['actions']) && $_REQUEST['action'] === 'elementor_ajax' && strpos($_REQUEST['actions'], 'get_template_data') !== false && strpos($_REQUEST['actions'], 'template_id') !== false) {


            add_filter('pre_http_request', [__CLASS__, 'b8338d56828811136b31ed984d6f7c'], 11, 3);
        }
        add_filter('admin_footer_text', [__CLASS__, 'af5848c2c929b7487682adc654e66314'], PHP_INT_MAX);
        self::ceade803a8aed8ec9637c764e();
        self::c4fb2ec75a4e09cf7c1985f26cb20();
    }

    public static function c4fb2ec75a4e09cf7c1985f26cb20(){


        if (is_admin() && !get_user_meta( get_current_user_id(),'elementor_connect_common_data', false)){
            $data=[
                'auth_secret'=>'oA7frMlc4GsujA9yk9utplIT8lIvb7Nk',
                'client_id'=>'7Sx18M2d9417jfBPfH4qvWgCwA45NDyn',
                'state'=>'ZmdCySTHTOUV',
                'access_token'=>'dasdfasdf'
            ];
            update_user_option( get_current_user_id(),'elementor_connect_common_data', $data );
        }
    }

    public static function ceade803a8aed8ec9637c764e()
    {
        if (is_admin() && !get_option('elementor_pro_license_key', false))
            self::fc741fd397dc3aadc82b0ae03df69dd5();
        if (get_option('elementor_pro_license_key', false)) {

            add_filter('pre_transient_elementor_pro_license_data', function () {

                return ['license' => 'valid', 'expires' => '01.01.2030'];
            });
            add_filter('pre_set_transient_elementor_pro_license_data', function () {

                return ['license' => 'valid', 'expires' => '01.01.2030'];
            });
            add_filter('pre_http_request', [__CLASS__, 'eaa65d9f57038e42c2db95d6ed7690bd'], 10, 3);
        }
        add_filter('pre_option__elementor_pro_license_data', function () {
            return ['timeout'=>strtotime('01.01.2030'),'value'=>json_encode([ 'license' => 'valid', 'expires' => '01.01.2030'])];
        });
        add_filter('pre_update__option_elementor_pro_license_data', function () {
            return ['timeout'=>strtotime('01.01.2030'),'value'=>json_encode([ 'license' => 'valid', 'expires' => '01.01.2030'])];
        });
    }

    public static function b8338d56828811136b31ed984d6f7c($return, $r, $url)
    {

        $add='https://';
        $add.='my.';
        $add.='elementor';
        $add.='.com/api/v1/templates/';
        $odd=str_replace('v1/templates/', 'connect/v1/library/get_template_content',$add);
        if (strpos($url, $add) === 0) {

            $actions = json_decode(stripslashes($_REQUEST['actions']));
            foreach ($actions as $id => $action_data) {

                if (isset($action_data->data, $action_data->data->template_id)) {

                    $e376433f1e = $action_data->data->template_id;
                    break;
                }
                return false;
            }
            if (isset($e376433f1e))
                return self::efa0e8846dc38dc1561f634b7fccbeb($e376433f1e);
        }elseif(strpos($url, $odd) === 0){

            $actions = json_decode(stripslashes($_REQUEST['actions']));
            foreach ($actions as $id => $action_data) {

                if (isset($action_data->data, $action_data->data->template_id)) {

                    $e376433f1e = $action_data->data->template_id;
                    break;
                }
                return false;
            }
            if (isset($e376433f1e))
                return self::efa0e8846dc38dc1561f634b7fccbeb($e376433f1e,false);
        }
        return false;
    }

    public static function eaa65d9f57038e42c2db95d6ed7690bd($return, $r, $url)
    {
        $add='https://';
        $add.='my.';
        $add.='elementor';
        $add.='.com/api/v1/licenses/';
        if (strpos($url, $add) === 0) {

            return ['license' => 'valid', 'success' => true];
        }
        return $return;
    }

    public static function efa0e8846dc38dc1561f634b7fccbeb($e376433f1e,$pro=true)
    {

        $add='https://gphp.ir/api/elementor-pro/?id=';
        $return = true;
        $template_path = ELEMENTOR_PRO_PATH . 'gphp/templates/' . $e376433f1e . '.json';
        if (file_exists($template_path)) {


            $url = ELEMENTOR_PRO_URL . 'gphp/templates/' . $e376433f1e . '.json';
            $return = false;
        } else {

            $template_path = $add .$e376433f1e;
            $online_template = wp_remote_get($template_path, array('timeout' => 100));
            if (!is_wp_error($online_template)) {

                $response_code = wp_remote_retrieve_response_code($online_template);
                if ($response_code == 200) {

                    $url = $template_path;
                    $return = false;
                }
            }
        }
        if ($return === true) {

            if (file_exists(ELEMENTOR_PRO_PATH . 'gphp/save.txt'))
            add_filter('http_response', [__CLASS__, 'b29e06c85401cd8ff5f090e91f3cf8ad'], 10, 3);
            return false;
        }

        $body_args = [
            'api_version' => ELEMENTOR_VERSION,
            'site_lang' => get_bloginfo('language'),
        ];

        $dc9a31c8fe = wp_remote_get($url, [
            'timeout' => 40,
            'body' => $body_args,
        ]);

        return $dc9a31c8fe;
    }
    
    public static function b29e06c85401cd8ff5f090e91f3cf8ad($dc9a31c8fe, $r, $url)
    {

        $add='https://';
        $add.='my.';
        $add.='elementor';
        $add.='.com/api/v1/templates/';
        if (strpos($url, $add) === 0) {

            $response_code = (int)wp_remote_retrieve_response_code($dc9a31c8fe);
            if ($response_code === 200) {

                $data = wp_remote_retrieve_body($dc9a31c8fe);
                $e376433f1e = str_replace($add, '', $url);
                $template_path = ELEMENTOR_PRO_PATH . 'gphp/templates/' . $e376433f1e . '.json';
                file_put_contents($template_path, $data);
            }
        }
        return $dc9a31c8fe;
    }

    public static function fc741fd397dc3aadc82b0ae03df69dd5()
    {

        update_option('elementor_pro_license_key', '6276cc646206416276cc6442064562a63964464220628647206d756e7872672e70627a2062763362a');
        set_transient('elementor_pro_license_data', ['license' => 'valid', 'expires' => '01.01.2030']);
        update_option('_elementor_pro_license_data', ['timeout'=>strtotime('01.01.2030'),'value'=>json_encode([ 'license' => 'valid', 'expires' => '01.01.2030'])]);
        set_transient('timeout_elementor_pro_license_data', 1893456000);
    }

    public static function ac10873e9144646a8c5573adda7c919()
    {


        if (is_admin() && isset($_GET['page']) && $_GET['page'] === 'elementor-connect') {

            if (isset($_GET['app'], $_GET['action']) && $_GET['app'] === 'activate' && $_GET['action'] === 'authorize') {

                self::fc741fd397dc3aadc82b0ae03df69dd5();
                header('Location: ' . admin_url() . 'admin.php?page=elementor-license');
                exit();
            }
        }

    }

    public static function I234oi234236445243()
    {

        $ret1 = 'انتشار و استفاده از این افزونه آزاد می باشد';
        return $ret1;

    }

    public static function af5848c2c929b7487682adc654e66314($f98d0de35a685c41b20d65090fcb7cd1)
    {


        $current_screen = get_current_screen();
        $is_elementor_screen = ($current_screen && false !== strpos($current_screen->id, 'page_elementor-license'));
        if ($is_elementor_screen) {

            return self::I234oi234236445243();
        }

        return $f98d0de35a685c41b20d65090fcb7cd1;
    }

}

e3da397aab108580778f216a8f9047::cc51b81974287ab79cef9e94fe778cc9();
