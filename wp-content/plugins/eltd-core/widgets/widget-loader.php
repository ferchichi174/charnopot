<?php

if (!function_exists('creator_elated_register_widgets')) {

	function creator_elated_register_widgets() {

		$widgets = array(
			'CreatorElatedFullScreenMenuOpener',
			'CreatorElatedLatestPosts',
			'CreatorElatedSearchOpener',
			'CreatorElatedSideAreaOpener',
			'CreatorElatedStickySidebar',
			'CreatorElatedSocialIconWidget',
			'CreatorElatedSeparatorWidget',
			'CreatorElatedStickySidebar'
		);

		if ( creator_elated_is_woocommerce_installed() ) {
			$widgets[] = 'CreatorElatedWoocommerceDropdownCart';
		}

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'creator_elated_register_widgets');