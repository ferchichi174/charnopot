<div class="eltd-promotion-item-holder">
	<?php if(has_post_thumbnail()){ ?>

		<div class="eltd-promotion-item-image">
			<?php the_post_thumbnail('full'); ?>
		</div>

	<?php } ?>
	<div class="eltd-promotion-item-text-holder">
		<div class="eltd-title-holder">
			<div class="eltd-promotion-item-title">
				<?php the_title(); ?>
			</div>
			<?php if(isset($price) && $price !== '' ){?>

				<div class="eltd-promotion-price-holder">

					<?php if(isset($discount_price) && $discount_price !== '' ){?>
						<div class="eltd-discount-price">
							<?php echo esc_attr($discount_price); ?>
						</div>
					<?php } ?>

					<div class="eltd-promotion-price">
						<?php echo esc_attr($price); ?>
					</div>


				</div>
			<?php } ?>
		</div>

		<?php if(isset($period) && $period !== '' ){?>


			<div class="eltd-promotion-period">
				<span class="eltd-period-icon">
					<?php echo creator_elated_icon_collections()->renderIcon( 'icon_calendar', 'font_elegant' ); ?>
				</span>
				<?php echo esc_attr($period); ?>
			</div>

		<?php } ?>

		<?php if(isset($destination) && $destination !== '' ){?>

			<div class="eltd-promotion-destination">
				<span class="eltd-destination-icon">
					<?php echo creator_elated_icon_collections()->renderIcon( 'icon_pin_alt', 'font_elegant' ); ?>
				</span>
				<?php echo esc_attr($destination); ?>
			</div>

		<?php } ?>
	</div>
</div>