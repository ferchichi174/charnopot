<?php

namespace ElatedCore\CPT\Promotion\Shortcodes;


use ElatedCore\Lib;

/**
 * Class Promotion
 * @package ElatedCore\CPT\Promotion\Shortcodes
 */
class Promotion implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_promotion_list';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );

		//Portfolio category filter
		add_filter( 'vc_autocomplete_eltd_promotion_list_category_callback', array(
			&$this,
			'promotionCategoryAutocompleteSuggester',
		), 10, 1 ); // Get suggestion(find). Must return an array

		//Portfolio category render
		add_filter( 'vc_autocomplete_eltd_promotion_list_category_render', array(
			&$this,
			'promotionCategoryAutocompleteRender',
		), 10, 1 ); // Get suggestion(find). Must return an array
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
				'name'                      => esc_html__('Promotion List', 'eltd_core'),
				'base'                      => $this->base,
				'category'                  => esc_html__('by ELATED', 'eltd_core'),
				'icon'                      => 'icon-wpb-promotion-list extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type'        => 'autocomplete',
						'admin_label' => true,
						'heading'     => esc_html__('Category', 'eltd_core'),
						'param_name'  => 'category',
						'description' => esc_html__('Category Slug (leave empty for all)', 'eltd_core')
					),
					array(
						'type' => 'textfield',
						'admin_label' => true,
						'heading' => esc_html__('Number of Items', 'eltd_core'),
						'param_name' => 'number',
						'description' => '',
					),
					array(
						'type' => 'dropdown',
						'admin_label' => true,
						'heading' => esc_html__('Number of Columns', 'eltd_core'),
						'param_name' => 'number_columns',
						'value' => array(
							esc_html__('Two', 'eltd_core') => '2',
							esc_html__('Three', 'eltd_core') => '3',
						),
						'description' => '',
						'save_always' => true
					)
				)
			) );
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$args   = array(
			'number' => '-1',
			'category'       => '',
			'number_columns' => '',
		);
		$params = shortcode_atts( $args, $atts );

		extract( $params );

		$query_args = $this->getQueryParams( $params );
		$query = new \WP_Query( $query_args );
		$params['holderClasses'] = $this->getHolderClasses($params);

		$html = '';
		$html .= '<div class="eltd-promotion-list-holder '.$params['holderClasses'].' clearfix">';

		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {
				$query->the_post();

				$params['price']     = get_post_meta( get_the_ID(), 'eltd_promotion_price', true );
				$params['discount_price']       = get_post_meta( get_the_ID(), 'eltd_promotion_discount_price', true );
				$params['period']       = get_post_meta( get_the_ID(), 'eltd_promotion_period', true );
				$params['destination']       = get_post_meta( get_the_ID(), 'eltd_promotion_destination', true );



				$html .= eltd_core_get_shortcode_module_template_part( 'promotion', 'promotion-template', '', $params );
			}

		} else {
			$html .= __( 'Sorry, no posts matched your criteria.', 'eltd_core' );
		}
		wp_reset_postdata();
		$html .= '</div>';

		return $html;
	}

	private function getHolderClasses($params){
		$classes='';

		$columnsNumber=$params['number_columns'];

		if(isset($columnsNumber) && $columnsNumber!==''){
			switch($columnsNumber){
				case '2':
					$classes .= 'eltd-promotion-two-columns';
					break;
				case '3':
					$classes  .= 'eltd-promotion-three-columns';
					break;
				default:
					break;
			}
		}

		return $classes;
	}

	/**
	 * Generates promotion query attribute array
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getQueryParams( $params ) {

		$args = array(
			'post_type'      => 'promotion-item',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'posts_per_page' => $params['number']
		);

		if ( $params['category'] != '' ) {
			$args['promotion-category'] = $params['category'];
		}

		return $args;
	}

	/**
	 * Filter testimonial categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function promotionCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS promotion_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'promotion-category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['promotion_title'] ) > 0 ) ? esc_html__( 'Category', 'eltd_core' ) . ': ' . $value['promotion_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find testimonials by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function promotionCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get testimonial category
			$promotion_category = get_term_by( 'slug', $query, 'promotion-category' );
			if ( is_object( $promotion_category ) ) {

				$promotion_category_slug = $promotion_category->slug;
				$promotion_category_title = $promotion_category->name;

				$promotion_category_title_display = '';
				if ( ! empty( $promotion_category_title ) ) {
					$promotion_category_title_display = esc_html__( 'Category', 'eltd_core' ) . ': ' . $promotion_category_title;
				}

				$data          = array();
				$data['value'] = $promotion_category_slug;
				$data['label'] = $promotion_category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

}