<?php
namespace ElatedCore\CPT\Promotion;

use ElatedCore\Lib\PostTypeInterface;

/**
 * Class PromotionRegister
 * @package ElatedCore\CPT\Portfolio
 */
class PromotionRegister implements PostTypeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'promotion-item';
		$this->taxBase = 'promotion-category';
	}

	/**
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Registers custom post type with WordPress
	 */
	public function register() {
		$this->registerPostType();
		$this->registerTax();
	}

	/**
	 * Registers custom post type with WordPress
	 */
	private function registerPostType() {

		$menuPosition = 5;
		$menuIcon = 'dashicons-admin-post';
		$slug = $this->base;

		register_post_type( $this->base,
			array(
				'labels' => array(
					'name' => __( 'Promotion','eltd_core' ),
					'singular_name' => __( 'Promotion Item','eltd_core' ),
					'add_item' => __('New Promotion Item','eltd_core'),
					'add_new_item' => __('Add New Promotion Item','eltd_core'),
					'edit_item' => __('Edit Promotion Item','eltd_core')
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => $slug),
				'menu_position' => $menuPosition,
				'show_ui' => true,
				'supports' => array('author', 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'comments'),
				'menu_icon'  =>  $menuIcon
			)
		);
	}

	/**
	 * Registers custom taxonomy with WordPress
	 */
	private function registerTax() {
		$labels = array(
			'name' => __( 'Promotion Categories', 'taxonomy general name' ),
			'singular_name' => __( 'Promotion Category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Promotion Categories','eltd_core' ),
			'all_items' => __( 'All Promotion Categories','eltd_core' ),
			'parent_item' => __( 'Parent Promotion Category','eltd_core' ),
			'parent_item_colon' => __( 'Parent Promotion Category:','eltd_core' ),
			'edit_item' => __( 'Edit Promotion Category','eltd_core' ),
			'update_item' => __( 'Update Promotion Category','eltd_core' ),
			'add_new_item' => __( 'Add New Promotion Category','eltd_core' ),
			'new_item_name' => __( 'New Promotion Category Name','eltd_core' ),
			'menu_name' => __( 'Promotion Categories','eltd_core' ),
		);

		register_taxonomy($this->taxBase, array($this->base), array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'promotion-category' ),
		));
	}
}