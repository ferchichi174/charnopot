<div  class="eltd-testimonial-content">
	<div class="eltd-testimonials-inner">
		<div class="eltd-testimonials-description">
			<h5><?php echo trim( $title ) ?></h5>
			<p><?php echo trim( $text ) ?></p>
		</div>
		<div class="eltd-testimonials-author-holder">
			<?php if ( $image ) { ?>
				<div class="eltd-testimonials-author-image">
					<?php echo creator_elated_kses_img( $image ); ?>
				</div>
			<?php } ?>
			<div class="eltd-testimonials-author">
				<h5><?php echo esc_attr( $author ) ?></h5>
				<span class="author-position">
					<?php echo esc_html( $position ); ?>
				</span>
			</div>
		</div>
	</div>
</div>