<div class="eltd-testimonial-content">
	<div class="eltd-testimonial-card">
		<div class="eltd-testimonials-text">
			<h5><?php echo trim( $title ) ?></h5>
			<p>"<?php echo trim( $text ) ?>"</p>
		</div>
		<div class="eltd-testimonials-author-holder">
			<?php if ( $image ) { ?>
				<div class="eltd-testimonials-author-image">
					<div class="eltd-testimonials-author-image-holder">
						<?php echo creator_elated_kses_img( $image ); ?>
					</div>
				</div>
			<?php } ?>
			<div class="eltd-testimonials-author">
				<span></span>
				<div class="eltd-testimonials-author-details">
					<h6><?php echo esc_attr( $author ) ?></h6>
					<span class="author-position">
						<?php echo esc_html($position); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>