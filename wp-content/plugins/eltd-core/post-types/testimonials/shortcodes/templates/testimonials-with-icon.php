<div class="eltd-testimonial-content">
	<?php if ( $icon !== '' ) { ?>
		<div class="eltd-testimonials-icon">
			<?php echo creator_elated_get_module_part( $icon ); ?>
		</div>
	<?php } ?>
	<div class="eltd-testimonials-text">
		<p><?php echo trim( $text ) ?></p>
	</div>
	<div class="eltd-testimonials-author">
		<h6><?php echo esc_attr( $author ) ?></h6>
	</div>
</div>