<?php

namespace ElatedCore\CPT\Testimonials\Shortcodes;


use ElatedCore\Lib;

/**
 * Class Testimonials
 * @package ElatedCore\CPT\Testimonials\Shortcodes
 */
class Testimonials implements Lib\ShortcodeInterface {
	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'eltd_testimonials';

		add_action( 'vc_before_init', array( $this, 'vcMap' ) );

		//Portfolio category filter
		add_filter( 'vc_autocomplete_eltd_testimonials_category_callback', array(
			&$this,
			'testimonialsCategoryAutocompleteSuggester',
		), 10, 1 ); // Get suggestion(find). Must return an array

		//Portfolio category render
		add_filter( 'vc_autocomplete_eltd_testimonials_category_render', array(
			&$this,
			'testimonialsCategoryAutocompleteRender',
		), 10, 1 ); // Get suggestion(find). Must return an array
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
				'name'                      => esc_html__('Testimonials','eltd_core'),
				'base'                      => $this->base,
				'category'                  => esc_html__('by ELATED','eltd_core'),
				'icon'                      => 'icon-wpb-testimonials extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type'        => 'autocomplete',
						'admin_label' => true,
						'heading'     => esc_html__('Category','eltd_core'),
						'param_name'  => 'category',
						'description' => esc_html__('Category Slug (leave empty for all)','eltd_core')
					),
					array(
						'type'        => 'textfield',
						'admin_label' => true,
						'heading'     => esc_html__('Number', 'eltd_core'),
						'param_name'  => 'number',
						'value'       => '',
						'description' => esc_html__('Number of Testimonials', 'eltd_core'),
					),
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__('Type','eltd_core'),
						'param_name'  => 'type',
						'value'       => array(
							esc_html__('With Icon', 'eltd_core') => 'with-icon',
							esc_html__('Standard', 'eltd_core')  => 'standard',
							esc_html__('Cards', 'eltd_core')     => 'cards',
						),
						'save_always' => true,
					),
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__('Skin', 'eltd_core'),
						'param_name'  => 'skin',
						'value'       => array(
							esc_html__('Light', 'eltd_core')    => 'light',
							esc_html__('Dark', 'eltd_core')    => 'dark',
						),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
                        'save_always' => true,
						'admin_label' => true,
						'heading'     => esc_html__('Carousel Items','eltd_core'),
						'param_name'  => 'carousel_items',
						'value'       => array(
						    '2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5',
						),
						'description' => esc_html__('Insert Number of Carousel Items (Default 2)', 'eltd_core'),
						'dependency'  => array(
							'element' => 'type',
							'value'   => array(
								'cards',
								'standard'
							)
						)
					),
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__('Show in two rows?', 'eltd_core'),
						'param_name'  => 'two_rows',
						'value'       => array(
							esc_html__('No','eltd_core')  => 'no',
							esc_html__('Yes','eltd_core') => 'yes'
						),
						'save_always' => true,
						'dependency'  => array(
							'element' => 'type',
							'value'   => array(
								'cards',
								'standard'
							)
						)
					),
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__('Navigation', 'eltd_core'),
						'param_name'  => 'navigation',
						'value'       => array(
							esc_html__('No', 'eltd_core')  => 'no',
							esc_html__('Yes', 'eltd_core') => 'yes'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type'        => 'dropdown',
						'admin_label' => true,
						'heading'     => esc_html__('Pagination', 'eltd_core'),
						'param_name'  => 'pagination',
						'value'       => array(
							esc_html__('No', 'eltd_core')  => 'no',
							esc_html__('Yes', 'eltd_core') => 'yes'
						),
						'save_always' => true,
						'description' => ''
					),
					array(
						'type'        => 'textfield',
						'admin_label' => true,
						'heading'     => esc_html__('Autoplay speed', 'eltd_core'),
						'param_name'  => 'autoplay_speed',
						'value'       => '',
						'description' => esc_html__('Autoplay speed (ms)', 'eltd_core')
					)
				)
			) );
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 *
	 * @return string
	 */
	public function render( $atts, $content = null ) {

		$args   = array(
			'number'         => '-1',
			'category'       => '',
			'type'           => 'with_icon',
			'skin'           => '',
			'carousel_items' => '2',
			'two_rows'       => 'no',
			'navigation'     => 'no',
			'pagination'     => 'no',
			'autoplay_speed' => ''
		);
		$params = shortcode_atts( $args, $atts );

		extract( $params );

		$data_attr  = $this->getDataParams( $params );
		$query_args = $this->getQueryParams( $params );

		$html = '';
		$html .= '<div class="eltd-testimonials-holder clearfix">';
		$html .= '<div class="eltd-testimonials eltd-' . $type . ' ' . $skin . '" ' . creator_elated_get_inline_attrs( $data_attr ) . '>';

		$query = new \WP_Query( $query_args );
		if ( $query->have_posts() ) {

			switch ( $type ) {
				case 'with-icon':
					while ( $query->have_posts() ) {
						$query->the_post();

						$params['icon']       = $this->getIconHtml();
						$params['author']     = get_post_meta( get_the_ID(), 'eltd_testimonial_author', true );
						$params['text']       = get_post_meta( get_the_ID(), 'eltd_testimonial_text', true );
						$params['current_id'] = get_the_ID();

						$html .= eltd_core_get_shortcode_module_template_part( 'testimonials', 'testimonials-with-icon', '', $params );
					}
					break;
				case 'cards':
					while ( $query->have_posts() ) {
						$query->the_post();

						$params['current_id'] = get_the_ID();
						$params['title']     = get_post_meta( get_the_ID(), 'eltd_testimonial_title', true );
						$params['text']       = get_post_meta( get_the_ID(), 'eltd_testimonial_text', true );
						$params['author']     = get_post_meta( get_the_ID(), 'eltd_testimonial_author', true );
						$params['position']   = get_post_meta( get_the_ID(), 'eltd_testimonial_author_position', true );
						if ( has_post_thumbnail() ) {
							$params['image'] = get_the_post_thumbnail(null, array( 67, 67));
						} else {
							$params['image'] = false;
						}

						if ( $params['two_rows'] == 'yes' && $query->current_post % 2 == 0 ) {
							$html .= '<div class="eltd-testimonial-items-holder">';
						}

						$html .= eltd_core_get_shortcode_module_template_part( 'testimonials', 'testimonials-cards', '', $params );

						if ( $params['two_rows'] == 'yes' && $query->current_post % 2 !== 0 ) {
							$html .= '</div>';
						}

					}
					break;
				case 'standard':
					while ( $query->have_posts() ) {
						$query->the_post();

						$params['current_id'] = get_the_ID();
						$params['title']     = get_post_meta( get_the_ID(), 'eltd_testimonial_title', true );
						$params['text']       = get_post_meta( get_the_ID(), 'eltd_testimonial_text', true );
						$params['author']     = get_post_meta( get_the_ID(), 'eltd_testimonial_author', true );
						$params['position']   = get_post_meta( get_the_ID(), 'eltd_testimonial_author_position', true );
						$params['social_network_objects'] = $this->getTestimonialSocialNetworks(get_the_ID());

						if ( has_post_thumbnail() ) {
							$params['image'] = get_the_post_thumbnail(null, array( 70, 70));
						} else {
							$params['image'] = false;
						}

						if ( $params['two_rows'] == 'yes' && $query->current_post % 2 == 0 ) {
							$html .= '<div class="eltd-testimonial-items-holder">';
						}

						$html .= eltd_core_get_shortcode_module_template_part( 'testimonials', 'testimonials-standard', '', $params );

						if ( $params['two_rows'] == 'yes' && $query->current_post % 2 !== 0 ) {
							$html .= '</div>';
						}
					}
					break;
				default:
					break;
			}

		} else {
			$html .= __( 'Sorry, no posts matched your criteria.', 'eltd_core' );
		}
		wp_reset_postdata();
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	/**
	 * Generates testimonial data attribute array
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getDataParams( $params ) {

		$data_attr = array();

		if ( $params['carousel_items'] !== '' ) {
			$data_attr['data-items'] = $params['carousel_items'];
		}
		if ( $params['autoplay_speed'] !== '' ) {
			$data_attr['data-autoplay-speed'] = $params['autoplay_speed'];
		}
		if ( $params['navigation'] !== '' ) {
			$data_attr['data-navigation'] = $params['navigation'];
		}
		if ( $params['pagination'] !== '' ) {
			$data_attr['data-pagination'] = $params['pagination'];
		}

		return $data_attr;

	}

	/**
	 * Generates testimonials query attribute array
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getQueryParams( $params ) {

		$args = array(
			'post_type'      => 'testimonials',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'posts_per_page' => $params['number']
		);

		if ( $params['category'] != '' ) {
			$args['testimonials_category'] = $params['category'];
		}

		return $args;
	}

	private function getIconHtml() {

		$icon_pack  = get_post_meta( get_the_ID(), 'testimonial_icon_pack', true );
		$icons      = creator_elated_icon_collections()->getIconCollection( $icon_pack );
		$icon_field = 'testimonial_icon_' . $icons->param;
		$icon       = get_post_meta( get_the_ID(), $icon_field, true );

		$icon_html = '';
		if ( $icon !== '' ) {
			$icon_html = creator_elated_icon_collections()->renderIcon( $icon, $icon_pack );
		}

		return $icon_html;


	}

	private function getTestimonialSocialNetworks($id){

		$social_icons = array();
		$social_networks_array = array(
			'vimeo',
			'instagram',
			'twitter',
			'facebook'
		);
		foreach($social_networks_array as $network){

			$link = get_post_meta($id, 'eltd_testimonial_author_'.$network.'_url', true);

			$icon_params                      = array();
			$icon_params['icon_pack']         = 'font_elegant';
			$icon_params['fe_icon']           = 'social_'.$network;
			$icon_params['link']              = $link;
			$icon_params['type']              = 'square';

			$social_icons[] = creator_elated_execute_shortcode( 'eltd_icon', $icon_params );

		}

		return $social_icons;

	}

	/**
	 * Filter testimonial categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function testimonialsCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS portfolio_testimonial_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'testimonials_category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );

		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['portfolio_testimonial_title'] ) > 0 ) ? esc_html__( 'Category', 'eltd_core' ) . ': ' . $value['portfolio_testimonial_title'] : '' );
				$results[]     = $data;
			}
		}

		return $results;

	}

	/**
	 * Find testimonials by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function testimonialsCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get testimonial category
			$testimonial_category = get_term_by( 'slug', $query, 'testimonials_category' );
			if ( is_object( $testimonial_category ) ) {

				$testimonial_category_slug = $testimonial_category->slug;
				$testimonial_category_title = $testimonial_category->name;

				$testimonial_category_title_display = '';
				if ( ! empty( $testimonial_category_title ) ) {
					$testimonial_category_title_display = esc_html__( 'Category', 'eltd_core' ) . ': ' . $testimonial_category_title;
				}

				$data          = array();
				$data['value'] = $testimonial_category_slug;
				$data['label'] = $testimonial_category_title_display;

				return ! empty( $data ) ? $data : false;
			}

			return false;
		}

		return false;
	}

}