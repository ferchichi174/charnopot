<?php if($query_results->max_num_pages>1){ ?>
	<div class="eltd-ptf-list-paging">
		<span class="eltd-ptf-list-load-more">
			<?php
				$second_main_color='#7dbfb8';
				if(creator_elated_options()->getOptionValue('second_color') !== ''){
					$second_main_color = creator_elated_options()->getOptionValue('second_color');
				}
				echo creator_elated_get_button_html(array(
					'link' => 'javascript: void(0)',
					'text' => esc_html__('Show More', 'eltd_core'),
					'type' => 'solid',
					'hover_color' => '#fff',
					'hover_border_color' => '#333',
					'hover_background_color' => '#333',
				));
			?>
		</span>
	</div>
<?php }