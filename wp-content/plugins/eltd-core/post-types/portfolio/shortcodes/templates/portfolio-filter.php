<div class = "eltd-portfolio-filter-holder <?php echo esc_attr($masonry_filter); ?>">
	<div class = "eltd-portfolio-filter-holder-inner" >
		<?php 
		$rand_number = rand();
		if(is_array($filter_categories) && count($filter_categories)){?>
		<ul>
			<?php if($type == 'masonry' || $type == 'masonry_with_space' || $type == 'pinterest' || $type == 'pinterest_no_space'){ ?>
				<li class="filter" data-filter="*"><span><?php  print __('All', 'eltd_core')?></span></li>
			<?php } else{ ?>
				<li data-class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" data-filter="all"><span><?php  print __('All', 'eltd_core')?></span></li>
			<?php } ?>
			<?php foreach($filter_categories as $cat){				
				if($type == 'masonry' || $type == 'masonry_with_space' || $type == 'pinterest'){?>
					<li data-class="filter" class="filter" data-filter = ".portfolio_category_<?php echo creator_elated_get_module_part( $cat->term_id );  ?>">
						<span>
							<?php echo creator_elated_get_module_part( $cat->name ); ?>
						</span>
					</li>
				<?php }else{ ?>
					<li data-class="filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" class="filter filter_<?php echo creator_elated_get_module_part( $rand_number ); ?>" data-filter = ".portfolio_category_<?php echo creator_elated_get_module_part( $cat->term_id );  ?>">
						<span>
							<?php echo creator_elated_get_module_part( $cat->name ); ?>
						</span>
					</li>
			<?php }} ?>
		</ul> 
		<?php }?>
	</div>		
</div>