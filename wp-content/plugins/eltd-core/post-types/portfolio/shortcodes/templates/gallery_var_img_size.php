<?php // This line is needed for mixItUp gutter ?>
<article class="eltd-portfolio-item mix <?php echo esc_attr($categories)?>" >

	<a class ="eltd-portfolio-link" href="<?php echo esc_url($item_link); ?>"></a>

	<div class = "eltd-item-image-holder">
		<?php
			echo creator_elated_kses_img($slider_image);
		?>
	</div>

	<div class="eltd-item-text-overlay">
		<div class="eltd-item-text-overlay-inner">
			<div class="eltd-item-text-holder">

				<<?php echo esc_attr($title_tag)?> class="eltd-item-title">
					<?php echo esc_attr(get_the_title()); ?>
				</<?php echo esc_attr($title_tag)?>>	

				<?php
				echo creator_elated_get_module_part( $category_html );
				?>

			</div>
		</div>	
	</div>

</article>
<?php // This line is needed for mixItUp gutter ?>