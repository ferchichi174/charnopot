<?php 
    $featured = '';
    $unique_id = rand();
    if(has_post_thumbnail()) {
         $featured = get_the_post_thumbnail_url(get_the_ID(),'full');
    }
?>
<?php // This line is needed for mixItUp gutter ?>
<article class="eltd-portfolio-item mix <?php echo esc_attr($categories)?>">

	<?php if(has_post_thumbnail(get_the_ID())){ ?>
		<?php
		if( $like_icon_html !==''){
			echo creator_elated_get_module_part( $like_icon_html );
		}
		?>
		<div class = "eltd-item-image-holder">
			<?php if($pretty_photo){ ?>
                    <a  data-rel="prettyPhoto[rel-gallery-item-<?php echo get_the_ID() . '-' . esc_attr($unique_id); ?>]" href="<?php echo esc_url($featured); ?>" title="<?php echo get_the_title(get_the_ID()); ?>">
                        <?php
                            echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
                        ?>
                    </a>
            <?php    } else { ?>

			<a href="<?php echo esc_url($item_link); ?>" target="_blank">
				<?php
					echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
				?>
			</a>
			<?php } ?>
		</div>

	<?php } ?>

	<div class="eltd-item-text-holder">

		<?php
		if($category_html !== ''){
			echo creator_elated_get_module_part( $category_html );
		}
		?>

		<<?php echo esc_attr($title_tag)?> class="eltd-item-title">

			<a href="<?php echo esc_url($item_link); ?>">
				<?php echo esc_attr(get_the_title()); ?>
			</a>

		</<?php echo esc_attr($title_tag)?>>

		<?php if( $show_excerpt == 'yes' ){ ?>

			<div class="eltd-ptf-excerpt">
				<?php creator_elated_excerpt_by_id( $excerpt_length , get_the_ID() ); ?>
			</div>

		<?php } ?>

	</div>

</article>
<?php // This line is needed for mixItUp gutter ?>