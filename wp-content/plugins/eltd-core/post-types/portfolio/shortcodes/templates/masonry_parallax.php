<article class="eltd-portfolio-item <?php echo esc_attr($article_masonry_size)?> <?php echo esc_attr($categories)?>">

	<a class ="eltd-portfolio-link" href="<?php echo esc_url($item_link); ?>"></a>

	<div class = "eltd-item-image-holder" <?php //echo creator_elated_get_inline_style($image_style)?>>
		<?php
			echo get_the_post_thumbnail(get_the_ID(),$thumb_size);
		?>
		
		<div class="eltd-item-text-overlay">
			<div class="eltd-item-text-overlay-inner">
				<div class="eltd-item-text-holder">

					<<?php echo esc_attr($title_tag)?> class="eltd-item-title">
					<?php echo esc_attr(get_the_title()); ?>
				</<?php echo esc_attr($title_tag)?>>

				<?php

				echo creator_elated_get_module_part( $category_html );

				?>

				<?php if($show_excerpt_on_hover == 'yes') : ?>
					<div class="eltd-item-excerpt">
						<?php creator_elated_excerpt_by_id( $excerpt_length , get_the_ID() ); ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
		</div>
	</div>


</article>
