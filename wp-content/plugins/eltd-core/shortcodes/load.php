<?php

if(!function_exists('creator_elated_load_shortcode_interface')) {

	function creator_elated_load_shortcode_interface() {

		include_once 'shortcode-interface.php';

	}

	add_action('creator_elated_before_options_map', 'creator_elated_load_shortcode_interface');

}

if(!function_exists('creator_elated_load_shortcodes')) {
	/**
	 * Loades all shortcodes by going through all folders that are placed directly in shortcodes folder
	 * and loads load.php file in each. Hooks to creator_elated_after_options_map action
	 *
	 * @see http://php.net/manual/en/function.glob.php
	 */
	function creator_elated_load_shortcodes() {
		foreach(glob(ELATED_FRAMEWORK_ROOT_DIR.'/modules/shortcodes/*/load.php') as $shortcode_load) {
			include_once $shortcode_load;
		}

		do_action('creator_elated_shortcode_loader');
	}

	add_action('creator_elated_before_options_map', 'creator_elated_load_shortcodes');
}