<?php
namespace CreatorElated\Modules\Shortcodes\Lib;

use CreatorElated\Modules\Shortcodes\Accordion\Accordion;
use CreatorElated\Modules\Shortcodes\AnimationHolder\AnimationHolder;
use CreatorElated\Modules\Shortcodes\AccordionTab\AccordionTab;
use CreatorElated\Modules\Shortcodes\Blockquote\Blockquote;
use CreatorElated\Modules\Shortcodes\BlogList\BlogList;
use CreatorElated\Modules\Shortcodes\Button\Button;
use CreatorElated\Modules\Shortcodes\CallToAction\CallToAction;
use CreatorElated\Modules\Shortcodes\Counter\Countdown;
use CreatorElated\Modules\Shortcodes\Counter\Counter;
use CreatorElated\Modules\Shortcodes\CustomFont\CustomFont;
use CreatorElated\Modules\Shortcodes\Dropcaps\Dropcaps;
use CreatorElated\Modules\Shortcodes\ElementsHolder\ElementsHolder;
use CreatorElated\Modules\Shortcodes\ElementsHolderItem\ElementsHolderItem;
use CreatorElated\Modules\Shortcodes\GoogleMap\GoogleMap;
use CreatorElated\Modules\Shortcodes\Highlight\Highlight;
use CreatorElated\Modules\Shortcodes\Icon\Icon;
use CreatorElated\Modules\Shortcodes\IconListItem\IconListItem;
use CreatorElated\Modules\Shortcodes\IconWithText\IconWithText;
use CreatorElated\Modules\Shortcodes\ImageGallery\ImageGallery;
use CreatorElated\Modules\Shortcodes\InteractiveElementsHolder\InteractiveElementsHolder;
use CreatorElated\Modules\Shortcodes\InteractiveElementsHolderLeftItem\InteractiveElementsHolderLeftItem;
use CreatorElated\Modules\Shortcodes\InteractiveElementsHolderRightItem\InteractiveElementsHolderRightItem;
use CreatorElated\Modules\Shortcodes\InteractiveImages\InteractiveImages;
use CreatorElated\Modules\Shortcodes\InteractiveImage\InteractiveImage;
use CreatorElated\Modules\Shortcodes\ParallaxSection\ParallaxSection;
use CreatorElated\Modules\Shortcodes\ParallaxSectionItem\ParallaxSectionItem;
use CreatorElated\Modules\Shortcodes\Message\Message;
use CreatorElated\Modules\Shortcodes\OrderedList\OrderedList;
use CreatorElated\Modules\Shortcodes\ParallaxImages\ParallaxImages;
use CreatorElated\Modules\Shortcodes\PieCharts\PieChartBasic\PieChartBasic;
use CreatorElated\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartDoughnut;
use CreatorElated\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartPie;
use CreatorElated\Modules\Shortcodes\PieCharts\PieChartWithIcon\PieChartWithIcon;
use CreatorElated\Modules\Shortcodes\PricingTables\PricingTables;
use CreatorElated\Modules\Shortcodes\PricingTable\PricingTable;
use CreatorElated\Modules\Shortcodes\Product\Product;
use CreatorElated\Modules\Shortcodes\ProgressBar\ProgressBar;
use CreatorElated\Modules\Shortcodes\Separator\Separator;
use CreatorElated\Modules\Shortcodes\SocialShare\SocialShare;
use CreatorElated\Modules\Shortcodes\Tabs\Tabs;
use CreatorElated\Modules\Shortcodes\Tab\Tab;
use CreatorElated\Modules\Shortcodes\Team\Team;
use CreatorElated\Modules\Shortcodes\UnorderedList\UnorderedList;
use CreatorElated\Modules\Shortcodes\VideoButton\VideoButton;
use CreatorElated\Modules\Shortcodes\Process\ProcessHolder;
use CreatorElated\Modules\Shortcodes\Process\Process;
use CreatorElated\Modules\Shortcodes\BlogCarousel\BlogCarousel;
use CreatorElated\Modules\Shortcodes\BlogSlider\BlogSlider;
use CreatorElated\Modules\Shortcodes\InfoBox\InfoBox;
use CreatorElated\Modules\Shortcodes\SectionTitle\SectionTitle;
use CreatorElated\Modules\Shortcodes\RestaurantMenu\RestaurantMenu;
use CreatorElated\Modules\Shortcodes\RestaurantItem\RestaurantItem;
use CreatorElated\Modules\Shortcodes\ProductList\ProductList;
use CreatorElated\Modules\Shortcodes\ProductSlider\ProductSlider;
use CreatorElated\Modules\Shortcodes\EmbeddedLinkHolder\EmbeddedLinkHolder;
use CreatorElated\Modules\Shortcodes\Workflow\Workflow;
use CreatorElated\Modules\Shortcodes\WorkflowItem\WorkflowItem;
use CreatorElated\Modules\Shortcodes\ProductCarousel\ProductCarousel;
use CreatorElated\Modules\Shortcodes\VerticalSplitSlider\VerticalSplitSlider;
use CreatorElated\Modules\Shortcodes\VerticalSplitSliderLeftPanel\VerticalSplitSliderLeftPanel;
use CreatorElated\Modules\Shortcodes\VerticalSplitSliderRightPanel\VerticalSplitSliderRightPanel;
use CreatorElated\Modules\Shortcodes\VerticalSplitSliderContentItem\VerticalSplitSliderContentItem;
use CreatorElated\Modules\Shortcodes\CardsGallery\CardsGallery;
/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader {
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}

	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AnimationHolder());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new Blockquote());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new Button());
		$this->addShortcode(new CallToAction());
		$this->addShortcode(new Counter());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new InteractiveImages());
		$this->addShortcode(new InteractiveImage());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new Icon());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new InteractiveElementsHolder());
		$this->addShortcode(new InteractiveElementsHolderLeftItem());
		$this->addShortcode(new InteractiveElementsHolderRightItem());
		$this->addShortcode(new Message());
		$this->addShortcode(new OrderedList());
		$this->addShortcode(new PieChartBasic());
		$this->addShortcode(new PieChartPie());
		$this->addShortcode(new PieChartDoughnut());
		$this->addShortcode(new PieChartWithIcon());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new ProgressBar());
		$this->addShortcode(new Separator());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new Team());
		$this->addShortcode(new UnorderedList());
		$this->addShortcode(new VideoButton());
		$this->addShortcode(new ParallaxImages());
		$this->addShortcode(new ProcessHolder());
		$this->addShortcode(new Process());
		$this->addShortcode(new BlogCarousel());
		$this->addShortcode(new BlogSlider());
		$this->addShortcode(new InfoBox());
		$this->addShortcode(new SectionTitle());
		$this->addShortcode(new RestaurantMenu());
		$this->addShortcode(new RestaurantItem());
		$this->addShortcode(new Product());
		$this->addShortcode(new ProductList());
		$this->addShortcode(new ProductSlider());
		$this->addShortcode(new EmbeddedLinkHolder());
		$this->addShortcode(new ParallaxSection());
		$this->addShortcode(new ParallaxSectionItem());
		$this->addShortcode(new Workflow());
		$this->addShortcode(new WorkflowItem());
		$this->addShortcode(new ProductCarousel());
		$this->addShortcode(new VerticalSplitSlider());
		$this->addShortcode(new VerticalSplitSliderLeftPanel());
		$this->addShortcode(new VerticalSplitSliderRightPanel());
		$this->addShortcode(new VerticalSplitSliderContentItem());
		$this->addShortcode(new CardsGallery());
	}

	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();

		foreach ($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}
	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();