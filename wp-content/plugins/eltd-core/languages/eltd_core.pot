# Copyright (C) 2016 Elated Core
# This file is distributed under the same license as the Elated Core package.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Elated Core 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/eltd-core\n"
"POT-Creation-Date: 2018-12-25 14:12+0100\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: Poedit 2.1.1\n"

#: import/class.wordpress-importer.php:143
#: import/class.wordpress-importer.php:194
#: import/class.wordpress-importer.php:198
#: import/class.wordpress-importer.php:207
msgid "Sorry, there has been an error."
msgstr ""

#: import/class.wordpress-importer.php:178
msgid "All done."
msgstr ""

#: import/class.wordpress-importer.php:178
msgid "Have fun!"
msgstr ""

#: import/class.wordpress-importer.php:179
msgid "Remember to update the passwords and roles of imported users."
msgstr ""

#: import/class.wordpress-importer.php:199
msgid "The export file could not be found at <code>%s</code>. It is likely that this was caused by a permissions problem."
msgstr ""

#: import/class.wordpress-importer.php:215
msgid "This WXR file (version %s) may not be supported by this version of the importer. Please consider updating."
msgstr ""

#: import/class.wordpress-importer.php:240
msgid "Failed to import author %s. Their posts will be attributed to the current user."
msgstr ""

#: import/class.wordpress-importer.php:266
msgid "Assign Authors"
msgstr ""

#: import/class.wordpress-importer.php:267
msgid "To make it easier for you to edit and save the imported content, you may want to reassign the author of the imported item to an existing user of this site. For example, you may want to import all the entries as <code>admin</code>s entries."
msgstr ""

#: import/class.wordpress-importer.php:269
msgid "If a new user is created by WordPress, a new password will be randomly generated and the new user&#8217;s role will be set as %s. Manually changing the new user&#8217;s details will be necessary."
msgstr ""

#: import/class.wordpress-importer.php:279
msgid "Import Attachments"
msgstr ""

#: import/class.wordpress-importer.php:282
msgid "Download and import file attachments"
msgstr ""

#: import/class.wordpress-importer.php:299
msgid "Import author:"
msgstr ""

#: import/class.wordpress-importer.php:310
msgid "or create new user with login name:"
msgstr ""

#: import/class.wordpress-importer.php:313
msgid "as a new user:"
msgstr ""

#: import/class.wordpress-importer.php:321
msgid "assign posts to an existing user:"
msgstr ""

#: import/class.wordpress-importer.php:323
msgid "or assign posts to an existing user:"
msgstr ""

#: import/class.wordpress-importer.php:324
msgid "- Select -"
msgstr ""

#: import/class.wordpress-importer.php:374
msgid "Failed to create new user for %s. Their posts will be attributed to the current user."
msgstr ""

#: import/class.wordpress-importer.php:426
msgid "Failed to import category %s"
msgstr ""

#: import/class.wordpress-importer.php:470
msgid "Failed to import post tag %s"
msgstr ""

#: import/class.wordpress-importer.php:520
#: import/class.wordpress-importer.php:744
msgid "Failed to import %s %s"
msgstr ""

#: import/class.wordpress-importer.php:610
msgid "Failed to import &#8220;%s&#8221;: Invalid post type %s"
msgstr ""

#: import/class.wordpress-importer.php:647
msgid "%s &#8220;%s&#8221; already exists."
msgstr ""

#: import/class.wordpress-importer.php:709
msgid "Failed to import %s &#8220;%s&#8221;"
msgstr ""

#: import/class.wordpress-importer.php:875
msgid "Menu item skipped due to missing menu slug"
msgstr ""

#: import/class.wordpress-importer.php:882
msgid "Menu item skipped due to invalid menu slug: %s"
msgstr ""

#: import/class.wordpress-importer.php:945
msgid "Fetching attachments is not enabled"
msgstr ""

#: import/class.wordpress-importer.php:958
msgid "Invalid file type"
msgstr ""

#: import/class.wordpress-importer.php:1002
msgid "Remote server did not respond"
msgstr ""

#: import/class.wordpress-importer.php:1008
msgid "Remote server returned error response %1$d %2$s"
msgstr ""

#: import/class.wordpress-importer.php:1015
msgid "Remote file is incorrect size"
msgstr ""

#: import/class.wordpress-importer.php:1020
msgid "Zero size file downloaded"
msgstr ""

#: import/class.wordpress-importer.php:1026
msgid "Remote file is too large, limit is %s"
msgstr ""

#: import/class.wordpress-importer.php:1127
msgid "Import WordPress"
msgstr ""

#: import/class.wordpress-importer.php:1134
msgid "A new version of this importer is available. Please update to version %s to ensure compatibility with newer export files."
msgstr ""

#: import/class.wordpress-importer.php:1149
msgid "Howdy! Upload your WordPress eXtended RSS (WXR) file and we&#8217;ll import the posts, pages, comments, custom fields, categories, and tags into this site."
msgstr ""

#: import/class.wordpress-importer.php:1150
msgid "Choose a WXR (.xml) file to upload, then click Upload file and import."
msgstr ""

#: import/eltd-import.php:42 import/eltd-import.php:59
msgid "Content imported successfully"
msgstr ""

#: import/eltd-import.php:56
msgid "An Error Occurred During Import"
msgstr ""

#: import/eltd-import.php:71
msgid "Widgets imported successfully"
msgstr ""

#: import/eltd-import.php:90
msgid "Custom sidebars imported successfully"
msgstr ""

#: import/eltd-import.php:96
msgid "Options imported successfully"
msgstr ""

#: import/parsers.php:42 import/parsers.php:78 import/parsers.php:86
#: import/parsers.php:280
msgid "There was an error when reading this WXR file"
msgstr ""

#: import/parsers.php:43
msgid "Details are shown above. The importer will now try again with a different parser..."
msgstr ""

#: import/parsers.php:90 import/parsers.php:95 import/parsers.php:285
#: import/parsers.php:474
msgid "This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr ""

#: lib/helpers/helpers.php:203
#: post-types/portfolio/shortcodes/portfolio-list.php:615
#: post-types/promotion/shortcodes/promotion-list.php:131
#: post-types/slider/shortcodes/slider.php:825
#: post-types/slider/shortcodes/slider.php:857
#: post-types/testimonials/shortcodes/testimonials.php:280
msgid "Sorry, no posts matched your criteria."
msgstr ""

#: lib/helpers/helpers.php:259
msgid "After Product Title"
msgstr ""

#: lib/helpers/helpers.php:264
#: post-types/portfolio/shortcodes/portfolio-list.php:731
msgid "Like"
msgstr ""

#: lib/helpers/helpers.php:265
msgid "Liked"
msgstr ""

#: lib/helpers/helpers.php:293
msgid "Facebook"
msgstr ""

#: lib/helpers/helpers.php:294
msgid "Twitter"
msgstr ""

#: lib/helpers/helpers.php:295
msgid "Google Plus"
msgstr ""

#: lib/helpers/helpers.php:296
msgid "Instagram"
msgstr ""

#: main.php:56 main.php:57
msgid "Elated Options"
msgstr ""

#: post-types/carousels/carousel-register.php:56
#: post-types/carousels/carousel-register.php:57
msgid "Elated Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:58
#: post-types/testimonials/shortcodes/testimonials.php:100
msgid "Carousel Items"
msgstr ""

#: post-types/carousels/carousel-register.php:59
#: post-types/carousels/carousel-register.php:62
msgid "Add New Carousel Item"
msgstr ""

#: post-types/carousels/carousel-register.php:60
msgid "Carousel Item"
msgstr ""

#: post-types/carousels/carousel-register.php:61
msgid "New Carousel Item"
msgstr ""

#: post-types/carousels/carousel-register.php:63
msgid "Edit Carousel Item"
msgstr ""

#: post-types/carousels/carousel-register.php:83
#: post-types/carousels/carousel-register.php:93
msgid "Carousels"
msgstr ""

#: post-types/carousels/carousel-register.php:84
#: post-types/carousels/shortcodes/carousel.php:38
msgid "Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:85
msgid "Search Carousels"
msgstr ""

#: post-types/carousels/carousel-register.php:86
msgid "All Carousels"
msgstr ""

#: post-types/carousels/carousel-register.php:87
msgid "Parent Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:88
msgid "Parent Carousel:"
msgstr ""

#: post-types/carousels/carousel-register.php:89
msgid "Edit Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:90
msgid "Update Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:91
msgid "Add New Carousel"
msgstr ""

#: post-types/carousels/carousel-register.php:92
msgid "New Carousel Name"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:40
#: post-types/portfolio/shortcodes/portfolio-list.php:70
#: post-types/portfolio/shortcodes/portfolio-slider.php:53
#: post-types/promotion/shortcodes/promotion-list.php:54
#: post-types/testimonials/shortcodes/testimonials.php:54
msgid "by ELATED"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:46
msgid "Carousel Slider"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:54
#: post-types/portfolio/shortcodes/portfolio-list.php:382
#: post-types/portfolio/shortcodes/portfolio-slider.php:105
msgid "Order By"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:58
#: post-types/portfolio/shortcodes/portfolio-list.php:387
#: post-types/portfolio/shortcodes/portfolio-list.php:1293
#: post-types/portfolio/shortcodes/portfolio-list.php:1322
#: post-types/portfolio/shortcodes/portfolio-slider.php:110
#: post-types/portfolio/shortcodes/portfolio-slider.php:273
#: post-types/portfolio/shortcodes/portfolio-slider.php:302
msgid "Title"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:59
#: post-types/portfolio/shortcodes/portfolio-list.php:385
#: post-types/portfolio/shortcodes/portfolio-slider.php:111
msgid "Date"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:65
#: post-types/portfolio/shortcodes/portfolio-list.php:396
#: post-types/portfolio/shortcodes/portfolio-slider.php:118
msgid "Order"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:69
#: post-types/portfolio/shortcodes/portfolio-list.php:400
#: post-types/portfolio/shortcodes/portfolio-slider.php:122
#: post-types/slider/shortcodes/slider.php:47
msgid "ASC"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:70
#: post-types/portfolio/shortcodes/portfolio-list.php:399
#: post-types/portfolio/shortcodes/portfolio-slider.php:123
msgid "DESC"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:76
msgid "Number of items showing"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:90
msgid "Image Animation"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:93
msgid "Image Change"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:94
msgid "Image Zoom"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:95
msgid "Underline"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:99
msgid "Note: Only on \"Image Change\" zoom image will be used. Underline animation can be used only for one row layout."
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:103
msgid "Carousel Line Color"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:106
msgid "Line color used in underline animation"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:111
msgid "Show navigation?"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:114
#: post-types/carousels/shortcodes/carousel.php:126
#: post-types/carousels/shortcodes/carousel.php:155
#: post-types/carousels/shortcodes/carousel.php:166
#: post-types/portfolio/shortcodes/portfolio-list.php:169
#: post-types/portfolio/shortcodes/portfolio-list.php:183
#: post-types/portfolio/shortcodes/portfolio-list.php:198
#: post-types/portfolio/shortcodes/portfolio-list.php:214
#: post-types/portfolio/shortcodes/portfolio-list.php:349
#: post-types/portfolio/shortcodes/portfolio-list.php:376
#: post-types/portfolio/shortcodes/portfolio-list.php:484
#: post-types/portfolio/shortcodes/portfolio-slider.php:198
#: post-types/portfolio/shortcodes/portfolio-slider.php:210
#: post-types/slider/tax-custom-fields.php:43
#: post-types/slider/tax-custom-fields.php:55
#: post-types/slider/tax-custom-fields.php:67
#: post-types/slider/tax-custom-fields.php:78
#: post-types/slider/tax-custom-fields.php:89
#: post-types/slider/tax-custom-fields.php:131
#: post-types/slider/tax-custom-fields.php:142
#: post-types/slider/tax-custom-fields.php:153
#: post-types/testimonials/shortcodes/testimonials.php:124
#: post-types/testimonials/shortcodes/testimonials.php:142
#: post-types/testimonials/shortcodes/testimonials.php:154
msgid "Yes"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:115
#: post-types/carousels/shortcodes/carousel.php:127
#: post-types/carousels/shortcodes/carousel.php:154
#: post-types/carousels/shortcodes/carousel.php:165
#: post-types/portfolio/shortcodes/portfolio-list.php:168
#: post-types/portfolio/shortcodes/portfolio-list.php:184
#: post-types/portfolio/shortcodes/portfolio-list.php:199
#: post-types/portfolio/shortcodes/portfolio-list.php:213
#: post-types/portfolio/shortcodes/portfolio-list.php:350
#: post-types/portfolio/shortcodes/portfolio-list.php:375
#: post-types/portfolio/shortcodes/portfolio-list.php:483
#: post-types/portfolio/shortcodes/portfolio-slider.php:197
#: post-types/portfolio/shortcodes/portfolio-slider.php:209
#: post-types/slider/tax-custom-fields.php:31
#: post-types/slider/tax-custom-fields.php:42
#: post-types/slider/tax-custom-fields.php:54
#: post-types/slider/tax-custom-fields.php:66
#: post-types/slider/tax-custom-fields.php:79
#: post-types/slider/tax-custom-fields.php:90
#: post-types/slider/tax-custom-fields.php:132
#: post-types/slider/tax-custom-fields.php:143
#: post-types/slider/tax-custom-fields.php:154
#: post-types/testimonials/shortcodes/testimonials.php:123
#: post-types/testimonials/shortcodes/testimonials.php:141
#: post-types/testimonials/shortcodes/testimonials.php:153
msgid "No"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:123
msgid "Show pagination?"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:135
msgid "Pagination Skin"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:138
#: post-types/testimonials/shortcodes/testimonials.php:92
msgid "Dark"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:139
#: post-types/testimonials/shortcodes/testimonials.php:91
msgid "Light"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:151
msgid "Show Items In Two Rows?"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:173
msgid "Autoplay Duration"
msgstr ""

#: post-types/carousels/shortcodes/carousel.php:177
msgid "Slide duration (in milliseconds)"
msgstr ""

#: post-types/portfolio/portfolio-register.php:81
msgid "Portfolio"
msgstr ""

#: post-types/portfolio/portfolio-register.php:82
msgid "Portfolio Item"
msgstr ""

#: post-types/portfolio/portfolio-register.php:83
msgid "New Portfolio Item"
msgstr ""

#: post-types/portfolio/portfolio-register.php:84
msgid "Add New Portfolio Item"
msgstr ""

#: post-types/portfolio/portfolio-register.php:85
msgid "Edit Portfolio Item"
msgstr ""

#: post-types/portfolio/portfolio-register.php:103
#: post-types/portfolio/portfolio-register.php:113
msgid "Portfolio Categories"
msgstr ""

#: post-types/portfolio/portfolio-register.php:104
msgid "Portfolio Category"
msgstr ""

#: post-types/portfolio/portfolio-register.php:105
msgid "Search Portfolio Categories"
msgstr ""

#: post-types/portfolio/portfolio-register.php:106
msgid "All Portfolio Categories"
msgstr ""

#: post-types/portfolio/portfolio-register.php:107
msgid "Parent Portfolio Category"
msgstr ""

#: post-types/portfolio/portfolio-register.php:108
msgid "Parent Portfolio Category:"
msgstr ""

#: post-types/portfolio/portfolio-register.php:109
msgid "Edit Portfolio Category"
msgstr ""

#: post-types/portfolio/portfolio-register.php:110
msgid "Update Portfolio Category"
msgstr ""

#: post-types/portfolio/portfolio-register.php:111
msgid "Add New Portfolio Category"
msgstr ""

#: post-types/portfolio/portfolio-register.php:112
msgid "New Portfolio Category Name"
msgstr ""

#: post-types/portfolio/portfolio-register.php:130
#: post-types/portfolio/portfolio-register.php:140
msgid "Portfolio Tags"
msgstr ""

#: post-types/portfolio/portfolio-register.php:131
msgid "Portfolio Tag"
msgstr ""

#: post-types/portfolio/portfolio-register.php:132
msgid "Search Portfolio Tags"
msgstr ""

#: post-types/portfolio/portfolio-register.php:133
msgid "All Portfolio Tags"
msgstr ""

#: post-types/portfolio/portfolio-register.php:134
msgid "Parent Portfolio Tag"
msgstr ""

#: post-types/portfolio/portfolio-register.php:135
msgid "Parent Portfolio Tags:"
msgstr ""

#: post-types/portfolio/portfolio-register.php:136
msgid "Edit Portfolio Tag"
msgstr ""

#: post-types/portfolio/portfolio-register.php:137
msgid "Update Portfolio Tag"
msgstr ""

#: post-types/portfolio/portfolio-register.php:138
msgid "Add New Portfolio Tag"
msgstr ""

#: post-types/portfolio/portfolio-register.php:139
msgid "New Portfolio Tag Name"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:68
msgid "Portfolio List"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:76
msgid "Portfolio List Template"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:79
msgid "Standard with Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:80
msgid "Standard without Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:81
msgid "Gallery with Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:82
msgid "Gallery without Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:83
msgid "Tiled Gallery"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:84
msgid "Masonry"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:85
msgid "Masonry with Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:86
msgid "Masonry Parallax"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:87
msgid "Pinterest with Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:88
msgid "Pinterest without Space"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:95
msgid "Number Of Columns"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:98
msgid "Three columns"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:99
msgid "Four columns"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:115
#: post-types/portfolio/shortcodes/portfolio-slider.php:73
msgid "Hover Type"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:118
msgid "Centered"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:119
#: post-types/portfolio/shortcodes/portfolio-slider.php:76
msgid "Centered with Crosshair"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:120
msgid "Color Overlay"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:121
msgid "Light Overlay"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:122
msgid "Light Shader"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:123
msgid "Dark Shader"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:124
msgid "Slide Up"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:143
msgid "Appear Effect"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:146
msgid "No effect"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:147
msgid "One by One"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:165
msgid "Show Excerpt"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:180
msgid "Display Like Button"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:195
msgid "Display Category"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:210
msgid "Show Excerpt On Hover"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:226
msgid "Excerpt Length"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:232
msgid "Spacing Between Items"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:235
msgid "Large Spacing"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:236
msgid "Small Spacing"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:245
#: post-types/portfolio/shortcodes/portfolio-slider.php:179
msgid "Title Tag"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:260
msgid "Title align"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:263
msgid "Left"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:264
msgid "Center"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:275
msgid "Image Proportions"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:278
msgid "Original"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:279
#: post-types/portfolio/shortcodes/portfolio-slider.php:90
msgid "Square"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:280
#: post-types/portfolio/shortcodes/portfolio-slider.php:91
msgid "Landscape"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:281
#: post-types/portfolio/shortcodes/portfolio-slider.php:92
msgid "Portrait"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:300
msgid "Row Height (px)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:304
msgid "Targeted row height, which may vary depending on the proportions of the images."
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:310
msgid "Spacing (px)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:314
msgid "Define horizontal and vertical spacing between items"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:320
msgid "Last Row Behavior"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:323
msgid "Align left"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:324
msgid "Align right"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:325
msgid "Align centrally"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:326
msgid "Justify"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:327
msgid "Hide"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:329
msgid "Defines whether to justify the last row, align it in a certain way, or hide it."
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:335
msgid "Justify Threshold (0-1)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:338
msgid "If the last row takes up more than this part of available width, it will be justified despite the defined alignment. Enter 1 to never justify the last row."
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:346
msgid "Enable Pagination"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:357
msgid "Pagination Type"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:360
msgid "Load More"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:361
msgid "Standard Pagination"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:372
msgid "Open PrettyPhoto on click"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:386
#: post-types/portfolio/shortcodes/portfolio-slider.php:109
msgid "Menu Order"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:392
#: post-types/portfolio/shortcodes/portfolio-list.php:405
#: post-types/portfolio/shortcodes/portfolio-list.php:413
#: post-types/portfolio/shortcodes/portfolio-list.php:422
#: post-types/portfolio/shortcodes/portfolio-list.php:448
#: post-types/portfolio/shortcodes/portfolio-list.php:462
#: post-types/portfolio/shortcodes/portfolio-list.php:475
#: post-types/portfolio/shortcodes/portfolio-list.php:489
#: post-types/portfolio/shortcodes/portfolio-list.php:505
msgid "Query and Layout Options"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:409
msgid "One-Category Portfolio List"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:412
msgid "Enter one category slug (leave empty for showing all categories)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:417
msgid "Number of Portfolios Per Page"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:421
msgid "(enter -1 to show all)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:426
#: post-types/promotion/shortcodes/promotion-list.php:75
msgid "Number of Columns"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:430
msgid "One"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:431
#: post-types/promotion/shortcodes/promotion-list.php:78
msgid "Two"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:432
#: post-types/promotion/shortcodes/promotion-list.php:79
msgid "Three"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:433
msgid "Four"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:434
msgid "Five"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:435
msgid "Six"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:438
msgid "Default value is Three"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:452
msgid "Grid Size"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:455
#: post-types/portfolio/shortcodes/portfolio-slider.php:88
msgid "Default"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:456
msgid "3 Columns Grid"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:457
msgid "4 Columns Grid"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:458
msgid "5 Columns Grid"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:466
#: post-types/portfolio/shortcodes/portfolio-slider.php:165
msgid "Show Only Projects with Listed IDs"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:474
#: post-types/portfolio/shortcodes/portfolio-slider.php:173
msgid "Input portfolio ID or portfolio title to see suggestions"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:480
msgid "Enable Category Filter"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:488
msgid "Default value is No"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:493
msgid "Filter Order By"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:496
#: post-types/slider/tax-custom-fields.php:185
msgid "Name"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:497
msgid "Count"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:498
#: post-types/portfolio/shortcodes/portfolio-list.php:1293
#: post-types/portfolio/shortcodes/portfolio-list.php:1325
#: post-types/portfolio/shortcodes/portfolio-slider.php:273
#: post-types/portfolio/shortcodes/portfolio-slider.php:305
msgid "Id"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:499
#: post-types/slider/tax-custom-fields.php:187
msgid "Slug"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:503
msgid "Default value is Name"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-list.php:1359
#: post-types/portfolio/shortcodes/portfolio-list.php:1388
#: post-types/portfolio/shortcodes/portfolio-slider.php:158
#: post-types/portfolio/shortcodes/portfolio-slider.php:339
#: post-types/portfolio/shortcodes/portfolio-slider.php:368
#: post-types/promotion/shortcodes/promotion-list.php:61
#: post-types/promotion/shortcodes/promotion-list.php:202
#: post-types/promotion/shortcodes/promotion-list.php:231
#: post-types/testimonials/shortcodes/testimonials.php:61
#: post-types/testimonials/shortcodes/testimonials.php:403
#: post-types/testimonials/shortcodes/testimonials.php:432
msgid "Category"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:51
msgid "Portfolio Slider"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:59
msgid "Portfolio Slider Template"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:62
msgid "Gallery"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:63
#: post-types/testimonials/shortcodes/testimonials.php:80
msgid "Standard"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:64
msgid "Variable Image Size"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:77
msgid "Overlay"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:85
msgid "Image size"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:89
msgid "Original Size"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:130
#: post-types/testimonials/shortcodes/testimonials.php:68
msgid "Number"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:134
msgid "Number of portolios on page (-1 is all)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:138
msgid "Number of Portfolios Shown"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:154
msgid "Number of portfolios that are showing at the same time in full width (on smaller screens is responsive so there will be less items shown)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:161
#: post-types/promotion/shortcodes/promotion-list.php:63
#: post-types/testimonials/shortcodes/testimonials.php:63
msgid "Category Slug (leave empty for all)"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:194
#: post-types/testimonials/shortcodes/testimonials.php:138
msgid "Navigation"
msgstr ""

#: post-types/portfolio/shortcodes/portfolio-slider.php:206
#: post-types/testimonials/shortcodes/testimonials.php:150
msgid "Pagination"
msgstr ""

#: post-types/portfolio/shortcodes/templates/load-more-template.php:11
msgid "Show More"
msgstr ""

#: post-types/portfolio/shortcodes/templates/portfolio-filter.php:8
#: post-types/portfolio/shortcodes/templates/portfolio-filter.php:10
msgid "All"
msgstr ""

#: post-types/promotion/promotion-register.php:48
msgid "Promotion"
msgstr ""

#: post-types/promotion/promotion-register.php:49
msgid "Promotion Item"
msgstr ""

#: post-types/promotion/promotion-register.php:50
msgid "New Promotion Item"
msgstr ""

#: post-types/promotion/promotion-register.php:51
msgid "Add New Promotion Item"
msgstr ""

#: post-types/promotion/promotion-register.php:52
msgid "Edit Promotion Item"
msgstr ""

#: post-types/promotion/promotion-register.php:70
#: post-types/promotion/promotion-register.php:80
msgid "Promotion Categories"
msgstr ""

#: post-types/promotion/promotion-register.php:71
msgid "Promotion Category"
msgstr ""

#: post-types/promotion/promotion-register.php:72
msgid "Search Promotion Categories"
msgstr ""

#: post-types/promotion/promotion-register.php:73
msgid "All Promotion Categories"
msgstr ""

#: post-types/promotion/promotion-register.php:74
msgid "Parent Promotion Category"
msgstr ""

#: post-types/promotion/promotion-register.php:75
msgid "Parent Promotion Category:"
msgstr ""

#: post-types/promotion/promotion-register.php:76
msgid "Edit Promotion Category"
msgstr ""

#: post-types/promotion/promotion-register.php:77
msgid "Update Promotion Category"
msgstr ""

#: post-types/promotion/promotion-register.php:78
msgid "Add New Promotion Category"
msgstr ""

#: post-types/promotion/promotion-register.php:79
msgid "New Promotion Category Name"
msgstr ""

#: post-types/promotion/shortcodes/promotion-list.php:52
msgid "Promotion List"
msgstr ""

#: post-types/promotion/shortcodes/promotion-list.php:68
msgid "Number of Items"
msgstr ""

#: post-types/slider/shortcodes/slider.php:798
msgid "Sorry, no slides matched your criteria."
msgstr ""

#: post-types/slider/shortcodes/slider.php:880
msgid "Previous"
msgstr ""

#: post-types/slider/shortcodes/slider.php:893
msgid "Next"
msgstr ""

#: post-types/slider/slider-register.php:53
#: post-types/slider/slider-register.php:54
msgid "Elated Slider"
msgstr ""

#: post-types/slider/slider-register.php:55
msgid "Slides"
msgstr ""

#: post-types/slider/slider-register.php:56
#: post-types/slider/slider-register.php:59
msgid "Add New Slide"
msgstr ""

#: post-types/slider/slider-register.php:57
msgid "Slide"
msgstr ""

#: post-types/slider/slider-register.php:58
msgid "New Slide"
msgstr ""

#: post-types/slider/slider-register.php:60
msgid "Edit Slide"
msgstr ""

#: post-types/slider/slider-register.php:80
#: post-types/slider/slider-register.php:90
msgid "Sliders"
msgstr ""

#: post-types/slider/slider-register.php:81
msgid "Slider"
msgstr ""

#: post-types/slider/slider-register.php:82
msgid "Search Sliders"
msgstr ""

#: post-types/slider/slider-register.php:83
msgid "All Sliders"
msgstr ""

#: post-types/slider/slider-register.php:84
msgid "Parent Slider"
msgstr ""

#: post-types/slider/slider-register.php:85
msgid "Parent Slider:"
msgstr ""

#: post-types/slider/slider-register.php:86
msgid "Edit Slider"
msgstr ""

#: post-types/slider/slider-register.php:87
msgid "Update Slider"
msgstr ""

#: post-types/slider/slider-register.php:88
msgid "Add New Slider"
msgstr ""

#: post-types/slider/slider-register.php:89
msgid "New Slider Name"
msgstr ""

#: post-types/slider/tax-custom-fields.php:10
msgid "Slider shortcode"
msgstr ""

#: post-types/slider/tax-custom-fields.php:14
msgid "Use this shortcode to insert it on page"
msgstr ""

#: post-types/slider/tax-custom-fields.php:19
msgid "Anchor"
msgstr ""

#: post-types/slider/tax-custom-fields.php:27
msgid "Effect on header (dark/light style)"
msgstr ""

#: post-types/slider/tax-custom-fields.php:32
msgid "yes"
msgstr ""

#: post-types/slider/tax-custom-fields.php:38
msgid "Show prev/next thumbs (except for side menu or passepartout)"
msgstr ""

#: post-types/slider/tax-custom-fields.php:50
msgid "Show navigation thumbs"
msgstr ""

#: post-types/slider/tax-custom-fields.php:62
msgid "Show slide number"
msgstr ""

#: post-types/slider/tax-custom-fields.php:74
msgid "Parallax effect"
msgstr ""

#: post-types/slider/tax-custom-fields.php:85
msgid "Auto start"
msgstr ""

#: post-types/slider/tax-custom-fields.php:96
msgid "Slides Animation"
msgstr ""

#: post-types/slider/tax-custom-fields.php:100
msgid "Slide Horizontally"
msgstr ""

#: post-types/slider/tax-custom-fields.php:101
msgid "Slide Vertically - Up"
msgstr ""

#: post-types/slider/tax-custom-fields.php:102
msgid "Slide Vertically - Down"
msgstr ""

#: post-types/slider/tax-custom-fields.php:103
msgid "Slide - Cover Previous Slide"
msgstr ""

#: post-types/slider/tax-custom-fields.php:104
msgid "Slide - Peek Into Next Slide"
msgstr ""

#: post-types/slider/tax-custom-fields.php:105
msgid "Fade"
msgstr ""

#: post-types/slider/tax-custom-fields.php:111
msgid "Time between slide transitions in milliseconds"
msgstr ""

#: post-types/slider/tax-custom-fields.php:119
msgid "Slider height in px"
msgstr ""

#: post-types/slider/tax-custom-fields.php:127
msgid "Responsive height"
msgstr ""

#: post-types/slider/tax-custom-fields.php:138
msgid "Show Navigation Arrows"
msgstr ""

#: post-types/slider/tax-custom-fields.php:149
msgid "Show Navigation Bullets"
msgstr ""

#: post-types/slider/tax-custom-fields.php:186
msgid "Shortcode"
msgstr ""

#: post-types/slider/tax-custom-fields.php:188
msgid "Posts"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:52
#: post-types/testimonials/testimonials-register.php:54
msgid "Testimonials"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:71
msgid "Number of Testimonials"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:76
msgid "Type"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:79
msgid "With Icon"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:81
msgid "Cards"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:88
msgid "Skin"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:108
msgid "Insert Number of Carousel Items (Default 2)"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:120
msgid "Show in two rows?"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:162
msgid "Autoplay speed"
msgstr ""

#: post-types/testimonials/shortcodes/testimonials.php:165
msgid "Autoplay speed (ms)"
msgstr ""

#: post-types/testimonials/testimonials-register.php:55
msgid "Testimonial"
msgstr ""

#: post-types/testimonials/testimonials-register.php:56
msgid "New Testimonial"
msgstr ""

#: post-types/testimonials/testimonials-register.php:57
msgid "Add New Testimonial"
msgstr ""

#: post-types/testimonials/testimonials-register.php:58
msgid "Edit Testimonial"
msgstr ""

#: post-types/testimonials/testimonials-register.php:78
#: post-types/testimonials/testimonials-register.php:88
msgid "Testimonials Categories"
msgstr ""

#: post-types/testimonials/testimonials-register.php:79
msgid "Testimonial Category"
msgstr ""

#: post-types/testimonials/testimonials-register.php:80
msgid "Search Testimonials Categories"
msgstr ""

#: post-types/testimonials/testimonials-register.php:81
msgid "All Testimonials Categories"
msgstr ""

#: post-types/testimonials/testimonials-register.php:82
msgid "Parent Testimonial Category"
msgstr ""

#: post-types/testimonials/testimonials-register.php:83
msgid "Parent Testimonial Category:"
msgstr ""

#: post-types/testimonials/testimonials-register.php:84
msgid "Edit Testimonials Category"
msgstr ""

#: post-types/testimonials/testimonials-register.php:85
msgid "Update Testimonials Category"
msgstr ""

#: post-types/testimonials/testimonials-register.php:86
msgid "Add New Testimonials Category"
msgstr ""

#: post-types/testimonials/testimonials-register.php:87
msgid "New Testimonials Category Name"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Elated Core"
msgstr ""

#. Description of the plugin/theme
msgid "Plugin that adds all post types needed by our theme"
msgstr ""

#. Author of the plugin/theme
msgid "Elated Themes"
msgstr ""
